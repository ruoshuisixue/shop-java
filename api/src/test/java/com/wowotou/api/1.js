function CommmonLightLine(a) {
    this.handler, this.dom, this.hypotenuse, this.lineWidth, this.base, this.skew, this.lineColor, this.intervalTime, this.time, this.timeCounter = 0, this.bgImage, this.subDom, this.init = function () {
        var b, c;
        this.dom = document.querySelector("" + a.domId), b = this.dom.offsetWidth, c = this.dom.offsetHeight, this.hypotenuse = Math.round(Math.sqrt(b * b + c * c)), this.lineWidth = a.lineWidth ? a.lineWidth : 5, this.base = a.base ? a.base : 0, this.skew = a.skew ? a.skew : "135deg", this.lineColor = a.lineColor ? " " + a.lineColor + " " : " rgba(255, 255, 255, .3) ", this.intervalTime = a.intervalTime ? a.intervalTime : 5, this.time = a.time ? a.time : "infinite", this.bgImage = a.bgImage ? ',url("' + a.bgImage + '")' : "", this.dom.style.backgroundImage = 'url("' + a.bgImage + '")', this.subDom = document.createElement("div"), this.subDom.setAttribute("style", "width: 100%; height: 100%;"), this.subDom.style.borderRadius = getComputedStyle(imk2Side, null).getPropertyValue("border-radius"), this.dom.appendChild(this.subDom)
    }, this.play = function () {
        this.init();
        var a = this;
        this.handler = setInterval(function () {
            a.subDom.style.backgroundImage = "linear-gradient(" + a.skew + ", transparent, transparent " + a.base + "px, " + a.lineColor + a.base + "px, " + a.lineColor + (a.base + a.lineWidth) + "px, transparent " + (a.base + a.lineWidth) + "px) ", a.base++, a.base > a.hypotenuse && (a.base = 0, a.timeCounter++), a.time < a.timeCounter && clearInterval(a.handler)
        }, this.intervalTime)
    }
}

function mCarouselNew(a) {
    this.intervel = a.intervel ? a.intervel : 3e3, this.transitionTime = a.transitionTime ? a.transitionTime : 800, this.carouselHeight = a.carouselHeight ? a.carouselHeight : 45, this.circle, this.carousel, this.silders, this.sildersLen, this.currentIndex = 0, this.handler, this._w, this.startPostionX = 0, this.endPostionX = 0, this.distance = 0, this.stopCarousel = a.stopCarousel, mCarouselNew.prototype.init = function () {
        this.carousel = document.querySelector(a.warpperId + " .carousel"), this.silders = document.querySelectorAll(a.warpperId + " .carousel li"), this.circles = document.querySelectorAll(a.warpperId + " .m-carousel-circle li"), this._w = getComputedStyle(this.carousel.parentNode, null).width.replace("px", ""), this.setCSS(), this.bindEvent()
    }, mCarouselNew.prototype.setCSS = function () {
        this.sildersLen = this.silders.length;
        for (var a = 0; a < this.sildersLen; a++) this.silders[a].style.width = this._w + "px", this.silders[a].style.left = a * this._w + "px";
        this.carousel.style.width = this.sildersLen * this._w + "px", this.carousel.parentNode.style.height = this.carouselHeight + "px"
    }, mCarouselNew.prototype.turningPage = function (a) {
        if (this.carousel.style.transition = this.transitionTime + "ms", this.carousel.style.transform = "translate3d(" + -this.currentIndex * this._w + "px, 0, 0)", this.silders[this.currentIndex % this.sildersLen].style.left = this.currentIndex * this._w + "px", "--\x3e" == a) this.currentIndex == this.sildersLen - 1 || this.currentIndex % this.sildersLen == this.sildersLen - 1 ? (this.circles[0].classList.remove("item-active"), this.circles[this.sildersLen - 1].classList.add("item-active")) : (this.circles[this.currentIndex % this.sildersLen + 1].classList.remove("item-active"), this.circles[this.currentIndex % this.sildersLen].classList.add("item-active")); else {
            if (this.currentIndex % this.sildersLen == 0) return this.circles[this.sildersLen - 1].classList.remove("item-active"), void this.circles[0].classList.add("item-active");
            this.circles[this.currentIndex % this.sildersLen - 1].classList.remove("item-active"), this.circles[this.currentIndex % this.sildersLen].classList.add("item-active")
        }
    }, mCarouselNew.prototype.bindEvent = function () {
        if (this.stopCarousel) return !1;
        var a = this;
        this.carousel.addEventListener("touchstart", function (b) {
            a.cancelPlay();
            var c = b.touches[0];
            a.startPostionX = c.clientX
        }), this.carousel.addEventListener("touchmove", function (b) {
            a.carousel.style.transition = "";
            var c = b.touches[0];
            a.endPostionX = c.clientX, this.distance = a.endPostionX - a.startPostionX;
            var d = -a.currentIndex * a._w + this.distance;
            0 != this.distance && b.preventDefault(), 0 == a.currentIndex && this.distance > 0 || (this.style.transform = "translate3d(" + d + "px, 0, 0)")
        }), this.carousel.addEventListener("touchend", function (b) {
            var c = b.changedTouches[0].clientX - a.startPostionX;
            0 == a.currentIndex && c > 0 || (a.autoPlay(), c < 0 && (a.currentIndex++, a.turningPage("<--")), c > 0 && (a.currentIndex--, a.turningPage("--\x3e")))
        })
    }, mCarouselNew.prototype.cancelPlay = function () {
        clearInterval(this.handler)
    }, mCarouselNew.prototype.autoPlay = function () {
        var a = this;
        this.handler = setInterval(function () {
            a.currentIndex++, a.turningPage()
        }, this.intervel)
    }, mCarouselNew.prototype.resetCSS = function () {
        this.cancelPlay(), this._w = getComputedStyle(this.carousel.parentNode, null).width.replace("px", ""), "100%" == this._w && (this._w = document.documentElement.clientWidth, this._w > 640 && (this._w = "640")), this.silders[this.currentIndex % this.sildersLen].style.width = this._w + "px", this.silders[this.currentIndex % this.sildersLen].style.left = this.currentIndex * this._w + "px";
        for (var a = 0; a < this.sildersLen; a++) this.silders[a].style.width = this._w + "px";
        this.carousel.style.transition = "", this.carousel.style.transform = "translate3d(" + -this.currentIndex * this._w + "px, 0, 0)", this.silders[this.currentIndex % this.sildersLen].style.left = this.currentIndex * this._w + "px", this.sildersLen > 1 && this.autoPlay()
    }, mCarouselNew.prototype.monitorOrientation = function () {
        var a = this;
        window.addEventListener("orientationchange", function () {
            setTimeout(function () {
                a.resetCSS()
            }, 200)
        })
    }, mCarouselNew.prototype.start = function () {
        this.init(), this.monitorOrientation(), this.stopCarousel || this.autoPlay()
    }
}

function weixinH5ToFinish() {
    var a = Ariel.parseGetParameters();
    window.location.href = "/wapWeiXinPay/weiXinH5PayQuery.action?appId=" + a.appId + "&payId=" + a.payId
}

var Zepto = function () {
    function a(a) {
        return null == a ? String(a) : U[V.call(a)] || "object"
    }

    function b(b) {
        return "function" == a(b)
    }

    function c(a) {
        return null != a && a == a.window
    }

    function d(a) {
        return null != a && a.nodeType == a.DOCUMENT_NODE
    }

    function e(b) {
        return "object" == a(b)
    }

    function f(a) {
        return e(a) && !c(a) && Object.getPrototypeOf(a) == Object.prototype
    }

    function g(a) {
        return "number" == typeof a.length
    }

    function h(a) {
        return D.call(a, function (a) {
            return null != a
        })
    }

    function i(a) {
        return a.length > 0 ? x.fn.concat.apply([], a) : a
    }

    function j(a) {
        return a.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase()
    }

    function k(a) {
        return a in G ? G[a] : G[a] = new RegExp("(^|\\s)" + a + "(\\s|$)")
    }

    function l(a, b) {
        return "number" != typeof b || H[j(a)] ? b : b + "px"
    }

    function m(a) {
        var b, c;
        return F[a] || (b = E.createElement(a), E.body.appendChild(b), c = getComputedStyle(b, "").getPropertyValue("display"), b.parentNode.removeChild(b), "none" == c && (c = "block"), F[a] = c), F[a]
    }

    function n(a) {
        return "children" in a ? C.call(a.children) : x.map(a.childNodes, function (a) {
            if (1 == a.nodeType) return a
        })
    }

    function o(a, b, c) {
        for (w in b) c && (f(b[w]) || Z(b[w])) ? (f(b[w]) && !f(a[w]) && (a[w] = {}), Z(b[w]) && !Z(a[w]) && (a[w] = []), o(a[w], b[w], c)) : b[w] !== v && (a[w] = b[w])
    }

    function p(a, b) {
        return null == b ? x(a) : x(a).filter(b)
    }

    function q(a, c, d, e) {
        return b(c) ? c.call(a, d, e) : c
    }

    function r(a, b, c) {
        null == c ? a.removeAttribute(b) : a.setAttribute(b, c)
    }

    function s(a, b) {
        var c = a.className, d = c && c.baseVal !== v;
        if (b === v) return d ? c.baseVal : c;
        d ? c.baseVal = b : a.className = b
    }

    function t(a) {
        var b;
        try {
            return a ? "true" == a || "false" != a && ("null" == a ? null : /^0/.test(a) || isNaN(b = Number(a)) ? /^[\[\{]/.test(a) ? x.parseJSON(a) : a : b) : a
        } catch (b) {
            return a
        }
    }

    function u(a, b) {
        b(a);
        for (var c in a.childNodes) u(a.childNodes[c], b)
    }

    var v, w, x, y, z, A, B = [], C = B.slice, D = B.filter, E = window.document, F = {}, G = {},
        H = {"column-count": 1, columns: 1, "font-weight": 1, "line-height": 1, opacity: 1, "z-index": 1, zoom: 1},
        I = /^\s*<(\w+|!)[^>]*>/, J = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        K = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, L = /^(?:body|html)$/i,
        M = /([A-Z])/g, N = ["val", "css", "html", "text", "data", "width", "height", "offset"],
        O = ["after", "prepend", "before", "append"], P = E.createElement("table"), Q = E.createElement("tr"),
        R = {tr: E.createElement("tbody"), tbody: P, thead: P, tfoot: P, td: Q, th: Q, "*": E.createElement("div")},
        S = /complete|loaded|interactive/, T = /^[\w-]*$/, U = {}, V = U.toString, W = {}, X = E.createElement("div"),
        Y = {
            tabindex: "tabIndex",
            readonly: "readOnly",
            for: "htmlFor",
            class: "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        }, Z = Array.isArray || function (a) {
            return a instanceof Array
        };
    return W.matches = function (a, b) {
        if (!b || !a || 1 !== a.nodeType) return !1;
        var c = a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.matchesSelector;
        if (c) return c.call(a, b);
        var d, e = a.parentNode, f = !e;
        return f && (e = X).appendChild(a), d = ~W.qsa(e, b).indexOf(a), f && X.removeChild(a), d
    }, z = function (a) {
        return a.replace(/-+(.)?/g, function (a, b) {
            return b ? b.toUpperCase() : ""
        })
    }, A = function (a) {
        return D.call(a, function (b, c) {
            return a.indexOf(b) == c
        })
    }, W.fragment = function (a, b, c) {
        var d, e, g;
        return J.test(a) && (d = x(E.createElement(RegExp.$1))), d || (a.replace && (a = a.replace(K, "<$1></$2>")), b === v && (b = I.test(a) && RegExp.$1), b in R || (b = "*"), g = R[b], g.innerHTML = "" + a, d = x.each(C.call(g.childNodes), function () {
            g.removeChild(this)
        })), f(c) && (e = x(d), x.each(c, function (a, b) {
            N.indexOf(a) > -1 ? e[a](b) : e.attr(a, b)
        })), d
    }, W.Z = function (a, b) {
        return a = a || [], a.__proto__ = x.fn, a.selector = b || "", a
    }, W.isZ = function (a) {
        return a instanceof W.Z
    }, W.init = function (a, c) {
        var d;
        if (!a) return W.Z();
        if ("string" == typeof a) if (a = a.trim(), "<" == a[0] && I.test(a)) d = W.fragment(a, RegExp.$1, c), a = null; else {
            if (c !== v) return x(c).find(a);
            d = W.qsa(E, a)
        } else {
            if (b(a)) return x(E).ready(a);
            if (W.isZ(a)) return a;
            if (Z(a)) d = h(a); else if (e(a)) d = [a], a = null; else if (I.test(a)) d = W.fragment(a.trim(), RegExp.$1, c), a = null; else {
                if (c !== v) return x(c).find(a);
                d = W.qsa(E, a)
            }
        }
        return W.Z(d, a)
    }, x = function (a, b) {
        return W.init(a, b)
    }, x.extend = function (a) {
        var b, c = C.call(arguments, 1);
        return "boolean" == typeof a && (b = a, a = c.shift()), c.forEach(function (c) {
            o(a, c, b)
        }), a
    }, W.qsa = function (a, b) {
        var c, e = "#" == b[0], f = !e && "." == b[0], g = e || f ? b.slice(1) : b, h = T.test(g);
        return d(a) && h && e ? (c = a.getElementById(g)) ? [c] : [] : 1 !== a.nodeType && 9 !== a.nodeType ? [] : C.call(h && !e ? f ? a.getElementsByClassName(g) : a.getElementsByTagName(b) : a.querySelectorAll(b))
    }, x.contains = function (a, b) {
        return a !== b && a.contains(b)
    }, x.type = a, x.isFunction = b, x.isWindow = c, x.isArray = Z, x.isPlainObject = f, x.isEmptyObject = function (a) {
        var b;
        for (b in a) return !1;
        return !0
    }, x.inArray = function (a, b, c) {
        return B.indexOf.call(b, a, c)
    }, x.camelCase = z, x.trim = function (a) {
        return null == a ? "" : String.prototype.trim.call(a)
    }, x.uuid = 0, x.support = {}, x.expr = {}, x.map = function (a, b) {
        var c, d, e, f = [];
        if (g(a)) for (d = 0; d < a.length; d++) null != (c = b(a[d], d)) && f.push(c); else for (e in a) null != (c = b(a[e], e)) && f.push(c);
        return i(f)
    }, x.each = function (a, b) {
        var c, d;
        if (g(a)) {
            for (c = 0; c < a.length; c++) if (!1 === b.call(a[c], c, a[c])) return a
        } else for (d in a) if (!1 === b.call(a[d], d, a[d])) return a;
        return a
    }, x.grep = function (a, b) {
        return D.call(a, b)
    }, window.JSON && (x.parseJSON = JSON.parse), x.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (a, b) {
        U["[object " + b + "]"] = b.toLowerCase()
    }), x.fn = {
        forEach: B.forEach,
        reduce: B.reduce,
        push: B.push,
        sort: B.sort,
        indexOf: B.indexOf,
        concat: B.concat,
        map: function (a) {
            return x(x.map(this, function (b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function () {
            return x(C.apply(this, arguments))
        },
        ready: function (a) {
            return S.test(E.readyState) && E.body ? a(x) : E.addEventListener("DOMContentLoaded", function () {
                a(x)
            }, !1), this
        },
        get: function (a) {
            return a === v ? C.call(this) : this[a >= 0 ? a : a + this.length]
        },
        toArray: function () {
            return this.get()
        },
        size: function () {
            return this.length
        },
        remove: function () {
            return this.each(function () {
                null != this.parentNode && this.parentNode.removeChild(this)
            })
        },
        each: function (a) {
            return B.every.call(this, function (b, c) {
                return !1 !== a.call(b, c, b)
            }), this
        },
        filter: function (a) {
            return b(a) ? this.not(this.not(a)) : x(D.call(this, function (b) {
                return W.matches(b, a)
            }))
        },
        add: function (a, b) {
            return x(A(this.concat(x(a, b))))
        },
        is: function (a) {
            return this.length > 0 && W.matches(this[0], a)
        },
        not: function (a) {
            var c = [];
            if (b(a) && a.call !== v) this.each(function (b) {
                a.call(this, b) || c.push(this)
            }); else {
                var d = "string" == typeof a ? this.filter(a) : g(a) && b(a.item) ? C.call(a) : x(a);
                this.forEach(function (a) {
                    d.indexOf(a) < 0 && c.push(a)
                })
            }
            return x(c)
        },
        has: function (a) {
            return this.filter(function () {
                return e(a) ? x.contains(this, a) : x(this).find(a).size()
            })
        },
        eq: function (a) {
            return -1 === a ? this.slice(a) : this.slice(a, +a + 1)
        },
        first: function () {
            var a = this[0];
            return a && !e(a) ? a : x(a)
        },
        last: function () {
            var a = this[this.length - 1];
            return a && !e(a) ? a : x(a)
        },
        find: function (a) {
            var b = this;
            return "object" == typeof a ? x(a).filter(function () {
                var a = this;
                return B.some.call(b, function (b) {
                    return x.contains(b, a)
                })
            }) : 1 == this.length ? x(W.qsa(this[0], a)) : this.map(function () {
                return W.qsa(this, a)
            })
        },
        closest: function (a, b) {
            var c = this[0], e = !1;
            for ("object" == typeof a && (e = x(a)); c && !(e ? e.indexOf(c) >= 0 : W.matches(c, a));) c = c !== b && !d(c) && c.parentNode;
            return x(c)
        },
        parents: function (a) {
            for (var b = [], c = this; c.length > 0;) c = x.map(c, function (a) {
                if ((a = a.parentNode) && !d(a) && b.indexOf(a) < 0) return b.push(a), a
            });
            return p(b, a)
        },
        parent: function (a) {
            return p(A(this.pluck("parentNode")), a)
        },
        children: function (a) {
            return p(this.map(function () {
                return n(this)
            }), a)
        },
        contents: function () {
            return this.map(function () {
                return C.call(this.childNodes)
            })
        },
        siblings: function (a) {
            return p(this.map(function (a, b) {
                return D.call(n(b.parentNode), function (a) {
                    return a !== b
                })
            }), a)
        },
        empty: function () {
            return this.each(function () {
                this.innerHTML = ""
            })
        },
        pluck: function (a) {
            return x.map(this, function (b) {
                return b[a]
            })
        },
        show: function () {
            return this.each(function () {
                "none" == this.style.display && (this.style.display = ""), "none" == getComputedStyle(this, "").getPropertyValue("display") && (this.style.display = m(this.nodeName))
            })
        },
        replaceWith: function (a) {
            return this.before(a).remove()
        },
        wrap: function (a) {
            var c = b(a);
            if (this[0] && !c) var d = x(a).get(0), e = d.parentNode || this.length > 1;
            return this.each(function (b) {
                x(this).wrapAll(c ? a.call(this, b) : e ? d.cloneNode(!0) : d)
            })
        },
        wrapAll: function (a) {
            if (this[0]) {
                x(this[0]).before(a = x(a));
                for (var b; (b = a.children()).length;) a = b.first();
                x(a).append(this)
            }
            return this
        },
        wrapInner: function (a) {
            var c = b(a);
            return this.each(function (b) {
                var d = x(this), e = d.contents(), f = c ? a.call(this, b) : a;
                e.length ? e.wrapAll(f) : d.append(f)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                x(this).replaceWith(x(this).children())
            }), this
        },
        clone: function () {
            return this.map(function () {
                return this.cloneNode(!0)
            })
        },
        hide: function () {
            return this.css("display", "none")
        },
        toggle: function (a) {
            return this.each(function () {
                var b = x(this);
                (a === v ? "none" == b.css("display") : a) ? b.show() : b.hide()
            })
        },
        prev: function (a) {
            return x(this.pluck("previousElementSibling")).filter(a || "*")
        },
        next: function (a) {
            return x(this.pluck("nextElementSibling")).filter(a || "*")
        },
        html: function (a) {
            return 0 === arguments.length ? this.length > 0 ? this[0].innerHTML : null : this.each(function (b) {
                var c = this.innerHTML;
                x(this).empty().append(q(this, a, b, c))
            })
        },
        text: function (a) {
            return 0 === arguments.length ? this.length > 0 ? this[0].textContent : null : this.each(function () {
                this.textContent = a === v ? "" : "" + a
            })
        },
        attr: function (a, b) {
            var c;
            return "string" == typeof a && b === v ? 0 == this.length || 1 !== this[0].nodeType ? v : "value" == a && "INPUT" == this[0].nodeName ? this.val() : !(c = this[0].getAttribute(a)) && a in this[0] ? this[0][a] : c : this.each(function (c) {
                if (1 === this.nodeType) if (e(a)) for (w in a) r(this, w, a[w]); else r(this, a, q(this, b, c, this.getAttribute(a)))
            })
        },
        removeAttr: function (a) {
            return this.each(function () {
                1 === this.nodeType && r(this, a)
            })
        },
        prop: function (a, b) {
            return a = Y[a] || a, b === v ? this[0] && this[0][a] : this.each(function (c) {
                this[a] = q(this, b, c, this[a])
            })
        },
        data: function (a, b) {
            var c = this.attr("data-" + a.replace(M, "-$1").toLowerCase(), b);
            return null !== c ? t(c) : v
        },
        val: function (a) {
            return 0 === arguments.length ? this[0] && (this[0].multiple ? x(this[0]).find("option").filter(function () {
                return this.selected
            }).pluck("value") : this[0].value) : this.each(function (b) {
                this.value = q(this, a, b, this.value)
            })
        },
        offset: function (a) {
            if (a) return this.each(function (b) {
                var c = x(this), d = q(this, a, b, c.offset()), e = c.offsetParent().offset(),
                    f = {top: d.top - e.top, left: d.left - e.left};
                "static" == c.css("position") && (f.position = "relative"), c.css(f)
            });
            if (0 == this.length) return null;
            var b = this[0].getBoundingClientRect();
            return {
                left: b.left + window.pageXOffset,
                top: b.top + window.pageYOffset,
                width: Math.round(b.width),
                height: Math.round(b.height)
            }
        },
        css: function (b, c) {
            if (arguments.length < 2) {
                var d = this[0], e = getComputedStyle(d, "");
                if (!d) return;
                if ("string" == typeof b) return d.style[z(b)] || e.getPropertyValue(b);
                if (Z(b)) {
                    var f = {};
                    return x.each(Z(b) ? b : [b], function (a, b) {
                        f[b] = d.style[z(b)] || e.getPropertyValue(b)
                    }), f
                }
            }
            var g = "";
            if ("string" == a(b)) c || 0 === c ? g = j(b) + ":" + l(b, c) : this.each(function () {
                this.style.removeProperty(j(b))
            }); else for (w in b) b[w] || 0 === b[w] ? g += j(w) + ":" + l(w, b[w]) + ";" : this.each(function () {
                this.style.removeProperty(j(w))
            });
            return this.each(function () {
                this.style.cssText += ";" + g
            })
        },
        index: function (a) {
            return a ? this.indexOf(x(a)[0]) : this.parent().children().indexOf(this[0])
        },
        hasClass: function (a) {
            return !!a && B.some.call(this, function (a) {
                return this.test(s(a))
            }, k(a))
        },
        addClass: function (a) {
            return a ? this.each(function (b) {
                y = [];
                var c = s(this);
                q(this, a, b, c).split(/\s+/g).forEach(function (a) {
                    x(this).hasClass(a) || y.push(a)
                }, this), y.length && s(this, c + (c ? " " : "") + y.join(" "))
            }) : this
        },
        removeClass: function (a) {
            return this.each(function (b) {
                if (a === v) return s(this, "");
                y = s(this), q(this, a, b, y).split(/\s+/g).forEach(function (a) {
                    y = y.replace(k(a), " ")
                }), s(this, y.trim())
            })
        },
        toggleClass: function (a, b) {
            return a ? this.each(function (c) {
                var d = x(this);
                q(this, a, c, s(this)).split(/\s+/g).forEach(function (a) {
                    (b === v ? !d.hasClass(a) : b) ? d.addClass(a) : d.removeClass(a)
                })
            }) : this
        },
        scrollTop: function (a) {
            if (this.length) {
                var b = "scrollTop" in this[0];
                return a === v ? b ? this[0].scrollTop : this[0].pageYOffset : this.each(b ? function () {
                    this.scrollTop = a
                } : function () {
                    this.scrollTo(this.scrollX, a)
                })
            }
        },
        scrollLeft: function (a) {
            if (this.length) {
                var b = "scrollLeft" in this[0];
                return a === v ? b ? this[0].scrollLeft : this[0].pageXOffset : this.each(b ? function () {
                    this.scrollLeft = a
                } : function () {
                    this.scrollTo(a, this.scrollY)
                })
            }
        },
        position: function () {
            if (this.length) {
                var a = this[0], b = this.offsetParent(), c = this.offset(),
                    d = L.test(b[0].nodeName) ? {top: 0, left: 0} : b.offset();
                return c.top -= parseFloat(x(a).css("margin-top")) || 0, c.left -= parseFloat(x(a).css("margin-left")) || 0, d.top += parseFloat(x(b[0]).css("border-top-width")) || 0, d.left += parseFloat(x(b[0]).css("border-left-width")) || 0, {
                    top: c.top - d.top,
                    left: c.left - d.left
                }
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var a = this.offsetParent || E.body; a && !L.test(a.nodeName) && "static" == x(a).css("position");) a = a.offsetParent;
                return a
            })
        }
    }, x.fn.detach = x.fn.remove, ["width", "height"].forEach(function (a) {
        var b = a.replace(/./, function (a) {
            return a[0].toUpperCase()
        });
        x.fn[a] = function (e) {
            var f, g = this[0];
            return e === v ? c(g) ? g["inner" + b] : d(g) ? g.documentElement["scroll" + b] : (f = this.offset()) && f[a] : this.each(function (b) {
                g = x(this), g.css(a, q(this, e, b, g[a]()))
            })
        }
    }), O.forEach(function (b, c) {
        var d = c % 2;
        x.fn[b] = function () {
            var b, e, f = x.map(arguments, function (c) {
                return b = a(c), "object" == b || "array" == b || null == c ? c : W.fragment(c)
            }), g = this.length > 1;
            return f.length < 1 ? this : this.each(function (a, b) {
                e = d ? b : b.parentNode, b = 0 == c ? b.nextSibling : 1 == c ? b.firstChild : 2 == c ? b : null, f.forEach(function (a) {
                    if (g) a = a.cloneNode(!0); else if (!e) return x(a).remove();
                    u(e.insertBefore(a, b), function (a) {
                        null == a.nodeName || "SCRIPT" !== a.nodeName.toUpperCase() || a.type && "text/javascript" !== a.type || a.src || window.eval.call(window, a.innerHTML)
                    })
                })
            })
        }, x.fn[d ? b + "To" : "insert" + (c ? "Before" : "After")] = function (a) {
            return x(a)[b](this), this
        }
    }), W.Z.prototype = x.fn, W.uniq = A, W.deserializeValue = t, x.zepto = W, x
}();
window.Zepto = Zepto, void 0 === window.$ && (window.$ = Zepto), function (a) {
    function b(a) {
        return a._zid || (a._zid = m++)
    }

    function c(a, c, f, g) {
        if (c = d(c), c.ns) var h = e(c.ns);
        return (q[b(a)] || []).filter(function (a) {
            return a && (!c.e || a.e == c.e) && (!c.ns || h.test(a.ns)) && (!f || b(a.fn) === b(f)) && (!g || a.sel == g)
        })
    }

    function d(a) {
        var b = ("" + a).split(".");
        return {e: b[0], ns: b.slice(1).sort().join(" ")}
    }

    function e(a) {
        return new RegExp("(?:^| )" + a.replace(" ", " .* ?") + "(?: |$)")
    }

    function f(a, b) {
        return a.del && !s && a.e in t || !!b
    }

    function g(a) {
        return u[a] || s && t[a] || a
    }

    function h(c, e, h, i, k, m, n) {
        var o = b(c), p = q[o] || (q[o] = []);
        e.split(/\s/).forEach(function (b) {
            if ("ready" == b) return a(document).ready(h);
            var e = d(b);
            e.fn = h, e.sel = k, e.e in u && (h = function (b) {
                var c = b.relatedTarget;
                if (!c || c !== this && !a.contains(this, c)) return e.fn.apply(this, arguments)
            }), e.del = m;
            var o = m || h;
            e.proxy = function (a) {
                if (a = j(a), !a.isImmediatePropagationStopped()) {
                    a.data = i;
                    var b = o.apply(c, a._args == l ? [a] : [a].concat(a._args));
                    return !1 === b && (a.preventDefault(), a.stopPropagation()), b
                }
            }, e.i = p.length, p.push(e), "addEventListener" in c && c.addEventListener(g(e.e), e.proxy, f(e, n))
        })
    }

    function i(a, d, e, h, i) {
        var j = b(a);
        (d || "").split(/\s/).forEach(function (b) {
            c(a, b, e, h).forEach(function (b) {
                delete q[j][b.i], "removeEventListener" in a && a.removeEventListener(g(b.e), b.proxy, f(b, i))
            })
        })
    }

    function j(b, c) {
        return !c && b.isDefaultPrevented || (c || (c = b), a.each(y, function (a, d) {
            var e = c[a];
            b[a] = function () {
                return this[d] = v, e && e.apply(c, arguments)
            }, b[d] = w
        }), (c.defaultPrevented !== l ? c.defaultPrevented : "returnValue" in c ? !1 === c.returnValue : c.getPreventDefault && c.getPreventDefault()) && (b.isDefaultPrevented = v)), b
    }

    function k(a) {
        var b, c = {originalEvent: a};
        for (b in a) x.test(b) || a[b] === l || (c[b] = a[b]);
        return j(c, a)
    }

    var l, m = 1, n = Array.prototype.slice, o = a.isFunction, p = function (a) {
            return "string" == typeof a
        }, q = {}, r = {}, s = "onfocusin" in window, t = {focus: "focusin", blur: "focusout"},
        u = {mouseenter: "mouseover", mouseleave: "mouseout"};
    r.click = r.mousedown = r.mouseup = r.mousemove = "MouseEvents", a.event = {
        add: h,
        remove: i
    }, a.proxy = function (c, d) {
        if (o(c)) {
            var e = function () {
                return c.apply(d, arguments)
            };
            return e._zid = b(c), e
        }
        if (p(d)) return a.proxy(c[d], c);
        throw new TypeError("expected function")
    }, a.fn.bind = function (a, b, c) {
        return this.on(a, b, c)
    }, a.fn.unbind = function (a, b) {
        return this.off(a, b)
    }, a.fn.one = function (a, b, c, d) {
        return this.on(a, b, c, d, 1)
    };
    var v = function () {
        return !0
    }, w = function () {
        return !1
    }, x = /^([A-Z]|returnValue$|layer[XY]$)/, y = {
        preventDefault: "isDefaultPrevented",
        stopImmediatePropagation: "isImmediatePropagationStopped",
        stopPropagation: "isPropagationStopped"
    };
    a.fn.delegate = function (a, b, c) {
        return this.on(b, a, c)
    }, a.fn.undelegate = function (a, b, c) {
        return this.off(b, a, c)
    }, a.fn.live = function (b, c) {
        return a(document.body).delegate(this.selector, b, c), this
    }, a.fn.die = function (b, c) {
        return a(document.body).undelegate(this.selector, b, c), this
    }, a.fn.on = function (b, c, d, e, f) {
        var g, j, m = this;
        return b && !p(b) ? (a.each(b, function (a, b) {
            m.on(a, c, d, b, f)
        }), m) : (p(c) || o(e) || !1 === e || (e = d, d = c, c = l), (o(d) || !1 === d) && (e = d, d = l), !1 === e && (e = w), m.each(function (l, m) {
            f && (g = function (a) {
                return i(m, a.type, e), e.apply(this, arguments)
            }), c && (j = function (b) {
                var d, f = a(b.target).closest(c, m).get(0);
                if (f && f !== m) return d = a.extend(k(b), {
                    currentTarget: f,
                    liveFired: m
                }), (g || e).apply(f, [d].concat(n.call(arguments, 1)))
            }), h(m, b, e, d, c, j || g)
        }))
    }, a.fn.off = function (b, c, d) {
        var e = this;
        return b && !p(b) ? (a.each(b, function (a, b) {
            e.off(a, c, b)
        }), e) : (p(c) || o(d) || !1 === d || (d = c, c = l), !1 === d && (d = w), e.each(function () {
            i(this, b, d, c)
        }))
    }, a.fn.trigger = function (b, c) {
        return b = p(b) || a.isPlainObject(b) ? a.Event(b) : j(b), b._args = c, this.each(function () {
            "dispatchEvent" in this ? this.dispatchEvent(b) : a(this).triggerHandler(b, c)
        })
    }, a.fn.triggerHandler = function (b, d) {
        var e, f;
        return this.each(function (g, h) {
            e = k(p(b) ? a.Event(b) : b), e._args = d, e.target = h, a.each(c(h, b.type || b), function (a, b) {
                if (f = b.proxy(e), e.isImmediatePropagationStopped()) return !1
            })
        }), f
    }, "focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select keydown keypress keyup error".split(" ").forEach(function (b) {
        a.fn[b] = function (a) {
            return a ? this.bind(b, a) : this.trigger(b)
        }
    }), ["focus", "blur"].forEach(function (b) {
        a.fn[b] = function (a) {
            return a ? this.bind(b, a) : this.each(function () {
                try {
                    this[b]()
                } catch (a) {
                }
            }), this
        }
    }), a.Event = function (a, b) {
        p(a) || (b = a, a = b.type);
        var c = document.createEvent(r[a] || "Events"), d = !0;
        if (b) for (var e in b) "bubbles" == e ? d = !!b[e] : c[e] = b[e];
        return c.initEvent(a, d, !0), j(c)
    }
}(Zepto), function (a) {
    function b(b, c, d) {
        var e = a.Event(c);
        return a(b).trigger(e, d), !e.isDefaultPrevented()
    }

    function c(a, c, d, e) {
        if (a.global) return b(c || s, d, e)
    }

    function d(b) {
        b.global && 0 == a.active++ && c(b, null, "ajaxStart")
    }

    function e(b) {
        b.global && !--a.active && c(b, null, "ajaxStop")
    }

    function f(a, b) {
        var d = b.context;
        if (!1 === b.beforeSend.call(d, a, b) || !1 === c(b, d, "ajaxBeforeSend", [a, b])) return !1;
        c(b, d, "ajaxSend", [a, b])
    }

    function g(a, b, d, e) {
        var f = d.context, g = "success";
        d.success.call(f, a, g, b), e && e.resolveWith(f, [a, g, b]), c(d, f, "ajaxSuccess", [b, d, a]), i(g, b, d)
    }

    function h(a, b, d, e, f) {
        var g = e.context;
        e.error.call(g, d, b, a), f && f.rejectWith(g, [d, b, a]), c(e, g, "ajaxError", [d, e, a || b]), i(b, d, e)
    }

    function i(a, b, d) {
        var f = d.context;
        d.complete.call(f, b, a), c(d, f, "ajaxComplete", [b, d]), e(d)
    }

    function j() {
    }

    function k(a) {
        return a && (a = a.split(";", 2)[0]), a && (a == x ? "html" : a == w ? "json" : u.test(a) ? "script" : v.test(a) && "xml") || "text"
    }

    function l(a, b) {
        return "" == b ? a : (a + "&" + b).replace(/[&?]{1,2}/, "?")
    }

    function m(b) {
        b.processData && b.data && "string" != a.type(b.data) && (b.data = a.param(b.data, b.traditional)), !b.data || b.type && "GET" != b.type.toUpperCase() || (b.url = l(b.url, b.data), b.data = void 0)
    }

    function n(b, c, d, e) {
        return a.isFunction(c) && (e = d, d = c, c = void 0), a.isFunction(d) || (e = d, d = void 0), {
            url: b,
            data: c,
            success: d,
            dataType: e
        }
    }

    function o(b, c, d, e) {
        var f, g = a.isArray(c), h = a.isPlainObject(c);
        a.each(c, function (c, i) {
            f = a.type(i), e && (c = d ? e : e + "[" + (h || "object" == f || "array" == f ? c : "") + "]"), !e && g ? b.add(i.name, i.value) : "array" == f || !d && "object" == f ? o(b, i, d, c) : b.add(c, i)
        })
    }

    var p, q, r = 0, s = window.document, t = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        u = /^(?:text|application)\/javascript/i, v = /^(?:text|application)\/xml/i, w = "application/json",
        x = "text/html", y = /^\s*$/;
    a.active = 0, a.ajaxJSONP = function (b, c) {
        if (!("type" in b)) return a.ajax(b);
        var d, e, i = b.jsonpCallback, j = (a.isFunction(i) ? i() : i) || "jsonp" + ++r, k = s.createElement("script"),
            l = window[j], m = function (b) {
                a(k).triggerHandler("error", b || "abort")
            }, n = {abort: m};
        return c && c.promise(n), a(k).on("load error", function (f, i) {
            clearTimeout(e), a(k).off().remove(), "error" != f.type && d ? g(d[0], n, b, c) : h(null, i || "error", n, b, c), window[j] = l, d && a.isFunction(l) && l(d[0]), l = d = void 0
        }), !1 === f(n, b) ? (m("abort"), n) : (window[j] = function () {
            d = arguments
        }, k.src = b.url.replace(/\?(.+)=\?/, "?$1=" + j), s.head.appendChild(k), b.timeout > 0 && (e = setTimeout(function () {
            m("timeout")
        }, b.timeout)), n)
    }, a.ajaxSettings = {
        type: "GET",
        beforeSend: j,
        success: j,
        error: j,
        complete: j,
        context: null,
        global: !0,
        xhr: function () {
            return new window.XMLHttpRequest
        },
        accepts: {
            script: "text/javascript, application/javascript, application/x-javascript",
            json: w,
            xml: "application/xml, text/xml",
            html: x,
            text: "text/plain"
        },
        crossDomain: !1,
        timeout: 0,
        processData: !0,
        cache: !0
    }, a.ajax = function (b) {
        var c = a.extend({}, b || {}), e = a.Deferred && a.Deferred();
        for (p in a.ajaxSettings) void 0 === c[p] && (c[p] = a.ajaxSettings[p]);
        d(c), c.crossDomain || (c.crossDomain = /^([\w-]+:)?\/\/([^\/]+)/.test(c.url) && RegExp.$2 != window.location.host), c.url || (c.url = window.location.toString()), m(c), !1 === c.cache && (c.url = l(c.url, "_=" + Date.now()));
        var i = c.dataType, n = /\?.+=\?/.test(c.url);
        if ("jsonp" == i || n) return n || (c.url = l(c.url, c.jsonp ? c.jsonp + "=?" : !1 === c.jsonp ? "" : "callback=?")), a.ajaxJSONP(c, e);
        var o, r = c.accepts[i], s = {}, t = function (a, b) {
            s[a.toLowerCase()] = [a, b]
        }, u = /^([\w-]+:)\/\//.test(c.url) ? RegExp.$1 : window.location.protocol, v = c.xhr(), w = v.setRequestHeader;
        if (e && e.promise(v), c.crossDomain || t("X-Requested-With", "XMLHttpRequest"), t("Accept", r || "*/*"), (r = c.mimeType || r) && (r.indexOf(",") > -1 && (r = r.split(",", 2)[0]), v.overrideMimeType && v.overrideMimeType(r)), (c.contentType || !1 !== c.contentType && c.data && "GET" != c.type.toUpperCase()) && t("Content-Type", c.contentType || "application/x-www-form-urlencoded"), c.headers) for (q in c.headers) t(q, c.headers[q]);
        if (v.setRequestHeader = t, v.onreadystatechange = function () {
            if (4 == v.readyState) {
                v.onreadystatechange = j, clearTimeout(o);
                var b, d = !1;
                if (v.status >= 200 && v.status < 300 || 304 == v.status || 0 == v.status && "file:" == u) {
                    i = i || k(c.mimeType || v.getResponseHeader("content-type")), b = v.responseText;
                    try {
                        "script" == i ? (0, eval)(b) : "xml" == i ? b = v.responseXML : "json" == i && (b = y.test(b) ? null : a.parseJSON(b))
                    } catch (a) {
                        d = a
                    }
                    d ? h(d, "parsererror", v, c, e) : g(b, v, c, e)
                } else h(v.statusText || null, v.status ? "error" : "abort", v, c, e)
            }
        }, !1 === f(v, c)) return v.abort(), h(null, "abort", v, c, e), v;
        if (c.xhrFields) for (q in c.xhrFields) v[q] = c.xhrFields[q];
        var x = !("async" in c) || c.async;
        v.open(c.type, c.url, x, c.username, c.password);
        for (q in s) w.apply(v, s[q]);
        return c.timeout > 0 && (o = setTimeout(function () {
            v.onreadystatechange = j, v.abort(), h(null, "timeout", v, c, e)
        }, c.timeout)), v.send(c.data ? c.data : null), v
    }, a.get = function () {
        return a.ajax(n.apply(null, arguments))
    }, a.post = function () {
        var b = n.apply(null, arguments);
        return b.type = "POST", a.ajax(b)
    }, a.getJSON = function () {
        var b = n.apply(null, arguments);
        return b.dataType = "json", a.ajax(b)
    }, a.fn.load = function (b, c, d) {
        if (!this.length) return this;
        var e, f = this, g = b.split(/\s/), h = n(b, c, d), i = h.success;
        return g.length > 1 && (h.url = g[0], e = g[1]), h.success = function (b) {
            f.html(e ? a("<div>").html(b.replace(t, "")).find(e) : b), i && i.apply(f, arguments)
        }, a.ajax(h), this
    };
    var z = encodeURIComponent;
    a.param = function (a, b) {
        var c = [];
        return c.add = function (a, b) {
            this.push(z(a) + "=" + z(b))
        }, o(c, a, b), c.join("&").replace(/%20/g, "+")
    }
}(Zepto), function (a) {
    a.fn.serializeArray = function () {
        var b, c = [];
        return a([].slice.call(this.get(0).elements)).each(function () {
            b = a(this);
            var d = b.attr("type");
            "fieldset" != this.nodeName.toLowerCase() && !this.disabled && "submit" != d && "reset" != d && "button" != d && ("radio" != d && "checkbox" != d || this.checked) && c.push({
                name: b.attr("name"),
                value: b.val()
            })
        }), c
    }, a.fn.serialize = function () {
        var a = [];
        return this.serializeArray().forEach(function (b) {
            a.push(encodeURIComponent(b.name) + "=" + encodeURIComponent(b.value))
        }), a.join("&")
    }, a.fn.submit = function (b) {
        if (b) this.bind("submit", b); else if (this.length) {
            var c = a.Event("submit");
            this.eq(0).trigger(c), c.isDefaultPrevented() || this.get(0).submit()
        }
        return this
    }
}(Zepto), function (a) {
    "__proto__" in {} || a.extend(a.zepto, {
        Z: function (b, c) {
            return b = b || [], a.extend(b, a.fn), b.selector = c || "", b.__Z = !0, b
        }, isZ: function (b) {
            return "array" === a.type(b) && "__Z" in b
        }
    });
    try {
        getComputedStyle(void 0)
    } catch (a) {
        var b = getComputedStyle;
        window.getComputedStyle = function (a) {
            try {
                return b(a)
            } catch (a) {
                return null
            }
        }
    }
}(Zepto);
var imk2Version = "imk2-7.3.7";
console.warn(">>> " + imk2Version);
var isLoadedImk2 = void 0 !== window.imk2Handler;
if (isLoadedImk2) ; else {
    var _headDom, _eyeDiv, _eyeUrL = "https://sobeta.m.jd.com/imk2/js/componentInfo.js";
    location.href.indexOf("showxinfo") > -1 && (_headDom = document.querySelector("head"), (_eyeDiv = document.createElement("script")).src = _eyeUrL, _headDom.appendChild(_eyeDiv));
    var _pushIMK2Params = {
        pushMsg: function (a, b) {
            b || (b = "xujinglong@jd.com");
            var c = "https://sobeta.m.jd.com/openapp/email.action?_format_=json&content=" + encodeURIComponent(a) + "&mailsTo=" + b;
            (new Image).src = c, console.log(c)
        }
    };
    window.BadJSService = function () {
        var a;
        return a = "https://wq.jd.com/webmonitor/collect/badjs.json?", {
            error: function (b, c) {
                c && b && "" !== c && "" !== b ? (new Image).src = a + "Content=" + encodeURIComponent(" [ " + new Date + " ] " + c) + "&referer=" + encodeURIComponent(b) + "&t=" + Math.random() : console.warn("The request is rejected by BadJSService as either [kContent] or [kReferer] is null or empty.")
            }
        }
    }(), window.IMK2_COMMON_OPTION = {
        imk2AppDownloadURL4Apple: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html",
        imk2AppDownloadURL: "//h5.m.jd.com/active/download/downLoadAppIOSMPage.html?channel=jd-m",
        imk2OpenAppType: 0
    }, window.imk2Handler = function () {
        function a(a, b, c) {
            ea ? ca("#" + a).bind(b, c) : ca("#" + a).addEventListener(b, c, !1)
        }

        function b(a) {
            var b = "", c = [], d = new RegExp("^" + a + "=", "ig");
            if (document.cookie) for (var e = 0, f = (c = document.cookie.split(";")).length; e < f; e++) if (d.test(c[e].trim())) {
                b = c[e].split("=")[1];
                break
            }
            return b
        }

        function c(a, b, c, d, e) {
            var f = a + "=" + escape(b);
            if ("" != c) {
                var g = new Date;
                g.setTime(g.getTime() + 24 * c * 3600 * 1e3), f += ";expires=" + g.toGMTString()
            }
            "" != d && (f += ";path=" + d), "" != e && (f += ";domain=" + e), document.cookie = f
        }

        function d(a, b) {
            var c = null;
            if (c = b ? {
                downAppURl: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?channel=jd-m",
                downAppIos: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html",
                downWeixin: "http://a.app.qq.com/o/simple.jsp?pkgname=com.jingdong.app.mall&g_f=991850",
                downIpad: "https://itunes.apple.com/cn/app/jing-dong-hd/id434374726?mt=8",
                downAppIosPage: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html",
                inteneUrl: "openapp.jdmobile://virtual?",
                inteneUrlParams: null,
                sourceType: "JSHOP_SOURCE_TYPE",
                sourceValue: "JSHOP_SOURCE_VALUE",
                M_sourceFrom: "mxz",
                NoJumpDownLoadPage: !1,
                kepler_param: null,
                autoOpenAppEventId: "MDownLoadFloat_AppArouse",
                autoOpenAppEventParam: "",
                autoOpenIntervalTime: 0,
                autoOpenAppCallback: null,
                autoOpenAppCallbackSource: null,
                cookieFlag: null,
                noJdApp: !1,
                universalLinksUrl: "https://linkst.m.jd.com"
            } : {
                downAppURl: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?channel=jd-m",
                downAppIos: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html",
                downWeixin: "http://a.app.qq.com/o/simple.jsp?pkgname=com.jingdong.app.mall&g_f=991850",
                downIpad: "https://itunes.apple.com/cn/app/jing-dong-hd/id434374726?mt=8",
                downAppIosPage: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html",
                inteneUrl: "openapp.jdmobile://virtual?",
                inteneUrlParams: null,
                openAppBtnId: "",
                closePanelBtnId: "",
                closePanelId: "",
                openAppCallback: null,
                openAppCallbackSource: null,
                closeCallblack: null,
                closeCallblackSource: null,
                cookieFlag: null,
                sourceType: "JSHOP_SOURCE_TYPE",
                sourceValue: "JSHOP_SOURCE_VALUE",
                openAppEventId: "MDownLoadFloat_OpenNow",
                openAppEventParam: "",
                closePanelEventId: "MDownLoadFloat_Close",
                closePanelEventParam: "",
                appDownCloseIntervalTime: 0,
                appDownOpenIntervalTime: 0,
                noRecord: !1,
                isnotWriteOpenAppCookie: !1,
                M_sourceFrom: "mxz",
                msf_type: "click",
                NoJumpDownLoadPage: !1,
                kepler_param: null,
                noJdApp: !1,
                universalLinksUrl: "https://linkst.m.jd.com"
            }, a) for (var d in a) if (d && a[d] || "appDownCloseIntervalTime" == d && 0 == a[d] || "appDownOpenIntervalTime" == d && 0 == a[d] || "autoOpenIntervalTime" == d && 0 == a[d]) if ("appDownCloseIntervalTime" == d || "appDownOpenIntervalTime" == d || "autoOpenIntervalTime" == d) {
                if (/^(-     |\+)?\d+$/.test(a[d]) && a[d] >= 0) try {
                    var e = parseInt(a[d], 10);
                    c[d] = e
                } catch (a) {
                    BadJSService.error("https://imk2.jd.com/auto/open/app/convertOptions/intervalTime", "ConvertOptions intervalTime锛�" + a)
                }
            } else c[d] = a[d];
            return c
        }

        function e(a, b) {
            var c = f(a), d = function (a) {
                var b, c, d;
                if (R) return a.downIpad;
                if (a.downAppURl) {
                    var e = a.downAppURl.split("?");
                    d = e[1] ? e[1] : "channel=jd-m"
                }
                if (ma.downLoadPageSchema) {
                    if ("o" === (c = ma.downLoadPageSchema)) return "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?" + d;
                    if ("a" === c) return "//st.360buyimg.com/common/downLoadCommon/downLoadAppA.html?" + d;
                    if ("b" === c) return "//st.360buyimg.com/common/downLoadCommon/downLoadAppB.html?" + d
                } else b = T ? a.downAppIos : a.downAppURl;
                return b
            }(a);
            if (function (a) {
                if (a && location.href.indexOf("showxintent") > -1) {
                    var b = y() ? "鑷姩鍞よ捣" : "鐐瑰嚮鍞よ捣";
                    alert(b + " >>>" + a);
                    try {
                        _pushIMK2Params.pushMsg(a, null)
                    } catch (a) {
                        alert(a)
                    }
                }
            }(c = function (a) {
                var b, c, d;
                if (ma.openAppParaFromUcc) {
                    var e = ma.openAppParaFromUcc, f = a.split("params="), g = JSON.parse(f[1]), h = function (a, b) {
                        var c = {};
                        for (var d in a) c[d] = a[d];
                        for (var d in b) c[d] = b[d];
                        return c
                    }(g, e);
                    a = f[0] + "params=" + JSON.stringify(h)
                }
                return b = I(location.href), c = {}, "item.m.jd.com" == location.host && (b.wjfrom && (c.wjfrom = b.wjfrom), b.wjwxpubid && (c.wjwxpubid = b.wjwxpubid), b.wjunionid && (c.wjunionid = b.wjunionid), b.wjopenid && (c.wjopenid = b.wjopenid), b.shareActivityId && (c.shareActivityId = b.shareActivityId), b.shareRuleType && (c.shareRuleType = b.shareRuleType), b.shareToken && (c.shareToken = b.shareToken)), "shop.m.jd.com" == location.host && (b.shareActivityId && (c.shareActivityId = b.shareActivityId), b.shareRuleType && (c.shareRuleType = b.shareRuleType), b.shareToken && (c.shareToken = b.shareToken)), location.href.indexOf("ShareTm=") > -1 && b.ShareTm && (c.ShareTm = b.ShareTm), location.href.indexOf("sharetm=") > -1 && b.sharetm && (c.sharetm = b.sharetm), location.href.indexOf("shareTm=") > -1 && b.shareTm && (c.shareTm = b.shareTm), "{}" != (d = JSON.stringify(c)) && (a = a.replace("={", "={" + d.substring(1, d.length - 1) + ",")), a
            }(c)), ma.isOpenWechat && navigator.userAgent.indexOf("MicroMessenger") > -1) {
                var e = c.split("virtual?params=")[1];
                e = decodeURIComponent(e), e = "virtual?params=" + encodeURIComponent(e), new wxOpenApp(e)
            } else !function () {
                if ((P && Q || Z) && (c = "intent://m.jd.com/#Intent;scheme=" + c + ";package=com.jingdong.app.mall;end", setTimeout(function () {
                    window.location.href = c
                }, 50)), U && V >= 9 || S ? setTimeout(function () {
                    var a = document.createElement("a");
                    a.setAttribute("href", c), a.style.display = "none", document.body.appendChild(a);
                    var b = document.createEvent("HTMLEvents");
                    b.initEvent("click", !1, !1), a.dispatchEvent(b)
                }, 0) : document.querySelector("#" + _).src = c, !a.NoJumpDownLoadPage) {
                    var b = Date.now();
                    setTimeout(function () {
                        setTimeout(function () {
                            var a, c, e;
                            a = b, c = d, e = Date.now(), a && e - a < 5e3 && (window.location.href = c)
                        }, 1500)
                    }, 100)
                }
            }()
        }

        function f(a, c) {
            var d = [], e = a.inteneUrlParams, f = {category: "jump", des: "HomePage"};
            if (a.sourceType && a.sourceValue && (f.sourceType = a.sourceType, f.sourceValue = a.sourceValue, !e || e.sourceType || e.sourceValue || (e.sourceType = a.sourceType, e.sourceValue = a.sourceValue)), a && a.M_sourceFrom && (c && (a.M_sourceFrom = a.M_sourceFrom.indexOf("_UL") >= 0 ? a.M_sourceFrom : a.M_sourceFrom + "_UL"), f.M_sourceFrom = a.M_sourceFrom, e && (e.M_sourceFrom = a.M_sourceFrom)), e) for (var g in e) g && e[g] && ("[object Object]" === Object.prototype.toString.call(e[g]) ? d.push('"' + g + '":' + JSON.stringify(e[g])) : d.push('"' + g + '":"' + e[g] + '"')); else for (var g in f) g && f[g] && d.push('"' + g + '":"' + f[g] + '"');
            a && a.msf_type && d.push('"msf_type":"' + a.msf_type + '"'), oa.keplerID && (d.push('"keplerID":"' + encodeURIComponent(oa.keplerID) + '"'), d.push('"keplerFrom":"' + oa.keplerFrom + '"')), oa.keplerParamJson && d.push('"kepler_param":' + encodeURIComponent(oa.keplerParamJson)), c && (d.push('"NoJumpDownLoadPage":' + a.NoJumpDownLoadPage), d.push('"downAppIos":"' + encodeURIComponent(a.downAppIos) + '"'), d.push('"locationHref":"' + encodeURIComponent(window.location.href) + '"'));
            try {
                var h = MPing.EventSeries.getSeries();
                if (h) {
                    var i = JSON.parse(h);
                    for (var g in i) g && i[g] && (i[g] = "utr" == g || "umd" == g || "ucp" == g ? encodeURIComponent(i[g]) : i[g]);
                    i.jdv = encodeURIComponent(b("__jdv")), i.unpl = encodeURIComponent(b("unpl")), i.mt_xid = encodeURIComponent(b("mt_xid")), i.mt_subsite = encodeURIComponent(b("mt_subsite"))
                }
                var j = {
                    mt_subsite: encodeURIComponent(b("mt_subsite")),
                    __jdv: encodeURIComponent(b("__jdv")),
                    unpl: encodeURIComponent(b("unpl")),
                    __jda: encodeURIComponent(b("__jda"))
                };
                h = JSON.stringify(i), d.push('"m_param":' + h), d.push('"SE":' + JSON.stringify(j))
            } catch (a) {
                d.push('"m_param":null'), BadJSService.error("https://imk2.jd.com/auto/open/app/getIntentUrl", "getIntentUrl() Exception锛�" + a)
            }
            var l = "{" + d.join(",") + "}", m = a.inteneUrl.split("?"), n = null, o = "";
            return ma.scheme_prame && (o = "/" + ma.scheme_prame), n = 2 == m.length && m[1] ? m[0] + o + "?" + m[1] + "&params=" + l : m[0] + o + "?params=" + l, ja && k({
                url: "//ccc-x.jd.com/dsp/op?openapp_url=" + encodeURI(l) + "&openapp_source_type=100",
                method: "GET",
                async: !0,
                timeout: 1e3,
                error: function () {
                },
                success: function (a) {
                }
            }), n
        }

        function g(b) {
            b.openAppBtnId && document.querySelector("#" + b.openAppBtnId) && (ba[b.openAppBtnId] = b, b && b.M_sourceFrom && "mxz" == b.M_sourceFrom && (b.openAppEventParam ? b.openAppEventParam = b.openAppEventParam + "_other_" + window.location.hostname : b.openAppEventParam = "other_" + window.location.hostname), l(b.openAppBtnId, b.openAppEventId, b.openAppEventParam), a(b.openAppBtnId, "click", function () {
                x("0");
                var a = this.getAttribute("id"), b = ba[a];
                if (!aa) {
                    var c = document.createElement("iframe");
                    c.id = _, document.body.appendChild(c), document.getElementById(_).style.display = "none", document.getElementById(_).style.width = "0px", document.getElementById(_).style.height = "0px", aa = !0
                }
                if (b.openAppCallback) {
                    var d = b.openAppCallbackSource ? b.openAppCallbackSource : null;
                    b.openAppCallback.call(this, d)
                }
                i(b, "appDownOpenIntervalTime"), v(b), pa.isUseUniversalLinks && !b.noJdApp ? function (a) {
                    var b = f(a, !0), c = a.universalLinksUrl + "/ul/ul.action?" + b;
                    if (navigator.userAgent.indexOf("baidubrowser") >= 0) window.location.href = c; else {
                        var d = document.createElement("a");
                        d.setAttribute("href", c), d.style.display = "none", document.body.appendChild(d);
                        var e = document.createEvent("HTMLEvents");
                        e.initEvent("click", !1, !1), d.dispatchEvent(e)
                    }
                }(b) : e(b)
            }))
        }

        function h(b, c) {
            if (b.closePanelBtnId && b.closePanelId && document.querySelector("#" + b.closePanelId) && document.querySelector("#" + b.closePanelBtnId)) {
                if (ba[b.closePanelBtnId] = b, l(b.closePanelBtnId, b.closePanelEventId, b.closePanelEventParam), !c) {
                    if (j(b)) {
                        if (document.querySelector("#" + b.closePanelId).style.display = "none", b.closeCallblack) {
                            var d = event || window.event, e = b.closeCallblackSource ? b.closeCallblackSource : null;
                            b.closeCallblack.call(this, e, d)
                        }
                        return
                    }
                    document.querySelector("#" + b.closePanelId).style.display = "block"
                }
                a(b.closePanelBtnId, "click", function () {
                    var a = this.getAttribute("id"), b = ba[a];
                    if (i(b, "appDownCloseIntervalTime"), document.querySelector("#" + b.closePanelId).style.display = "none", b.closeCallblack) {
                        var c = event || window.event, d = b.closeCallblackSource ? b.closeCallblackSource : null;
                        b.closeCallblack.call(this, d, c)
                    }
                })
            }
        }

        function i(a, b) {
            var d = null;
            a.cookieFlag && a[b] ? d = "downloadAppPlugIn_downCloseDate_" + a.cookieFlag : (d = "downloadAppPlugIn_downCloseDate", a[b] = ma[b]), !a.isnotWriteOpenAppCookie && a[b] && (document.domain.indexOf(".m.jd.com") < 0 && "m.jd.com" != document.domain ? z(d, a[b]) : (c(d, Date.now() + "_" + a[b], 60, "/", ".m.jd.com"), c(d, Date.now() + "_" + a[b], 60, "/", "m.jd.hk")))
        }

        function j(a, c) {
            var d = b(na.mediaTimes), e = b(na.stepTime);
            if (d && e) {
                var f = parseInt(d), g = parseInt(e);
                return !(f && g && f > 0 & g > 0 && f % g == 0)
            }
            var h = !1, i = null;
            if (c) i = ma.msf_type && "auto" == ma.msf_type && a.cookieFlag && a.autoOpenIntervalTime ? "autoOpenApp_downCloseDate_" + a.cookieFlag : "autoOpenApp_downCloseDate_" + ma.msf_type, !ia && ma.AROUSAL_APP || (h = !0); else {
                var j = Y.indexOf("jdmsxh"), k = Y.indexOf("jdmsxh2");
                (Y.indexOf("Html5Plus") >= 0 || j >= 0 && j != k || ha || !ma.DOWNLOAD_LAYER) && (h = !0), i = a.cookieFlag && a.appDownCloseIntervalTime ? "downloadAppPlugIn_downCloseDate_" + a.cookieFlag : "downloadAppPlugIn_downCloseDate"
            }
            var l = b(i);
            !l && la.configCenterAjax && document.domain.indexOf(".m.jd.com") < 0 && "m.jd.com" != document.domain && ka && ka.hasOwnProperty(i) && (l = ka[i]);
            var m = null;
            l && (2 == (m = l.split("_")).length ? (m[0] = parseInt(m[0], 10), m[1] = parseInt(m[1], 10)) : m = null);
            var n = Date.now();
            return !h && !a.noRecord && m && 2 == m.length && n - m[0] < m[1] && (a.closePanelBtnId || c) && (h = !0), h
        }

        function k(a) {
            var b;
            try {
                b = new ActiveXObject("Msxml2.XMLHTTP")
            } catch (a) {
                try {
                    b = new ActiveXObject("Microsoft.XMLHTTP")
                } catch (a) {
                    b = new XMLHttpRequest
                }
            }
            if (b.ajaxRunError = !0, a.withCredentials) try {
                b.withCredentials = !0
            } catch (a) {
            }
            "storage.360buyimg.com" === location.host && location.href.indexOf("advertising") > -1 && (a.method = "GET");
            try {
                b.open(a.method, a.url, a.async), b.onreadystatechange = function () {
                    var c = a.source ? a.source : null;
                    if (4 == b.readyState) if (200 == b.status) {
                        b.ajaxRunError = !1;
                        var d = b.responseText;
                        a.success.call(c, d)
                    } else a.error.call(c)
                }, (T || R) && navigator.userAgent.indexOf("QQBrowser") > -1 ? b.send("xxxxx=ccccc") : b.send(null)
            } catch (a) {
            }
        }

        function l(a, b, c) {
            try {
                var d = document.getElementById(a), e = d.className;
                e ? e += " J_ping" : e = "J_ping", d.className = e, d.setAttribute("report-eventid", b), c && d.setAttribute("report-eventparam", c)
            } catch (a) {
            }
        }

        function m(a, b) {
            try {
                var c = new MPing.inputs.Click(a);
                b && (c.event_param = b), (new MPing).send(c)
            } catch (a) {
                console.log("imk2 send ping error: %o ", a)
            }
        }

        function n(a, b, c) {
            var d = b || 1, e = !!c;
            if (!j(a) && a.closePanelId) {
                var f = document.getElementById(a.closePanelId);
                if (f) {
                    var g = f.getBoundingClientRect(), h = g.height || g.bottom - g.top, i = o(f, "opacity"),
                        l = o(f, "display");
                    l && h && "none" != l && 0 == h ? e = !0 : h && i && 50 == h && 0 == i && (e = !0)
                } else e = !0;
                d < 3 && 0 == e && (d++, setTimeout(function () {
                    n(a, d, e)
                }, 2e3))
            }
            if (e) try {
                var m = new MPing.inputs.Click("MDownLoadFloat_FloatShield"), p = new MPing;
                m.event_param = a.openAppEventId, p.send(m), k({
                    url: "//mapi.m.jd.com/config/checkShield.action?_format_=json",
                    method: "POST",
                    async: !0,
                    timeout: 1e3,
                    error: function () {
                    },
                    success: function (a) {
                    }
                })
            } catch (a) {
                BadJSService.error("https://imk2.jd.com/auto/open/app/checkShield", "checkShield() Exception锛�" + a)
            }
        }

        function o(a, b) {
            return a.currentStyle ? a.currentStyle[b] : document.defaultView.getComputedStyle(a, !1)[b]
        }

        function p(a, b) {
            var c = !b && j(a.funcPrame), d = !0;
            if (!c && a && "[object Array]" === Object.prototype.toString.apply(a.ajaxPrame) && a.ajaxPrame.length > 0) {
                a.ajaxPrameIsArray = !0;
                for (var e = 0; e < a.ajaxPrame.length; e++) la[a.ajaxPrame[e].hasAjax] || (d = !1, a.ajaxPrame[e].hasAjaxSend || (k(a.ajaxPrame[e]), a.ajaxPrame[e].hasAjaxSend = !0))
            } else c || la[a.ajaxPrame.hasAjax] || (d = !1, a.ajaxPrame.hasAjaxSend || (k(a.ajaxPrame), a.ajaxPrame.hasAjaxSend = !0));
            d ? q(a.funcList, a.funcPrame) : function a(b, c) {
                var d = c || 0, e = !0;
                if (b.ajaxPrameIsArray) {
                    for (var f = 0; f < b.ajaxPrame.length; f++) if (!la[b.ajaxPrame[f].hasAjax]) {
                        e = !1;
                        break
                    }
                } else e = la[b.ajaxPrame.hasAjax];
                b.funcList && b.funcList.length > 0 && (e ? q(b.funcList, b.funcPrame) : setTimeout(function () {
                    ++d < 15 ? a(b, d) : q(b.funcList, b.funcPrame)
                }, 100))
            }(a)
        }

        function q(a, b) {
            if (1 == a.length && "[object Function]" === Object.prototype.toString.apply(a)) a(b); else for (var c = 0; c < a.length; c++) a[c](b)
        }

        function r() {
            try {
                var a, b, c = navigator.userAgent;
                if (ma.sideDownAppConfigData) {
                    if ((a = ma.sideDownAppConfigData).closeUa) for (var d = 0, e = (b = a.closeUa.split("|")).length; d < e; d++) if ("" != b[d].trim() && c.indexOf(b[d].trim()) > -1) return;
                    !function (a) {
                        var b, c, d = a.sideDownAppConfigData;
                        try {
                            if (d.jumpOpt && (c = d.jumpOpt), !(b = document.getElementById("imk2Side"))) {
                                (b = document.createElement("div")).id = "imk2Side", b.setAttribute("style", "position: fixed;  z-index: 9999; background-repeat: no-repeat; -webkit-background-size: cover; background-size: cover; -webkit-box-shadow: rgb(0, 0, 0) 0px 0px 12px 0px; box-shadow: rgb(0, 0, 0) 0px 0px 12px 0px; -webkit-border-radius: 6PX; border-radius: 6PX; transform: scale(0); transition: 800ms cubic-bezier(.17,.67,.72,1.49);  transform-origin: center;"), b.style.right = "calc(" + d.positionX + " - " + d.width + " - 5px)", b.style.top = d.positionY, b.style.width = d.width, b.style.height = d.width, document.body.appendChild(b);
                                new CommmonLightLine({
                                    domId: "#imk2Side",
                                    lineWidth: 5,
                                    base: 0,
                                    skew: "135deg",
                                    lineColor: "rgba(255, 255, 255, .3)",
                                    intervalTime: 17,
                                    time: "3",
                                    bgImage: d.iconUrl
                                }).play(), b.style.transform = "scale(1)";
                                var e = window.getComputedStyle(b, null).left;
                                (e = e.replace("px", "")) <= 0 && (b.style.right = document.documentElement.clientWidth - b.offsetWidth - 5 + "PX")
                            }
                            (function () {
                                this.addEventListener("touchmove", function (a) {
                                    a.preventDefault(a), a.stopPropagation(), this.style.transition = "";
                                    var b = a.changedTouches[0].clientX >= 0 && a.changedTouches[0].clientX < document.documentElement.clientWidth - this.offsetWidth,
                                        c = a.changedTouches[0].clientY >= 0 && a.changedTouches[0].clientY < document.documentElement.clientHeight - this.offsetHeight;
                                    (b || c) && (this.style.right = "auto", this.style.left = a.changedTouches[0].clientX + "px", this.style.top = a.changedTouches[0].clientY + "px")
                                }), this.addEventListener("touchend", function () {
                                    var a = this.getBoundingClientRect().left + this.offsetWidth / 2;
                                    document.documentElement.clientWidth / 2 >= a ? this.style.left = "5px" : this.style.left = document.documentElement.clientWidth - this.offsetWidth - 5 + "px";
                                    var b = this.getBoundingClientRect(),
                                        c = document.documentElement.clientHeight - this.offsetHeight - 5;
                                    b.top <= 0 ? this.style.top = "5px" : b.top >= c && (this.style.top = c + "px");
                                    var d = document.documentElement.clientWidth - this.offsetWidth - 5;
                                    b.left <= 0 ? this.style.left = "5px" : b.left >= d && (this.style.left = d + "px"), this.style.transition = "left 50ms ease-in"
                                })
                            }).apply(b, null);
                            var f = s();
                            $.downloadAppPlugIn({
                                openAppBtnId: "imk2Side",
                                inteneUrl: "openapp.jdmobile://virtual?",
                                inteneUrlParams: f,
                                msf_type: "click",
                                downAppURl: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?channel=jd-m&" + c,
                                downAppIos: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?" + c,
                                downWeixin: "http://a.app.qq.com/o/simple.jsp?pkgname=com.jingdong.app.mall&g_f=991850" + c,
                                downIpad: "https://itunes.apple.com/cn/app/jing-dong-hd/id434374726?mt=8" + c,
                                downAppIosPage: "//st.360buyimg.com/common/downLoadCommon/downLoadAppIOSMPage.html?" + c
                            }), d.pingData && b && b.addEventListener("click", function () {
                                m(d.pingData.eventId)
                            })
                        } catch (a) {
                            console.error(a)
                        }
                    }(ma)
                }
            } catch (a) {
            }
        }

        function s() {
            var a, b, c = {};
            switch (location.host) {
                case"shop.m.jd.com":
                    c = {
                        category: "jump",
                        des: "jshopMain",
                        shopId: I(location.href).shopId,
                        sourceType: "showside",
                        sourceValue: "true"
                    };
                    break;
                case"item.m.jd.com":
                    a = "", (b = location.pathname.split("/")) && b.length > 0 && (a = (a = b[b.length - 1]).replace(".html", "")), c = {
                        category: "jump",
                        des: "productDetail",
                        skuId: a,
                        sourceType: "showside",
                        sourceValue: "true"
                    };
                    break;
                case"h5.m.jd.com":
                    c = {
                        action: "to",
                        category: "jump",
                        des: "getCoupon",
                        url: encodeURIComponent("http://h5.m.jd.com/active/download/downLoadAppIOSMPage.html"),
                        sourceType: "showside",
                        sourceValue: "true"
                    };
                    break;
                case"pro.m.jd.com":
                    c = {
                        category: "jump",
                        des: "m",
                        url: encodeURIComponent(location.href),
                        sourceType: "showside",
                        sourceValue: "true"
                    };
                    break;
                case"sale.jd.com":
                    c = {
                        category: "jump",
                        des: "DM",
                        dmurl: location.href,
                        sourceType: "showside",
                        sourceValue: "true"
                    };
                    break;
                case"ms.m.jd.com":
                    c = {category: "jump", des: "seckill", sourceType: "showside", sourceValue: "true"};
                    break;
                case"coupon.m.jd.com":
                    c = {category: "jump", des: "couponCenter", sourceType: "showside", sourceValue: "true"}
            }
            return c
        }

        function t(a) {
            if (!j(a, !0)) {
                if (ma.msf_type && "auto" == ma.msf_type && a.cookieFlag && a.autoOpenIntervalTime ? cookieName = "autoOpenApp_downCloseDate_" + a.cookieFlag : (cookieName = "autoOpenApp_downCloseDate_" + ma.msf_type, a.autoOpenIntervalTime = ma.cooldown_time), a.autoOpenIntervalTime && (document.domain.indexOf(".m.jd.com") < 0 && "m.jd.com" != document.domain ? z(cookieName, a.autoOpenIntervalTime) : (c(cookieName, Date.now() + "_" + a.autoOpenIntervalTime, 60, "/", ".m.jd.com"), c(cookieName, Date.now() + "_" + a.autoOpenIntervalTime, 60, "/", "m.jd.hk"))), v(a), a && a.M_sourceFrom && "mxz" == a.M_sourceFrom && (a.autoOpenAppEventParam ? a.autoOpenAppEventParam = a.autoOpenAppEventParam + "_other_" + window.location.hostname : a.autoOpenAppEventParam = "other_" + window.location.hostname), m(a.autoOpenAppEventId, a.autoOpenAppEventParam), a.msf_type = ma.msf_type, a.autoOpenAppCallback) {
                    var b = a.autoOpenAppCallbackSource ? a.autoOpenAppCallbackSource : null;
                    a.autoOpenAppCallback.call(this, b)
                }
                e(a)
            }
        }

        function u(a) {
            a && ga <= a && (ia = !0)
        }

        function v(a) {
            a && a.kepler_param && (oa.keplerParamJson = a.kepler_param), oa.keplerParamJson && m("MDownLoadFloat_ArouseParam", oa.keplerParamJson)
        }

        function w(a) {
            pa.isUniversalLinksOs && pa.isUniversalLinksUa && !a.noJdApp && a.universalLinksUrl && ma.use_universallinks && (pa.isUseUniversalLinks = !0)
        }

        function x(a) {
            window.IMK2_COMMON_OPTION.imk2OpenAppType = a
        }

        function y() {
            return "1" == window.IMK2_COMMON_OPTION.imk2OpenAppType
        }

        function z(a, b) {
            try {
                var c = [];
                c.push('"' + a + '":"' + Date.now() + "_" + b + '"'), d = {
                    url: "//mapi.m.jd.com/cookie/addCookie.action?cookiePrame={" + c.join(",") + "}",
                    callback: function () {
                    }
                }, e = "MAuthentication", f = document.createElement("script"), g = function () {
                    var a, b;
                    (b = (a = f).parentNode) && b.removeChild(a), delete window[e]
                }, h = function () {
                    g()
                }, window[e] = function (a) {
                    d.callback.call(this, a), g()
                }, f.onerror = function () {
                    h()
                }, f.onload = function () {
                    h()
                }, f.src = d.url + "&callbackName=" + e, document.getElementsByTagName("head")[0].appendChild(f)
            } catch (a) {
                BadJSService.error("https://imk2.jd.com/auto/open/app/addCookieByJsonp", "addCookieByJsonp() Exception锛�" + a)
            }
            var d, e, f, g, h
        }

        function A(a) {
            !function () {
                var a = document.getElementsByTagName("head");
                if (a && a[0]) {
                    var b = document.createElement("style");
                    b.innerHTML = "\n             .carousel-wrapper{\n                width: 100%;\n                overflow: hidden;\n                position: relative;\n                height: 200px;\n                box-sizing: border-box;\n    \n            }\n            .carousel{\n                position: absolute;\n                z-index: 1;\n                left: 0;\n                top: 0;\n                padding: 0;\n                margin: 0;\n                height: 100%;\n                list-style: none;\n                box-sizing: border-box;\n    \n            }\n            .carousel li{\n                float: left;\n                position: absolute;\n                z-index: 1;\n                left: 0;\n                top: 0;\n                height: 100%;\n                box-sizing: border-box;\n    \n                /*border: 1px solid red;*/\n            }\n    \n            .m-carousel-item .m-carousel-item-col {\n                float: left;\n                width: 25%;\n                line-height: 45px;\n                height: 45px;\n                box-sizing: border-box;\n                text-align: center;\n                box-sizing: border-box;\n            }\n    \n            .carousel .m-carousel-item .m-carousel-item-col:nth-child(1){\n                width: 8%;\n            }\n            .carousel .m-carousel-item .m-carousel-item-col:nth-child(2){\n                width: 10%;\n            }\n            .carousel .m-carousel-item .m-carousel-item-col:nth-child(3) {\n                width: 57%;\n                white-space: nowrap;\n                overflow: hidden;\n                text-overflow: ellipsis;\n                font-size: 14px;\n            }\n            .carousel .m-carousel-item .m-carousel-item-col:nth-child(4){\n                width: 25%;\n                background-color: #F63515;\n                font-size: 14px;\n                color: #fff;\n                letter-spacing: 0;\n    \n            }\n    \n            .m-carousel-circle {\n                position: absolute;\n                bottom: 0;\n                left: 0;\n                z-index: 2;\n                padding: 0;\n                margin: 0;\n                list-style: none;\n                width: 100%;\n                background-color: transparent;\n                box-sizing: border-box;\n                padding: 3px 0;\n                text-align: center;\n                font-size: 0;\n                box-sizing: border-box;\n            }\n    \n            .m-carousel-circle .m-carousel-circle-item {\n                display: inline-block;\n                width: 3px;\n                height: 3px;\n                box-sizing: border-box;\n                border-radius: 50%;\n                margin: 0 3px;\n                box-sizing: border-box;\n            }\n    \n    \n    \n            .style-a .carousel li{\n                background-color: #333;\n                color: #fff;\n            }\n            .style-a .m-carousel-circle .m-carousel-circle-item{\n                background-color: #5C5C5C;\n            }\n            .style-a .m-carousel-circle .item-active{\n                background-color: #E5E5E5 !important;\n            }\n    \n    \n            .style-b .carousel li{\n                background-color: #fff;\n                color: #000;\n            }\n            .style-b .m-carousel-circle .m-carousel-circle-item{\n                background-color: #5C5C5C;\n            }\n            .style-b .m-carousel-circle .item-active{\n                background-color: #E5E5E5 !important;\n            }\n            .style-b .m-carousel-item .m-carousel-item-col:nth-child(4) {\n                width: 22%;\n                height: 30px;\n                line-height: 30px;\n                margin: 8px 1% 0 0;\n                border-radius: 5px;\n            }\n            ", a[0].appendChild(b)
                }
            }(), function (a) {
                var b = [], c = "a", d = [], e = 0;
                if (ma.carouselDownAppConfigData) {
                    c = 1 == ma.carouselDownAppConfigData.styleScheme ? "a" : "b", e = (d = ma.carouselDownAppConfigData.slides ? ma.carouselDownAppConfigData.slides : []).length, b.push('<ul class="carousel">');
                    for (var f = 0; f < e; f++) b.push('    <li class="m-carousel-item imk2-slide">'), b.push('        <div class="m-carousel-item-col imk2-btn-close" id="m_common_tip_x_' + f + '" style="display: flex; align-items: center; justify-content: center;">'), b.push('            <img src="//m.360buyimg.com/mobilecms/jfs/t19480/10/1439571805/820/787bec2c/5ac9d730N04e6d766.png" alt="浜笢" style="width: 10px; vertical-align: middle;  margin-top: 0px">'), b.push("        </div>"), b.push('        <div class="m-carousel-item-col" style="display: flex;  align-items: center; justify-content: center;"> '), b.push('            <img src="' + d[f].appLogo + '" alt="浜笢" style="width: 30px; vertical-align: middle; margin-top: 0px;"/>'), b.push("        </div>"), b.push('        <div class="m-carousel-item-col" style="padding-top: 2px">' + d[f].ad + "</div>"), b.push('        <div class="m-carousel-item-col open-app-btn" style="padding-top: 2px" id="m_common_tip_trynow_' + f + '">' + d[f].openBtn.txt + "</div>"), b.push("    </li>");
                    if (b.push("</ul>"), d && d.length > 1) {
                        b.push('<ul class="m-carousel-circle">');
                        for (var g = 0; g < e; g++) 0 == g ? b.push('    <li class="m-carousel-circle-item item-active"></li>') : b.push('    <li class="m-carousel-circle-item"></li>');
                        b.push("</ul>")
                    }
                    var h = document.getElementById(a.tipId), i = document.createElement("div");
                    i.id = a.tipId + "_div", i.classList.add("carousel-wrapper"), i.innerHTML = b.join(" "), h.appendChild(i), h.classList.add("style-" + c)
                }
            }(a);
            var b = ma.carouselDownAppConfigData.slides, c = !1;
            b && 1 == b.length && (c = !0), new mCarouselNew({
                warpperId: "#m_common_tip_div",
                intervel: 3500,
                transitionTime: 500,
                carouselHeight: 45,
                stopCarousel: c
            }).start()
        }

        function B(b) {
            if ("m.jd.com" === location.host || "beta-m.m.jd.com" === location.host) {
                var c = navigator.userAgent.indexOf("MQQBrowser") > -1 || navigator.userAgent.indexOf("UCBrowser") > -1 || navigator.userAgent.indexOf("MiuiBrowser") > -1,
                    e = location.href.indexOf("utm_campaign=t_meizubrowser_meizubrowser") > -1;
                if (c || e) return ""
            }
            var f, g, h;
            if (b && (b.downloadAppPlugIn = b.downloadAppPlugIn ? b.downloadAppPlugIn : {}, b.downloadAppPlugIn.openAppBtnId = b.tipId + "_trynow", b.downloadAppPlugIn.closePanelBtnId = b.tipId + "_x", b.downloadAppPlugIn.closePanelId = b.tipId + "_div", !j(d(b.downloadAppPlugIn)))) if (void 0 === ma.carouselDownAppConfigData) {
                if ("m.jd.com" == location.host && !document.querySelector('[href$="header.css"]')) {
                    var k = document.querySelector("head"), l = document.createElement("link");
                    l.setAttribute("rel", "stylesheet"), l.setAttribute("type", "text/css"), l.setAttribute("href", "//st.360buyimg.com/common/commonH_B/css/header.css"), k.appendChild(l)
                }
                var n = (g = "", h = {
                    downloadAppContentBtn: "绔嬪嵆鎵撳紑",
                    downloadAppContentBtnStyle: "",
                    downloadAppContentDown: "鏂颁汉棰�188鍏冪孩鍖�",
                    downloadAppContentDownStyle: "",
                    downloadAppContentUP: "鎵撳紑浜笢APP璐墿",
                    downloadAppContentUpStyle: "",
                    downloadAppImg: "//st.360buyimg.com/common/commonH_B/images/2015/download-bg.png",
                    downloadApplogo: "//st.360buyimg.com/common/commonH_B/images/2015/top-jdlogo.png"
                }, (f = b).downAppConfigData && ma.downAppConfigData && ma.downAppConfigData.hasOwnProperty("highPriority") && !ma.downAppConfigData.highPriority || f.downAppConfigData && !ma.downAppConfigData ? (h.downloadAppContentBtn = f.downAppConfigData.downloadAppContentBtn || "绔嬪嵆鎵撳紑", h.downloadAppContentBtnStyle = f.downAppConfigData.downloadAppContentBtnStyle || "", h.downloadAppContentDown = f.downAppConfigData.downloadAppContentDown || "鏂颁汉棰�188鍏冪孩鍖�", h.downloadAppContentDownStyle = f.downAppConfigData.downloadAppContentDownStyle || "", h.downloadAppContentUP = f.downAppConfigData.downloadAppContentUP || "鎵撳紑浜笢APP璐墿", h.downloadAppContentUpStyle = f.downAppConfigData.downloadAppContentUpStyle || "", h.downloadAppImg = f.downAppConfigData.downloadAppImg || "//st.360buyimg.com/common/commonH_B/images/2015/download-bg.png", h.downloadApplogo = f.downAppConfigData.downloadApplogo || "//st.360buyimg.com/common/commonH_B/images/2015/top-jdlogo.png") : ma.downAppConfigData && (h.downloadAppContentBtn = ma.downAppConfigData.downloadAppContentBtn || "绔嬪嵆鎵撳紑", h.downloadAppContentBtnStyle = ma.downAppConfigData.downloadAppContentBtnStyle || "", h.downloadAppContentDown = ma.downAppConfigData.downloadAppContentDown || "鏂颁汉棰�188鍏冪孩鍖�", h.downloadAppContentDownStyle = ma.downAppConfigData.downloadAppContentDownStyle || "", h.downloadAppContentUP = ma.downAppConfigData.downloadAppContentUP || "鎵撳紑浜笢APP璐墿", h.downloadAppContentUpStyle = ma.downAppConfigData.downloadAppContentUpStyle || "", h.downloadAppImg = ma.downAppConfigData.downloadAppImg || "//st.360buyimg.com/common/commonH_B/images/2015/download-bg.png", h.downloadApplogo = ma.downAppConfigData.downloadApplogo || "//st.360buyimg.com/common/commonH_B/images/2015/top-jdlogo.png"), f.location && (g = ' style = "' + f.location + '"'), '\t<div  id="' + f.tipId + '_div" ' + g + ' class="download-pannel download-noBg">\t    <div class="pannel-bg"><img src="' + h.downloadAppImg + '"></div>\t    <div id="' + f.tipId + '_x" class="download-close"><img src="//st.360buyimg.com/common/commonH_B/images/2015/icon-close.png"></div>\t    <div class="download-logo"><img src="' + h.downloadApplogo + '"></div>\t    <div class="download-txt">\t            <span class="download-content">\t                <em style="' + h.downloadAppContentUpStyle + '" class="content-up">' + h.downloadAppContentUP + '</em>\t                <em style="' + h.downloadAppContentDownStyle + '" class="content-down">' + h.downloadAppContentDown + '</em>\t            </span>\t    </div>\t    <div id="' + f.tipId + '_trynow" class="download-action">\t        <span style="' + h.downloadAppContentBtnStyle + '" class="font-large">' + h.downloadAppContentBtn + "</span>\t    </div>\t</div>");
                if (document.getElementById(b.tipId).innerHTML = n, "m.jd.com" == location.host) {
                    var o = document.querySelector("#m_common_tip_div");
                    o && (o.style.height = "50PX")
                }
                m("MDownLoadFloat_TopOldExpo", location.host), a(b.tipId + "_x", "click", function () {
                    b.onClickTipX && b.onClickTipX.call(this)
                }), a(b.tipId + "_trynow", "click", function () {
                    b.onClickTrynow && b.onClickTrynow.call(this)
                }), b.callFunShowTip && b.callFunShowTip.call(this);
                var p = b.downloadAppPlugIn.downAppURl, q = oa.keplerParamJson;
                p = (p = p.indexOf("M_sourceFrom") >= 0 ? p : p + "&M_sourceFrom=" + b.downloadAppPlugIn.M_sourceFrom).indexOf("isneworold") < 0 && ma.isNewUser ? p + "&isneworold=new" : p, q && q.source && q.channel && (p = (p = p.indexOf("kplsource") >= 0 ? p : p + "&kplsource=" + q.source).indexOf("kplchannel") >= 0 ? p : p + "&kplchannel=" + q.channel), b.downloadAppPlugIn.downAppURl = p, D(b.downloadAppPlugIn)
            } else {
                A(b), b.callFunShowTip && b.callFunShowTip.call(this);
                var r = ma.carouselDownAppConfigData.slides;
                m("MDownLoadFloat_TopNewExpo", location.host + "_" + r.length);
                for (var s = document.querySelectorAll("#m_common_tip_div .imk2-btn-close"), t = document.querySelectorAll("#m_common_tip_div .open-app-btn"), u = s.length, v = 0; v < u; v++) a(b.tipId + "_x_" + v, "click", function () {
                    b.onClickTipX && b.onClickTipX.call(this)
                }), s[v].addEventListener("click", function (a) {
                    ma.isnotWriteOpenAppCookie = !1, i(ma, "cooldown_time"), document.querySelector("#" + b.tipId + "_div").style.display = "none", m("MDownLoadFloat_Close")
                }), a(b.tipId + "_trynow_" + v, "click", function () {
                    b.onClickTrynow && b.onClickTrynow.call(this)
                }), b.downloadAppPlugIn.openAppBtnId = b.tipId + "_trynow_" + v, D(b.downloadAppPlugIn), r[v].openBtn.pingData.eventId && t[v].setAttribute("report-eventid", r[v].openBtn.pingData.eventId), r[v].openBtn.pingData.eventParas && t[v].setAttribute("report-eventparam", r[v].openBtn.pingData.eventParas), r[v].openBtn.pingData.eventLevel && t[v].setAttribute("report-eventlevel", r[v].openBtn.pingData.eventLevel)
            }
        }

        function C(a, b) {
            location.href.indexOf("showxintent=") > -1 && (alert("鍞よ捣绫诲瀷锛�" + a + ", 鍞よ捣鍙傛暟锛�" + JSON.stringify(b)), _pushIMK2Params.pushMsg(JSON.stringify(b), null))
        }

        function D(a) {
            a.msf_type, a.msf_type = "click", C("click", a);
            var b = d(a);
            g(b), p({ajaxPrame: [sa, ra], funcList: [w, h, n], funcPrame: b})
        }

        function E(a, b, c, d, e) {
            try {
                var f = a + "=" + escape(b);
                if ("" != c) {
                    var g = new Date;
                    g.setTime(g.getTime() + 1e3 * c), f += ";expires=" + g.toGMTString()
                }
                "" != d && (f += ";path=" + d), "" != e && (f += ";domain=" + e), document.cookie = f
            } catch (a) {
            }
        }

        function F() {
            var a = b(na.mediaTimes), c = b(na.stepTime), d = parseInt(a), e = parseInt(c);
            if (d && e && d > 0 & e > 0 && d % e == 0) {
                var f = s();
                $.downloadAppPlugInOpenApp({inteneUrlParams: f})
            }
        }

        function G() {
            var a = document.createElement("div");
            a.setAttribute("id", "_aDChannelMark"), a.style.position = "fixed", a.style.zIndex = 1e5, a.style.width = "1px", a.style.height = "1px", a.style.backgroundColor = "rgba(0, 0, 255 , .5)", a.style.left = "2px", a.style.bottom = "2px", document.body.appendChild(a)
        }

        function H() {
            var a, c,
                d = ma.mediaConfigData && ma.mediaConfigData.stepTimesLifeTime ? ma.mediaConfigData.stepTimesLifeTime : 3600;
            try {
                var e = function (a) {
                    var b = {}, c = a.split("?")[1];
                    if (c) for (var d = c.split("&"), e = 0; e < d.length; e++) {
                        var f = d[e].split("=");
                        b[f[0].toLocaleLowerCase()] = f[1]
                    }
                    return b
                }(location.href);
                if (e.ad_od && "0" == e.ad_od && e.steptime) if (a = b(na.firstAdChannelURL)) if (unescape(a) == location.href) {
                    var f = b(na.mediaTimes);
                    E(na.mediaTimes, parseInt(f) + 1, d, "/", ".jd.com"), E(na.stepTime, e.steptime, d, "/", ".jd.com"), G(), F()
                } else E(na.firstAdChannelURL, location.href, d, "/", ".jd.com"),
                    E(na.mediaTimes, 1, d, "/", ".jd.com"), E(na.stepTime, e.steptime, d, "/", ".jd.com"), G(); else E(na.firstAdChannelURL, location.href, d, "/", ".jd.com"), E(na.mediaTimes, 1, d, "/", ".jd.com"), E(na.stepTime, e.steptime, d, "/", ".jd.com"), G(); else {
                    f = b(na.mediaTimes);
                    var g = b(na.stepTime);
                    f && g ? (E(na.mediaTimes, parseInt(f) + 1, d, "/", ".jd.com"), E(na.stepTime, g, d, "/", ".jd.com"), G(), F()) : (c = document.getElementById("_aDChannelMark")) && c.remove()
                }
            } catch (a) {
                console.error(a)
            }
        }

        function I(a) {
            var b = {}, c = a.split("?")[1];
            if (c) for (var d = c.split("&"), e = 0; e < d.length; e++) {
                var f = d[e].split("=");
                b[f[0]] = f[1]
            }
            return b
        }

        var J, K, L = document.URL.toLowerCase(), M = (J = L, K = document.createElement("a"), K.href = J, {
                source: J,
                protocol: K.protocol.replace(":", ""),
                host: K.hostname,
                port: K.port,
                query: K.search,
                params: function () {
                    for (var a, b = {}, c = K.search.replace(/^\?/, "").split("&"), d = c.length, e = 0; e < d; e++) c[e] && (b[(a = c[e].split("="))[0]] = a[1]);
                    return b
                }(),
                file: (K.pathname.match(/\/([^\/?#]+)$/i) || [, ""])[1],
                hash: K.hash.replace("#", ""),
                path: K.pathname.replace(/^([^\/])/, "/$1"),
                relative: (K.href.match(/tps?:\/\/[^\/]+(.+)/) || [, ""])[1],
                segments: K.pathname.replace(/^\//, "").split("/")
            }).params, N = M.ad_od, O = M.pc_source,
            P = null != (Y = navigator.userAgent).match(/Chrome/i) && null == Y.match(/Version\/\d+\.\d+(\.\d+)?\sChrome\//i),
            Q = !!Y.match(/(Android);?[\s\/]+([\d.]+)?/), R = !!Y.match(/(iPad).*OS\s([\d_]+)/),
            S = !!Y.match(/(Mac\sOS)\sX\s([\d_]+)/), T = !(R || !Y.match(/(iPhone\sOS)\s([\d_]+)/)),
            U = (T || R) && Y.match(/Safari/), V = 0, W = Y.match(/(iPhone\sOS)\s([\d_]+)/),
            X = W && W.length > 2 && W[2].split("_").length > 0 ? W[2].split("_")[0] : "";
        U && (V = Y.match(/Version\/([\d\.]+)/));
        try {
            V = parseFloat(V[1], 10)
        } catch (J) {
        }
        var Y = window.navigator.userAgent, Z = !1,
            _ = (Z = -1 != Y.toUpperCase().indexOf("SAMSUNG-SM-N7508V"), "plugIn_downloadAppPlugIn_loadIframe"),
            aa = !1, ba = {}, ca = null, da = {}, ea = !(!window.Zepto && !window.jQuery), fa = [],
            ga = (window.localStorage, Math.floor(100 * Math.random()) + 1), ha = !1, ia = !1;
        -1 != Y.indexOf("baiduboxapp") && -1 != Y.indexOf("light") && (N && (1 == N || 2 == N || 3 == N) || O || (ia = !0, ha = !0));
        var ja = !1, ka = null, la = {closeUaAjax: !1, keplerAjax: !1, configCenterAjax: !1}, ma = {
                AROUSAL_APP: !0,
                DOWNLOAD_LAYER: !0,
                GENERAL_HEAD: !0,
                msf_type: "auto",
                cooldown_time: 0,
                scheme_prame: "",
                use_universallinks: !0,
                appDownCloseIntervalTime: 0,
                appDownOpenIntervalTime: 0,
                downAppConfigData: null,
                isNewUser: !1,
                isOpenWechat: !0,
                sideDownAppConfigData: null,
                downAppUrl: "http://h5.m.jd.com/active/download/downLoadAppIOSMPage.html",
                mediaConfigData: null
            }, na = {mediaTimes: "mediaTimes", stepTime: "stepTime", firstAdChannelURL: "firstAdChannelURL"},
            oa = {keplerID: null, keplerFrom: 1, keplerParamJson: null},
            pa = {isUniversalLinksUa: !1, isUniversalLinksOs: !1, isUseUniversalLinks: !1},
            qa = {login_State: !1, pcScan_Layer: null, newPeople_Layer: null}, ra = {
                url: "//mapi.m.jd.com/config/display.action?_format_=json" + (location.href.indexOf("showSide=true") > -1 || location.href.indexOf("showside=true") > -1 ? "&showside=true" : "") + function () {
                    var a, b;
                    return b = location.href, (a = b.indexOf("?")) > -1 ? "&" + b.substring(a + 1, b.length) : ""
                }(),
                method: "POST",
                async: !0,
                timeout: 1400,
                withCredentials: !0,
                hasAjax: "configCenterAjax",
                hasAjaxSend: !1,
                error: function () {
                    la.configCenterAjax = !0
                },
                success: function (a) {
                    la.configCenterAjax = !0;
                    try {
                        !function (a) {
                            if (a) {
                                if (a.data) for (var b = 0; b < a.data.length; b++) {
                                    var c = a.data[b];
                                    c.compent && ma.hasOwnProperty(c.compent) && (ma[c.compent] = c.display, "AROUSAL_APP" == c.compent && c.msf_type && (ma.msf_type = c.msf_type, a.cooldown && a.cooldown[c.msf_type] && (ma.cooldown_time = a.cooldown[c.msf_type]), c.openAppParam && (ma.scheme_prame = c.openAppParam), c.hasOwnProperty("isSpportUL") && !c.isSpportUL && (ma.use_universallinks = !1)))
                                }
                                a.kepler && (a.kepler.kepler_data && a.kepler.kepler_data.keplerID && a.kepler.kepler_data.keplerFrom && (oa.keplerID = a.kepler.kepler_data.keplerID, oa.keplerFrom = a.kepler.kepler_data.keplerFrom), a.kepler.kepler_param && (oa.keplerParamJson = JSON.stringify(a.kepler.kepler_param))), a.isNewUser && (ma.isNewUser = !0), a.loginState && (qa.login_State = a.loginState), a.shieldingLayer && (qa.pcScan_Layer = a.shieldingLayer.pcScan_Layer || "", qa.newPeople_Layer = a.shieldingLayer.newPeople_Layer || ""), a.openSendOutUrl && (ja = a.openSendOutUrl), a.coolDownTimeUtil && (ka = a.coolDownTimeUtil), a.downAppConfigData && (ma.downAppConfigData = a.downAppConfigData), ma.isOpenWechat = !!a.isOpenWechat, a.sideDownAppConfigData && (ma.sideDownAppConfigData = a.sideDownAppConfigData), a.downAppUrl && (ma.downAppUrl = a.downAppUrl), a.mediaConfigData && (ma.mediaConfigData = a.mediaConfigData), a.carouselDownAppConfigData && (ma.carouselDownAppConfigData = a.carouselDownAppConfigData), a.openAppParaFromUcc && (ma.openAppParaFromUcc = a.openAppParaFromUcc), a.downLoadPageSchema && (ma.downLoadPageSchema = a.downLoadPageSchema)
                            }
                        }(a = JSON.parse(a))
                    } catch (a) {
                        BadJSService.error("https://imk2.jd.com/auto/open/app/configCenter/ajax/success/exception", "configCenterAjaxPrame Exception锛�" + a)
                    }
                }
            }, sa = {
                url: "//mapi.m.jd.com/config/closeUa.action?_format_=json",
                method: "POST",
                async: !0,
                timeout: 1200,
                hasAjax: "closeUaAjax",
                hasAjaxSend: !1,
                error: function () {
                    la.closeUaAjax = !0
                },
                success: function (a) {
                    la.closeUaAjax = !0;
                    try {
                        (a = JSON.parse(a)) && a.ua && function (a) {
                            if (a) {
                                if (a && a.clickCloseUa) for (var b = a.clickCloseUa.split("|"), c = 0; c < b.length; c++) {
                                    var d = b[c];
                                    if (d && Y.indexOf(d) >= 0) {
                                        ha = !0;
                                        break
                                    }
                                }
                                if (a && !ha && a.closeUaMoreKeys) for (var c = 0; c < a.closeUaMoreKeys.length; c++) {
                                    var e = a.closeUaMoreKeys[c];
                                    if (e && e.uaMoreKeys) {
                                        for (var f = e.uaMoreKeys.split("|"), g = 0, h = 0, i = 0; i < f.length; i++) {
                                            var j = f[i];
                                            j && h++, j && Y.indexOf(j) >= 0 && g++
                                        }
                                        ha = h == g || ha
                                    }
                                    if (ha) break
                                }
                                if (a && a.autoCloseBrowser) for (var c = 0; c < a.autoCloseBrowser.length; c++) {
                                    var j = a.autoCloseBrowser[c];
                                    if (j.browser && j.abtest && Y.indexOf(j.browser) >= 0) {
                                        u(j.abtest);
                                        break
                                    }
                                }
                                if (!ia && a && a.autoCloseOs) for (var c = 0; c < a.autoCloseOs.length; c++) {
                                    var j = a.autoCloseOs[c];
                                    if ("IOS" == j.os && j.abtest && (R || T)) {
                                        u(j.abtest);
                                        break
                                    }
                                    if ("Android" == j.os && j.abtest && Q) {
                                        u(j.abtest);
                                        break
                                    }
                                }
                                if (!ia && a && a.autoCloseOsAndBrowser) for (var c = 0; c < a.autoCloseOsAndBrowser.length; c++) {
                                    var j = a.autoCloseOsAndBrowser[c];
                                    if (j.os && j.browser && j.abtest && "IOS" == j.os && (R || T) && Y.indexOf(j.browser) >= 0) {
                                        u(j.abtest);
                                        break
                                    }
                                    if (j.os && j.browser && j.abtest && "Android" == j.os && Q && Y.indexOf(j.browser) >= 0) {
                                        u(j.abtest);
                                        break
                                    }
                                }
                                if (a && a.universalLinksUa) for (var k = a.universalLinksUa.split("|"), c = 0; c < k.length; c++) {
                                    var d = k[c];
                                    if (d && Y.indexOf(d) >= 0 && !Q) {
                                        pa.isUniversalLinksUa = !0;
                                        break
                                    }
                                }
                                if (a && a.universalLinksOs) for (var l = a.universalLinksOs.split("|"), c = 0; c < l.length; c++) {
                                    var d = l[c];
                                    if (d && X && d == X && X > 8) {
                                        pa.isUniversalLinksOs = !0;
                                        break
                                    }
                                }
                                a && a.appDownCloseIntervalTime && (ma.appDownCloseIntervalTime = a.appDownCloseIntervalTime), a && a.appDownOpenIntervalTime && (ma.appDownOpenIntervalTime = a.appDownOpenIntervalTime), -1 != Y.indexOf("baiduboxapp") && -1 != Y.indexOf("light") && (N && (1 == N || 2 == N || 3 == N) || O || (ia = !0, ha = !0))
                            }
                        }(a = JSON.parse(a.ua))
                    } catch (a) {
                        BadJSService.error("https://imk2.jd.com/auto/open/app/closeUaAjaxPrame/ajax/success/exception", "closeUaAjaxPrame Exception锛�" + a)
                    }
                }
            };
        return ea ? (ca = window.$, da = window.$) : (ca = function (a) {
            return "object" == typeof a ? a : document.querySelector(a)
        }, window.$ ? da = window.$ : window.$ = da = ca), window.onblur = function () {
            for (var a = 0; a < fa.length; a++) clearTimeout(fa[a])
        }, da.isAutoOpenJDApp = y, da.downloadAppPlugIn = D, da.downloadAppPlugInOpenApp = function (a) {
            x("1"), C("auto", a), a.msf_type, a.msf_type = "auto";
            var b = d(a, !0);
            if (!aa) {
                var c = document.createElement("iframe");
                c.id = _, document.body.appendChild(c), document.getElementById(_).style.display = "none", document.getElementById(_).style.width = "0px", document.getElementById(_).style.height = "0px", aa = !0
            }
            p({ajaxPrame: [sa, ra], funcList: t, funcPrame: b}, !0)
        }, da.downloadAppLayerConfigData = function (a) {
            p({ajaxPrame: [sa, ra], funcList: [B], funcPrame: a})
        }, {
            immediateExecutionFun: function (a) {
                p({ajaxPrame: [ra], funcList: [r, H], funcPrame: {}}, !0)
            }
        }
    }(), window.imk2Handler.immediateExecutionFun(), function (a, b) {
        var c, d, e = (c = a, (d = function (a, b) {
            this.init(a, b)
        }).prototype = {
            init: function (a, b) {
                this.cb = b, this.param = a, this.scheme = "openapp.jdmobile", this.appId = "wxe75a2e68877315fb", this.packageName = "com.jingdong.app.mall", this.packageUrl = "openApp.jdMobile://", this.configUrl = "//wq.jd.com/bases/jssdk/GetWxJsApiSign?url=" + encodeURIComponent(location.href.split("#")[0]) + "&callback=callback", this.loadWxJsSdk()
            }, loadWxJsSdk: function () {
                var a = this, b = "//res.wx.qq.com/open/js/jweixin-1.2.0.js";
                if ("function" == typeof define && (define.amd || define.cmd)) try {
                    require([b], function (b) {
                        c.wx = b, a.requestConfig()
                    })
                } catch (a) {
                    BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/jweixin/load", "amd or cmd load -- wxJsSdk( " + b + " ) load fail!!! , e = " + a + " CurrentURL = " + location.href)
                } else {
                    var d = document.createElement("script");
                    d.src = b, document.body.appendChild(d), d.onload = d.onreadystatechange = function () {
                        if (!this.readyState || "loaded" === this.readyState || "complete" === this.readyState) {
                            if ("undefined" == typeof wx) return console.error("wxJsSdk( " + b + " ) load fail!"), void BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/jweixin/load", "script tag load ---  wxJsSdk( " + b + " ) load fail!!! CurrentURL = " + location.href);
                            a.requestConfig(), d.onload = d.onreadystatechange = null
                        }
                    }
                }
            }, requestConfig: function () {
                var a = this;
                a.JSONP(a.configUrl, function (b) {
                    a.openApp(b)
                })
            }, openApp: function (a) {
                var b = this;
                wx.config({
                    beta: !0,
                    debug: !1,
                    appId: a.appId,
                    timestamp: a.timestamp,
                    nonceStr: a.nonceStr,
                    signature: a.signature,
                    jsApiList: ["checkJsApi", "getInstallState", "launchApplication"]
                }), wx.ready(function () {
                    wx.checkJsApi({
                        jsApiList: ["getInstallState", "launchApplication"], success: function (a) {
                            a.checkResult.getInstallState && a.checkResult.launchApplication ? wx.invoke("getInstallState", {
                                packageName: b.packageName,
                                packageUrl: b.packageUrl
                            }, function (a) {
                                a.err_msg && a.err_msg.indexOf("get_install_state:yes") > -1 ? wx.invoke("launchApplication", {
                                    appID: b.scheme,
                                    parameter: b.param
                                }, function (a) {
                                    a ? a.err_msg && a.err_msg.indexOf("fail") > -1 && (BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/invoke/result", "wechat launchApplication method锛歠alil!  res = " + JSON.stringify(a) + "銆� res = " + JSON.stringify(a) + " ,   CurrentURL = " + location.href), $.isAutoOpenJDApp() || setTimeout(function () {
                                        navigator.userAgent.match(/(Android);?[\s\/]+([\d.]+)?/) ? location.href = c.IMK2_COMMON_OPTION.imk2AppDownloadURL : location.href = c.IMK2_COMMON_OPTION.imk2AppDownloadURL4Apple
                                    }, 200)) : BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/invoke/result", "wechat launchApplication method 锛欵xception! res = " + JSON.stringify(a) + " ,   CurrentURL = " + location.href)
                                }) : a.err_msg && a.err_msg.indexOf("get_install_state:no") > -1 ? (BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/invoke/result", "wechat getInstallState method res.err_msg 锛�" + a.err_msg + "  ,   CurrentURL = " + location.href), $.isAutoOpenJDApp() || setTimeout(function () {
                                    navigator.userAgent.match(/(Android);?[\s\/]+([\d.]+)?/) ? location.href = c.IMK2_COMMON_OPTION.imk2AppDownloadURL : location.href = c.IMK2_COMMON_OPTION.imk2AppDownloadURL4Apple
                                }, 200)) : a.err_msg && a.err_msg.indexOf("missing auguments") > -1 ? BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/invoke/result", "wechat getInstallState method  res.err_msg 锛�" + a.err_msg + "  ,   CurrentURL = " + location.href) : BadJSService.error("https://imk2.jd.com/auto/open/app/wechat/invoke/result", "wechat getInstallState method res.err_msg 锛�" + a.err_msg + "  ,   CurrentURL = " + location.href)
                            }) : "function" == typeof this.cb && this.cb()
                        }
                    })
                })
            }, JSONP: function (a, b) {
                var d = function () {
                    return (new Date).getTime()
                };
                !function (a, b) {
                    var e, f = /callback=(\w+)/.exec(a);
                    f && f[1] ? e = f[1] : (e = "jsonp_" + d() + "_" + Math.random().toString().substr(2), a = (a = a.replace("callback=?", "callback=" + e)).replace("callback=%3F", "callback=" + e));
                    var g = document.createElement("script");
                    g.type = "text/javascript", g.src = a, g.id = "id_" + e, c[e] = function (a) {
                        c[e] = void 0;
                        var d, f;
                        (f = (d = document.getElementById("id_" + e)).parentNode) && 11 !== f.nodeType && f.removeChild(d), b(a)
                    };
                    var h = document.getElementsByTagName("head");
                    h && h[0] && h[0].appendChild(g)
                }(a, b)
            }
        }, d);
        a.wxOpenApp = e, "object" == typeof module && module.exports && (module.exports = e)
    }(window)
}
!function (a, b, c) {
    function d(a, c) {
        this.wrapper = "string" == typeof a ? b.querySelector(a) : a, this.scroller = this.wrapper.children[0], this.scrollerStyle = this.scroller.style, this.options = {
            resizeScrollbars: !0,
            mouseWheelSpeed: 20,
            snapThreshold: .334,
            startX: 0,
            startY: 0,
            scrollY: !0,
            directionLockThreshold: 5,
            momentum: !0,
            bounce: !0,
            bounceTime: 600,
            bounceEasing: "",
            preventDefault: !0,
            preventDefaultException: {tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT)$/},
            HWCompositing: !0,
            useTransition: !0,
            useTransform: !0
        };
        for (var d in c) this.options[d] = c[d];
        this.translateZ = this.options.HWCompositing && h.hasPerspective ? " translateZ(0)" : "", this.options.useTransition = h.hasTransition && this.options.useTransition, this.options.useTransform = h.hasTransform && this.options.useTransform, this.options.eventPassthrough = !0 === this.options.eventPassthrough ? "vertical" : this.options.eventPassthrough, this.options.preventDefault = !this.options.eventPassthrough && this.options.preventDefault, this.options.scrollY = "vertical" != this.options.eventPassthrough && this.options.scrollY, this.options.scrollX = "horizontal" != this.options.eventPassthrough && this.options.scrollX, this.options.freeScroll = this.options.freeScroll && !this.options.eventPassthrough, this.options.directionLockThreshold = this.options.eventPassthrough ? 0 : this.options.directionLockThreshold, this.options.bounceEasing = "string" == typeof this.options.bounceEasing ? h.ease[this.options.bounceEasing] || h.ease.circular : this.options.bounceEasing, this.options.resizePolling = void 0 === this.options.resizePolling ? 60 : this.options.resizePolling, !0 === this.options.tap && (this.options.tap = "tap"), "scale" == this.options.shrinkScrollbars && (this.options.useTransition = !1), this.options.invertWheelDirection = this.options.invertWheelDirection ? -1 : 1, this.x = 0, this.y = 0, this.directionX = 0, this.directionY = 0, this._events = {}, this._init(), this.refresh(), this.scrollTo(this.options.startX, this.options.startY), this.enable()
    }

    function e(a, c, d) {
        var e = b.createElement("div"), f = b.createElement("div");
        return !0 === d && (e.style.cssText = "position:absolute;z-index:9999", f.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px"), f.className = "iScrollIndicator", "h" == a ? (!0 === d && (e.style.cssText += ";height:7px;left:2px;right:2px;bottom:0", f.style.height = "100%"), e.className = "iScrollHorizontalScrollbar") : (!0 === d && (e.style.cssText += ";width:7px;bottom:2px;top:2px;right:1px", f.style.width = "100%"), e.className = "iScrollVerticalScrollbar"), e.style.cssText += ";overflow:hidden", c || (e.style.pointerEvents = "none"), e.appendChild(f), e
    }

    function f(c, d) {
        this.wrapper = "string" == typeof d.el ? b.querySelector(d.el) : d.el, this.wrapperStyle = this.wrapper.style, this.indicator = this.wrapper.children[0], this.indicatorStyle = this.indicator.style, this.scroller = c, this.options = {
            listenX: !0,
            listenY: !0,
            interactive: !1,
            resize: !0,
            defaultScrollbars: !1,
            shrink: !1,
            fade: !1,
            speedRatioX: 0,
            speedRatioY: 0
        };
        for (var e in d) this.options[e] = d[e];
        this.sizeRatioX = 1, this.sizeRatioY = 1, this.maxPosX = 0, this.maxPosY = 0, this.options.interactive && (this.options.disableTouch || (h.addEvent(this.indicator, "touchstart", this), h.addEvent(a, "touchend", this)), this.options.disablePointer || (h.addEvent(this.indicator, h.prefixPointerEvent("pointerdown"), this), h.addEvent(a, h.prefixPointerEvent("pointerup"), this)), this.options.disableMouse || (h.addEvent(this.indicator, "mousedown", this), h.addEvent(a, "mouseup", this))), this.options.fade && (this.wrapperStyle[h.style.transform] = this.scroller.translateZ, this.wrapperStyle[h.style.transitionDuration] = h.isBadAndroid ? "0.001s" : "0ms", this.wrapperStyle.opacity = "0")
    }

    var g = a.requestAnimationFrame || a.webkitRequestAnimationFrame || a.mozRequestAnimationFrame || a.oRequestAnimationFrame || a.msRequestAnimationFrame || function (b) {
        a.setTimeout(b, 1e3 / 60)
    }, h = function () {
        function d(a) {
            return !1 !== g && ("" === g ? a : g + a.charAt(0).toUpperCase() + a.substr(1))
        }

        var e = {}, f = b.createElement("div").style, g = function () {
            for (var a = ["t", "webkitT", "MozT", "msT", "OT"], b = 0, c = a.length; b < c; b++) if (a[b] + "ransform" in f) return a[b].substr(0, a[b].length - 1);
            return !1
        }();
        e.getTime = Date.now || function () {
            return (new Date).getTime()
        }, e.extend = function (a, b) {
            for (var c in b) a[c] = b[c]
        }, e.addEvent = function (a, b, c, d) {
            a.addEventListener(b, c, !!d)
        }, e.removeEvent = function (a, b, c, d) {
            a.removeEventListener(b, c, !!d)
        }, e.prefixPointerEvent = function (b) {
            return a.MSPointerEvent ? "MSPointer" + b.charAt(9).toUpperCase() + b.substr(10) : b
        }, e.momentum = function (a, b, d, e, f, g) {
            var h, i, j = a - b, k = c.abs(j) / d;
            return g = void 0 === g ? 6e-4 : g, h = a + k * k / (2 * g) * (j < 0 ? -1 : 1), i = k / g, h < e ? (h = f ? e - f / 2.5 * (k / 8) : e, j = c.abs(h - a), i = j / k) : h > 0 && (h = f ? f / 2.5 * (k / 8) : 0, j = c.abs(a) + h, i = j / k), {
                destination: c.round(h),
                duration: i
            }
        };
        var h = d("transform");
        return e.extend(e, {
            hasTransform: !1 !== h,
            hasPerspective: d("perspective") in f,
            hasTouch: "ontouchstart" in a,
            hasPointer: a.PointerEvent || a.MSPointerEvent,
            hasTransition: d("transition") in f
        }), e.isBadAndroid = /Android /.test(a.navigator.appVersion) && !/Chrome\/\d/.test(a.navigator.appVersion), e.extend(e.style = {}, {
            transform: h,
            transitionTimingFunction: d("transitionTimingFunction"),
            transitionDuration: d("transitionDuration"),
            transitionDelay: d("transitionDelay"),
            transformOrigin: d("transformOrigin")
        }), e.hasClass = function (a, b) {
            return new RegExp("(^|\\s)" + b + "(\\s|$)").test(a.className)
        }, e.addClass = function (a, b) {
            if (!e.hasClass(a, b)) {
                var c = a.className.split(" ");
                c.push(b), a.className = c.join(" ")
            }
        }, e.removeClass = function (a, b) {
            if (e.hasClass(a, b)) {
                var c = new RegExp("(^|\\s)" + b + "(\\s|$)", "g");
                a.className = a.className.replace(c, " ")
            }
        }, e.offset = function (a) {
            for (var b = -a.offsetLeft, c = -a.offsetTop; a = a.offsetParent;) b -= a.offsetLeft, c -= a.offsetTop;
            return {left: b, top: c}
        }, e.preventDefaultException = function (a, b) {
            for (var c in b) if (b[c].test(a[c])) return !0;
            return !1
        }, e.extend(e.eventType = {}, {
            touchstart: 1,
            touchmove: 1,
            touchend: 1,
            mousedown: 2,
            mousemove: 2,
            mouseup: 2,
            pointerdown: 3,
            pointermove: 3,
            pointerup: 3,
            MSPointerDown: 3,
            MSPointerMove: 3,
            MSPointerUp: 3
        }), e.extend(e.ease = {}, {
            quadratic: {
                style: "cubic-bezier(0.25, 0.46, 0.45, 0.94)", fn: function (a) {
                    return a * (2 - a)
                }
            }, circular: {
                style: "cubic-bezier(0.1, 0.57, 0.1, 1)", fn: function (a) {
                    return c.sqrt(1 - --a * a)
                }
            }, back: {
                style: "cubic-bezier(0.175, 0.885, 0.32, 1.275)", fn: function (a) {
                    var b = 4;
                    return (a -= 1) * a * ((b + 1) * a + b) + 1
                }
            }, bounce: {
                style: "", fn: function (a) {
                    return (a /= 1) < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
                }
            }, elastic: {
                style: "", fn: function (a) {
                    var b = .22;
                    return 0 === a ? 0 : 1 == a ? 1 : .4 * c.pow(2, -10 * a) * c.sin((a - b / 4) * (2 * c.PI) / b) + 1
                }
            }
        }), e.tap = function (a, c) {
            var d = b.createEvent("Event");
            d.initEvent(c, !0, !0), d.pageX = a.pageX, d.pageY = a.pageY, a.target.dispatchEvent(d)
        }, e.click = function (a) {
            var c, d = a.target;
            /(SELECT|INPUT|TEXTAREA)/i.test(d.tagName) || (c = b.createEvent("MouseEvents"), c.initMouseEvent("click", !0, !0, a.view, 1, d.screenX, d.screenY, d.clientX, d.clientY, a.ctrlKey, a.altKey, a.shiftKey, a.metaKey, 0, null), c._constructed = !0, d.dispatchEvent(c))
        }, e
    }();
    d.prototype = {
        version: "5.1.2", _init: function () {
            this._initEvents(), (this.options.scrollbars || this.options.indicators) && this._initIndicators(), this.options.mouseWheel && this._initWheel(), this.options.snap && this._initSnap(), this.options.keyBindings && this._initKeys()
        }, destroy: function () {
            this._initEvents(!0), this._execEvent("destroy")
        }, _transitionEnd: function (a) {
            a.target == this.scroller && this.isInTransition && (this._transitionTime(), this.resetPosition(this.options.bounceTime) || (this.isInTransition = !1, this._execEvent("scrollEnd")))
        }, _start: function (a) {
            if ((1 == h.eventType[a.type] || 0 === a.button) && this.enabled && (!this.initiated || h.eventType[a.type] === this.initiated)) {
                !this.options.preventDefault || h.isBadAndroid || h.preventDefaultException(a.target, this.options.preventDefaultException) || a.preventDefault();
                var b, d = a.touches ? a.touches[0] : a;
                this.initiated = h.eventType[a.type], this.moved = !1, this.distX = 0, this.distY = 0, this.directionX = 0, this.directionY = 0, this.directionLocked = 0, this._transitionTime(), this.startTime = h.getTime(), this.options.useTransition && this.isInTransition ? (this.isInTransition = !1, b = this.getComputedPosition(), this._translate(c.round(b.x), c.round(b.y)), this._execEvent("scrollEnd")) : !this.options.useTransition && this.isAnimating && (this.isAnimating = !1, this._execEvent("scrollEnd")), this.startX = this.x, this.startY = this.y, this.absStartX = this.x, this.absStartY = this.y, this.pointX = d.pageX, this.pointY = d.pageY, this._execEvent("beforeScrollStart")
            }
        }, _move: function (a) {
            if (this.enabled && h.eventType[a.type] === this.initiated) {
                this.options.preventDefault && a.preventDefault();
                var b, d, e, f, g = a.touches ? a.touches[0] : a, i = g.pageX - this.pointX, j = g.pageY - this.pointY,
                    k = h.getTime();
                if (this.pointX = g.pageX, this.pointY = g.pageY, this.distX += i, this.distY += j, e = c.abs(this.distX), f = c.abs(this.distY), !(k - this.endTime > 300 && e < 10 && f < 10)) {
                    if (this.directionLocked || this.options.freeScroll || (e > f + this.options.directionLockThreshold ? this.directionLocked = "h" : f >= e + this.options.directionLockThreshold ? this.directionLocked = "v" : this.directionLocked = "n"), "h" == this.directionLocked) {
                        if ("vertical" == this.options.eventPassthrough) a.preventDefault(); else if ("horizontal" == this.options.eventPassthrough) return void (this.initiated = !1);
                        j = 0
                    } else if ("v" == this.directionLocked) {
                        if ("horizontal" == this.options.eventPassthrough) a.preventDefault(); else if ("vertical" == this.options.eventPassthrough) return void (this.initiated = !1);
                        i = 0
                    }
                    i = this.hasHorizontalScroll ? i : 0, j = this.hasVerticalScroll ? j : 0, b = this.x + i, d = this.y + j, (b > 0 || b < this.maxScrollX) && (b = this.options.bounce ? this.x + i / 3 : b > 0 ? 0 : this.maxScrollX), (d > 0 || d < this.maxScrollY) && (d = this.options.bounce ? this.y + j / 3 : d > 0 ? 0 : this.maxScrollY), this.directionX = i > 0 ? -1 : i < 0 ? 1 : 0, this.directionY = j > 0 ? -1 : j < 0 ? 1 : 0, this.moved || this._execEvent("scrollStart"), this.moved = !0, this._translate(b, d), k - this.startTime > 300 && (this.startTime = k, this.startX = this.x, this.startY = this.y)
                }
            }
        }, _end: function (a) {
            if (this.enabled && h.eventType[a.type] === this.initiated) {
                this.options.preventDefault && !h.preventDefaultException(a.target, this.options.preventDefaultException) && a.preventDefault();
                var b, d, e = (a.changedTouches && a.changedTouches[0], h.getTime() - this.startTime),
                    f = c.round(this.x), g = c.round(this.y), i = c.abs(f - this.startX), j = c.abs(g - this.startY),
                    k = 0, l = "";
                if (this.isInTransition = 0, this.initiated = 0, this.endTime = h.getTime(), !this.resetPosition(this.options.bounceTime)) {
                    if (this.scrollTo(f, g), !this.moved) return this.options.tap && h.tap(a, this.options.tap), this.options.click && h.click(a), void this._execEvent("scrollCancel");
                    if (this._events.flick && e < 200 && i < 100 && j < 100) return void this._execEvent("flick");
                    if (this.options.momentum && e < 300 && (b = this.hasHorizontalScroll ? h.momentum(this.x, this.startX, e, this.maxScrollX, this.options.bounce ? this.wrapperWidth : 0, this.options.deceleration) : {
                        destination: f,
                        duration: 0
                    }, d = this.hasVerticalScroll ? h.momentum(this.y, this.startY, e, this.maxScrollY, this.options.bounce ? this.wrapperHeight : 0, this.options.deceleration) : {
                        destination: g,
                        duration: 0
                    }, f = b.destination, g = d.destination, k = c.max(b.duration, d.duration), this.isInTransition = 1), this.options.snap) {
                        var m = this._nearestSnap(f, g);
                        this.currentPage = m, k = this.options.snapSpeed || c.max(c.max(c.min(c.abs(f - m.x), 1e3), c.min(c.abs(g - m.y), 1e3)), 300), f = m.x, g = m.y, this.directionX = 0, this.directionY = 0, l = this.options.bounceEasing
                    }
                    if (f != this.x || g != this.y) return (f > 0 || f < this.maxScrollX || g > 0 || g < this.maxScrollY) && (l = h.ease.quadratic), void this.scrollTo(f, g, k, l);
                    this._execEvent("scrollEnd")
                }
            }
        }, _resize: function () {
            var a = this;
            clearTimeout(this.resizeTimeout), this.resizeTimeout = setTimeout(function () {
                a.refresh()
            }, this.options.resizePolling)
        }, resetPosition: function (a) {
            var b = this.x, c = this.y;
            return a = a || 0, !this.hasHorizontalScroll || this.x > 0 ? b = 0 : this.x < this.maxScrollX && (b = this.maxScrollX), !this.hasVerticalScroll || this.y > 0 ? c = 0 : this.y < this.maxScrollY && (c = this.maxScrollY), (b != this.x || c != this.y) && (this.scrollTo(b, c, a, this.options.bounceEasing), !0)
        }, disable: function () {
            this.enabled = !1
        }, enable: function () {
            this.enabled = !0
        }, refresh: function () {
            this.wrapper.offsetHeight;
            this.wrapperWidth = this.wrapper.clientWidth, this.wrapperHeight = this.wrapper.clientHeight, this.scrollerWidth = this.scroller.offsetWidth, this.scrollerHeight = this.scroller.offsetHeight, this.maxScrollX = this.wrapperWidth - this.scrollerWidth, this.maxScrollY = this.wrapperHeight - this.scrollerHeight, this.hasHorizontalScroll = this.options.scrollX && this.maxScrollX < 0, this.hasVerticalScroll = this.options.scrollY && this.maxScrollY < 0, this.hasHorizontalScroll || (this.maxScrollX = 0, this.scrollerWidth = this.wrapperWidth), this.hasVerticalScroll || (this.maxScrollY = 0, this.scrollerHeight = this.wrapperHeight), this.endTime = 0, this.directionX = 0, this.directionY = 0, this.wrapperOffset = h.offset(this.wrapper), this._execEvent("refresh"), this.resetPosition()
        }, on: function (a, b) {
            this._events[a] || (this._events[a] = []), this._events[a].push(b)
        }, off: function (a, b) {
            if (this._events[a]) {
                var c = this._events[a].indexOf(b);
                c > -1 && this._events[a].splice(c, 1)
            }
        }, _execEvent: function (a) {
            if (this._events[a]) {
                var b = 0, c = this._events[a].length;
                if (c) for (; b < c; b++) this._events[a][b].apply(this, [].slice.call(arguments, 1))
            }
        }, scrollBy: function (a, b, c, d) {
            a = this.x + a, b = this.y + b, c = c || 0, this.scrollTo(a, b, c, d)
        }, scrollTo: function (a, b, c, d) {
            d = d || h.ease.circular, this.isInTransition = this.options.useTransition && c > 0, !c || this.options.useTransition && d.style ? (this._transitionTimingFunction(d.style), this._transitionTime(c), this._translate(a, b)) : this._animate(a, b, c, d.fn)
        }, scrollToElement: function (a, b, d, e, f) {
            if (a = a.nodeType ? a : this.scroller.querySelector(a)) {
                var g = h.offset(a);
                g.left -= this.wrapperOffset.left, g.top -= this.wrapperOffset.top, !0 === d && (d = c.round(a.offsetWidth / 2 - this.wrapper.offsetWidth / 2)), !0 === e && (e = c.round(a.offsetHeight / 2 - this.wrapper.offsetHeight / 2)), g.left -= d || 0, g.top -= e || 0, g.left = g.left > 0 ? 0 : g.left < this.maxScrollX ? this.maxScrollX : g.left, g.top = g.top > 0 ? 0 : g.top < this.maxScrollY ? this.maxScrollY : g.top, b = void 0 === b || null === b || "auto" === b ? c.max(c.abs(this.x - g.left), c.abs(this.y - g.top)) : b, this.scrollTo(g.left, g.top, b, f)
            }
        }, _transitionTime: function (a) {
            if (a = a || 0, this.scrollerStyle[h.style.transitionDuration] = a + "ms", !a && h.isBadAndroid && (this.scrollerStyle[h.style.transitionDuration] = "0.001s"), this.indicators) for (var b = this.indicators.length; b--;) this.indicators[b].transitionTime(a)
        }, _transitionTimingFunction: function (a) {
            if (this.scrollerStyle[h.style.transitionTimingFunction] = a, this.indicators) for (var b = this.indicators.length; b--;) this.indicators[b].transitionTimingFunction(a)
        }, _translate: function (a, b) {
            if (this.options.useTransform ? this.scrollerStyle[h.style.transform] = "translate(" + a + "px," + b + "px)" + this.translateZ : (a = c.round(a), b = c.round(b), this.scrollerStyle.left = a + "px", this.scrollerStyle.top = b + "px"), this.x = a, this.y = b, this.indicators) for (var d = this.indicators.length; d--;) this.indicators[d].updatePosition()
        }, _initEvents: function (b) {
            var c = b ? h.removeEvent : h.addEvent, d = this.options.bindToWrapper ? this.wrapper : a;
            c(a, "orientationchange", this), c(a, "resize", this), this.options.click && c(this.wrapper, "click", this, !0), this.options.disableMouse || (c(this.wrapper, "mousedown", this), c(d, "mousemove", this), c(d, "mousecancel", this), c(d, "mouseup", this)), h.hasPointer && !this.options.disablePointer && (c(this.wrapper, h.prefixPointerEvent("pointerdown"), this), c(d, h.prefixPointerEvent("pointermove"), this), c(d, h.prefixPointerEvent("pointercancel"), this), c(d, h.prefixPointerEvent("pointerup"), this)), h.hasTouch && !this.options.disableTouch && (c(this.wrapper, "touchstart", this), c(d, "touchmove", this), c(d, "touchcancel", this), c(d, "touchend", this)), c(this.scroller, "transitionend", this), c(this.scroller, "webkitTransitionEnd", this), c(this.scroller, "oTransitionEnd", this), c(this.scroller, "MSTransitionEnd", this)
        }, getComputedPosition: function () {
            var b, c, d = a.getComputedStyle(this.scroller, null);
            return this.options.useTransform ? (d = d[h.style.transform].split(")")[0].split(", "), b = +(d[12] || d[4]), c = +(d[13] || d[5])) : (b = +d.left.replace(/[^-\d.]/g, ""), c = +d.top.replace(/[^-\d.]/g, "")), {
                x: b,
                y: c
            }
        }, _initIndicators: function () {
            function a(a) {
                for (var b = h.indicators.length; b--;) a.call(h.indicators[b])
            }

            var b, c = this.options.interactiveScrollbars, d = "string" != typeof this.options.scrollbars, g = [],
                h = this;
            this.indicators = [], this.options.scrollbars && (this.options.scrollY && (b = {
                el: e("v", c, this.options.scrollbars),
                interactive: c,
                defaultScrollbars: !0,
                customStyle: d,
                resize: this.options.resizeScrollbars,
                shrink: this.options.shrinkScrollbars,
                fade: this.options.fadeScrollbars,
                listenX: !1
            }, this.wrapper.appendChild(b.el), g.push(b)), this.options.scrollX && (b = {
                el: e("h", c, this.options.scrollbars),
                interactive: c,
                defaultScrollbars: !0,
                customStyle: d,
                resize: this.options.resizeScrollbars,
                shrink: this.options.shrinkScrollbars,
                fade: this.options.fadeScrollbars,
                listenY: !1
            }, this.wrapper.appendChild(b.el), g.push(b))), this.options.indicators && (g = g.concat(this.options.indicators));
            for (var i = g.length; i--;) this.indicators.push(new f(this, g[i]));
            this.options.fadeScrollbars && (this.on("scrollEnd", function () {
                a(function () {
                    this.fade()
                })
            }), this.on("scrollCancel", function () {
                a(function () {
                    this.fade()
                })
            }), this.on("scrollStart", function () {
                a(function () {
                    this.fade(1)
                })
            }), this.on("beforeScrollStart", function () {
                a(function () {
                    this.fade(1, !0)
                })
            })), this.on("refresh", function () {
                a(function () {
                    this.refresh()
                })
            }), this.on("destroy", function () {
                a(function () {
                    this.destroy()
                }), delete this.indicators
            })
        }, _initWheel: function () {
            h.addEvent(this.wrapper, "wheel", this), h.addEvent(this.wrapper, "mousewheel", this), h.addEvent(this.wrapper, "DOMMouseScroll", this), this.on("destroy", function () {
                h.removeEvent(this.wrapper, "wheel", this), h.removeEvent(this.wrapper, "mousewheel", this), h.removeEvent(this.wrapper, "DOMMouseScroll", this)
            })
        }, _wheel: function (a) {
            if (this.enabled) {
                a.preventDefault(), a.stopPropagation();
                var b, d, e, f, g = this;
                if (void 0 === this.wheelTimeout && g._execEvent("scrollStart"), clearTimeout(this.wheelTimeout), this.wheelTimeout = setTimeout(function () {
                    g._execEvent("scrollEnd"), g.wheelTimeout = void 0
                }, 400), "deltaX" in a) b = -a.deltaX, d = -a.deltaY; else if ("wheelDeltaX" in a) b = a.wheelDeltaX / 120 * this.options.mouseWheelSpeed, d = a.wheelDeltaY / 120 * this.options.mouseWheelSpeed; else if ("wheelDelta" in a) b = d = a.wheelDelta / 120 * this.options.mouseWheelSpeed; else {
                    if (!("detail" in a)) return;
                    b = d = -a.detail / 3 * this.options.mouseWheelSpeed
                }
                if (b *= this.options.invertWheelDirection, d *= this.options.invertWheelDirection, this.hasVerticalScroll || (b = d, d = 0), this.options.snap) return e = this.currentPage.pageX, f = this.currentPage.pageY, b > 0 ? e-- : b < 0 && e++, d > 0 ? f-- : d < 0 && f++, void this.goToPage(e, f);
                e = this.x + c.round(this.hasHorizontalScroll ? b : 0), f = this.y + c.round(this.hasVerticalScroll ? d : 0), e > 0 ? e = 0 : e < this.maxScrollX && (e = this.maxScrollX), f > 0 ? f = 0 : f < this.maxScrollY && (f = this.maxScrollY), this.scrollTo(e, f, 0)
            }
        }, _initSnap: function () {
            this.currentPage = {}, "string" == typeof this.options.snap && (this.options.snap = this.scroller.querySelectorAll(this.options.snap)), this.on("refresh", function () {
                var a, b, d, e, f, g, h = 0, i = 0, j = 0, k = this.options.snapStepX || this.wrapperWidth,
                    l = this.options.snapStepY || this.wrapperHeight;
                if (this.pages = [], this.wrapperWidth && this.wrapperHeight && this.scrollerWidth && this.scrollerHeight) {
                    if (!0 === this.options.snap) for (d = c.round(k / 2), e = c.round(l / 2); j > -this.scrollerWidth;) {
                        for (this.pages[h] = [], a = 0, f = 0; f > -this.scrollerHeight;) this.pages[h][a] = {
                            x: c.max(j, this.maxScrollX),
                            y: c.max(f, this.maxScrollY),
                            width: k,
                            height: l,
                            cx: j - d,
                            cy: f - e
                        }, f -= l, a++;
                        j -= k, h++
                    } else for (g = this.options.snap, a = g.length, b = -1; h < a; h++) (0 === h || g[h].offsetLeft <= g[h - 1].offsetLeft) && (i = 0, b++), this.pages[i] || (this.pages[i] = []), j = c.max(-g[h].offsetLeft, this.maxScrollX), f = c.max(-g[h].offsetTop, this.maxScrollY), d = j - c.round(g[h].offsetWidth / 2), e = f - c.round(g[h].offsetHeight / 2), this.pages[i][b] = {
                        x: j,
                        y: f,
                        width: g[h].offsetWidth,
                        height: g[h].offsetHeight,
                        cx: d,
                        cy: e
                    }, j > this.maxScrollX && i++;
                    this.goToPage(this.currentPage.pageX || 0, this.currentPage.pageY || 0, 0), this.options.snapThreshold % 1 == 0 ? (this.snapThresholdX = this.options.snapThreshold, this.snapThresholdY = this.options.snapThreshold) : (this.snapThresholdX = c.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width * this.options.snapThreshold), this.snapThresholdY = c.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height * this.options.snapThreshold))
                }
            }), this.on("flick", function () {
                var a = this.options.snapSpeed || c.max(c.max(c.min(c.abs(this.x - this.startX), 1e3), c.min(c.abs(this.y - this.startY), 1e3)), 300)
                ;this.goToPage(this.currentPage.pageX + this.directionX, this.currentPage.pageY + this.directionY, a)
            })
        }, _nearestSnap: function (a, b) {
            if (!this.pages.length) return {x: 0, y: 0, pageX: 0, pageY: 0};
            var d = 0, e = this.pages.length, f = 0;
            if (c.abs(a - this.absStartX) < this.snapThresholdX && c.abs(b - this.absStartY) < this.snapThresholdY) return this.currentPage;
            for (a > 0 ? a = 0 : a < this.maxScrollX && (a = this.maxScrollX), b > 0 ? b = 0 : b < this.maxScrollY && (b = this.maxScrollY); d < e; d++) if (a >= this.pages[d][0].cx) {
                a = this.pages[d][0].x;
                break
            }
            for (e = this.pages[d].length; f < e; f++) if (b >= this.pages[0][f].cy) {
                b = this.pages[0][f].y;
                break
            }
            return d == this.currentPage.pageX && (d += this.directionX, d < 0 ? d = 0 : d >= this.pages.length && (d = this.pages.length - 1), a = this.pages[d][0].x), f == this.currentPage.pageY && (f += this.directionY, f < 0 ? f = 0 : f >= this.pages[0].length && (f = this.pages[0].length - 1), b = this.pages[0][f].y), {
                x: a,
                y: b,
                pageX: d,
                pageY: f
            }
        }, goToPage: function (a, b, d, e) {
            e = e || this.options.bounceEasing, a >= this.pages.length ? a = this.pages.length - 1 : a < 0 && (a = 0), b >= this.pages[a].length ? b = this.pages[a].length - 1 : b < 0 && (b = 0);
            var f = this.pages[a][b].x, g = this.pages[a][b].y;
            d = void 0 === d ? this.options.snapSpeed || c.max(c.max(c.min(c.abs(f - this.x), 1e3), c.min(c.abs(g - this.y), 1e3)), 300) : d, this.currentPage = {
                x: f,
                y: g,
                pageX: a,
                pageY: b
            }, this.scrollTo(f, g, d, e)
        }, next: function (a, b) {
            var c = this.currentPage.pageX, d = this.currentPage.pageY;
            c++, c >= this.pages.length && this.hasVerticalScroll && (c = 0, d++), this.goToPage(c, d, a, b)
        }, prev: function (a, b) {
            var c = this.currentPage.pageX, d = this.currentPage.pageY;
            c--, c < 0 && this.hasVerticalScroll && (c = 0, d--), this.goToPage(c, d, a, b)
        }, _initKeys: function (b) {
            var c, d = {pageUp: 33, pageDown: 34, end: 35, home: 36, left: 37, up: 38, right: 39, down: 40};
            if ("object" == typeof this.options.keyBindings) for (c in this.options.keyBindings) "string" == typeof this.options.keyBindings[c] && (this.options.keyBindings[c] = this.options.keyBindings[c].toUpperCase().charCodeAt(0)); else this.options.keyBindings = {};
            for (c in d) this.options.keyBindings[c] = this.options.keyBindings[c] || d[c];
            h.addEvent(a, "keydown", this), this.on("destroy", function () {
                h.removeEvent(a, "keydown", this)
            })
        }, _key: function (a) {
            if (this.enabled) {
                var b, d = this.options.snap, e = d ? this.currentPage.pageX : this.x,
                    f = d ? this.currentPage.pageY : this.y, g = h.getTime(), i = this.keyTime || 0, j = .25;
                switch (this.options.useTransition && this.isInTransition && (b = this.getComputedPosition(), this._translate(c.round(b.x), c.round(b.y)), this.isInTransition = !1), this.keyAcceleration = g - i < 200 ? c.min(this.keyAcceleration + j, 50) : 0, a.keyCode) {
                    case this.options.keyBindings.pageUp:
                        this.hasHorizontalScroll && !this.hasVerticalScroll ? e += d ? 1 : this.wrapperWidth : f += d ? 1 : this.wrapperHeight;
                        break;
                    case this.options.keyBindings.pageDown:
                        this.hasHorizontalScroll && !this.hasVerticalScroll ? e -= d ? 1 : this.wrapperWidth : f -= d ? 1 : this.wrapperHeight;
                        break;
                    case this.options.keyBindings.end:
                        e = d ? this.pages.length - 1 : this.maxScrollX, f = d ? this.pages[0].length - 1 : this.maxScrollY;
                        break;
                    case this.options.keyBindings.home:
                        e = 0, f = 0;
                        break;
                    case this.options.keyBindings.left:
                        e += d ? -1 : 5 + this.keyAcceleration >> 0;
                        break;
                    case this.options.keyBindings.up:
                        f += d ? 1 : 5 + this.keyAcceleration >> 0;
                        break;
                    case this.options.keyBindings.right:
                        e -= d ? -1 : 5 + this.keyAcceleration >> 0;
                        break;
                    case this.options.keyBindings.down:
                        f -= d ? 1 : 5 + this.keyAcceleration >> 0;
                        break;
                    default:
                        return
                }
                if (d) return void this.goToPage(e, f);
                e > 0 ? (e = 0, this.keyAcceleration = 0) : e < this.maxScrollX && (e = this.maxScrollX, this.keyAcceleration = 0), f > 0 ? (f = 0, this.keyAcceleration = 0) : f < this.maxScrollY && (f = this.maxScrollY, this.keyAcceleration = 0), this.scrollTo(e, f, 0), this.keyTime = g
            }
        }, _animate: function (a, b, c, d) {
            function e() {
                var m, n, o, p = h.getTime();
                if (p >= l) return f.isAnimating = !1, f._translate(a, b), void (f.resetPosition(f.options.bounceTime) || f._execEvent("scrollEnd"));
                p = (p - k) / c, o = d(p), m = (a - i) * o + i, n = (b - j) * o + j, f._translate(m, n), f.isAnimating && g(e)
            }

            var f = this, i = this.x, j = this.y, k = h.getTime(), l = k + c;
            this.isAnimating = !0, e()
        }, handleEvent: function (a) {
            switch (a.type) {
                case"touchstart":
                case"pointerdown":
                case"MSPointerDown":
                case"mousedown":
                    this._start(a);
                    break;
                case"touchmove":
                case"pointermove":
                case"MSPointerMove":
                case"mousemove":
                    this._move(a);
                    break;
                case"touchend":
                case"pointerup":
                case"MSPointerUp":
                case"mouseup":
                case"touchcancel":
                case"pointercancel":
                case"MSPointerCancel":
                case"mousecancel":
                    this._end(a);
                    break;
                case"orientationchange":
                case"resize":
                    this._resize();
                    break;
                case"transitionend":
                case"webkitTransitionEnd":
                case"oTransitionEnd":
                case"MSTransitionEnd":
                    this._transitionEnd(a);
                    break;
                case"wheel":
                case"DOMMouseScroll":
                case"mousewheel":
                    this._wheel(a);
                    break;
                case"keydown":
                    this._key(a);
                    break;
                case"click":
                    a._constructed || (a.preventDefault(), a.stopPropagation())
            }
        }
    }, f.prototype = {
        handleEvent: function (a) {
            switch (a.type) {
                case"touchstart":
                case"pointerdown":
                case"MSPointerDown":
                case"mousedown":
                    this._start(a);
                    break;
                case"touchmove":
                case"pointermove":
                case"MSPointerMove":
                case"mousemove":
                    this._move(a);
                    break;
                case"touchend":
                case"pointerup":
                case"MSPointerUp":
                case"mouseup":
                case"touchcancel":
                case"pointercancel":
                case"MSPointerCancel":
                case"mousecancel":
                    this._end(a)
            }
        }, destroy: function () {
            this.options.interactive && (h.removeEvent(this.indicator, "touchstart", this), h.removeEvent(this.indicator, h.prefixPointerEvent("pointerdown"), this), h.removeEvent(this.indicator, "mousedown", this), h.removeEvent(a, "touchmove", this), h.removeEvent(a, h.prefixPointerEvent("pointermove"), this), h.removeEvent(a, "mousemove", this), h.removeEvent(a, "touchend", this), h.removeEvent(a, h.prefixPointerEvent("pointerup"), this), h.removeEvent(a, "mouseup", this)), this.options.defaultScrollbars && this.wrapper.parentNode.removeChild(this.wrapper)
        }, _start: function (b) {
            var c = b.touches ? b.touches[0] : b;
            b.preventDefault(), b.stopPropagation(), this.transitionTime(), this.initiated = !0, this.moved = !1, this.lastPointX = c.pageX, this.lastPointY = c.pageY, this.startTime = h.getTime(), this.options.disableTouch || h.addEvent(a, "touchmove", this), this.options.disablePointer || h.addEvent(a, h.prefixPointerEvent("pointermove"), this), this.options.disableMouse || h.addEvent(a, "mousemove", this), this.scroller._execEvent("beforeScrollStart")
        }, _move: function (a) {
            var b, c, d, e, f = a.touches ? a.touches[0] : a;
            h.getTime();
            this.moved || this.scroller._execEvent("scrollStart"), this.moved = !0, b = f.pageX - this.lastPointX, this.lastPointX = f.pageX, c = f.pageY - this.lastPointY, this.lastPointY = f.pageY, d = this.x + b, e = this.y + c, this._pos(d, e), a.preventDefault(), a.stopPropagation()
        }, _end: function (b) {
            if (this.initiated) {
                if (this.initiated = !1, b.preventDefault(), b.stopPropagation(), h.removeEvent(a, "touchmove", this), h.removeEvent(a, h.prefixPointerEvent("pointermove"), this), h.removeEvent(a, "mousemove", this), this.scroller.options.snap) {
                    var d = this.scroller._nearestSnap(this.scroller.x, this.scroller.y),
                        e = this.options.snapSpeed || c.max(c.max(c.min(c.abs(this.scroller.x - d.x), 1e3), c.min(c.abs(this.scroller.y - d.y), 1e3)), 300);
                    this.scroller.x == d.x && this.scroller.y == d.y || (this.scroller.directionX = 0, this.scroller.directionY = 0, this.scroller.currentPage = d, this.scroller.scrollTo(d.x, d.y, e, this.scroller.options.bounceEasing))
                }
                this.moved && this.scroller._execEvent("scrollEnd")
            }
        }, transitionTime: function (a) {
            a = a || 0, this.indicatorStyle[h.style.transitionDuration] = a + "ms", !a && h.isBadAndroid && (this.indicatorStyle[h.style.transitionDuration] = "0.001s")
        }, transitionTimingFunction: function (a) {
            this.indicatorStyle[h.style.transitionTimingFunction] = a
        }, refresh: function () {
            this.transitionTime(), this.options.listenX && !this.options.listenY ? this.indicatorStyle.display = this.scroller.hasHorizontalScroll ? "block" : "none" : this.options.listenY && !this.options.listenX ? this.indicatorStyle.display = this.scroller.hasVerticalScroll ? "block" : "none" : this.indicatorStyle.display = this.scroller.hasHorizontalScroll || this.scroller.hasVerticalScroll ? "block" : "none", this.scroller.hasHorizontalScroll && this.scroller.hasVerticalScroll ? (h.addClass(this.wrapper, "iScrollBothScrollbars"), h.removeClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "8px" : this.wrapper.style.bottom = "8px")) : (h.removeClass(this.wrapper, "iScrollBothScrollbars"), h.addClass(this.wrapper, "iScrollLoneScrollbar"), this.options.defaultScrollbars && this.options.customStyle && (this.options.listenX ? this.wrapper.style.right = "2px" : this.wrapper.style.bottom = "2px"));
            this.wrapper.offsetHeight;
            this.options.listenX && (this.wrapperWidth = this.wrapper.clientWidth, this.options.resize ? (this.indicatorWidth = c.max(c.round(this.wrapperWidth * this.wrapperWidth / (this.scroller.scrollerWidth || this.wrapperWidth || 1)), 8), this.indicatorStyle.width = this.indicatorWidth + "px") : this.indicatorWidth = this.indicator.clientWidth, this.maxPosX = this.wrapperWidth - this.indicatorWidth, "clip" == this.options.shrink ? (this.minBoundaryX = 8 - this.indicatorWidth, this.maxBoundaryX = this.wrapperWidth - 8) : (this.minBoundaryX = 0, this.maxBoundaryX = this.maxPosX), this.sizeRatioX = this.options.speedRatioX || this.scroller.maxScrollX && this.maxPosX / this.scroller.maxScrollX), this.options.listenY && (this.wrapperHeight = this.wrapper.clientHeight, this.options.resize ? (this.indicatorHeight = c.max(c.round(this.wrapperHeight * this.wrapperHeight / (this.scroller.scrollerHeight || this.wrapperHeight || 1)), 8), this.indicatorStyle.height = this.indicatorHeight + "px") : this.indicatorHeight = this.indicator.clientHeight, this.maxPosY = this.wrapperHeight - this.indicatorHeight, "clip" == this.options.shrink ? (this.minBoundaryY = 8 - this.indicatorHeight, this.maxBoundaryY = this.wrapperHeight - 8) : (this.minBoundaryY = 0, this.maxBoundaryY = this.maxPosY), this.maxPosY = this.wrapperHeight - this.indicatorHeight, this.sizeRatioY = this.options.speedRatioY || this.scroller.maxScrollY && this.maxPosY / this.scroller.maxScrollY), this.updatePosition()
        }, updatePosition: function () {
            var a = this.options.listenX && c.round(this.sizeRatioX * this.scroller.x) || 0,
                b = this.options.listenY && c.round(this.sizeRatioY * this.scroller.y) || 0;
            this.options.ignoreBoundaries || (a < this.minBoundaryX ? ("scale" == this.options.shrink && (this.width = c.max(this.indicatorWidth + a, 8), this.indicatorStyle.width = this.width + "px"), a = this.minBoundaryX) : a > this.maxBoundaryX ? "scale" == this.options.shrink ? (this.width = c.max(this.indicatorWidth - (a - this.maxPosX), 8), this.indicatorStyle.width = this.width + "px", a = this.maxPosX + this.indicatorWidth - this.width) : a = this.maxBoundaryX : "scale" == this.options.shrink && this.width != this.indicatorWidth && (this.width = this.indicatorWidth, this.indicatorStyle.width = this.width + "px"), b < this.minBoundaryY ? ("scale" == this.options.shrink && (this.height = c.max(this.indicatorHeight + 3 * b, 8), this.indicatorStyle.height = this.height + "px"), b = this.minBoundaryY) : b > this.maxBoundaryY ? "scale" == this.options.shrink ? (this.height = c.max(this.indicatorHeight - 3 * (b - this.maxPosY), 8), this.indicatorStyle.height = this.height + "px", b = this.maxPosY + this.indicatorHeight - this.height) : b = this.maxBoundaryY : "scale" == this.options.shrink && this.height != this.indicatorHeight && (this.height = this.indicatorHeight, this.indicatorStyle.height = this.height + "px")), this.x = a, this.y = b, this.scroller.options.useTransform ? this.indicatorStyle[h.style.transform] = "translate(" + a + "px," + b + "px)" + this.scroller.translateZ : (this.indicatorStyle.left = a + "px", this.indicatorStyle.top = b + "px")
        }, _pos: function (a, b) {
            a < 0 ? a = 0 : a > this.maxPosX && (a = this.maxPosX), b < 0 ? b = 0 : b > this.maxPosY && (b = this.maxPosY), a = this.options.listenX ? c.round(a / this.sizeRatioX) : this.scroller.x, b = this.options.listenY ? c.round(b / this.sizeRatioY) : this.scroller.y, this.scroller.scrollTo(a, b)
        }, fade: function (a, b) {
            if (!b || this.visible) {
                clearTimeout(this.fadeTimeout), this.fadeTimeout = null;
                var c = a ? 250 : 500, d = a ? 0 : 300;
                a = a ? "1" : "0", this.wrapperStyle[h.style.transitionDuration] = c + "ms", this.fadeTimeout = setTimeout(function (a) {
                    this.wrapperStyle.opacity = a, this.visible = +a
                }.bind(this, a), d)
            }
        }
    }, d.utils = h, "undefined" != typeof module && module.exports ? module.exports = d : a.IScroll = d
}(window, document, Math), function (a) {
    var b = a.Ariel = {};
    b.guidOpenHoneyPay = !1, b.VERSION = "0.1.0", b.$ = a.Zepto;
    var c = function (b) {
        navigator.userAgent.toLocaleLowerCase().indexOf("iphone") >= 0 && a.webkit && a.webkit.messageHandlers && a.webkit.messageHandlers.JDAppUnite && a.webkit.messageHandlers.JDAppUnite.postMessage && a.webkit.messageHandlers.JDAppUnite.postMessage(b)
    };
    b.originIosJumpProtocol = c;
    var d = function (a) {
        for (var b in a) return !0;
        return !1
    };
    b.isNotEmptyObject = d;
    var e = function (a) {
        if ("" === a) return "";
        var b = [];
        b.push("0x");
        for (var c = 0; c < a.length; c++) b.push(a.charCodeAt(c).toString(16));
        return b.join("")
    };
    b.stringToHex = e;
    var f = function (a) {
        try {
            var b = a.trim(), c = "0x" === b.substr(0, 2).toLowerCase() ? b.substr(2) : b, d = c.length;
            if (d % 2 != 0) return a;
            for (var e, f = [], g = 0; g < d; g += 2) e = parseInt(c.substr(g, 2), 16), f.push(String.fromCharCode(e));
            return f.join("")
        } catch (a) {
        }
    };
    b.hexCharToStr = f;
    var g = function (b) {
        b = b || a.location.href;
        var c = {}, d = b.indexOf("?");
        if (d >= 0) {
            var e = b.substring(d + 1), f = e.split("&");
            if (!f && f.indexOf("=") > 0) {
                var g = f[h].split("=");
                c[g[0]] = g[1]
            }
            if (f && f.length > 0) for (var h = f.length - 1; h >= 0; h--) {
                var g = f[h].split("=");
                c[g[0]] = g[1]
            }
        }
        return c
    };
    b.parseGetParameters = g;
    var h = function (b) {
        b = b || a.location.href;
        var c = b.match(/#\!?(.*)$/), b = c ? c[1] : "", d = {}, e = b.indexOf("?");
        if (e >= 0 && (b = b.substring(0, e)), b) {
            var f = b.split("&");
            if (!f && f.indexOf("=") > 0) {
                var g = f[h].split("=");
                d[g[0]] = g[1]
            }
            if (f && f.length > 0) for (var h = f.length - 1; h >= 0; h--) {
                var g = f[h].split("=");
                d[g[0]] = g[1]
            }
        }
        return d
    };
    b.parseHashParameters = h;
    var i = function () {
        return Math.random().toString(16).substr(2)
    };
    b.uniqueId = i;
    var j = function (a, b) {
        for (var c in b) a[c] = b[c]
    };
    b.override = j;
    var k = function (a, b) {
        var c;
        "[object Object]" == Object.prototype.toString.call(a) ? (c = this, b = a) : c = a;
        var d = c.prototype, e = function () {
            return c.apply(this, arguments)
        }, f = function () {
        };
        return f.prototype = d, e.prototype = new f, b && j(e.prototype, b), e.prototype.constructor = e, e.__super__ = d, e
    };
    b.extend = k;
    var l = function () {
    };
    j(l.prototype, {
        _storage: a.localStorage, add: function (a, b) {
            try {
                return this._storage.setItem(a, b)
            } catch (a) {
                return
            }
        }, remove: function (a) {
            try {
                return this._storage.removeItem(a)
            } catch (a) {
                return
            }
        }, get: function (a) {
            try {
                return this._storage.getItem(a)
            } catch (a) {
                return
            }
        }, clear: function (a) {
            if (a) for (var b = this, c = b._storage, d = c.length - 1; d >= 0; d--) {
                var e = c.key(d);
                a.test(e) && b.remove(e)
            } else try {
                return this._storage.clear()
            } catch (a) {
                return
            }
        }, _key: function (a) {
            try {
                return this._storage.key(a)
            } catch (a) {
                return
            }
        }
    }), b.Storage = new l;
    var m = function () {
    };
    j(m.prototype, {
        _storage: a.sessionStorage, add: function (a, b) {
            try {
                return this._storage.setItem(a, b)
            } catch (a) {
                return
            }
        }, remove: function (a) {
            try {
                return this._storage.removeItem(a)
            } catch (a) {
                return
            }
        }, get: function (a) {
            try {
                return this._storage.getItem(a)
            } catch (a) {
                return
            }
        }, clear: function (a) {
            if (a) for (var b = this, c = b._storage, d = c.length - 1; d >= 0; d--) {
                var e = c.key(d);
                a.test(e) && b.remove(e)
            } else try {
                return this._storage.clear()
            } catch (a) {
                return
            }
        }, _canStorage: function () {
            try {
                return a.sessionStorage.setItem("test", "1"), !0
            } catch (a) {
                return !1
            }
        }, _key: function (a) {
            try {
                return this._storage.key(a)
            } catch (a) {
                return
            }
        }
    }), b.Courier = new m;
    var n = function () {
        var a = this;
        a._wrapEvent("_listenHash", a)
    };
    j(n.prototype, {
        _routes: [], _previousHash: "", _wrapEvent: function (a, b) {
            var c = b[a];
            b[a] = function (a) {
                c.call(b, a)
            }
        }, startListen: function () {
            var c = this;
            "onhashchange" in a ? b.$(a).on("hashchange", c._listenHash) : c._intervalId = setInterval(c._listenHash, 50), c._listenHash()
        }, stopListen: function () {
            var c = this;
            "onhashchange" in a ? b.$(a).off("hashchange", c._listenHash) : a.clearInterval(c._intervalId)
        }, _routeToRegExp: function (a) {
            return "[object RegExp]" == Object.prototype.toString.call(a) ? a : "[object String]" == Object.prototype.toString.call(a) ? new RegExp("^" + a + "$") : void 0
        }, route: function (a, b, c) {
            var d = this;
            d._routes.push({route: d._routeToRegExp(a), isDefault: b, handler: c})
        }, setRoutes: function (a) {
            if (a) for (var b = a.length - 1; b >= 0; b--) {
                var c = a[b];
                this.route(c.route, c.isDefault, c.handler)
            }
        }, _listenHash: function (a) {
            var b = this, c = b._previousHash, d = b._getCurrentHash();
            b._eachRoutes(c, d), b._previousHash = d
        }, _eachRoutes: function (a, b) {
            for (var c = this, d = c._routes, e = !a && !b, f = d.length - 1; f >= 0; f--) {
                var g, h = d[f];
                if (e ? h.isDefault && (g = h.handler) : h.route.test(b) && (g = h.handler), g) return void g.call(c, a, b)
            }
        }, _getCurrentHash: function () {
            var b = a.location.href.match(/#\!?(.*)$/);
            return b ? b[1] : ""
        }, go: function (c) {
            a.location.hash = [c, b.uniqueId()].join("/")
        }
    }), b.History = new n;
    var o = function () {
    };
    j(o.prototype, {
        _path: "", _urls: {}, setPath: function (a) {
            this._path = a
        }, load: function (a, b) {
            var c = this, d = c._path + a;
            if (c._urls.hasOwnProperty(d)) b.call(c); else {
                var e = document.createElement("script");
                e.src = d, e.onload = function () {
                    c._urls[d] = "", b.call(c), e.onload = null
                }, $("body").append(e)
            }
        }
    }), b.JSLoader = new o;
    var p = function () {
    };
    j(p.prototype, {
        _instances: {}, get: function (b, c) {
            var d = this;
            d._instances[b] || a[b] && (d._instances[b] = new a[b]), c.call(d, d._instances[b])
        }
    }), b.InstanceManager = new p, b.mpingEvent = function (a, b, c, d) {
        try {
            var e = new MPing.inputs.Click(a);
            if (b) {
                var f = b.indexOf("?");
                e.page_name = -1 != f ? b.substr(0, f) : b
            }
            null != c && void 0 != c && (e.event_param = c + ""), null != d && void 0 != d && (e.event_level = d + ""), e.updateEventSeries();
            (new MPing).send(e)
        } catch (a) {
        }
    }, b.mpingPV = function () {
        try {
            var a = new MPing.inputs.PV("Cashier_Home");
            (new MPing).send(a)
        } catch (a) {
        }
    }
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="cascade-combo-mask"></div>',
        _template: '<div class="cascade-combo-wrap">    <div class="cascade-combo">        <div class="cascade-combo-bar">            <a href="javascript:void(0);" class="cascade-combo-cancel">鍙栨秷</a>            <a href="javascript:void(0);" class="cascade-combo-enter">纭畾</a>        </div>        <div class="cascade-combo-zone">            <div class="cascade-combo-left-column-wrap">               <div class="cascade-combo-scroll">                   <ul class="cascade-combo-list"></ul>               </div>               <div class="cascade-combo-gradient-mask-up"></div>               <div class="cascade-combo-gradient-mask-down"></div>               <div class="cascade-combo-magnifier"></div>            </div>            <div class="cascade-combo-right-column-wrap">               <div class="cascade-combo-scroll">                   <ul class="cascade-combo-list"></ul>               </div>               <div class="cascade-combo-gradient-mask-up"></div>               <div class="cascade-combo-gradient-mask-down"></div>               <div class="cascade-combo-magnifier"></div>            </div>        </div>    </div></div>',
        _ITEM_HEIGHT: 34,
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _mainScroll: null,
        _childrenScroll: null,
        _onEnter: null,
        _mainData: null,
        _childrenData: null,
        _dataModel: [],
        _comboNumber: 1,
        _displayName: null,
        _formatDisplayName: null,
        _isCascade: !0,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._wrapper = b.$(a._template), a._viewport = $("#viewport"), a._mainScroll = new IScroll(a._wrapper.find(".cascade-combo-left-column-wrap").get(0)), a._childrenScroll = new IScroll(a._wrapper.find(".cascade-combo-right-column-wrap").get(0))
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".cascade-combo-cancel").click(function () {
                a.hide()
            }), a._wrapper.find(".cascade-combo-enter").click(function () {
                var b = {};
                b.mainData = a._getMainSelectedData(), "娣诲姞閾惰鍗�" == b.mainData.bankName && Ariel.mpingEvent("JDCashierFastPay_AddNewCard", location.href), 1 != a._comboNumber && (b.childrenData = a._getChildrenSelectedData()), a._onEnter && a._onEnter.call(a, b), a.hide()
            }), a._mainScroll.on("scrollEnd", function () {
                if (a._fixedScrollPosition.call(a, this), 1 != a._comboNumber && a._isCascade) for (var b = a._getMainSelectedData(), c = a._mainData.length - 1; c >= 0; c--) {
                    var d = a._mainData[c];
                    if (b.id == d.id) return void a._renderChildrenListData(d.children)
                }
            }), a._childrenScroll.on("scrollEnd", function () {
                a._fixedScrollPosition.call(a, this)
            })
        },
        _fixedScrollPosition: function (a) {
            var b, c = this, d = a.y, e = c._ITEM_HEIGHT, f = d % e, g = f >= 0 ? 1 : -1;
            b = Math.abs(f) - c._ITEM_HEIGHT / 2 > 0 ? d + g * (c._ITEM_HEIGHT - Math.abs(f)) : d + -1 * g * Math.abs(f), a.scrollTo(0, b)
        },
        _getMainSelectedData: function () {
            var a = this;
            return a._getSelectedData(".cascade-combo-left-column-wrap .cascade-combo-list", a._mainScroll.y)
        },
        _getChildrenSelectedData: function () {
            var a = this;
            return a._getSelectedData(".cascade-combo-right-column-wrap .cascade-combo-list", a._childrenScroll.y)
        },
        _getSelectedData: function (a, b) {
            var c = this, d = c._dataModel, e = {};
            selectedIndex = Math.abs(b) / c._ITEM_HEIGHT + 1 + 2, selectedNode = $(c._wrapper.find(a + " li").get(selectedIndex - 1));
            for (var f = d.length - 1; f >= 0; f--) e[d[f]] = selectedNode.attr("data-" + d[f]);
            return e
        },
        setConfig: function (a) {
            var b = this;
            b._setOnEnter(a), b._setComboNumber(a), b._setDisplay(a), b._setCascade(a), b._setDataModel(a), b._setData(a)
        },
        _setOnEnter: function (a) {
            a.onEnter && (this._onEnter = a.onEnter)
        },
        _setComboNumber: function (a) {
            this._comboNumber = a.comboNumber ? a.comboNumber : 1
        },
        _setData: function (a) {
            var b = this;
            b._isCascade ? b._mainData = a.data : (b._mainData = a.data[0], b._childrenData = a.data[1]), b._renderData()
        },
        _setDisplay: function (a) {
            var b = this;
            "[object String]" == Object.prototype.toString.call(a.displayName) && (b._displayName = a.displayName), "[object Function]" == Object.prototype.toString.call(a.displayName) && (b._formatDisplayName = a.displayName)
        },
        _setCascade: function (a) {
            this._isCascade = void 0 === a.isCascade || a.isCascade
        },
        _setDataModel: function (a) {
            var b = this;
            b._dataModel = [];
            for (var c = a.dataModel.length - 1; c >= 0; c--) b._dataModel.push(a.dataModel[c])
        },
        _renderData: function () {
            var a = this;
            a._mainData && (a._renderMainListData(), 1 != a._comboNumber && a._renderDefaultChildrenListData())
        },
        _renderMainListData: function () {
            var a = this;
            a._renderListData(".cascade-combo-left-column-wrap .cascade-combo-list", a._mainData)
        },
        _renderDefaultChildrenListData: function () {
            var a = this;
            a._renderChildrenListData(a._isCascade ? a._mainData[0].children : a._childrenData)
        },
        _renderChildrenListData: function (a) {
            var b = this, c = ".cascade-combo-right-column-wrap .cascade-combo-list";
            b._wrapper.find(c).empty(), b._renderListData(c, a), b._childrenScroll.refresh(), b._childrenScroll.scrollTo(0, 0)
        },
        _renderListData: function (a, b) {
            var c = this, d = b.length, e = c._dataModel;
            c._wrapper.find(a).empty(), c._wrapper.find(a).append($('<li class="cascade-combo-list-item">&nbsp;</li>')), c._wrapper.find(a).append($('<li class="cascade-combo-list-item">&nbsp;</li>'));
            for (var f = 0; f < d; f++) {
                for (var g = $('<li class="cascade-combo-list-item"></li>'), h = b[f], i = e.length - 1; i >= 0; i--) {
                    var j = e[i];
                    g.attr("data-" + j, h[j])
                }
                c._displayName && g.html(h[c._displayName]), c._formatDisplayName && g.html(c._formatDisplayName(h)), c._wrapper.find(a).append(g)
            }
            c._wrapper.find(a).append($('<li class="cascade-combo-list-item">&nbsp;</li>')), c._wrapper.find(a).append($('<li class="cascade-combo-list-item">&nbsp;</li>'))
        },
        show: function () {
            var a = this;
            if (a._mainData) {
                a._viewport.append(a._mask), a._viewport.append(a._wrapper);
                var b = setTimeout(function () {
                    a._wrapper.find(".cascade-combo").css("transition", "transform 400ms ease-out"), a._wrapper.find(".cascade-combo").css("-webkit-transition", "-webkit-transform 400ms ease-out"), a._wrapper.find(".cascade-combo").css("-ms-transition", "-ms-transform 400ms ease-out"), a._wrapper.find(".cascade-combo").css("transform", "translate3d(0px, 0px, 0px)"), a._wrapper.find(".cascade-combo").css("-webkit-transform", "translate3d(0px, 0px, 0px)"), a._wrapper.find(".cascade-combo").css("-ms-transform", "translate3d(0px, 0px, 0px)");
                    var c = setTimeout(function () {
                        a._mainScroll.refresh(), 1 == a._comboNumber && (a._wrapper.find(".cascade-combo-left-column-wrap").css("width", "100%"), a._wrapper.find(".cascade-combo-right-column-wrap").css("width", "0%")), 1 != a._comboNumber && a._childrenScroll.refresh(), clearTimeout(c)
                    }, 200);
                    clearTimeout(b)
                }, 100)
            }
        },
        hide: function () {
            var a, b = this;
            b._mainData && (b._wrapper.find(".cascade-combo").css("transform", "translate3d(0px, 224px, 0px)"), b._wrapper.find(".cascade-combo").css("-webkit-transform", "translate3d(0px, 224px, 0px)"), b._wrapper.find(".cascade-combo").css("-ms-transform", "translate3d(0px, 224px, 0px)"), a = setTimeout(function () {
                b._mainScroll.scrollTo(0, 0), 1 != b._comboNumber && b._childrenScroll.scrollTo(0, 0), b._wrapper.remove(), b._mask.remove(), clearTimeout(a)
            }, 500))
        }
    }), b.Components.CascadeCombo = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="choose-layer-mask"></div>',
        _template: '<div class="choose-layer">   <span class="choose-layer-close" ></span>   <h2 class="choose-layer-title">閫夋嫨閾惰鍗�</h2>   <ul class="choose-layer-list">       <li class="choose-layer-list-item"><a class="choose-layer-radio"></a></li>   </ul> </div>',
        _item: '<li class="COMM_BOTTOM_1PX_BORDER choose-layer-list-item"><a class="choose-layer-radio"><span class="choose-layer-name"></span></a></li>',
        _item_btn: '<li class="add-new-item"><a class="add-btn">浣跨敤鏂伴摱琛屽崱</a></li>',
        _ITEM_HEIGHT: 34,
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _itemList: null,
        _data: null,
        _selectData: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._wrapper = b.$(a._template), a._itemList = a._wrapper.find(".choose-layer-list"), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".choose-layer-close").click(function () {
                a.hide()
            })
        },
        setConfig: function (a) {
            var b = this;
            b._selectData = a.selectData, b._setOnEnter(a), b._setData(a)
        },
        _setOnEnter: function (a) {
            a.onEnter && (this._onEnter = a.onEnter), a._displayName && (this._displayName = a._displayName)
        },
        _setData: function (a) {
            var b = this;
            b._data = a.data || [], b._dataModel = a.dataModel || [], b._itemList.find(".choose-layer-list-item").remove(), b._itemList.find(".add-new-item").remove(), b._initBanks();
            var c = 400, d = b._data.length;
            c = d >= 6 ? 400 : 0 == d ? 130 : 55 + 45 * (d + 1), b._wrapper.css("height", c + "px"), b._itemList.css("height", c - 55 + "px"), a.bandedBank ? b._addBankBtn() : b._wrapper.find(".choose-layer-title").html(a.title)
        },
        _initBanks: function () {
            for (var a = this, b = 0; b < a._data.length; b++) {
                a._itemList.append($(a._item));
                var c = a._itemList.find(".choose-layer-list-item")[b], d = a._data[b];
                for (var e in a._dataModel) {
                    var f = a._dataModel[e];
                    $(c).attr(f, d[f])
                }
                var g = !1;
                for (var f in a._selectData) {
                    if (a._selectData[f] != d[f]) {
                        g = !1;
                        break
                    }
                    g = !0
                }
                g && $(c).find(".choose-layer-radio").addClass("checked"), $(c).find(".choose-layer-name").append(a._displayName(d))
            }
            touchFlag = !1, a._itemList.find(".choose-layer-list-item").click(function (b) {
                a._selectItem(b)
            })
        },
        _selectItem: function (a) {
            var b = this, c = $(a.target), d = $(a.target);
            c.hasClass("choose-layer-name") && (d = c.parent()), b._itemList.find(".choose-layer-radio").removeClass("checked"), d.addClass("checked");
            var e = d.parent(), f = {};
            for (var g in b._dataModel) {
                var h = b._dataModel[g];
                f[h] = e.attr(h)
            }
            b._selectData = {acronym: f.acronym, cardType: f.cardType}, b._onEnter(f), b.hide()
        },
        _addBankBtn: function () {
            var b = this;
            b._itemList.append(b._item_btn), b._itemList.find(".add-new-item").click(function (b) {
                var c = Ariel.parseGetParameters();
                a.location.href = "/pay/new-card.html?payId=" + c.payId + "&appId=" + c.appId
            })
        },
        _displayName: function (a) {
        },
        _onEnter: function (a) {
        },
        show: function () {
            var a = this;
            a._viewport.find(".choose-layer-mask").length <= 0 && (a._viewport.append(a._mask), a._viewport.append(a._wrapper)), a._mask.click(function () {
                a.hide()
            }), a._mask.show(), a._wrapper.removeClass("choose-layer-out"), a._wrapper.addClass("choose-layer-in")
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("choose-layer-in"), a._wrapper.addClass("choose-layer-out"), setTimeout(function () {
                a._mask.hide(), a._wrapper.remove(), a._mask.remove()
            }, 200)
        }
    }), b.Components.BankList = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="loading-mask"></div>',
        _template: '<div class="loading-wrap">    <div class="loading">        <div class="loading-logo"></div>        <div class="loading-mum"></div>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._wrapper = b.$(a._template), a._viewport = $("#viewport")
        },
        show: function () {
            var a = this;
            a._viewport.append(a._mask), a._viewport.append(a._wrapper)
        },
        hide: function () {
            var a = this;
            a._wrapper.remove(), a._mask.remove()
        }
    }), b.Components.Loading = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="pop-mask"></div>',
        _template: '<div class="pop-wrapper">    <div class="pop">        <div class="pop-description-wrap">            <p class="pop-description"></p>        </div>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._wrapper = b.$(a._template), a._viewport = $("#viewport")
        },
        show: function (a, b, c) {
            var d, e = this;
            !c && e._viewport.append(e._mask), e._viewport.append(e._wrapper), d = setTimeout(function () {
                var f;
                e._wrapper.find(".pop-description").text(a), e._wrapper.find(".pop").css("opacity", "1"), clearTimeout(d), f = setTimeout(function () {
                    e.hide(b, c), clearTimeout(f)
                }, 2500)
            }, 100)
        },
        hide: function (a, b) {
            var c, d = this;
            d._wrapper.find(".pop").css("opacity", "0"), c = setTimeout(function () {
                d._wrapper.remove(), !b && d._mask.remove(), clearTimeout(c), a && a()
            }, 500)
        }
    }), b.Components.Pop = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="pop-mask"></div>',
        _template: '<div class="pop-wrapper">    <div class="pop">        <div class="pop-description-wrap-icon"><p class="pop-icon"></p>            <p class="pop-description"></p>        </div>    </div></div>',
        _tipTemplate: '<div class="tip-pop-wrap">  <div class="tip-pop">    <p class="tip-pop-description"></p>  </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _tipWrapper: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._wrapper = b.$(a._template), a._viewport = $("#viewport")
        },
        show: function (a, b) {
            var c, d = this;
            d._viewport.append(d._mask), d._viewport.append(d._wrapper), c = setTimeout(function () {
                var e;
                d._wrapper.find(".pop-description").text(a), d._wrapper.find(".pop").css("opacity", "1"), clearTimeout(c), e = setTimeout(function () {
                    d.hide(b), clearTimeout(e)
                }, 2500)
            }, 100)
        },
        showTip: function (a, c) {
            var d = this;
            d._tipWrapper = b.$(d._tipTemplate), d._viewport.append(d._tipWrapper), d._tipWrapper.find(".tip-pop-description").text(a);
            var e;
            d._tipWrapper.css("opacity", "1"), e = setTimeout(function () {
                d.hideTip(c), clearTimeout(e)
            }, 2500)
        },
        hide: function (a) {
            var b, c = this;
            c._wrapper.find(".pop").css("opacity", "0"), b = setTimeout(function () {
                c._wrapper.remove(), c._mask.remove(), clearTimeout(b), a && a()
            }, 500)
        },
        hideTip: function (a) {
            var b, c = this;
            c._tipWrapper.css("opacity", "0"), b = setTimeout(function () {
                c._tipWrapper.remove(), clearTimeout(b), a && a()
            }, 500)
        }
    }), b.Components.PopWithIcon = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function () {
        this._init()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="pop-mask"></div>',
        _template: '<div class="agreeBox-wrap">  <div class="agreeBox">    <h3 class="agreeBox-title"></h3>    <p class="agreeBox-description"></p>    <a href="javascript:void(0);" class="agreeBox-button COMM_TOP_1PX_BORDER"></a>  </div></div>',
        _mask: null,
        _Wrapper: null,
        _viewport: null,
        _button: null,
        _agreeBoxTitle: null,
        _agreeBoxDescription: null,
        onClickButton: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._viewport = $("#viewport")
        },
        show: function (a, c) {
            var d = this;
            d._Wrapper = b.$(d._template), a.title ? (d._Wrapper.find(".agreeBox-title").html(a.title), d._agreeBoxTitle = a.title) : d._Wrapper.find(".agreeBox-title").remove(), a.description ? (d._Wrapper.find(".agreeBox-description").html(a.description), d._agreeBoxDescription = a.description) : d._Wrapper.find(".agreeBox-description").remove(), a.button ? (d._Wrapper.find(".agreeBox-button").html(a.button), d._button = a.button) : d._Wrapper.find(".agreeBox-button").remove(), a.onClickButton && (d.onClickButton = a.onClickButton), d._viewport.append(d._mask), d._viewport.append(d._Wrapper), d._Wrapper.find(".agreeBox-wrap").css("opacity", "1"), d._Wrapper.find(".agreeBox-button").bind("click", function () {
                d.hide()
            }), !0 === c ? d._Wrapper.bind("click", function () {
                d.hide()
            }) : d._Wrapper.unbind("click")
        },
        hide: function () {
            var a, b = this;
            b._Wrapper.find(".agreeBox-wrap").css("opacity", "0"), a = setTimeout(function () {
                b._Wrapper.remove(), b._mask.remove(), clearTimeout(a), b.onClickButton && b.onClickButton.call(b)
            }, 500)
        }
    }), b.Components.PopAgreeBox = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {})
    ;var b = a.Ariel, c = function () {
        this._init()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="pop-mask"></div>',
        _template: '<div class="agreeBox-wrap">  <div class="agreeBox">    <h3 class="agreeBoxAndCancel-title"></h3>    <p class="agreeBox-desc"></p>       <div class="agreeBox-btn-container">           <a href="javascript:void(0);" class="agreeBox-button-cancel"></a>           <a href="javascript:void(0);" class="agreeBox-button-next"></a>       </div>  </div></div>',
        _mask: null,
        _Wrapper: null,
        _viewport: null,
        _cancelButton: null,
        _nextButton: null,
        _agreeBoxTitle: null,
        _agreeBoxDescription: null,
        onClickButton: null,
        _init: function () {
            var a = this;
            a._mask = b.$(a._maskTemplate), a._viewport = $("#viewport")
        },
        show: function (a, c) {
            var d = this;
            d._Wrapper = b.$(d._template), a.title ? (d._Wrapper.find(".agreeBoxAndCancel-title").html(a.title), d._agreeBoxTitle = a.title) : d._Wrapper.find(".agreeBoxAndCancel-title").remove(), a.description ? (d._Wrapper.find(".agreeBox-desc").html(a.description), d._agreeBoxDescription = a.description) : d._Wrapper.find(".agreeBox-desc").remove(), a.cancel && a.next ? (d._Wrapper.find(".agreeBox-button-cancel").html(a.cancel), d._Wrapper.find(".agreeBox-button-next").html(a.next), d._cancelButton = a.cancel, d._nextButton = a.next) : (d._Wrapper.find(".agreeBox-button-cancel").remove(), d._Wrapper.find(".agreeBox-button-next").remove()), a.onClickButton && (d.onClickButton = a.onClickButton), d._viewport.append(d._mask), d._viewport.append(d._Wrapper), d._Wrapper.find(".agreeBox-wrap").css("opacity", "1"), d._Wrapper.find(".agreeBox-button-cancel").bind("click", function () {
                d.hide()
            }), d._Wrapper.find(".agreeBox-button-next").bind("click", function () {
                d.nextTry()
            }), !0 === c ? d._Wrapper.bind("click", function () {
                d.hide()
            }) : d._Wrapper.unbind("click")
        },
        hide: function () {
            var a, b = this;
            b._Wrapper.find(".agreeBox-wrap").css("opacity", "0"), a = setTimeout(function () {
                b._Wrapper.remove(), b._mask.remove(), clearTimeout(a)
            }, 500)
        },
        nextTry: function () {
            var a, b = this;
            a = setTimeout(function () {
                clearTimeout(a), b.onClickButton && b.onClickButton.getRate()
            }, 500)
        }
    }), b.Components.PopAgreeAndCancelBox = new c
}(window), function (a) {
    a.Ariel.Components || (a.Ariel.Components = {});
    var b = a.Ariel, c = function (a) {
        var b = this;
        b._init(a), b._initEvent()
    };
    b.override(c.prototype, {
        _pattern: null, _tap: null, _wrapper: null, _init: function (a) {
            var b = this;
            b._pattern = a.pattern ? a.pattern : /\w+/, b._tap = void 0 === a.tap || a.tap, b._wrapper = $(a.selector), a._displayReset && (b._displayReset = a._displayReset)
        }, _initEvent: function () {
            var a = this;
            a._fixedIOS7Focus(), a._wrapper.find(".textfile-input").unbind("keyup.txtfile.init").bind("keyup.txtfile.init", function () {
                a._displayReset()
            }), a._wrapper.find(".textfile-input").unbind("input").bind("input", function () {
                a._displayReset()
            }), a._wrapper.find(".textfile-input").unbind("blur").bind("blur", function () {
                a._fixedIOS7Focus(), a._validate()
            }), a._wrapper.find(".textfile-reset").unbind(a._tap ? "tap" : "click").bind(a._tap ? "tap" : "click", function () {
                a._resetInput()
            })
        }, _fixedIOS7Focus: function () {
            var b = a.navigator.userAgent;
            if (/iPhone\ OS\ 7/.test(b) || /iPhone\;.+\;7/.test(b)) {
                var c = this;
                c._wrapper.find(".textfile-input").one("touchstart", function (a) {
                    c._wrapper.find(".textfile-input").focus(), a.preventDefault && (a.preventDefault(), a.stopPropagation())
                }).bind("touchmove", function (a) {
                    a.preventDefault && (a.preventDefault(), a.stopPropagation())
                })
            }
        }, _displayReset: function () {
            var a = this;
            0 == a._wrapper.find(".textfile-input").val().length ? a._wrapper.find(".textfile-reset").addClass("textfile-reset-hide") : a._wrapper.find(".textfile-reset").removeClass("textfile-reset-hide")
        }, _validate: function () {
            var a = this;
            a.isValid() ? a._wrapper.removeClass("textfile-invalid") : a._wrapper.addClass("textfile-invalid")
        }, isValid: function () {
            var a = this;
            return a._pattern.test(a._wrapper.find(".textfile-input").val())
        }, _resetInput: function () {
            var a = this;
            a._wrapper.find(".textfile-input").val(""), a._wrapper.removeClass("textfile-invalid"), a._displayReset()
        }, refresh: function () {
            var a = this;
            0 != a._wrapper.find(".textfile-input").val().length && a._validate(), a._displayReset()
        }
    }), b.Components.Textfile = c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.DataKeys || (a.Common.DataKeys = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        ACROSS_PAGES_PAY_REQUEST_DATA: "ACROSS_PAGES_PAY_REQUEST_DATA",
        ACROSS_PAGES_PAY_BUSINESS_DATA: "ACROSS_PAGES_PAY_BUSINESS_DATA",
        ACROSS_PAGES_BLANK_NOTE_STAGE_DATA: "ACROSS_PAGES_BLANK_NOTE_STAGE_DATA",
        ACROSS_PAGES_CLIENT_ENTRANCE_DATA: "ACROSS_PAGES_CLIENT_ENTRANCE_DATA",
        CACHE_PAY_BANKS_DATA: "CACHE_PAY_BANKS_DATA_2014110701",
        ACROSS_PAGES_DIRECT_DATA: "ACROSS_PAGES_DIRECT_DATA",
        ACROSS_PAGES_UNBIND_CARD_DATA: "ACROSS_PAGES_UNBIND_CARD_DATA",
        ACROSS_PAGES_PAYMENT_DATA: "ACROSS_PAGES_PAYMENT_DATA",
        ACROSS_PAGES_VERIFY_CODE_DATA: "ACROSS_PAGES_VERIFY_CODE_DATA",
        NEW_CARD_INFO_DATA: "NEW_CARD_INFO_DATA",
        SEND_USER_CLICK_DATA_SWITCH: "SEND_USER_CLICK_DATA_SWITCH"
    }), a.Common.DataKeys = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.DataKeys || (a.Common.DataKeys = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        _wrapper: null,
        _panel: null,
        _hourB: null,
        _minuteB: null,
        _secondB: null,
        popInfo: null,
        _template: '<span class="remain-time">璇锋偍鍦�<span class="remain-time-hour"></span><span class="count-down-clon"></span><span class="remain-time-minute"></span><span class="count-down-clon"></span><span class="remain-time-second"></span>鍐呭畬鎴愭敮浠�</span>',
        countingDown: function (a, b) {
            var c = this;
            c._wrapper = $(c._template), c._panel = $(".JS-pay-tip"), isNaN(a) || (c._hourB = c._wrapper.find(".remain-time-hour"), c._minuteB = c._wrapper.find(".remain-time-minute"), c._secondB = c._wrapper.find(".remain-time-second"), c.popInfo = b || {}, c.refreshTime(a))
        },
        _getTime: function (a) {
            var b = {}, c = this;
            return b.hour = c._timeFormat(Math.floor(a / 60 / 60)), b.minute = c._timeFormat(Math.floor(a / 60 % 60)), b.second = c._timeFormat(Math.floor(a % 60)), b
        },
        _timeFormat: function (a) {
            return a < 10 ? "0" + a : a
        },
        refreshTime: function (a) {
            var b = this;
            b._getTime(a).hour < 12 ? (b._countDownSecond(a), b._showCountDownWraper()) : b.toCountTime(a), b._refreshWraper(a)
        },
        toCountTime: function (a) {
            var b = this, c = a - 43200 + 1, d = setTimeout(function () {
                b._countDownSecond(43199), b._showCountDownWraper(), clearTimeout(d)
            }, 1e3 * c)
        },
        _countDownSecond: function (a) {
            var b = this;
            b._refreshWraper(a);
            var c = setInterval(function () {
                b._refreshWraper(a), --a < 0 && (clearInterval(c), b.handlerOrderExpiration())
            }, 1e3)
        },
        _refreshWraper: function (a) {
            var b = this, c = b._getTime(a);
            b._hourB.html(c.hour), b._minuteB.html(c.minute), b._secondB.html(c.second)
        },
        handlerOrderExpiration: function () {
            var b = this;
            b.popInfo.urlParam && b.popInfo.btnUrl && (b.popInfo.btnUrl = b.popInfo.btnUrl + "?params=" + JSON.stringify(b.popInfo.urlParam), b.popInfo.btnUrl = b.popInfo.btnUrl.replace("[", ""), b.popInfo.btnUrl = b.popInfo.btnUrl.replace("]", ""));
            var c = JSON.stringify(b.popInfo);
            orderExpiration = function () {
                return c
            };
            var d = navigator.userAgent;
            if (d.match(/(iPhone\sOS)\s([\d_]+)/) || d.match(/(iPad).*OS\s([\d_]+)/)) if ("jdltapp" == d.toLocaleLowerCase().match(/jdltapp/i)) {
                var e = {method: "notifyMessageToCustomWebView", params: {type: "count_down"}};
                a.webkit.messageHandlers.JDAppUnite.postMessage(e)
            } else a.location.href = 'jdmobileCashier://popToast?params={"type":"count_down"}';
            a.JdAndroid && a.JdAndroid.showCountDownDialog && a.JdAndroid.showCountDownDialog(c)
        },
        _showCountDownWraper: function () {
            var a = this;
            $(".order-bar").addClass("count-down"), a._panel.html(a._wrapper)
        }
    }), a.Common.CountDeadLine = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.JSON || (a.Common.JSON = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        parse: function (a) {
            return a ? $.parseJSON(a) : null
        }
    }), a.Common.JSON = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Net || (a.Common.Net = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        ajax: function (a) {
            var b = this;
            return b._processUrl(a), b._processParameters(a), b._processSuccess(a), $.ajax(a)
        }, _processUrl: function (a) {
            1 != a.skipProcessUrl && (/^\/pay\/.+$/.test(a.url) || (a.url = "/newpay/" + a.url))
        }, _processParameters: function (a) {
            this._setParameters(a, ["appId", "payId"]), a.data._format_ = "JSON"
        }, _setParameters: function (a, c) {
            for (var d = Ariel.parseGetParameters(), e = !0, f = c.length - 1; f >= 0; f--) d[c[f]] || (e = !1);
            if (a.data || (a.data = {}), "jdltapp" == navigator.userAgent.toLowerCase().match(/jdltapp/i) && (a.data.appid = "lite-h5"), e) {
                for (var g = {}, f = 0; f < c.length; f++) g[c[f]] = d[c[f]], a.data[c[f]] = d[c[f]];
                Ariel.Courier._canStorage() && Ariel.Courier.add(Common.DataKeys.ACROSS_PAGES_PAY_REQUEST_DATA, JSON.stringify(g))
            } else {
                var h = Common.JSON.parse(b.Courier.get(Common.DataKeys.ACROSS_PAGES_PAY_REQUEST_DATA));
                if (h) for (var f = 0; f < c.length; f++) a.data[c[f]] = h[c[f]]
            }
        }, _processSuccess: function (b) {
            var c = this;
            if (b.success) {
                var d = b.success;
                b.success = function (b) {
                    if (b.returnurl) return void (a.location.href = b.returnurl);
                    var e = a.location.href, f = Ariel.parseGetParameters(), g = {
                        downAppURl: "//h5.m.jd.com/active/download/download.html?channel=jd-m",
                        downAppIos: "https://itunes.apple.com/us/app/jing-dong-wang-gou-shou-dan/id414245413",
                        downWeixin: "http://a.app.qq.com/o/simple.jsp?pkgname=com.jingdong.app.mall&g_f=991850",
                        downIpad: "https://itunes.apple.com/cn/app/jing-dong-hd/id434374726?mt=8",
                        inteneUrl: "openapp.jdmobile://virtual?",
                        inteneUrlParams: {category: "jump", des: "m", url: e},
                        sourceType: "SYT",
                        sourceValue: "SYT",
                        M_sourceFrom: "syt_pcauto",
                        NoJumpDownLoadPage: !0,
                        kepler_param: null,
                        autoOpenAppEventId: "MDownLoadFloat_AppArouse",
                        autoOpenAppEventParam: "",
                        autoOpenIntervalTime: 0,
                        cookieFlag: null,
                        noJdApp: !1,
                        autoOpenAppCallback: c._before,
                        autoOpenAppCallbackSource: null
                    }, h = navigator.userAgent.toLowerCase();
                    if (-1 == h.indexOf("jdapp")) {
                        if ("syt" == f.pc_source) return "micromessenger" == h.match(/MicroMessenger/i) ? Ariel.mpingEvent("MCashierNew_HomeWXVirtual", location.href) : Ariel.mpingEvent("MCashierNew_HomeBrowserVirtual", location.href), void $.downloadAppPlugInOpenApp(g)
                    } else "syt" == f.pc_source && Ariel.mpingEvent("MCashierNew_HomeAppVirtual", location.href);
                    if (!1 === b.flag) {
                        var i, j = "", k = {}, l = "", m = a.location.href;
                        if (Ariel.Components.Loading.hide(), "diffrent" === b.code) {
                            if (j = "鍒囨崲璐﹀彿", b.loginAppInfo) {
                                var n = b.loginAppInfo.params;
                                i = b.loginAppInfo.loginUrl + "?params=" + encodeURIComponent(JSON.stringify(n))
                            } else l = b.loginUrl + encodeURIComponent(m), i = b.logoutUrl + encodeURIComponent(l);
                            k = {
                                title: b.info,
                                button: j,
                                loginUrl: i
                            }, Ariel.Components.PopAgreeBox.show(k, !1), Ariel.mpingEvent("MCashierNew_DifferExpo", location.href), $(".agreeBox-button").attr("href", i), $(".agreeBox-button").on("click", function () {
                                Ariel.mpingEvent("MCashierNew_SwitchAccount", location.href), a.isliteApp && Ariel.originIosJumpProtocol({
                                    method: "notifyMessageToCustomWebView",
                                    params: {needLogin: "true"}
                                })
                            })
                        } else {
                            if ("nologin" !== b.code) return void (a.location.href = "error.html?type=1&description=" + encodeURI(b.info));
                            if (j = "鍘荤櫥褰�", b.loginAppInfo) {
                                var n = b.loginAppInfo.params;
                                i = b.loginAppInfo.loginUrl + "?params=" + encodeURIComponent(JSON.stringify(n))
                            } else i = b.loginUrl + encodeURIComponent(m);
                            k = {
                                title: b.info,
                                button: j,
                                loginUrl: i
                            }, Ariel.Components.PopAgreeBox.show(k, !1), Ariel.mpingEvent("MCashierNew_LoginFloatExpo", location.href), $(".agreeBox-button").attr("href", i), $(".agreeBox-button").on("click", function () {
                                Ariel.mpingEvent("MCashierNew_LoginFloat", location.href), a.isliteApp && Ariel.originIosJumpProtocol({
                                    method: "notifyMessageToCustomWebView",
                                    params: {needLogin: "true"}
                                })
                            })
                        }
                    }
                    d.call(this, b)
                }
            }
        }, _before: function () {
            var b = a.location.href;
            b = b.split("&pc_source=syt"), "micromessenger" == navigator.userAgent.toLowerCase().match(/MicroMessenger/i) ? setTimeout(function () {
                a.location.href = b[0]
            }, 2e3) : a.location.href = b[0]
        }
    }), a.Common.Net = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.FinishPage || (a.Common.FinishPage = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        go: function (b, c) {
            var b = b || {}, c = c || {}, d = Ariel.parseGetParameters() || {};
            for (var e in b) d[e] = b[e];
            var f = "";
            for (var e in c) f = "" == f ? e + "=" + c[e] : url + "&" + e + "=" + c[e];
            Ariel.Components.Loading.show(), Common.Net.ajax({
                url: "getSuccessUrlInfo.action",
                data: d,
                success: function (b) {
                    Ariel.Components.Loading.hide(), b && "0" == b.code && (b.url ? (b.url.indexOf("?") > 0 ? b.url += "&" + f : b.url += "?" + f, a.location.href = b.url) : a.location.href = "finish.html?" + f)
                }
            })
        }
    }), a.Common.FinishPage = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MSGBOX_MASK"></div>',
        _template: '<div class="COMM_ID_VALIDATION_MSGBOX_WRAPPER">    <div class="COMM_ID_VALIDATION_MSGBOX">        <div class="COMM_ID_VALIDATION_MSGBOX_FORM">            <span class="COMM_ID_VALIDATION_MSGBOX_TIP">楠岃瘉鏀粯瀵嗙爜</span>             <div class="textfile COMM_ID_VALIDATION_MSGBOX_PASSWORD">                <input type="password" class="textfile-input COMM_ID_VALIDATION_MSGBOX_PASSWORD_INPUT" placeholder="璇疯緭鍏ユ敮浠樺瘑鐮�" />                <a href="javascript:void(0);"  class="switchtextfile-switch">                    <span class="switchtextfile-status switchtextfile-status-password"></span>                </a>            </div>            <span class="COMM_ID_VALIDATION_MSGBOX_PASSWORD_TIP"></span>            <a href="javascript:void(0)" class="COMM_ID_VALIDATION_MSGBOX_FORGET">蹇樿鏀粯瀵嗙爜锛�</a>        </div>        <div class="COMM_ID_VALIDATION_MSGBOX_ACTION_WRAPPER">           <a href="javascript:void(0);" class="COMM_ID_VALIDATION_MSGBOX_CANCEL">鍙栨秷</a>           <a href="javascript:void(0);" class="COMM_ID_VALIDATION_MSGBOX_OK COMM_ID_VALIDATION_MSGBOX_DISABLED">纭畾</a>        </div>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _payType: null,
        _payAction: null,
        _payActionParameter: null,
        _txtPassword: null,
        onBTQuickPayFailed: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var c = this;
            c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_INPUT").unbind("input keyup").bind("input keyup", function () {
                c._txtPassword.isValid() ? c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_OK").removeClass("COMM_ID_VALIDATION_MSGBOX_DISABLED") : c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_OK").addClass("COMM_ID_VALIDATION_MSGBOX_DISABLED");
                var a = c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_INPUT").val();
                a && a.length > 0 && c._txtPassword._wrapper.find(".textfile-reset").removeClass("textfile-reset-hide")
            }), c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_OK").click(function () {
                if (!$(this).hasClass("COMM_ID_VALIDATION_MSGBOX_DISABLED")) {
                    if (!c._txtPassword.isValid()) return void c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_TIP").text("璇疯緭鍏ユ敮浠樺瘑鐮�");
                    Ariel.mpingEvent("JDCashierIousSecurityVerify_Sure", location.href), c.hide(), Ariel.Components.Loading.show(), Common.Net.ajax({
                        url: c._payAction,
                        type: "POST",
                        data: c._getParameters(),
                        success: function (a) {
                            Ariel.Components.Loading.hide(), "blank-note" == c._payType && c._blankNoteCallBack.call(c, a), "blank-note-quick" == c._payType && c._blankNoteQuickCallBack.call(c, a)
                        }
                    })
                }
            }), c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_CANCEL").click(function () {
                c.hide(), Ariel.Components.Loading.hide()
            }), c._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_FORGET").click(function () {
                Ariel.mpingEvent("JDCashierIousSecurityVerify_ForgetPW", location.href);
                var c = Common.JSON.parse(b.Courier.get(Common.DataKeys.ACROSS_PAGES_PAY_REQUEST_DATA)), d = c.appId,
                    e = c.payId;
                c.sid ? a.location.href = "https://passport.m.jd.com/payPassword/validateFindPayPassword.action?urlFrom=1&sid=" + c.sid : a.location.href = "/newpay/jumpPage.action?appId=" + d + "&payId=" + e
            })
        },
        _blankNoteCallBack: function (a) {
            var c = this;
            a.baiTiaoPayResult.isSuccess && "1" == a.baiTiaoPayResult.tradeStatus ? c._sendVerifyCode(a) : a.baiTiaoPayResult.isSuccess && 1 == a.baiTiaoPayResult.txnStatus ? (c._validateAcrossPagesBlankNoteStageData(a), c._processFinishData(a)) : b.Components.Pop.show(a.baiTiaoPayResult.info ? a.baiTiaoPayResult.info : "绯荤粺寮傚父")
        },
        _blankNoteQuickCallBack: function (a) {
            var b = this;
            Ariel.mpingEvent("MCashierPay_BaiTiaoOpen", location.href, "a"), a.resultParam && a.resultParam.retCode && "SUCCESS" == a.resultParam.retCode ? (b._validateAcrossPagesBTQuickData(a.resultParam.data), b._processFinishData(a)) : b.onBTQuickPayFailed.call(b, a)
        },
        _validateAcrossPagesBTQuickData: function (a) {
            var c = {
                payType: "blank-note-quick",
                isValid: "1",
                planText: a.planText ? a.planText : "涓嶅垎鏈�",
                price: !(!a.price || "null" == a.price) && a.price,
                total: !(!a.total || "null" == a.total) && a.total,
                stage: a.installmentNum ? a.installmentNum : "1",
                repayDate: !(!a.repayDate || "null" == a.repayDate) && a.repayDate
            };
            b.Courier.add(Common.DataKeys.ACROSS_PAGES_BLANK_NOTE_STAGE_DATA, JSON.stringify(c))
        },
        _sendVerifyCode: function (c) {
            var d = this;
            Common.Net.ajax({
                url: "baiTiaoSendCode.action", type: "POST", success: function (e) {
                    if (Ariel.Components.Loading.hide(), e.success) {
                        delete d._payActionParameter.password;
                        var f = {payType: "blank-note", params: d._payActionParameter, tel: e.phone, data: c};
                        b.Courier.add(Common.DataKeys.ACROSS_PAGES_VERIFY_CODE_DATA, JSON.stringify(f));
                        var g = Ariel.parseGetParameters();
                        setTimeout(function () {
                            a.location.href = "verify-code.html?payId=" + g.payId + "&appId=" + g.appId
                        }, 50)
                    } else b.Components.Pop.show(e.info)
                }
            })
        },
        _validateAcrossPagesBlankNoteStageData: function (a) {
            var c = a.baiTiaoPayResult || {}, d = {
                isValid: 1,
                planText: c.planText ? c.planText : "涓嶅垎鏈�",
                price: !(!c.price || "null" == c.price) && c.price,
                total: !(!c.total || "null" == c.total) && c.total,
                stage: c.installmentNum ? c.installmentNum : "1",
                repayDate: !(!c.repayDate || "null" == c.repayDate) && c.repayDate,
                randomDiscount: !!c.randomDiscount && c.randomDiscount
            };
            b.Courier.add(Common.DataKeys.ACROSS_PAGES_BLANK_NOTE_STAGE_DATA, JSON.stringify(d))
        },
        _processFinishData: function (c) {
            var d = Ariel.parseGetParameters();
            d.payId && d.appId || (d = Common.JSON.parse(b.Courier.get(Common.DataKeys.ACROSS_PAGES_PAY_REQUEST_DATA))), a.Common.FinishPage.go(d)
        },
        _saveClientEntranceData: function (a) {
            b.Courier.add(Common.DataKeys.ACROSS_PAGES_CLIENT_ENTRANCE_DATA, JSON.stringify({
                showIcon: a.showIcon ? "1" : "0",
                showMIcon: a.showMIcon ? "1" : "0"
            }))
        },
        _getParameters: function () {
            var a = this, b = a._payActionParameter;
            b.password = a._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_INPUT").val(), RSAUtils.setMaxDigits(131);
            var c = RSAUtils.getKeyPair(publicKey.po, "", publicKey.mu), d = b.password.split("").reverse().join("");
            return b.password = RSAUtils.encryptedString(c, d), b
        },
        show: function (a) {
            var b = this;
            b._payType = a.payType, "blank-note" == b._payType && (b._payAction = "baiTiaoPayResult.action"), "blank-note-quick" == b._payType && (b._payAction = "baiTiaoQuickPay.action"), b._payActionParameter = a.data, b._resetPassword(), b._hideForgetPassword(), b._viewport.append(b._mask), b._viewport.append(b._wrapper), b._txtPassword = new Common.Components.SwitchTextfile({
                selector: ".COMM_ID_VALIDATION_MSGBOX .COMM_ID_VALIDATION_MSGBOX_PASSWORD",
                tap: !1
            })
        },
        _resetPassword: function () {
            var a = this;
            a._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_INPUT").val("").attr("type", "password"), a._wrapper.find(".switchtextfile-status").removeClass("switchtextfile-status-code").addClass("switchtextfile-status-password"), a._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_PASSWORD_TIP").text("")
        },
        _hideForgetPassword: function () {
            var a = this;
            "blank-note" != a._payType && "blank-note-quick" != a._payType || a._wrapper.find(".COMM_ID_VALIDATION_MSGBOX_FORGET").show()
        },
        hide: function () {
            var a = this;
            a._wrapper.remove(), a._mask.remove()
        }
    }), a.Common.Components.IdValidationMsgBox = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MSGBOX_MASK"></div>',
        _template: '<div class="COMM_MSGBOX_WRAPPER">    <div class="COMM_MSGBOX">        <div class="COMM_MSGBOX_FORM">            <span class="COMM_MSGBOX_TIP">鏀粯纭</span>             <div>                <span class="COMM_MSGBOX_CONTENT">1銆佽鍦ㄥ井淇″唴瀹屾垚鏀粯锛屽鏋滄偍宸叉敮浠樻垚鍔燂紝璇风偣鍑烩€滃凡瀹屾垚鏀粯鈥濇寜閽�</span>                 <span class="COMM_MSGBOX_CONTENT">2銆佸鏋滄偍杩樻湭瀹夎寰俊6.0.2 鍙婁互涓婄増鏈鎴风锛岃鐐瑰嚮"鍙栨秷"骞堕€夋嫨鍏跺畠鏀粯鏂瑰紡浠樻</span>             </div>        </div>        <div class="COMM_MSGBOX_ACTION_WRAPPER">           <a href="javascript:void(0);" class="COMM_MSGBOX_CANCEL">鍙栨秷</a>           <a href="javascript:void(0);" class="COMM_MSGBOX_OK">宸插畬鎴愭敮浠�</a>        </div>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _prePayId: null,
        _finishData: null,
        _tid: null,
        _startPollTime: null,
        _pollNum: 1,
        _pollQueryBetween: [10, 5, 5, 5, 5],
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".COMM_MSGBOX_OK").click(function () {
                a._wrapper.find(".COMM_MSGBOX_OK").hasClass("COMM_MSGBOX_OK_DISABLE") || (Ariel.mpingEvent("MCashierWeChat_Paid"), a._wrapper.find(".COMM_MSGBOX_OK").addClass("COMM_MSGBOX_OK_DISABLE"), a._verifyTradeStatus())
            }), a._wrapper.find(".COMM_MSGBOX_CANCEL").click(function () {
                Ariel.mpingEvent("MCashierWeChat_Cancel"), a.hide(), a.reloadCurrentPage()
            })
        },
        _verifyTradeStatus: function () {
            var c = this;
            Common.Net.ajax({
                url: "../index.action",
                type: "POST",
                skipProcessUrl: !0,
                data: {
                    functionId: "wapWeiXinPayQueryForMobile",
                    body: JSON.stringify({
                        payId: c._finishData.jdPrePayId,
                        payEnum: c._finishData.payEnum,
                        mobliePayId: c._finishData.mobliePayId,
                        time: ""
                    })
                },
                success: function (d) {
                    d.isSuccess ? "1" != d.payStatus ? (c.hide(), b.Components.Pop.show(d.message ? d.message : "璁㈠崟杩樻湭瀹屾垚浠樻锛岃鎮ㄩ噸鏂版敮浠�"), setTimeout(function () {
                        c.reloadCurrentPage()
                    }, 800)) : c._processFinishData(d) : a.location.href = "error.html?type=1&description=" + (d.message ? d.message : "绯荤粺寮傚父")
                }
            })
        },
        _pollQueryPayResult: function () {
            var a = this;
            a._tid = setInterval(function () {
                var b = a._pollQueryBetween[a._pollNum - 1], c = (new Date).valueOf();
                c - a._startPollTime < 1e3 * b || (a._startPollTime = c, Common.Net.ajax({
                    url: "../index.action",
                    type: "POST",
                    skipProcessUrl: !0,
                    data: {
                        functionId: "wapWeiXinPayQueryForMobile",
                        body: JSON.stringify({
                            payId: a._finishData.jdPrePayId,
                            payEnum: a._finishData.payEnum,
                            mobliePayId: a._finishData.mobliePayId,
                            time: a._pollNum + ""
                        })
                    },
                    success: function (b) {
                        if (b.isSuccess && "1" == b.payStatus) return a._pollNum = 1, clearInterval(a._tid), void a._processFinishData(b);
                        ++a._pollNum > a._pollQueryBetween.length && (a._pollNum = 1, clearInterval(a._tid))
                    }
                }))
            }, 400)
        },
        _setFinishData: function (a) {
            this._finishData = a, this._startPollTime = (new Date).valueOf(), this._pollQueryPayResult(), this._pollNum = 1
        },
        _processFinishData: function (b) {
            var c = this;
            c.hide();
            var d = Ariel.parseGetParameters();
            a.Common.FinishPage.go(d, {_total: c._finishData._total})
        },
        reloadCurrentPage: function () {
            var b = this;
            if (!b._finishData || !0 !== b._finishData.isH5) {
                var c = navigator.userAgent;
                c.indexOf("AppleWebKit") > 0 && c.indexOf("Safari") > 0 && c.indexOf("Chrome") < 0 && c.indexOf("UCBrowser") < 0 && c.indexOf("Firefox") < 0 && a.location.reload()
            }
        },
        show: function (a) {
            var b = this;
            b._viewport.append(b._mask), b._viewport.append(b._wrapper), b._wrapper.find(".COMM_MSGBOX_OK").hasClass("COMM_MSGBOX_OK_DISABLE") && b._wrapper.find(".COMM_MSGBOX_OK").removeClass("COMM_MSGBOX_OK_DISABLE")
        },
        hide: function () {
            var a = this;
            clearInterval(a._tid), a._wrapper.remove(), a._mask.remove()
        }
    }), a.Common.Components.WapPayMsgBox = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MSGBOX_MASK"></div>',
        _template: '<div class="COMM_MSGBOX_WRAPPER">    <div class="COMM_BACK_MSGBOX">        <div class="COMM_MSGBOX_FORM">            <span class="COMM_MSGBOX_TIP">纭瑕佺寮€鏀堕摱鍙帮紵</span>             <div>                <span class="COMM_MSGBOX_CONTENT"></span>             </div>        </div>        <div class="COMM_MSGBOX_ACTION_WRAPPER">           <a href="javascript:void(0);" class="COMM_MSGBOX_CANCEL">缁х画鏀粯</a>           <a href="javascript:void(0);" class="COMM_MSGBOX_OK">纭绂诲紑</a>        </div>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".COMM_MSGBOX_OK").click(function () {
                a.goBack()
            }), a._wrapper.find(".COMM_MSGBOX_CANCEL").click(function () {
                a.hide()
            })
        },
        goBack: function () {
            var b = {};
            Ariel.Courier._canStorage() && (b = Common.JSON.parse(Ariel.Courier.get(Common.DataKeys.ACROSS_PAGES_PAY_BUSINESS_DATA))), b && b.quitUrl ? 0 == b.quitUrl.indexOf("https://") || 0 == b.quitUrl.indexOf("http://") ? a.location.replace(b.quitUrl) : a.location.href = b.quitUrl : a.history.go(-1)
        },
        show: function (a) {
            var b = this;
            if (a.dialogSwitch = a.dialogSwitch || 0, 1 != a.dialogSwitch) return void b.goBack();
            a ? (b._wrapper.find(".COMM_MSGBOX_CONTENT").html(a.message), a.title && b._wrapper.find(".COMM_MSGBOX_TIP").html(a.title), a.leftBtn && b._wrapper.find(".COMM_MSGBOX_CANCEL").html(a.leftBtn), a.rightBtn && b._wrapper.find(".COMM_MSGBOX_OK").html(a.rightBtn)) : b._wrapper.find(".COMM_BACK_MSGBOX").addClass("COMM_BACK_MSGBOX_NO_MSG"), b._viewport.append(b._mask), b._viewport.append(b._wrapper)
        },
        hide: function () {
            var a = this;
            a._wrapper.remove(), a._mask.remove()
        }
    }), a.Common.Components.GoBackMsgBox = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        HELP_FOR_EXPIRE: "HELP_FOR_EXPIRE",
        HELP_FOR_SAFE_CODE: "HELP_FOR_SAFE_CODE",
        AUTHENTICATION: "AUTHENTICATION",
        AGAIN_BIND_CARD: "AGAIN_BIND_CARD",
        SHOW_ID_CARD_INFO: "SHOW_ID_CARD_INFO",
        SHOW_PHONE_INFO: "SHOW_PHONE_INFO",
        _maskTemplate: '<div class="COMM_DIALOG_MASK"></div>',
        _template: '<div class="COMM_DIALOG_WRAPPER">    <div class="COMM_DIALOG">        <span class="COMM_DIALOG_TITLE"></span>        <a href="javascript:void(0);" class="COMM_DIALOG_CLOSE"></a>        <p class="COMM_DIALOG_DESCRIPTION"></p>        <a href="javascript:void(0);" class="COMM_DIALOG_BTN">鎴戠煡閬撲簡</a>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".COMM_DIALOG_BTN").click(function () {
                a.hide()
            })
        },
        show: function (a, b) {
            var c = this;
            a == c.HELP_FOR_EXPIRE && (c._wrapper.find(".COMM_DIALOG_TITLE").text("鍗＄墖鏈夋晥鏈�"), c._wrapper.find(".COMM_DIALOG_DESCRIPTION").html("鏈夋晥鏈熶负鍗＄墖姝ｉ潰鏍煎紡涓�</br>鏈堜唤/骞翠唤鐨勬暟瀛�").removeClass("COMM_DIALOG_SAFE_CODE_DESCRIPTION").addClass("COMM_DIALOG_EXPIRE_DESCRIPTION"), c._wrapper.removeClass("COMM_DIALOG_AUTHENTICATION COMM_AGAIN_BIND_CARD")), a == c.HELP_FOR_SAFE_CODE && (c._wrapper.find(".COMM_DIALOG_TITLE").text("鍗＄墖瀹夊叏鐮�"), c._wrapper.find(".COMM_DIALOG_DESCRIPTION").html("瀹夊叏鐮佷负鍗¤儗闈㈢鍚嶅</br>涓€涓叉暟瀛楃殑鏈笁浣�").removeClass("COMM_DIALOG_EXPIRE_DESCRIPTION").addClass("COMM_DIALOG_SAFE_CODE_DESCRIPTION"), c._wrapper.removeClass("COMM_DIALOG_AUTHENTICATION COMM_AGAIN_BIND_CARD")), a == c.AUTHENTICATION && (c._wrapper.find(".COMM_DIALOG_TITLE").html(b), c._wrapper.find(".COMM_DIALOG_DESCRIPTION").html('<div class="type-info-box"><p class="username"><label>濮撳悕</label><input id="holderName" type="text" placeholder="璇疯緭鍏ュ鍚�"></p><p class="authentication-num"><label>韬唤璇�</label><input id="holderId" type="text" placeholder="璇疯緭鍏ヨ韩浠借瘉鍙�"></p></div><p class="COMM_DIALOG_TIP">璇峰～鍐欐纭殑涓汉韬唤淇℃伅骞舵巿鏉冩垜浠悜澶栨眹绠＄悊灞€鍜屾捣鍏崇敵鎶ワ紝鎴戜滑鎵胯濡ュ杽淇濈鍙婁娇鐢ㄦ偍鐨勪俊鎭�<span class="pop-protocol">鍚屾剰<a href="http://sp.jd.com/paymentAsset/protocol/crossBorder/protocol.html" class="protocol">銆婁唬鐞嗚喘缁撴眹鍗忚銆�</a></span></p>').removeClass("COMM_DIALOG_SAFE_CODE_DESCRIPTION"), c._wrapper.find(".COMM_DIALOG_BTN").remove(), c._wrapper.find(".btn-wraps").remove(), c._wrapper.find(".COMM_DIALOG").append('<div class="btn-wraps"><a href="javascript:void(0);" class="confirm-auther-btn">纭畾</a></div>'), c._wrapper.addClass("COMM_DIALOG_AUTHENTICATION")), a == c.AGAIN_BIND_CARD && (c._wrapper.find(".COMM_DIALOG_TITLE").text("鏀粯澶辫触"), c._wrapper.find(".COMM_DIALOG_DESCRIPTION").html('<div class="pop-failure"><dl><dt>浠ヤ笅涓ょ鎯呭喌闇€瑕佹偍閲嶆柊缁戝崱锛�</dt><dd>1. 淇＄敤鍗℃湁鏁堟湡杩囨湡鎴栨洿鎹簡鍗＄墖 <br />2. 淇敼浜嗛摱琛岄鐣欐墜鏈哄彿</dd></dl><div class="btns-box"><a class="btn-item btn-cancel" href="javascript:Common.Components.HelpDialog.hideRebindDialog();">鍙栨秷</a><a class="btn-item btn-rebind" href="javascript:Common.Components.HelpDialog.rebindCard();" >閲嶆柊缁戝畾</a></div></div>').removeClass("COMM_DIALOG_SAFE_CODE_DESCRIPTION"), c._wrapper.addClass("COMM_AGAIN_BIND_CARD")), a == c.SHOW_ID_CARD_INFO && (c._wrapper.find(".COMM_DIALOG_TITLE").text("鍗＄墖瀹夊叏鐮�"), c._wrapper.find(".COMM_DIALOG_DESCRIPTION").html("姝よ韩浠借瘉鍙蜂粎鐢ㄤ簬浜笢鏀粯淇℃伅<br/>楠岃瘉锛屼笉鐢遍摱琛岄獙璇�").removeClass("COMM_DIALOG_EXPIRE_DESCRIPTION").addClass("COMM_DIALOG_SAFE_CODE_DESCRIPTION"), c._wrapper.addClass("COMM_AGAIN_BIND_CARD")), c._viewport.append(c._mask), c._viewport.append(c._wrapper)
        },
        hide: function () {
            var a = this;
            a._wrapper.remove(), a._mask.remove()
        },
        hideRebindDialog: function () {
            var a = this;
            Ariel.mpingEvent("JDCashierFastPay_Cancel", location.href), a.hide()
        },
        rebindCard: function () {
            Ariel.mpingEvent("JDCashierFastPay_Rebind", location.href);
            var b = Ariel.parseGetParameters();
            setTimeout(function () {
                a.location.href = "/pay/new-card.html?payId=" + b.payId + "&appId=" + b.appId
            }, 200)
        }
    }), a.Common.Components.HelpDialog = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel;
    a.Common.Components.SwitchTextfile = b.extend(b.Components.Textfile, {
        _initEvent: function () {
            var a = this;
            a._fixedIOS7Focus(), a._wrapper.find(".textfile-input").unbind("blur").bind("blur", function () {
                a._fixedIOS7Focus(), a._validate()
            }), a._wrapper.find(".switchtextfile-switch").unbind(a._tap ? "tap" : "click").bind(a._tap ? "tap" : "click", function () {
                Ariel.mpingEvent("JDCashierIousSecurityVerify_PlaintextSwitch", location.href), a._switch()
            })
        }, _switch: function () {
            var a = this, b = a._wrapper.find(".switchtextfile-status"), c = a._wrapper.find(".textfile-input");
            b.hasClass("switchtextfile-status-password") ? (b.removeClass("switchtextfile-status-password").addClass("switchtextfile-status-code"), c.attr("type", "input")) : (b.removeClass("switchtextfile-status-code").addClass("switchtextfile-status-password"), c.attr("type", "password"))
        }, refresh: function () {
            var a = this;
            0 != a._wrapper.find(".textfile-input").val().length && a._validate()
        }
    })
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_DIALOG_MASK"></div>',
        _template: '<div class="COMM_DIALOG_WRAPPER">    <div class="SHOW_INFO_DIALOG">        <span class="COMM_DIALOG_TITLE"></span>        <p class="COMM_DIALOG_DESCRIPTION SHOW_INFO"></p>        <a href="javascript:void(0);" class="COMM_DIALOG_BTN">鎴戠煡閬撲簡</a>    </div></div>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._wrapper.find(".COMM_DIALOG_BTN").click(function () {
                a.hide()
            })
        },
        show: function (a) {
            var b = this;
            b._wrapper.find(".COMM_DIALOG_DESCRIPTION").html(a), b._viewport.append(b._mask), b._viewport.append(b._wrapper)
        },
        hide: function () {
            var a = this;
            a._wrapper.remove(), a._mask.remove()
        }
    }), a.Common.Components.CommonDialog = new c
}(window), function () {
    window.Common || (window.Common = {}), window.Common.Components || (window.Common.Components = {}), window.Common.Components.changeSkin = function (a, b) {
        var c = b || "", d = "/pay/pay2015011312/css/" + a.appId + c + ".min.css",
            e = ["jd_m_tlife", "jd_m_tlife_beta", "jd_newtlife", "jd_newtlife_beta"];
        if (-1 != $.inArray(a.appId, e)) {
            var f = document.createElement("link");
            f.type = "text/css", f.rel = "stylesheet", f.href = "cssPath", f.href = d, document.getElementsByTagName("head")[0].appendChild(f)
        }
    }
}(window), function (a) {
    function b(a) {
        var b = e, c = b.biDivideByRadixPower(a, this.k - 1), d = b.biMultiply(c, this.mu),
            f = b.biDivideByRadixPower(d, this.k + 1), g = b.biModuloByRadixPower(a, this.k + 1),
            h = b.biMultiply(f, this.modulus), i = b.biModuloByRadixPower(h, this.k + 1), j = b.biSubtract(g, i);
        j.isNeg && (j = b.biAdd(j, this.bkplus1));
        for (var k = b.biCompare(j, this.modulus) >= 0; k;) j = b.biSubtract(j, this.modulus), k = b.biCompare(j, this.modulus) >= 0;
        return j
    }

    function c(a, b) {
        var c = e.biMultiply(a, b);
        return this.modulo(c)
    }

    function d(a, b) {
        var c = new r;
        c.digits[0] = 1;
        for (var d = a, f = b; ;) {
            if (0 != (1 & f.digits[0]) && (c = this.multiplyMod(c, d)), f = e.biShiftRight(f, 1), 0 == f.digits[0] && 0 == e.biHighIndex(f)) break;
            d = this.multiplyMod(d, d)
        }
        return c
    }

    if (void 0 === a.RSAUtils) var e = a.RSAUtils = {};
    var f, g, h, k, l = 16, m = l, n = 65536, o = n >>> 1, p = n * n, q = n - 1, r = a.BigInt = function (a) {
        this.digits = "boolean" == typeof a && 1 == a ? null : g.slice(0), this.isNeg = !1
    };
    e.setMaxDigits = function (a) {
        f = a, g = new Array(f);
        for (var b = 0; b < g.length; b++) g[b] = 0;
        h = new r, k = new r, k.digits[0] = 1
    },
        e.setMaxDigits(20);
    var s = 15;
    e.biFromNumber = function (a) {
        var b = new r;
        b.isNeg = a < 0, a = Math.abs(a);
        for (var c = 0; a > 0;) b.digits[c++] = a & q, a = Math.floor(a / n);
        return b
    };
    var t = e.biFromNumber(1e15);
    e.biFromDecimal = function (a) {
        for (var b, c = "-" == a.charAt(0), d = c ? 1 : 0; d < a.length && "0" == a.charAt(d);) ++d;
        if (d == a.length) b = new r; else {
            var f = a.length - d, g = f % s;
            for (0 == g && (g = s), b = e.biFromNumber(Number(a.substr(d, g))), d += g; d < a.length;) b = e.biAdd(e.biMultiply(b, t), e.biFromNumber(Number(a.substr(d, s)))), d += s;
            b.isNeg = c
        }
        return b
    }, e.biCopy = function (a) {
        var b = new r(!0);
        return b.digits = a.digits.slice(0), b.isNeg = a.isNeg, b
    }, e.reverseStr = function (a) {
        for (var b = "", c = a.length - 1; c > -1; --c) b += a.charAt(c);
        return b
    };
    var u = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    e.biToString = function (a, b) {
        var c = new r;
        c.digits[0] = b;
        for (var d = e.biDivideModulo(a, c), f = u[d[1].digits[0]]; 1 == e.biCompare(d[0], h);) d = e.biDivideModulo(d[0], c), digit = d[1].digits[0], f += u[d[1].digits[0]];
        return (a.isNeg ? "-" : "") + e.reverseStr(f)
    }, e.biToDecimal = function (a) {
        var b = new r;
        b.digits[0] = 10;
        for (var c = e.biDivideModulo(a, b), d = String(c[1].digits[0]); 1 == e.biCompare(c[0], h);) c = e.biDivideModulo(c[0], b), d += String(c[1].digits[0]);
        return (a.isNeg ? "-" : "") + e.reverseStr(d)
    };
    var v = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    e.digitToHex = function (a) {
        var b = "";
        for (i = 0; i < 4; ++i) b += v[15 & a], a >>>= 4;
        return e.reverseStr(b)
    }, e.biToHex = function (a) {
        for (var b = "", c = (e.biHighIndex(a), e.biHighIndex(a)); c > -1; --c) b += e.digitToHex(a.digits[c]);
        return b
    }, e.charToHex = function (a) {
        var b = 48, c = b + 9, d = 97, e = d + 25, f = 65, g = 90;
        return a >= b && a <= c ? a - b : a >= f && a <= g ? 10 + a - f : a >= d && a <= e ? 10 + a - d : 0
    }, e.hexToDigit = function (a) {
        for (var b = 0, c = Math.min(a.length, 4), d = 0; d < c; ++d) b <<= 4, b |= e.charToHex(a.charCodeAt(d));
        return b
    }, e.biFromHex = function (a) {
        for (var b = new r, c = a.length, d = c, f = 0; d > 0; d -= 4, ++f) b.digits[f] = e.hexToDigit(a.substr(Math.max(d - 4, 0), Math.min(d, 4)));
        return b
    }, e.biFromString = function (a, b) {
        var c = "-" == a.charAt(0), d = c ? 1 : 0, f = new r, g = new r;
        g.digits[0] = 1;
        for (var h = a.length - 1; h >= d; h--) {
            var i = a.charCodeAt(h), j = e.charToHex(i), k = e.biMultiplyDigit(g, j);
            f = e.biAdd(f, k), g = e.biMultiplyDigit(g, b)
        }
        return f.isNeg = c, f
    }, e.biDump = function (a) {
        return (a.isNeg ? "-" : "") + a.digits.join(" ")
    }, e.biAdd = function (a, b) {
        var c;
        if (a.isNeg != b.isNeg) b.isNeg = !b.isNeg, c = e.biSubtract(a, b), b.isNeg = !b.isNeg; else {
            c = new r;
            for (var d, f = 0, g = 0; g < a.digits.length; ++g) d = a.digits[g] + b.digits[g] + f, c.digits[g] = d % n, f = Number(d >= n);
            c.isNeg = a.isNeg
        }
        return c
    }, e.biSubtract = function (a, b) {
        var c;
        if (a.isNeg != b.isNeg) b.isNeg = !b.isNeg, c = e.biAdd(a, b), b.isNeg = !b.isNeg; else {
            c = new r;
            var d, f;
            f = 0;
            for (var g = 0; g < a.digits.length; ++g) d = a.digits[g] - b.digits[g] + f, c.digits[g] = d % n, c.digits[g] < 0 && (c.digits[g] += n), f = 0 - Number(d < 0);
            if (-1 == f) {
                f = 0;
                for (var g = 0; g < a.digits.length; ++g) d = 0 - c.digits[g] + f, c.digits[g] = d % n, c.digits[g] < 0 && (c.digits[g] += n), f = 0 - Number(d < 0);
                c.isNeg = !a.isNeg
            } else c.isNeg = a.isNeg
        }
        return c
    }, e.biHighIndex = function (a) {
        for (var b = a.digits.length - 1; b > 0 && 0 == a.digits[b];) --b;
        return b
    }, e.biNumBits = function (a) {
        var b, c = e.biHighIndex(a), d = a.digits[c], f = (c + 1) * m;
        for (b = f; b > f - m && 0 == (32768 & d); --b) d <<= 1;
        return b
    }, e.biMultiply = function (a, b) {
        for (var c, d, f, g = new r, h = e.biHighIndex(a), i = e.biHighIndex(b), k = 0; k <= i; ++k) {
            for (c = 0, f = k, j = 0; j <= h; ++j, ++f) d = g.digits[f] + a.digits[j] * b.digits[k] + c, g.digits[f] = d & q, c = d >>> l;
            g.digits[k + h + 1] = c
        }
        return g.isNeg = a.isNeg != b.isNeg, g
    }, e.biMultiplyDigit = function (a, b) {
        var c, d, f;
        result = new r, c = e.biHighIndex(a), d = 0;
        for (var g = 0; g <= c; ++g) f = result.digits[g] + a.digits[g] * b + d, result.digits[g] = f & q, d = f >>> l;
        return result.digits[1 + c] = d, result
    }, e.arrayCopy = function (a, b, c, d, e) {
        for (var f = Math.min(b + e, a.length), g = b, h = d; g < f; ++g, ++h) c[h] = a[g]
    };
    var w = [0, 32768, 49152, 57344, 61440, 63488, 64512, 65024, 65280, 65408, 65472, 65504, 65520, 65528, 65532, 65534, 65535];
    e.biShiftLeft = function (a, b) {
        var c = Math.floor(b / m), d = new r;
        e.arrayCopy(a.digits, 0, d.digits, c, d.digits.length - c);
        for (var f = b % m, g = m - f, h = d.digits.length - 1, i = h - 1; h > 0; --h, --i) d.digits[h] = d.digits[h] << f & q | (d.digits[i] & w[f]) >>> g;
        return d.digits[0] = d.digits[h] << f & q, d.isNeg = a.isNeg, d
    };
    var x = [0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535];
    e.biShiftRight = function (a, b) {
        var c = Math.floor(b / m), d = new r;
        e.arrayCopy(a.digits, c, d.digits, 0, a.digits.length - c);
        for (var f = b % m, g = m - f, h = 0, i = h + 1; h < d.digits.length - 1; ++h, ++i) d.digits[h] = d.digits[h] >>> f | (d.digits[i] & x[f]) << g;
        return d.digits[d.digits.length - 1] >>>= f, d.isNeg = a.isNeg, d
    }, e.biMultiplyByRadixPower = function (a, b) {
        var c = new r;
        return e.arrayCopy(a.digits, 0, c.digits, b, c.digits.length - b), c
    }, e.biDivideByRadixPower = function (a, b) {
        var c = new r;
        return e.arrayCopy(a.digits, b, c.digits, 0, c.digits.length - b), c
    }, e.biModuloByRadixPower = function (a, b) {
        var c = new r;
        return e.arrayCopy(a.digits, 0, c.digits, 0, b), c
    }, e.biCompare = function (a, b) {
        if (a.isNeg != b.isNeg) return 1 - 2 * Number(a.isNeg);
        for (var c = a.digits.length - 1; c >= 0; --c) if (a.digits[c] != b.digits[c]) return a.isNeg ? 1 - 2 * Number(a.digits[c] > b.digits[c]) : 1 - 2 * Number(a.digits[c] < b.digits[c]);
        return 0
    }, e.biDivideModulo = function (a, b) {
        var c, d, f = e.biNumBits(a), g = e.biNumBits(b), h = b.isNeg;
        if (f < g) return a.isNeg ? (c = e.biCopy(k), c.isNeg = !b.isNeg, a.isNeg = !1, b.isNeg = !1, d = biSubtract(b, a), a.isNeg = !0, b.isNeg = h) : (c = new r, d = e.biCopy(a)), [c, d];
        c = new r, d = a;
        for (var i = Math.ceil(g / m) - 1, j = 0; b.digits[i] < o;) b = e.biShiftLeft(b, 1), ++j, ++g, i = Math.ceil(g / m) - 1;
        d = e.biShiftLeft(d, j), f += j;
        for (var l = Math.ceil(f / m) - 1, s = e.biMultiplyByRadixPower(b, l - i); -1 != e.biCompare(d, s);) ++c.digits[l - i], d = e.biSubtract(d, s);
        for (var t = l; t > i; --t) {
            var u = t >= d.digits.length ? 0 : d.digits[t], v = t - 1 >= d.digits.length ? 0 : d.digits[t - 1],
                w = t - 2 >= d.digits.length ? 0 : d.digits[t - 2], x = i >= b.digits.length ? 0 : b.digits[i],
                y = i - 1 >= b.digits.length ? 0 : b.digits[i - 1];
            c.digits[t - i - 1] = u == x ? q : Math.floor((u * n + v) / x);
            for (var z = c.digits[t - i - 1] * (x * n + y), A = u * p + (v * n + w); z > A;) --c.digits[t - i - 1], z = c.digits[t - i - 1] * (x * n | y), A = u * n * n + (v * n + w);
            s = e.biMultiplyByRadixPower(b, t - i - 1), d = e.biSubtract(d, e.biMultiplyDigit(s, c.digits[t - i - 1])), d.isNeg && (d = e.biAdd(d, s), --c.digits[t - i - 1])
        }
        return d = e.biShiftRight(d, j), c.isNeg = a.isNeg != h, a.isNeg && (c = h ? e.biAdd(c, k) : e.biSubtract(c, k), b = e.biShiftRight(b, j), d = e.biSubtract(b, d)), 0 == d.digits[0] && 0 == e.biHighIndex(d) && (d.isNeg = !1), [c, d]
    }, e.biDivide = function (a, b) {
        return e.biDivideModulo(a, b)[0]
    }, e.biModulo = function (a, b) {
        return e.biDivideModulo(a, b)[1]
    }, e.biMultiplyMod = function (a, b, c) {
        return e.biModulo(e.biMultiply(a, b), c)
    }, e.biPow = function (a, b) {
        for (var c = k, d = a; ;) {
            if (0 != (1 & b) && (c = e.biMultiply(c, d)), 0 == (b >>= 1)) break;
            d = e.biMultiply(d, d)
        }
        return c
    }, e.biPowMod = function (a, b, c) {
        for (var d = k, f = a, g = b; ;) {
            if (0 != (1 & g.digits[0]) && (d = e.biMultiplyMod(d, f, c)), g = e.biShiftRight(g, 1), 0 == g.digits[0] && 0 == e.biHighIndex(g)) break;
            f = e.biMultiplyMod(f, f, c)
        }
        return d
    }, a.BarrettMu = function (a) {
        this.modulus = e.biCopy(a), this.k = e.biHighIndex(this.modulus) + 1;
        var f = new r;
        f.digits[2 * this.k] = 1, this.mu = e.biDivide(f, this.modulus), this.bkplus1 = new r, this.bkplus1.digits[this.k + 1] = 1, this.modulo = b, this.multiplyMod = c, this.powMod = d
    };
    var y = function (b, c, d) {
        var f = e;
        this.e = f.biFromHex(b), this.d = f.biFromHex(c), this.m = f.biFromHex(d), this.chunkSize = 2 * f.biHighIndex(this.m), this.radix = 16, this.barrett = new a.BarrettMu(this.m)
    };
    e.getKeyPair = function (a, b, c) {
        return new y(a, b, c)
    }, void 0 === a.twoDigit && (a.twoDigit = function (a) {
        return (a < 10 ? "0" : "") + String(a)
    }), e.encryptedString = function (a, b) {
        for (var c = [], d = b.length, f = 0; f < d;) c[f] = b.charCodeAt(f), f++;
        for (; c.length % a.chunkSize != 0;) c[f++] = 0;
        var g, h, i, j = c.length, k = "";
        for (f = 0; f < j; f += a.chunkSize) {
            for (i = new r, g = 0, h = f; h < f + a.chunkSize; ++g) i.digits[g] = c[h++], i.digits[g] += c[h++] << 8;
            var l = a.barrett.powMod(i, a.e);
            k += (16 == a.radix ? e.biToHex(l) : e.biToString(l, a.radix)) + " "
        }
        return k.substring(0, k.length - 1)
    }, e.decryptedString = function (a, b) {
        var c, d, f, g = b.split(" "), h = "";
        for (c = 0; c < g.length; ++c) {
            var i;
            for (i = 16 == a.radix ? e.biFromHex(g[c]) : e.biFromString(g[c], a.radix), f = a.barrett.powMod(i, a.d), d = 0; d <= e.biHighIndex(f); ++d) h += String.fromCharCode(255 & f.digits[d], f.digits[d] >> 8)
        }
        return 0 == h.charCodeAt(h.length - 1) && (h = h.substring(0, h.length - 1)), h
    }, e.setMaxDigits(130)
}(window);
var publicKey = {};
publicKey.mu = "9f274d9e2d73d3c4df83d4e58cd6c9e57aead58463fdec0cf3734ecef398ebc1773019d9ae4a93c0a8cbed066e3021b8495ca6a8d5d9f57a676b37920f431ebc998c646cf013bd62ed53e8e099eb8582ea7bb1ef1ef7edb89ecc99c96ea2e5323c78fdd9256216bdfbfe58f4f13ae9c224bf7c4e6a17893b45a970693ccd5f0b", publicKey.po = "10001", function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK choose-card-layer-mask"></div>',
        _template: '<div class="choose-card-Layer-wrap pay-index">    <h6 class="layer-title choose-card-Layer-title">閫夋嫨閾惰鍗�</h6>    <a href="javascript:void(0);" class="choose-card-Layer-close left-close"></a>    <div class="card-list-scroll-wrap"><div class="layer-list card-list"></div></div></div>',
        _cardListItem: '<a href="javascript:void(0);" class="layer-list-item can-use card-list-item card-list-item-card crl-radio pr" data-card-li="{$i}">   <img class="layer-bank-image" src="{$cardIconUrl}" alt=""/>   <div class="layer-bank-name">{$cardName}</div></a>',
        _wrapper: null,
        _viewport: null,
        _onSelectCard: null,
        onSelectedCart: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".card-list-item-card.can-use", "click", function () {
                var b = $(this);
                $(a._wrapper).find(".card-list-item-card").removeClass("checked"), b.addClass("checked"), a.onSelectedCart.call(a, a._getCardData(b)), "JDP_ADD_NEWCARD" == a._getCardData(b).channelId && selectedPayment && (Ariel.mpingEvent("MCashierNew_ConfirmPayment", location.href, selectedPayment._maiDianPayType), selectedPayment.startPayAction(), selectedPayment.setPayButton("姝ｅ湪鏀粯")), a.hide()
            }), a._viewport.delegate(".choose-card-layer-mask,.choose-card-Layer-close", "click", function () {
                a.hide()
            })
        },
        show: function (a) {
            var b = this;
            a && b._initCardList(a), b._viewport.append(b._wrapper);
            setTimeout(function () {
                b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN")
            }, 320)
        },
        initCardList: function (a, b) {
            var c, d = this, e = a.length, f = [];
            if (d.cardList = a, d._mask = $(d._maskTemplate), d._wrapper = $(d._template), e > 0) {
                for (var g = 0; g < e; g++) {
                    c = d._cardListItem;
                    var h = '<p class="bank-name-title">' + a[g].channelName;
                    a[g].preferentiaNum && (h += '<span class="bank-name-preferential">' + a[g].preferentiaNum + "</span>"), h += "</p>", a[g].tip && (h = h.replace("bank-name-title", "bank-name-title has-tip"), h += '<p class="bank-name-tip">' + a[g].tip + "</p>"), 0 == a[g].status ? (c = c.replace("can-use", "cant-use"), c = c.replace("{$cardName}", h)) : c = c.replace("{$cardName}", h), c = c.replace("{$cardIconUrl}", a[g].logo), c = c.replace("{$i}", g), b == a[g].channelId && (c = c.replace("crl-radio", "crl-radio checked")), "JDP_ADD_NEWCARD" == a[g].channelId && (c = c.replace("crl-radio", "crl-radio crl-arrow")), f.push(c)
                }
                d._wrapper.find(".card-list").html(f.join("")), d._viewport.append(d._wrapper), d.onSelectedCart.call(d, d._getCardData(d._wrapper.find(".crl-radio.checked")))
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 300)
        },
        _getCardData: function (a) {
            var b = parseInt(a.attr("data-card-li"));
            return this.cardList[b]
        }
    }), Common.Components.chooseCardLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK choose-card-layer-mask"  aria-hidden="true"></div>',
        _template: '<div class="choose-card-Layer-wrap pay-index js-jd-payment" aria-hidden="true">    <h6 class="layer-title choose-card-Layer-title">浠樻鏂瑰紡</h6>    <a href="javascript:void(0);" class="choose-card-Layer-close right-close" role="button" aria-label="鍏抽棴璋堢獥"></a>    <div class="card-list-scroll-wrap"><div class="layer-list card-list"></div></div></div>',
        _cardListItem: '<a href="javascript:void(0);" class="layer-list-item can-use {$tips} card-list-item js-card-list-item-card crl-radio pr" data-card-li="{$i}"  role="option" {$aria-selected}>   <img class="layer-bank-image" src="{$cardIconUrl}" alt=""/>   <div class="layer-bank-name">{$cardName}</div></a>',
        _wrapper: null,
        _viewport: null,
        _onSelectCard: null,
        onSelectedCart: null,
        isGraduallyPayMode: !1,
        setIsGraduallyPayMode: function (a) {
            this.isGraduallyPayMode = !!a
        },
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".js-card-list-item-card.can-use", "click", function () {
                var b = $(this);
                $(a._wrapper).find(".js-card-list-item-card").removeClass("checked"), b.addClass("checked");
                var c = a._getCardData(b);
                c._initiativeSelected = !0, a.onSelectedCart.call(a, c), a.hide()
            }), a._viewport.delegate(".choose-card-layer-mask,.choose-card-Layer-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_SecToastClose"), a.hide()
            })
        },
        show: function (a) {
            var b = this;
            a && b._initCardList(a), b._viewport.append(b._wrapper);
            setTimeout(function () {
                b._viewport.append(b._mask), b._mask.attr({"aria-hidden": "false"}), b._wrapper.attr({"aria-hidden": "false"}), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN")
            }, 320)
        },
        updateCardLis: function (a) {
            var b = this;
            b._wrapper.find(".crl-radio").removeClass("checked");
            for (var c = 0; c < b.cardList.length; c++) if (a && b.cardList[c].uniqueChannelId === a) {
                checkedIndex = c, b._wrapper.find(".crl-radio").eq(c).addClass("checked");
                break
            }
        },
        initCardList: function (a, b) {
            var c = this;
            c._viewport.find(".js-jd-payment").remove();
            var d, e = a.length, f = [];
            c.cardList = a, c._mask = $(c._maskTemplate), c._wrapper = $(c._template);
            var g = [];
            if (e > 0) {
                for (var h = 0; h < e; h++) {
                    d = c._cardListItem;
                    var i = '<p class="bank-name-title">' + a[h].channelName;
                    switch (a[h].recommand && a[h].recommandIcon && (i += '<span class="cardlayer-recommend-icon"></span>', g.push(a[h].recommandIcon)), 5 == a[h].status && (i += '<span class="bank-name-statusDesc">' + a[h].statusDesc + "</span>"), 3 == a[h].status && this.isGraduallyPayMode && a[h].statusDesc && (i += '<span class="bank-name-statusDesc">' + a[h].statusDesc + "</span>"), a[h].preferentiaNum && 5 !== a[h].status && (i += '<span class="bank-name-preferential">' + a[h].preferentiaNum + "</span>"), i += "</p>", a[h].tip && (i = i.replace("bank-name-title", "bank-name-title has-tip"), i += '<p class="bank-name-tip">' + a[h].tip + "</p>", d = d.replace("{$tips}", "hasTips")), a[h].status) {
                        case 0:
                            d = d.replace("can-use", "cant-see"), d = d.replace("{$cardName}", i);
                            break;
                        case 3:
                            d = d.replace("can-use", "cant-use"), d = d.replace("{$cardName}", i);
                            break;
                        case 7:
                            d = d.replace("can-use", "cant-see"), d = d.replace("{$cardName}", i);
                            break;
                        default:
                            d = d.replace("{$cardName}", i)
                    }
                    d = d.replace("{$cardIconUrl}", a[h].logo), d = d.replace("{$i}", h), b == a[h].uniqueChannelId ? (d = d.replace("crl-radio", "crl-radio checked"), d = d.replace("{$aria-selected}", 'aria-selected="true"')) : d = d.replace("{$aria-selected}", 'aria-selected="false"'), "JDP_ADD_NEWCARD" == a[h].channelId && (d = d.replace("crl-radio", "crl-radio crl-arrow")), f.push(d)
                }
                if (c._wrapper.find(".card-list").html(f.join("")), g && g.length > 0) for (var h = 0; h < g.length; h++) c._wrapper.find(".cardlayer-recommend-icon").eq(h).css("background-image", "url(" + g[h] + ")");
                c._viewport.append(c._wrapper)
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 300)
        },
        _getCardData: function (a) {
            var b = parseInt(a.attr("data-card-li"));
            return this.cardList[b]
        }
    }), Common.Components.chooseOtherJDLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK"></div>',
        _COUPON_LIST_ITEM_TEMPLATE: ' <li class ="selectCoupon-list-item COMM_TOP_1PX_BORDER {$couponInfoNum}" >    <a href="javascript:void(0);" class="selectCoupon-list-item-link COM_CRL_RADIO com-coupon" data-li="{$li}">    <span class="coupon-icon">{$couponTypeDesc}</span>     <p class="coupon-title">{$couponInfo}</p>      <p class="coupon-validity">鏈夋晥鏈�: {$startData}-{$deadline}</p>      <p class="coupon-descript">{$couponDesc}</p></a></li>',
        _COUPON_LAYER_TEMPLATE: '<div class="selectCoupon-panel com-layer COMM_COUPON_LAYER">   <h2 class="selectCoupon-title">      <span class="select-panel-close"></span>          閫夋嫨浼樻儬鍒�<span class="select-panel-cancel">鏆備笉浣跨敤</span>   </h2>   <div class="selectCoupon-list-wrap">     <ul class="selectCoupon-list available-coupon">     </ul>     <h3 class="unavailable-coupon-title">涓嶅彲鐢ㄤ紭鎯�</h3>     <ul class="selectCoupon-list unavailable-coupon">     </ul>   </div></div>',
        mask: null,
        _wrapper: null,
        _viewport: null,
        onSelectedCoupon: null,
        _canUseCouponData: {},
        _defultCoupon: null,
        _maiDianType: "MCashierNew_Coupon",
        _init: function (a) {
            var b = this;
            b._mask = $(b._maskTemplate), b._wrapper = $(b._COUPON_LAYER_TEMPLATE), b._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".available-coupon .com-coupon", "click", function () {
                if (couponType = $(this), couponType.hasClass("checked")) return void a.hide();
                Ariel.mpingEvent("MCashierNew_CouponChose", location.href, "1"), $(this).hasClass("selectCoupon-list-item-link-common") || $(".blank-note-stage-layer1").find(".blank-note-stage-list").parents(".pay-list-item").addClass("selected"), $(a._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked"), couponType.addClass("checked"), a.onSelectedCoupon.call(a, a.getCouponData(couponType)), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponClose", location.href), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-cancel-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponNotUse", location.href), a.hide()
            }), a._viewport.delegate(".com-layer .select-panel-cancel-refresh", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponNotUse", location.href), $(a._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked"), a.onSelectedCoupon.call(a, {
                    promotionDesc: "鏆備笉浣跨敤",
                    couponInfo: "鏆備笉浣跨敤",
                    plans: [1, 3, 6, 12, 24]
                }), a.hide()
            })
        },
        show: function (a, b) {
            var c = this;
            c._viewport.append(c._wrapper), c._defultCoupon = b || null, a && c.initCouponLayer(a, b);
            var d = setTimeout(function () {
                Ariel.Components.Loading.hide(), c._viewport.append(c._mask), c._mask.show(), c._wrapper.addClass("COM_LAYER_IN"), clearTimeout(d)
            }, 320)
        },
        initCouponLayer: function (a, b) {
            var c, d = this, e = [];
            d._defultCoupon = b || null, d._mask = $(d._maskTemplate), d._wrapper = $(d._COUPON_LAYER_TEMPLATE), a.couponAndCutOffs ? a.couponAndCutOffs && a.couponAndCutOffs.length > 0 ? (d._maiDianType = "MCashierNew_Coupon", d._canUseCouponData = a.couponAndCutOffs, e = d.initCommonCouponList(a.couponAndCutOffs), d._wrapper.find(".selectCoupon-list.available-coupon").html(e.join("")), c = d._wrapper.find(".selectCoupon-list.available-coupon li").eq(0).find(".selectCoupon-list-item-link"), c.addClass("checked"), d._wrapper.find(".unavailable-coupon-title").remove(), d._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-refresh")) : d._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-close") : d._wrapper.find(".selectCoupon-list-wrap").html(""), c && c.length > 0 ? d.onSelectedCoupon.call(d, d.getCouponData(c)) : d.onSelectedCoupon.call(d)
        },
        initCommonCouponList: function (a) {
            for (var b, c = this, d = a.length, e = [], f = 0; f < d; f++) {
                b = c._COUPON_LIST_ITEM_TEMPLATE, b = b.replace("{$couponInfo}", a[f].promotionDesc), b = b.replace("{$couponTypeDesc}", a[f].couponTypeDesc), b = b.replace("selectCoupon-list-item-link COM_CRL_RADIO", "selectCoupon-list-item-link COM_CRL_RADIO selectCoupon-list-item-link-common");
                var g = 1;
                a[f].beginDate && a[f].endDate ? (b = b.replace("{$startData}", a[f].beginDate), b = b.replace("{$deadline}", a[f].endDate), g++) : b = b.replace('<p class="coupon-validity">鏈夋晥鏈�: {$startData}-{$deadline}</p>', ""), b = b.replace("{$li}", f), a[f].useDesc ? (g++, b = b.replace("{$couponDesc}", a[f].useDesc)) : b = b.replace('<p class="coupon-descript">{$couponDesc}</p>', ""), b = b.replace("{$couponInfoNum}", "couponInfoNum" + g), e.push(b)
            }
            return e
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 500)
        },
        getCouponData: function (a) {
            return this._canUseCouponData[parseInt(a.attr("data-li"))]
        }
    }), Common.Components.chooseCouponLayer = new c;
    var d = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(d.prototype, {
        _maskTemplate: '<div class="COMM_MASK"></div>',
        _COUPON_LIST_ITEM_TEMPLATE: ' <li class ="selectCoupon-list-item COMM_TOP_1PX_BORDER {$couponInfoNum}" >    <a href="javascript:void(0);" class="selectCoupon-list-item-link COM_CRL_RADIO baitiao-coupon" data-li="{$li}">    <span class="coupon-icon">{$couponTypeDesc}</span>    <p class="coupon-title">{$couponInfo}</p>    <p class="coupon-validity">鏈夋晥鏈�: {$startData}-{$deadline}</p>    <p class="coupon-descript">{$couponDesc}</p></a></li>',
        _COUPON_LAYER_TEMPLATE: '<div class="selectCoupon-panel baitiao-layer COMM_COUPON_LAYER">   <h2 class="selectCoupon-title">      <span class="select-panel-close"></span>          閫夋嫨浼樻儬鍒�<span class="select-panel-cancel">鏆備笉浣跨敤</span>   </h2>   <div class="selectCoupon-list-wrap">     <ul class="selectCoupon-list available-coupon">     </ul>     <h3 class="unavailable-coupon-title">涓嶅彲鐢ㄤ紭鎯�</h3>     <ul class="selectCoupon-list unavailable-coupon">     </ul>   </div></div>',
        mask: null,
        _wrapper: null,
        _viewport: null,
        onSelectedCoupon: null,
        _canUseCouponData: {},
        _maiDianType: "MCashierNew_CreditCoupon",
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._COUPON_LAYER_TEMPLATE), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".available-coupon .baitiao-coupon", "click", function () {
                var b = $(this);
                if (b.hasClass("checked")) return void a.hide();
                Ariel.mpingEvent("MCashierNew_CouponChose", location.href, "1"), a.setCouponChoosed(b.attr("data-li")), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponClose", location.href), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-cancel-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponNotUse", location.href), a.hide()
            }), a._viewport.delegate(".baitiao-layer .select-panel-cancel-refresh", "click", function () {
                a.setCouponChoosed(-1, "鏆備笉浣跨敤"), Ariel.mpingEvent("MCashierNew_CouponNotUse", location.href), a.hide()
            })
        },
        show: function (a) {
            var b = this;
            a && b.initCouponLayer(a), b._viewport.append(b._wrapper);
            var c = setTimeout(function () {
                Ariel.Components.Loading.hide(), b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN"), clearTimeout(c)
            }, 320)
        },
        initCouponLayer: function (a) {
            var b = this, c = [];
            b._mask = $(b._maskTemplate), b._wrapper = $(b._COUPON_LAYER_TEMPLATE), a.canUseCouponList || a.cantUseCouponList ? (a.canUseCouponList && a.canUseCouponList.length > 0 ? (b._canUseCouponData = a.canUseCouponList, c = b.initBlanNoteCouponList(a.canUseCouponList), b._wrapper.find(".selectCoupon-list.available-coupon").html(c.join("")), b._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-refresh")) : b._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-close"), a.cantUseCouponList && a.cantUseCouponList.length > 0 ? (c = b.initBlanNoteCouponList(a.cantUseCouponList), b._wrapper.find(".selectCoupon-list.unavailable-coupon").html(c.join(""))) : b._wrapper.find(".unavailable-coupon-title").remove()) : b._wrapper.find(".unavailable-coupon-title").remove()
        },
        initBlanNoteCouponList: function (a) {
            for (var b, c = this, d = a.length, e = [], f = 0; f < d; f++) b = c._COUPON_LIST_ITEM_TEMPLATE, b = b.replace("{$couponTypeDesc}", a[f].couponTypeDesc), b = b.replace("{$couponInfo}", a[f].couponInfo), b = b.replace("{$couponDesc}", a[f].couponDesc), a[f].startTime && a[f].finishTime ? (b = b.replace("{$startData}", a[f].startTime), b = b.replace("{$deadline}", a[f].finishTime)) : b = b.replace('<p class="coupon-validity">鏈夋晥鏈�: {$startData}-{$deadline}</p>', ""), b = b.replace("{$li}", f), b = b.replace("{$couponType}", a[f].couponType), e.push(b);
            return e
        },
        chooseToCoupon: function (a, b) {
            var c = this, d = c._canUseCouponData || [], e = d.length;
            if (0 !== e && a) {
                if (a.couponId || a.activityId) for (var f = 0; f < e; f++) if (c._matchCoupon(a, d[f])) return void c.setCouponChoosed(f, void 0, b);
                c.setCouponChoosed(-1)
            } else c.setCouponChoosed(-1)
        },
        _matchCoupon: function (a, b) {
            return !!a.couponId && (a.couponId.indexOf("nothing") < 0 ? a.couponId == b.couponId : !(!a.activityId || a.activityId != b.activityId))
        },
        setCouponChoosed: function (a, b, c) {
            if (void 0 !== a) {
                var d = this, e = $(d._wrapper).find(".selectCoupon-list-item-link.checked"),
                    f = parseInt(e.attr("data-li")), g = d._canUseCouponData[f],
                    h = g && ("MX" == g.couponType ? "MianXi" : "BenJin");
                if (1 == e.length && e.attr("data-li") == a || 0 == e.length && -1 == a && $(".blank-note-pay .COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length > 0 && !b) return void d.onSelectedCoupon.call(d, a);
                if ($(d._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked"), -1 === a) d.onSelectedCoupon.call(d, -1, b); else {
                    var i = d._wrapper.find(".available-coupon .selectCoupon-list-item-link").eq(a);
                    i.addClass("checked");
                    var j = i.attr("data-li"), k = d._canUseCouponData[j],
                        l = k && ("MX" == k.couponType ? "MianXi" : "BenJin"), m = d._viewport.find(".judge-checked"),
                        n = parseInt(m.attr("data-li")), o = ["1", "3", "6", "12", "24"], p = o[n],
                        q = d._wrapper.hasClass("COM_LAYER_IN"), r = g && g.plans.indexOf(p) >= 0,
                        s = k && k.plans.indexOf(p) >= 0, t = r && s;
                    q && t && h !== l && Ariel.Components.Pop.show("閲戦浼樻儬涓庢湇鍔¤垂浼樻儬涓嶅彲鍙犲姞鍝�"), !c && d.onSelectedCoupon.call(d, a)
                }
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                $(".COMM_MASK").remove(), a._wrapper.remove(), clearTimeout(b)
            }, 500)
        },
        getCouponData: function (a) {
            return this._canUseCouponData[parseInt(a.attr("data-li"))]
        }
    }), Common.Components.chooseBTCouponLayer = new d
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(c.prototype, {
        _BLANK_NOTE_LIST_ITEM_TEMPLATE: '<li class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM canChooseStage" data-li={$i}> <div class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM_INNER_WRAP">{$discountLogo}     <span class="COMM_BLANK_NOTE_STAGE_AMOUNT">楼{$repayment} 脳 {$stage}鏈�</span>     <p class="COMM_BLANK_NOTE_STAGE_FEE">鍚湇鍔¤垂楼{$plan-fee}/鏈�  璐圭巼 {$rate}%/鏈�</p> </div></li>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _choosedCouponData: {},
        onSelectedStage: null,
        _init: function (a) {
            var b = this;
            b._mask = $(b._maskTemplate), b._wrapper = $(".blank-note-stage-layer1"), b._viewport = $("#viewport")
        },
        _initEvent: function () {
            this._viewport.delegate(".COMM_BLANK_NOTE_LAYER_MASK", "click", function () {
                $(".COMM_BLANK_NOTE_COUPON_LAYER").removeClass("COM_LAYER_IN"), $(".COMM_BLANK_NOTE_LAYER_MASK").hide()
            })
        },
        updataStageLayer: function (a, c) {
            var d = this;
            Ariel.Components.Loading.show(), d._choosedCouponData = a;
            var e = $(".blank-note-stage-layer1").find(".judge-checked"), f = parseInt(e.attr("data-li")),
                g = ["1", "3", "6", "12", "24"], h = g[f], i = a.plans.indexOf(h) >= 0 ? h : a.defaultPlan;
            Common.Net.ajax({
                url: "jdBaiTiaoCalculateRate.action",
                type: "POST",
                data: {
                    couponCode: a.couponId || "",
                    activityId: a.activityId || "",
                    couponType: a.couponType || "",
                    plans: $.isArray(a.plans) ? a.plans.join(",") : a.plans || "",
                    channelType: a.channelType || "",
                    planRate: a.planRate || "",
                    selectedPlan: i || "1"
                },
                success: function (e) {
                    if (0 == e.code) $(".blank-note-stage-layer1").hasClass("load-failed") && $(".blank-note-stage-layer1").removeClass("load-failed"), d._updataBlankNoteStageList(e.payBillList, e.marketingDisplayMode); else {
                        if (b.Components.Pop.show(e.info ? e.info : "鑾峰彇鐧芥潯淇℃伅澶辫触锛岃閲嶈瘯"), Ariel.Components.Loading.hide(), d._wrapper.hasClass("load-failed")) return;
                        0 == $(".blank-note-stage-layer1").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length && d.loadFailedHandle()
                    }
                    c && (e.isMarketRecommendModify && 1 == e.isMarketRecommendModify ? c.call(d, e.payBillList, e.baiTiaoCoupons, a, e.recommendCoupons, e.marketingDisplayMode) : c.call(d, e.payBillList, void 0, void 0, e.recommendCoupons, e.marketingDisplayMode)), Ariel.Components.Loading.hide()
                }
            })
        },
        _updataBlankNoteStageList: function (a, b) {
            var c, d, e = [1, 3, 6, 12, 24], f = this, g = a.length, h = [];
            f._choosedCouponData.plans;
            if ($(".blank-note-stage-layer1").hasClass("total-less-10") && (e = [1]), !(g <= 0)) {
                "2" == b && $(".blank-note-stage-layer1").addClass("marketing-display-style");
                for (var i = 0; i < g; i++) {
                    if (c = a[i].plan, d = f._BLANK_NOTE_LIST_ITEM_TEMPLATE, 0 == i && (d = d.replace("楼{$repayment} 脳 {$stage}鏈�", "涓嶅垎鏈�"), d = d.replace("鍚湇鍔¤垂楼{$plan-fee}/鏈�  璐圭巼 {$rate}%/鏈�", "0鏈嶅姟璐�")), f._inArray(c, e) || (d = d.replace("canChooseStage", "cantChooseStage")), d = d.replace("{$stage}", c), "2" == b) {
                        d = d.replace("鍚湇鍔¤垂楼{$plan-fee}/鏈�  璐圭巼 {$rate}%/鏈�", "鍚湇鍔¤垂锟�" + a[i].planFee + "/鏈�");
                        var j = a[i].recommandCoupon;
                        j && j.discountLogoType ? "mianxi" == j.discountLogoType ? d = d.replace("{$discountLogo}", '<div class="COMM_BLANK_NOTE_STAGE_LOGO mianxi">' + j.discountLogoText + "</div>") : "zhekou" == j.discountLogoType && (d = d.replace("{$discountLogo}", '<div class="COMM_BLANK_NOTE_STAGE_LOGO zhekou">' + j.discountLogoText + "</div>")) : d = d.replace("{$discountLogo}", "")
                    } else d = d.replace("{$discountLogo}", ""), d = d.replace("{$plan-fee}", a[i].planFee), d = d.replace("{$rate}", a[i].rate);
                    d = d.replace("{$repayment}", a[i].laterPay), d = d.replace("{$total}", a[i].total), d = d.replace("{$i}", i), h.push(d)
                }
                $(".blank-note-stage-layer1").find(".blank-note-stage-list").html(h.join("")), Ariel.Components.Loading.hide()
            }
        },
        loadFailedHandle: function () {
            $(".blank-note-stage-layer1").addClass("load-failed")
        },
        _inArray: function (a, b) {
            var c = b, d = parseInt(a);
            $.isArray(b) || (c = c.replace("[", ""), c = c.replace("]", ""), c = c.split(","));
            for (var e = 0; e < c.length; e++) if (parseInt(c[e]) == d) return !0;
            return !1
        }
    }), a.Common.Components.BlankNoteLayers = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(c.prototype, {
        _BLANK_NOTE_LIST_ITEM_TEMPLATE: '<li class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM canChooseStage" data-li={$i}>     <div class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM_INNER_WRAP"><span class="COMM_BLANK_NOTE_STAGE_AMOUNT">楼{$repayment} 脳 {$stage}鏈�</span>     <p class="COMM_BLANK_NOTE_STAGE_FEE">鍚湇鍔¤垂楼{$plan-fee}/鏈�  璐圭巼 {$rate}%/鏈�</p></div></li>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _choosedCouponData: {},
        onSelectedStage: null,
        _init: function (a) {
            var b = this;
            b._mask = $(b._maskTemplate), b._wrapper = $(".blank-note-stage-layer2"), b._viewport = $("#viewport")
        },
        _initEvent: function () {
            this._viewport.delegate(".COMM_BLANK_NOTE_LAYER_MASK", "click", function () {
                $(".COMM_BLANK_NOTE_COUPON_LAYER").removeClass("COM_LAYER_IN"), $(".COMM_BLANK_NOTE_LAYER_MASK").hide()
            })
        },
        updataStageLayer: function (a, b) {
            var c = this;
            Ariel.Components.Loading.show(), c._choosedCouponData = a, Common.Net.ajax({
                url: "jdBaiTiaoCalculateRate.action",
                type: "POST",
                data: {
                    couponCode: a.couponId || "",
                    activityId: a.activityId || "",
                    couponType: a.couponType || "",
                    plans: $.isArray(a.plans) ? a.plans.join(",") : a.plans || "",
                    channelType: a.channelType || "",
                    planRate: a.planRate || ""
                },
                success: function (a) {
                    0 == a.code ? ($(".blank-note-stage-layer2").hasClass("load-failed") && ($(".blank-note-stage-layer2").removeClass("load-failed"), $(".blank-note-stage-layer2-switch").html('<span class="show-or-hide-title">鏌ョ湅鍏ㄩ儴</span><span class="show-or-hide"></span>')), c._updataBlankNoteStageList(a.payBillList)) : ($(".blank-note-stage-layer2").parents(".pay-list-item").css("border-bottom", "0"), Ariel.Components.Loading.hide(), $(".blank-note-stage-layer2").hide(), $(".blank-note-stage-layer2-switch").hide()), b && b.call(c, a.payBillList), Ariel.Components.Loading.hide()
                }
            })
        },
        _updataBlankNoteStageList: function (a) {
            var b, c, d, e = this, f = a.length, g = [],
                h = $(".selected-stage em").attr("data-plan") || e._choosedCouponData.defaultPlan || 1,
                i = e._choosedCouponData.allPlans;
            if ($(".blank-note-stage-layer2").hasClass("total-less-10") && (i = [1]), !(f <= 0)) {
                d = e._choosedCouponData.defaultPlan ? e._choosedCouponData.defaultPlan : i[i.length - 1], e._inArray(h, i) || (h = d);
                for (var j = 0; j < f; j++) b = a[j].plan, c = e._BLANK_NOTE_LIST_ITEM_TEMPLATE, 0 == j && (c = c.replace("楼{$repayment} 脳 {$stage}鏈�", "涓嶅垎鏈�"), c = c.replace("鍚湇鍔¤垂楼{$plan-fee}/鏈�  璐圭巼 {$rate}%/鏈�", "0鏈嶅姟璐�")), b == h ? (c = c.replace("canChooseStage", "canChooseStage checked"), Ariel.mpingEvent("MCashierNew_Instalment", location.href, h + "_0")) : e._inArray(b, i) || (c = c.replace("canChooseStage", "cantChooseStage")),
                    c = c.replace("{$stage}", b), c = c.replace("{$plan-fee}", a[j].planFee), c = c.replace("{$repayment}", a[j].laterPay), c = c.replace("{$rate}", a[j].rate), c = c.replace("{$total}", a[j].total), c = c.replace("{$i}", j), g.push(c);
                $(".blank-note-stage-layer2").find(".blank-note-stage-list").html(g.join("")), $(".blank-note-stage-layer2").find(".blank-note-stage-list").parents(".pay-list-item").hasClass("selected") || $(".blank-note-stage-layer2").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").removeClass("checked"), Ariel.Components.Loading.hide()
            }
        },
        loadFailedHandle: function () {
            $(".blank-note-stage-layer2").addClass("load-failed")
        },
        _inArray: function (a, b) {
            var c = b, d = parseInt(a);
            $.isArray(b) || (c = c.replace("[", ""), c = c.replace("]", ""), c = c.split(","));
            for (var e = 0; e < c.length; e++) if (parseInt(c[e]) == d) return !0;
            return !1
        }
    }), a.Common.Components.BaiTiaoQuickLayers = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK choose-card-layer-mask"></div>',
        _template: '<div class="choose-card-Layer-wrap pay-index choose-credit-Layer-wrap">    <h6 class="layer-title choose-card-Layer-title">閫夋嫨鍒嗘湡淇＄敤鍗�</h6>    <div class="choose-card-Layer-notice">鐩墠浠呮敮鎸佷互涓嬮摱琛岃繘琛屼俊鐢ㄥ崱鍒嗘湡浠樻</div>    <a href="javascript:void(0);" class="choose-card-Layer-close right-close"></a>    <div class="card-list-scroll-wrap credit-card"><div class="layer-list card-list"></div></div></div>',
        _cardListItem: '<a href="javascript:void(0);" class="layer-list-item can-use card-list-item card-list-item-credit crl-radio pr" data-card-li="{$i}">   <img class="layer-bank-image" src="{$cardIconUrl}" alt=""/>   <div class="layer-bank-name">{$cardName}</div></a>',
        _wrapper: null,
        _viewport: null,
        _onSelectCard: null,
        onSelectedCart: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".card-list-item-credit.can-use", "click", function () {
                var b = $(this);
                $(a._wrapper).find(".card-list-item-credit").removeClass("checked"), b.addClass("checked"), a.onSelectedCart.call(a, a._getCardData(b)), a.hide()
            }), a._viewport.delegate(".choose-card-layer-mask,.choose-card-Layer-close", "click", function () {
                a.hide()
            })
        },
        show: function (a) {
            var b = this;
            a && b.initCardList(a), b._viewport.append(b._wrapper);
            setTimeout(function () {
                b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN")
            }, 320)
        },
        initCardList: function (a, b) {
            var c, d = this, e = a.length, f = [];
            if (d.cardList = a, d._mask = $(d._maskTemplate), d._wrapper = $(d._template), e > 0) {
                for (var g = 0; g < e; g++) {
                    c = d._cardListItem;
                    var h = '<p class="bank-name-title">' + a[g].bankName;
                    a[g].preferentiaNum && (h += '<span class="bank-name-preferential">' + a[g].preferentiaNum + "</span>"), h += "</p>", a[g].tip ? (h = h.replace("bank-name-title", "bank-name-title has-tip"), h += '<p class="bank-name-tip">' + a[g].tip + "</p>") : h += '<p class="bank-name-tip"></p>', 0 == a[g].status ? (c = c.replace("can-use", "cant-use"), c = c.replace("{$cardName}", h)) : c = c.replace("{$cardName}", h), a[g].cardCanUse ? c = c.replace("{$cardName}", h) : (c = c.replace("can-use", "cant-use"), c = c.replace("{$cardName}", h)), c = c.replace("{$cardIconUrl}", a[g].logo), c = c.replace("{$i}", g), b == a[g].uniqueChannelId && (c = c.replace("crl-radio", "crl-radio checked")), f.push(c)
                }
                d._wrapper.find(".card-list").html(f.join("")), d._viewport.append(d._wrapper)
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 300)
        },
        _getCardData: function (a) {
            var b = parseInt(a.attr("data-card-li"));
            return this.cardList[b]
        }
    }), Common.Components.chooseCreditLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK"></div>',
        _COUPON_LIST_ITEM_TEMPLATE: ' <li class ="selectCoupon-list-item COMM_TOP_1PX_BORDER {$couponInfoNum}" >    <a href="javascript:void(0);" class="selectCoupon-list-item-link credit-coupon COM_CRL_RADIO" data-li="{$li}">    <span class="coupon-icon">{$couponTypeDesc}</span>     <p class="coupon-title">{$couponInfo}</p>      <p class="coupon-validity">{$time}</p>      <p class="coupon-descript">{$couponDesc}</p></a></li>',
        _COUPON_LAYER_TEMPLATE: '<div class="selectCoupon-panel credit-layer COMM_COUPON_LAYER">   <h2 class="selectCoupon-title">      <span class="select-panel-close"></span>          閫夋嫨浼樻儬鍒�<span class="select-panel-cancel">鏆備笉浣跨敤</span>   </h2>   <div class="selectCoupon-list-wrap">     <ul class="selectCoupon-list available-coupon">     </ul>   </div></div>',
        mask: null,
        _wrapper: null,
        _viewport: null,
        onSelectedCoupon: null,
        _couponData: {},
        _defultCoupon: null,
        _maiDianType: "",
        _init: function (a) {
            var b = this;
            b._mask = $(b._maskTemplate), b._wrapper = $(b._COUPON_LAYER_TEMPLATE), b._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".available-coupon .credit-coupon", "click", function () {
                var b = $(this);
                if (b.hasClass("checked")) return void a.hide();
                a.onSelectedCoupon.call(a, a.getCouponData(b)), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-close", "click", function () {
                a.hide()
            }), a._viewport.delegate(".credit-layer .select-panel-cancel-refresh", "click", function () {
                $(a._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked"), a.onSelectedCoupon.call(a, !1), a.hide()
            })
        },
        show: function (a) {
            var b = this;
            b._viewport.append(b._wrapper);
            var c = setTimeout(function () {
                Ariel.Components.Loading.hide(), b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN"), clearTimeout(c)
            }, 320)
        },
        initCouponLayer: function (a, b) {
            var c, d, e = this, f = [];
            e._defultCoupon = b || null, e._mask = $(e._maskTemplate), e._wrapper = $(e._COUPON_LAYER_TEMPLATE), a && a.length > 0 ? (e._couponData = a, f = e.initCommonCouponList(a), e._wrapper.find(".selectCoupon-list.available-coupon").html(f.join("")), d = e.chooseDefaultCoupon(b), c = e._wrapper.find(".selectCoupon-list.available-coupon li").eq(d).find(".selectCoupon-list-item-link"), c.addClass("checked"), e._wrapper.find(".unavailable-coupon-title").remove(), e._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-refresh")) : e._wrapper.find(".selectCoupon-list-wrap").html(""), e.show()
        },
        initCommonCouponList: function (a) {
            for (var b, c = this, d = a.length, e = [], f = 0; f < d; f++) {
                if (b = c._COUPON_LIST_ITEM_TEMPLATE, b = b.replace("{$couponInfo}", a[f].couponInfo), b = b.replace("{$couponTypeDesc}", a[f].couponTypeDesc), a[f].startTime && a[f].finishTime) {
                    var g = "鏈夋晥鏈�: " + a[f].startTime + "-" + a[f].finishTime;
                    b = b.replace("{$time}", g)
                } else b = b.replace("{$time}", "");
                b = a[f].couponDesc ? b.replace("{$couponDesc}", a[f].couponDesc) : b.replace('<p class="coupon-descript">{$couponDesc}</p>', ""), b = b.replace("{$li}", f), e.push(b)
            }
            return e
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 500)
        },
        getCouponData: function (a) {
            return this._couponData[parseInt(a.attr("data-li"))]
        },
        updateCouponLists: function (a, b) {
            var c = this;
            Ariel.Components.Loading.show(), c._defultCoupon = b || null, c._choosedCreditData = a, Common.Net.ajax({
                url: "jdPayCreditCardCoupons.action",
                type: "POST",
                data: {bankCode: a.bankCode || "", cardId: a.bankId || "", isNewCard: a.isNewCard},
                success: function (a) {
                    0 == a.code ? c.initCouponLayer(a.creditCardCoupons, b) : Ariel.Components.Pop.show(a.message ? a.message : "淇＄敤鍗′紭鎯犲埜淇℃伅鑾峰彇澶辫触锛岃閲嶈瘯"), Ariel.Components.Loading.hide()
                }
            })
        },
        chooseDefaultCoupon: function (a) {
            var b = this, c = b._couponData || [], d = c.length;
            if (0 === d || !a) return !1;
            if (a.couponId || a.activityId) for (var e = 0; e < d; e++) if (b._matchCoupon(a, c[e])) return e
        },
        _matchCoupon: function (a, b) {
            return !!a.couponId && (a.couponId.indexOf("nothing") < 0 ? a.couponId == b.couponId : !(!a.activityId || a.activityId != b.activityId))
        }
    }), Common.Components.creditCouponListsLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(c.prototype, {
        _BLANK_NOTE_LIST_ITEM_TEMPLATE: '<li class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM canChooseStage" data-li={$i}>     <div class="COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM_INNER_WRAP"><span class="COMM_BLANK_NOTE_STAGE_AMOUNT">楼{$repayment} 脳 {$stage}鏈�</span>     <p class="COMM_BLANK_NOTE_STAGE_FEE">{$plan-fee}</p></div></li>',
        _mask: null,
        _wrapper: null,
        _viewport: null,
        _choosedCreditData: {},
        _choosedCouponData: {},
        onSelectedStage: null,
        _init: function (a) {
            var b = this;
            b._mask = $(b._maskTemplate), b._wrapper = $(".blank-note-stage-layer3"), b._viewport = $("#viewport")
        },
        _initEvent: function () {
            this._viewport.delegate(".COMM_BLANK_NOTE_LAYER_MASK", "click", function () {
                $(".COMM_BLANK_NOTE_COUPON_LAYER").removeClass("COM_LAYER_IN"), $(".COMM_BLANK_NOTE_LAYER_MASK").hide()
            })
        },
        updataStageLayerBecaseOfCard: function (a, c) {
            var d = this;
            Ariel.Components.Loading.show(), d._choosedCreditData = a, d.onSelectedStage = a.recommendPlanId, Common.Net.ajax({
                url: "jDPayCreditCardChannel.action",
                type: "POST",
                data: {
                    bankCode: a.bankCode || "",
                    planNum: a.recommendPlanId || "",
                    cardId: a.bankId || "",
                    requireUUID: a.requireUUID || "",
                    isNewCard: a.isNewCard,
                    channelId: a.channelId || ""
                },
                success: function (a) {
                    if (0 == a.code) $(".blank-note-stage-layer3").show(), $(".card-title-display-container").show(), $(".blank-note-stage-layer3").hasClass("load-failed") && ($(".blank-note-stage-layer3").removeClass("load-failed"), $(".blank-note-stage-layer3-switch").html('<span class="show-or-hide-title">鏌ョ湅鍏ㄩ儴</span><span class="show-or-hide"></span>')), a.bankCardPayChannel && a.bankCardPayChannel.planList && d._updataBlankNoteStageList(a.bankCardPayChannel.planList); else {
                        if ($(".blank-note-stage-layer3").hide(), $(".card-title-display-container").hide(), b.Components.Pop.show(a.info ? a.info : "鑾峰彇鍒嗘湡淇℃伅澶辫触锛岃閲嶈瘯"), Ariel.Components.Loading.hide(), d._wrapper.hasClass("load-failed")) return;
                        0 == $(".blank-note-stage-layer3").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length && d.loadFailedHandle()
                    }
                    c && c.call(d, a.bankCardPayChannel), Ariel.Components.Loading.hide()
                }
            })
        },
        updataStageLayerBecaseOfCoupon: function (a, c) {
            var d = this;
            Ariel.Components.Loading.show(), d._choosedCouponData = a, d.onSelectedStage = a.defaultPlan, Common.Net.ajax({
                url: "jdPayCreditCardFee.action",
                type: "POST",
                data: {
                    bankCode: a.bankCode || "",
                    defaultPlanId: a.defaultPlan || "",
                    couponCode: a.couponId || "",
                    couponType: a.couponType || "",
                    activityCode: a.activityId || "",
                    cardId: a.bankId || "",
                    plans: $.isArray(a.plans) ? a.plans.join(",") : a.plans || "",
                    isNewCard: a.isNewCard
                },
                success: function (a) {
                    if (0 == a.code) $(".blank-note-stage-layer3").show(), $(".card-title-display-container").show(), $(".blank-note-stage-layer3").hasClass("load-failed") && ($(".blank-note-stage-layer3").removeClass("load-failed"), $(".blank-note-stage-layer3-switch").html('<span class="show-or-hide-title">鏌ョ湅鍏ㄩ儴</span><span class="show-or-hide"></span>')), d._updataBlankNoteStageList(a.creditPeriodArray); else {
                        if ($(".blank-note-stage-layer3").hide(), $(".card-title-display-container").hide(), b.Components.Pop.show(a.info ? a.info : "鑾峰彇鍒嗘湡淇℃伅澶辫触锛岃閲嶈瘯"), Ariel.Components.Loading.hide(), d._wrapper.hasClass("load-failed")) return;
                        0 == $(".blank-note-stage-layer3").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length && d.loadFailedHandle()
                    }
                    c && c.call(d, a.creditPeriodArray), Ariel.Components.Loading.hide()
                }
            })
        },
        _updataBlankNoteStageList: function (a) {
            var b, c, d = this, e = a.length, f = [], g = d.onSelectedStage;
            if (!(e <= 0)) {
                d._choosedCreditData.defaultPlanId && d._choosedCreditData.defaultPlanId;
                for (var h = 0; h < e; h++) b = a[h].plan, c = d._BLANK_NOTE_LIST_ITEM_TEMPLATE, b == g ? c = c.replace("canChooseStage", "canChooseStage checked") : 0 == a[h].canUse && (c = c.replace("canChooseStage", "cantChooseStage")), c = c.replace("{$stage}", b), c = c.replace("{$plan-fee}", a[h].planFeeInfo), c = c.replace("{$repayment}", a[h].firstPay), c = c.replace("{$total}", a[h].total), c = c.replace("{$i}", h), f.push(c);
                f && f.length <= 4 && f.length > 0 ? ($(".blank-note-stage-layer3").show(), $(".card-title-display-container").hide()) : ($(".blank-note-stage-layer3").show(), $(".card-title-display-container").show()), $(".blank-note-stage-layer3").find(".blank-note-stage-list").html(f.join("")), $(".blank-note-stage-layer3").find(".blank-note-stage-list").parents(".pay-list-item").hasClass("selected") || $(".blank-note-stage-layer3").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").removeClass("checked"), Ariel.Components.Loading.hide()
            }
        },
        loadFailedHandle: function () {
            $(".blank-note-stage-layer3").addClass("load-failed")
        },
        loadFailedReset: function (a, c) {
            var d = this;
            Ariel.Components.Loading.show(), d._choosedCreditData = a, Common.Net.ajax({
                url: "jDPayCreditCardChannel.action",
                type: "POST",
                data: {bankCode: a.bankCode || "", defaultPlanId: a.defaultPlanId || ""},
                success: function (a) {
                    if (0 == a.code) $(".blank-note-stage-layer3").show(), $(".card-title-display-container-container").show(), $(".blank-note-stage-layer3").hasClass("load-failed") && ($(".blank-note-stage-layer3").removeClass("load-failed"), $(".blank-note-stage-layer3-switch").html('<span class="show-or-hide-title card-title-display">鏌ョ湅鍏ㄩ儴</span><span class="show-or-hide card-title-display"></span>')), a.bankCardPayChannel && a.bankCardPayChannel.planList && d._updataBlankNoteStageList(a.bankCardPayChannel.planList); else {
                        if ($(".blank-note-stage-layer3").hide(), $(".card-title-display-container").hide(), b.Components.Pop.show(a.info ? a.info : "鑾峰彇鍒嗘湡淇℃伅澶辫触锛岃閲嶈瘯"), Ariel.Components.Loading.hide(), d._wrapper.hasClass("load-failed")) return;
                        0 == $(".blank-note-stage-layer3").find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length && d.loadFailedHandle()
                    }
                    c && a.bankCardPayChannel && a.bankCardPayChannel.planList && c.call(d, a.bankCardPayChannel.planList), Ariel.Components.Loading.hide()
                }
            })
        },
        _inArray: function (a, b) {
            var c = b, d = parseInt(a);
            $.isArray(b) || (c = c.replace("[", ""), c = c.replace("]", ""), c = c.split(","));
            for (var e = 0; e < c.length; e++) if (parseInt(c[e]) == d) return !0;
            return !1
        }
    }), a.Common.Components.CreditBlankNoteLayers = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        this._init(), this._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK choose-jxj-layer-mask"></div>',
        _template: '<div class="choose-jxj-Layer-wrap pay-index">    <h6 class="layer-title choose-jxj-Layer-title">閫夋嫨浜笢鑶ㄨ儉閲�</h6>    <a href="javascript:void(0);" class="choose-jxj-Layer-close left-close"></a>    <div class="jxj-list-scroll-wrap"><div class="layer-list jxj-list"></div></div></div>',
        _jxjListItem: '<a href="javascript:void(0);" class="layer-list-item can-use card-list-item crl-radio pr jxj-list-item" data-jxj-li="{$i}">   <img class="layer-jxj-image" src="{$jxjIconUrl}" alt=""/>   <div class="layer-jxj-name">{$jxjName}</div></a>',
        _jxjSkuListContainer: '<div class="jxj-sku-container">    <div class="jxj-grid">       <h6 class="jxj-sku-title">鑶ㄨ儉閲戜娇鐢ㄨ鎯�</h6>       <p class="jxj-can-num">{$jxjNum}</p>       <div class="jxj-sku-scroll-wrap"></div>       <p class="jxj-sku-total">鏈鍙秷璐归噾棰濓細<span class="jxj-sku-price">{$jxjPrice}</span></p>       <div class="jxj-sku-other-info">{$otherInfo}</div>    </div>    <div class="jxj-sku-btn">鎴戠煡閬撲簡</div></div>',
        _jxjSkuListItem: '<div class="jxj-img">   <img class="jxj-sku-item-image" src="{$skuIconUrl}" alt=""/></div>',
        _wrapper: null,
        _skuWrapper: null,
        _viewport: null,
        _onSelectJxj: null,
        onSelectedJxj: null,
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._template), a._skuWrapper = $(a._jxjSkuListContainer), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".jxj-list-item.can-use", "click", function () {
                var b = $(this);
                $(a._wrapper).find(".jxj-list-item").removeClass("checked"), b.addClass("checked"), a.onSelectedJxj.call(a, a._getJxjData(b)), 5 == a._getJxjData(b).status && selectedPayment && (selectedPayment.startPayAction(), selectedPayment.setPayButton("姝ｅ湪鏀粯")), a.hide()
            }), a._viewport.delegate(".choose-jxj-layer-mask,.choose-jxj-Layer-close", "click", function () {
                a.hide()
            }), a._viewport.delegate(".choose-jxj-layer-mask,.jxj-sku-btn", "click", function () {
                a.hideSku()
            })
        },
        show: function (a) {
            var b = this;
            a && b._initJxjList(a), b._viewport.append(b._wrapper);
            setTimeout(function () {
                b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN")
            }, 320)
        },
        showSku: function (a) {
            var b = this;
            if (!b._skuWrapper.hasClass("COM_JXJ_SKU_DISPLAY")) {
                setTimeout(function () {
                    b._viewport.append(b._skuWrapper), b._viewport.append(b._mask), b._mask.show(), b._skuWrapper.addClass("COM_JXJ_SKU_DISPLAY"), $(".logo-icon").css("pointer-events", "auto")
                }, 320)
            }
        },
        initJxjList: function (a, b) {
            var c, d = this, e = a.length, f = [];
            if (d.jxjList = a, d._mask = $(d._maskTemplate), d._wrapper = $(d._template), e > 0) {
                for (var g = 0; g < e; g++) {
                    c = d._jxjListItem;
                    var h = '<p class="jxj-name-title">' + a[g].channelName;
                    5 == a[g].status && a[g].statusDesc && (c = c.replace("crl-radio", "crl-radio crl-arrow"), h += '<span class="jxj-list-need-combine">' + a[g].statusDesc + "</span>"), h += "</p>", a[g].tip && (h = h.replace("jxj-name-title", "jxj-name-title has-tip"), h += '<p class="jxj-name-tip">' + a[g].tip + "</p>"), 3 == a[g].status ? (c = c.replace("can-use", "cant-use"), c = c.replace("{$jxjName}", h)) : c = c.replace("{$jxjName}", h), c = c.replace("{$jxjIconUrl}", a[g].logo), c = c.replace("{$i}", g), b == a[g].channelId && (c = c.replace("crl-radio", "crl-radio checked")), f.push(c)
                }
                d._wrapper.find(".jxj-list").html(f.join("")), d._viewport.append(d._wrapper), d.onSelectedJxj.call(d, d._getJxjData(d._wrapper.find(".crl-radio.checked")))
            }
        },
        initJxjSkuList: function (a) {
            var b, c = this, d = a.productInfos || [], e = d.length, f = a.imgAddress;
            if (c._mask = $(c._maskTemplate), c._skuWrapper = $(c._jxjSkuListContainer), e > 0) {
                for (var g = 0; g < e; g++) {
                    b = c._jxjSkuListItem;
                    var h;
                    if (!d[g].productImgUrl || !f) return c._skuWrapper.hide(), c._mask.hide(), void $(".logo-icon").css("pointer-events", "auto");
                    c._skuWrapper.show(), c._mask.show(), h = f + d[g].productImgUrl, b = b.replace("{$skuIconUrl}", h), c._skuWrapper.find(".jxj-sku-scroll-wrap").append(b)
                }
                c._skuWrapper.find(".jxj-sku-item-image") && (c._skuWrapper.find(".jxj-sku-price").text("楼" + a.canUseBalance), c._skuWrapper.find(".jxj-can-num").text(a.skuSizeInfo), c._skuWrapper.find(".jxj-sku-other-info").text(a.jxjDesc), c._viewport.append(c._skuWrapper), this.showSku())
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN");
            var b = setTimeout(function () {
                a._mask.remove(), a._wrapper.remove(), clearTimeout(b)
            }, 300)
        },
        hideSku: function () {
            var a = this;
            a._skuWrapper.removeClass("COM_JXJ_SKU_DISPLAY");
            var b = setTimeout(function () {
                a._mask.remove(), a._skuWrapper.remove(), clearTimeout(b)
            }, 300)
        },
        _getJxjData: function (a) {
            var b = parseInt(a.attr("data-jxj-li"));
            return this.jxjList[b]
        }
    }), Common.Components.chooseJingXiJinLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var b = a.Ariel, c = function () {
        var a = this;
        a._init(), a._initEvent()
    };
    b.override(c.prototype, {
        _maskTemplate: '<div class="COMM_MASK bank-coupons-mask"></div>',
        _COUPON_LIST_ITEM_TEMPLATE: ' <li class ="selectCoupon-list-item COMM_TOP_1PX_BORDER {$couponInfoNum}" >    <a href="javascript:void(0);" class="selectCoupon-list-item-link bank-coupon COM_CRL_RADIO" data-li="{$li}">    <span class="coupon-icon">{$couponTypeDesc}</span>     <p class="coupon-title">{$couponInfo}</p>      <p class="coupon-validity">{$time}</p>      <p class="coupon-descript">{$couponDesc}</p></a></li>',
        _COUPON_LAYER_TEMPLATE: '<div class="selectCoupon-panel bank-layer bank-coupons-layer COMM_COUPON_LAYER">   <h2 class="selectCoupon-title">      <span class="select-panel-close"></span>          閫夋嫨浼樻儬鍒�<span class="select-panel-cancel">鏆備笉浣跨敤</span>   </h2>   <div class="selectCoupon-list-wrap">     <ul class="selectCoupon-list available-coupon">     </ul>   </div></div>',
        mask: null,
        _wrapper: null,
        _viewport: null,
        onSelectedCoupon: null,
        _canUseCouponData: {},
        _hasCouponLayer: !1,
        _maiDianType: "MCashierNew_CreditCoupon",
        _init: function () {
            var a = this;
            a._mask = $(a._maskTemplate), a._wrapper = $(a._COUPON_LAYER_TEMPLATE), a._viewport = $("#viewport")
        },
        _initEvent: function () {
            var a = this;
            a._viewport.delegate(".available-coupon .bank-coupon", "click", function () {
                var b = $(this);
                if (b.hasClass("checked")) return void a.hide();
                a.setCouponChoosed(b.attr("data-li"), a.getCouponData(b)), a.hide()
            }), a._viewport.delegate(".COMM_MASK,.select-panel-close", "click", function () {
                Ariel.mpingEvent("MCashierNew_CouponClose", location.href), a.hide()
            }), a._viewport.delegate(".bank-layer .select-panel-cancel-refresh", "click", function () {
                a.setCouponChoosed(-1, "鏆備笉浣跨敤"), Ariel.mpingEvent("MCashierNew_CouponNotUse", location.href), a.hide()
            })
        },
        show: function (a) {
            var b = this, c = setTimeout(function () {
                b._viewport.append(b._wrapper), Ariel.Components.Loading.hide(), b._viewport.append(b._mask), b._mask.show(), b._wrapper.addClass("COM_LAYER_IN"), b._hasCouponLayer = !1, clearTimeout(c)
            }, 500)
        },
        updateCouponLists: function (a, b) {
            var c = this;
            Ariel.Components.Loading.show(), Common.Net.ajax({
                url: "getBankCardCouponInfo.action",
                type: "POST",
                data: {appId: a.appId, payId: a.payId, channelId: a.channelId},
                success: function (a) {
                    if (0 == a.code) {
                        var d = a.canUseCouponList || [];
                        c.initCouponLayer(d), c.chooseToCoupon(b)
                    } else Ariel.Components.Pop.show(a.message ? a.message : "閾惰鍗′紭鎯犲埜淇℃伅鑾峰彇澶辫触锛岃閲嶈瘯");
                    Ariel.Components.Loading.hide()
                },
                error: function () {
                    Ariel.Components.Loading.hide()
                }
            })
        },
        initCouponLayer: function (a) {
            var b = this, c = [];
            b._mask = $(b._maskTemplate), b._wrapper = $(b._COUPON_LAYER_TEMPLATE), a ? a && a.length > 0 ? (b._canUseCouponData = a, c = b.initBlanNoteCouponList(a), b._wrapper.find(".selectCoupon-list.available-coupon").html(c.join("")), b._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-refresh")) : b._wrapper.find(".select-panel-cancel").addClass("select-panel-cancel-close") : b._wrapper.find(".unavailable-coupon-title").remove(), b._hasCouponLayer || (b._viewport.find(".bank-coupons-layer").remove(), b._viewport.find(".bank-coupons-mask").remove(), b._hasCouponLayer = !0, b.show())
        },
        initBlanNoteCouponList: function (a) {
            for (var b, c = this, d = a.length, e = [], f = 0; f < d; f++) {
                if (b = c._COUPON_LIST_ITEM_TEMPLATE, b = b.replace("{$couponTypeDesc}", a[f].couponTypeDesc), b = b.replace("{$couponInfo}", a[f].promotionDesc), a[f].beginDate && a[f].endDate) {
                    var g = "鏈夋晥鏈�: " + a[f].beginDate + "-" + a[f].endDate;
                    b = b.replace("{$time}", g)
                } else b = b.replace("{$time}", "");
                b = a[f].usingScene ? b.replace("{$couponDesc}", a[f].usingScene) : b.replace("{$couponDesc}", "闄愪含涓滃晢鍩嶢PP浣跨敤"), b = b.replace("{$li}", f), b = b.replace("{$couponType}", a[f].couponType), e.push(b)
            }
            return e
        },
        chooseToCoupon: function (a) {
            var b = this, c = b._canUseCouponData || [], d = c.length;
            if (0 !== d && a) {
                if (a) for (var e = 0; e < d; e++) if (b._matchCoupon(a, c[e])) return void b.setCouponChoosedSimple(e, c[e]);
                b.setCouponChoosed(-1)
            } else b.setCouponChoosed(-1)
        },
        _matchCoupon: function (a, b) {
            var c;
            return !!a && (a.indexOf("nothing"), c = a == b.payMarketingUUID, c)
        },
        setCouponChoosedSimple: function (a, b) {
            if (void 0 !== a) {
                var c = this;
                $(c._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked");
                c._wrapper.find(".available-coupon .selectCoupon-list-item-link").eq(a).addClass("checked")
            }
        },
        setCouponChoosed: function (a, b) {
            if (void 0 !== a) {
                var c = this, d = $(c._wrapper).find(".selectCoupon-list-item-link.checked");
                if (!(1 == d.length && d.attr("data-li") == a || 0 == d.length && -1 == a && $(".blank-note-pay .COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").length > 0 && !b)) if ($(c._wrapper).find(".selectCoupon-list-item-link.checked").removeClass("checked"), -1 === a) c.onSelectedCoupon.call(c, -1, b); else {
                    var e = c._wrapper.find(".available-coupon .selectCoupon-list-item-link").eq(a);
                    e.addClass("checked"), c.onSelectedCoupon.call(c, a, b)
                }
            }
        },
        hide: function () {
            var a = this;
            a._wrapper.removeClass("COM_LAYER_IN"), a._viewport.find(".bank-coupons-layer").remove(), a._viewport.find(".bank-coupons-mask").remove()
        },
        getCouponData: function (a) {
            return this._canUseCouponData[parseInt(a.attr("data-li"))]
        }
    }), a.Common.Components.chooseBankCouponLayer = new c
}(window), function (a) {
    a.Common || (a.Common = {}), a.Common.DataKeys || (a.Common.DataKeys = {});
    var b = a.Ariel, c = function () {
    };
    b.override(c.prototype, {
        _wrapper: null,
        _panel: null,
        _hourB: null,
        _minuteB: null,
        _secondB: null,
        popInfo: null,
        _template: '<span class="remain-time" role="text">   鏀粯鍓╀綑鏃堕棿   <span class="remain-time-hour" aria-hiden="true"></span>   <span class="count-down-clon" aria-hiden="true"></span>   <span class="remain-time-minute" aria-hiden="true"></span>   <span class="count-down-clon" aria-hiden="true"></span>   <span class="remain-time-second" aria-hiden="true"></span></span>',
        countingDown: function (a, b) {
            var c = this;
            c._wrapper = $(c._template), c._panel = $(".JS-pay-tip"), isNaN(a) || (c._hourB = c._wrapper.find(".remain-time-hour"), c._minuteB = c._wrapper.find(".remain-time-minute"), c._secondB = c._wrapper.find(".remain-time-second"), c.popInfo = b || {}, c.refreshTime(a), this.started = !0)
        },
        stop: function () {
            this.mtd && clearTimeout(this.mtd), this.std && clearInterval(this.std)
        },
        _getTime: function (a) {
            var b = {}, c = this;
            return b.hour = c._timeFormat(Math.floor(a / 60 / 60)), b.minute = c._timeFormat(Math.floor(a / 60 % 60)), b.second = c._timeFormat(Math.floor(a % 60)), b
        },
        _timeFormat: function (a) {
            return a < 10 ? "0" + a : a
        },
        refreshTime: function (a) {
            var b = this;
            b._getTime(a).hour < 12 ? (b._countDownSecond(a), b._showCountDownWraper()) : b.toCountTime(a), b._refreshWraper(a)
        },
        toCountTime: function (a) {
            var b = this, c = a - 43200 + 1, d = setTimeout(function () {
                b._countDownSecond(43199), b._showCountDownWraper(), clearTimeout(d)
            }, 1e3 * c);
            this.mtd = d
        },
        _countDownSecond: function (a) {
            var b = this;
            b._refreshWraper(a);
            var c = setInterval(function () {
                b._refreshWraper(a), --a < 0 && (clearInterval(c), b.handlerOrderExpiration())
            }, 1e3);
            this.std = c
        },
        _refreshWraper: function (a) {
            var b = this, c = b._getTime(a);
            b._hourB.html(c.hour), b._minuteB.html(c.minute), b._secondB.html(c.second), b._wrapper.attr({"aria-label": "鏀粯鍓╀綑鏃堕棿" + parseInt(c.hour, 10) + "灏忔椂" + parseInt(c.minute, 10) + "鍒�" + parseInt(c.second, 10) + "绉�"})
        },
        handlerOrderExpiration: function () {
            var b = this;
            b.popInfo.urlParam && b.popInfo.btnUrl && (b.popInfo.btnUrl = b.popInfo.btnUrl + "?params=" + JSON.stringify(b.popInfo.urlParam), b.popInfo.btnUrl = b.popInfo.btnUrl.replace("[", ""), b.popInfo.btnUrl = b.popInfo.btnUrl.replace("]", ""));
            var c = JSON.stringify(b.popInfo);
            orderExpiration = function () {
                return c
            };
            var d = navigator.userAgent;
            if (d.match(/(iPhone\sOS)\s([\d_]+)/) || d.match(/(iPad).*OS\s([\d_]+)/)) if ("jdltapp" == d.toLocaleLowerCase().match(/jdltapp/i)) {
                var e = {method: "notifyMessageToCustomWebView", params: {type: "count_down"}};
                a.webkit.messageHandlers.JDAppUnite.postMessage(e)
            } else a.location.href = 'jdmobileCashier://popToast?params={"type":"count_down"}';
            a.JdAndroid && a.JdAndroid.showCountDownDialog && a.JdAndroid.showCountDownDialog(c)
        },
        _showCountDownWraper: function () {
            var a = this;
            $(".order-bar").addClass("count-down"), a._panel.html(a._wrapper)
        }
    }), a.Common.NewCountDeadLine = new c
}(window), Page = function () {
    var a = this;
    a.init(), a.initEvent()
}, Page.prototype.init = function () {
    var a = this;
    a._loadFiles(), Ariel.mpingPV(), !0 !== a._noNeedScroller && (a._pageScroller = new IScroll($(".JS-page-ct").get(0), {
        tap: !0,
        freeScroll: !0
    }), $(".JS-page-ct").bind("touchmove", function (a) {
        a.preventDefault()
    }, !1)), viewPageName = function () {
        try {
            var a = window.location.pathname.split("/");
            return a[a.length - 1].split(".")[0]
        } catch (a) {
        }
        return ""
    }, document.domain = "jd.com"
}, Page.prototype.initEvent = function () {
    window.navigator.userAgent;
    if ($(".JS-page-ct").bind("tap", function (a) {
        var b = a.target, c = b.tagName;
        "INPUT" != c && "A" == c && $(b).focus()
    }), /iP(hone|od|ad)/.test(navigator.userAgent)) {
        var a = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
        parseInt(a[1], 10) >= 8 && ($("#viewport").addClass("js-ios8-plus"), $(".JS-page-ct").addClass("js-ios8-plus"))
    }
}, Page.prototype._loadFiles = function () {
    0 == Ariel.parseGetParameters().appId.indexOf("jd_m_mini") && ($script = $("<script />", {
        src: "https://res.wx.qq.com/open/js/jweixin-1.3.2.js",
        id: "ui-zepto"
    }), $script.appendTo($("body")))
}, Page.prototype._showHeader = function () {
    try {
        var a = new MCommonHeaderBottom, b = $("title")[0].innerText, c = {
            hrederId: "m_common_header",
            title: b,
            isShowShortCut: !1,
            selectedShortCut: "4",
            onClickJdkey: function () {
            },
            onClickGoback: function () {
            },
            onClickIndex: function () {
            },
            onClickSearch: function () {
            },
            onClickCart: function () {
            },
            onClickHome: function () {
            }
        };
        a.header(c), setTimeout(function () {
            try {
                var a = "m_common_header_goback";
                if (location.pathname.indexOf("index.html") >= 0) {
                    if (!document.getElementById(a)) return;
                    var b = $("#" + a).clone().wrap("<div>").parent().html(), c = document.getElementById(a).parentNode;
                    $("#" + a).remove();
                    var d = $(b);
                    c.insertBefore(d[0], c.firstChild), $("#" + a).bind("click", function () {
                        var a = {}, b = "";
                        closeIndexPageSwitch && (a = Common.JSON.parse(closeIndexPageSwitch())), b = closeIndexPage ? closeIndexPage() : a && a.content ? a.content : "", a.message = b, Common.Components.GoBackMsgBox.show(a)
                    })
                }
            } catch (a) {
            }
        }, 1e3)
    } catch (a) {
    }
}, Page.prototype._displayHeader = function (a) {
    function b(a) {
        for (var b = document.getElementsByTagName("script"), c = 0; c < b.length; c++) if (b[c].src == a) return !0;
        return !1
    }

    var c, d, e = this;
    if (a) c = Number(a); else {
        if (!(d = Common.JSON.parse(Ariel.Courier.get(Common.DataKeys.ACROSS_PAGES_PAY_BUSINESS_DATA)))) return;
        c = Number(d.isDisplayHeader)
    }
    if (c) {
        var f = document.createElement("script");
        f.type = "text/javascript", f.src = "//st.360buyimg.com/common/commonH_B/js/m_common_header_bottom2.1.js", f.onload = f.onreadystatechange = function () {
            this.readyState && "loading" == this.readyState || (e._showHeader(), $("#m_common_header").show())
        }, f.onerror = function () {
            h.removeChild(f)
        };
        var g = document.createElement("script");
        if (g.type = "text/javascript", g.src = "//st.360buyimg.com/common/commonH_B/js/m_common2.1.js", !b(g.src)) {
            var h = document.getElementsByTagName("head")[0];
            h.appendChild(g)
        }
        if (!b(f.src)) {
            var h = document.getElementsByTagName("head")[0];
            h.appendChild(f)
        }
        $(".PAY_SCROLL_LEFT").addClass("PAY_SCROLL_LEFT-M")
    } else $("#m_common_header").hide()
}, Page.prototype._sendMonitorData = function () {
    function a() {
        var a = new R_Time, b = a.calculateTime(rTimeArr), c = a.urlParse("sid") || "",
            d = a.extend({flag: "2", id: f, sid: c}, b);
        a.sendTime(d)
    }

    try {
        var b = {
            error: "34",
            "blank-note": "35",
            "quick-pay": "37",
            "verify-code": "38",
            "new-card": "39",
            "new-card-info": "40",
            "quick-pay-open": "42",
            finish: "33",
            "binded-list": "",
            "unbind-card": "",
            "other-pay-info": "81",
            "other-pay": "82",
            "other-pay-success": "83",
            "global-blank-note": "84",
            "blank-note-quick": "127",
            protocol: "126",
            "custom-finish": "16708",
            "pay-index": "22537",
            "newPay-index": "423568"
        }, c = {index: "24"}, d = window.location.pathname.split("/"), e = d[d.length - 1].split(".")[0], f = "";
        c[e] ? (f = c[e], rTimeArr[5] = {time: new Date, sTime: "4"}, rTimeArr[6] = {
            time: new Date,
            sTime: "0"
        }) : (f = b[e], rTimeArr[5] = {time: new Date, sTime: "0"});
        var g = "//h5.m.jd.com/active/reporttime/reportTime.min.js";
        if (e && e.indexOf("index") >= 0) {
            for (var h = $("script"), i = h.length, j = !1, k = 0; k < i; k++) {
                var l = h[k];
                if (l && l.src == g) {
                    j = !0;
                    break
                }
            }
            if (0 == j) {
                var m = document.getElementsByTagName("head")[0], n = document.createElement("script");
                n.src = g, n.type = "text/javascript", n.onload = n.onreadystatechange = function (b) {
                    this.readyState && "loading" == this.readyState || a()
                }, m.appendChild(n)
            }
        } else a()
    } catch (a) {
    }
}, function (a) {
    function b(a) {
        return String(a).indexOf(".") >= 0 && (a = a.toFixed(2)), a
    }

    a.Common || (a.Common = {}), a.Common.Components || (a.Common.Components = {});
    var c = 1, d = function () {
    };
    Ariel.override(d.prototype, {
        id: null, keyboard: null, options: {
            suggestedAmount: 1e4, readme: [], onConfirm: function () {
            }, onCancel: function () {
            }
        }, inputValue: 0, init: function () {
            this.id = c++;
            var a = this, b = "installment-layer-mask-" + this.id, d = "installment-layer-" + this.id,
                e = "installment-input-" + this.id;
            this.inputId = e,
                a.wrapper = $('<div class="choose-card-Layer-wrap installments-layer">    <div class="installments-layer-main-screen">        <div class="installments-layer-header">            <h6 class="layer-title choose-card-Layer-title installments-layer-title">鍒嗘鏀粯</h6>            <a href="javascript:void(0);" class="choose-card-Layer-close right-close j-close"></a>            <span class="installments-layer-sub-title">浣跨敤璇存槑</span>        </div>        <div class="installments-layer-content">            <div class="installments-layer-desc"><span>寤鸿鏈鏀粯閲戦</span></div>            <div class="installments-layer-input-wrapper">            <span class="installments-layer-money-icon">楼</span>            <span class="installments-layer-input" >                <span class="custom-placeholder">璇疯緭鍏ラ噾棰�</span>                <span class="custom-input"></span>            </span>            <span class="installments-layer-input-delete u-delete-icon"></span>        </div>            <div class="installments-layer-extra-info">                <span>                    璁㈠崟閲戦<em class="installments-layer-total-amount"></em>;                    鍓╀綑鏀粯<em class="installments-layer-remaining-amount"></em>                </span>            </div>        </div>        <div class="installments-layer-footer">            <a class="installments-layer-footer-btn">纭畾骞堕€夋嫨鏀粯鏂瑰紡</a>            <span class="installments-layer-footer-cancel">鍙栨秷閫夋嫨鍒嗘鏀粯</span>        </div>    </div>    <div class="installments-layer-extra-screen">        <div class="installments-layer-header">            <span class="installments-layer-back j-back u-back-icon">杩斿洖</span>            <h6 class="layer-title choose-card-Layer-title installments-layer-title">浣跨敤璇存槑</h6>            <a href="javascript:void(0);" class="choose-card-Layer-close right-close j-close"></a>        </div>        <div class="installments-layer-content installments-readme">        </div>    </div></div>').attr("id", d).addClass(d), $("head").append('<style>\n.installments-layer {\n    font-family: "PingFang SC", "Helvetica Neue",Helvetica,"Microsoft YaHei","寰蒋闆呴粦",Arial,sans-serif;\n}\n.installments-layer.installments-layer-in {\n    height: 80%;}\n.installments-layer .right-close {    top: 50%;    transform: translateY(-50%);    -webkit-transform: translateY(-50%);    background-position: center right;}\n.installments-layer-main-screen{\n    width: 100%;\n    height: 100%;\n    position: relative;\n    -webkit-transition: opacity 300ms ease-in-out;\n    transition: opacity 300ms ease-in-out;\n}\n.installments-layer-main-screen.out {\n    opacity: 0;\n}\n.installments-layer-extra-screen {\n    width: 100%;\n    height: 100%;\n    transition: transform 300ms ease-in-out;\n    -webkit-transition: -webkit-transform 300ms ease-in-out;\n    -webkit-transform: translateX(0);\n    transform: translateX(0);\n    position: absolute;\n    left: 100%;\n    top: 0;\n}\n.installments-layer-extra-screen.in{\n    -webkit-transform: translateX(-100%);\n    transform: translateX(-100%);\n}\n.installments-layer-header {\n    height: 54px;\n    position: relative;\n}\n.installments-layer-sub-title {\n    position: absolute;\n    right: 50px;\n    top: 0px;\n    font-size: 12px;\n    height: 100%;    line-height: 54px;    color: #262626;\n}\n.installments-layer-content {\n    padding: 30px 18px;\n    width: 100%;\n    box-sizing: border-box;\n}\n.installments-layer-desc {\n    font-size: 12px;\n    color: #262626;\n    line-height: 24px;\n    height: 24px;\n}\n.installments-layer-input-wrapper {\n    height: 60px;\n    border-bottom: 1px solid #F2F2F2;\n    font-size: 24px;\n    color: #F2270C;\n    line-height: 36px;\n    padding-top: 12px;\n    padding-bottom: 12px;\n    box-sizing: border-box;\n    font-weight: bold;\n    display: flex;\n    justify-content: start;\n    align-items: center;\n    display: -webkit-flex;    -webkit-justify-content: start;    -webkit-align-items: center;}\n.installments-layer-input-delete {\n    width: 40px;height: 20px;    background-size: 20px;    background-position: center right;    background-repeat: no-repeat;    display: none;}\n.installments-layer-input-delete.shown {\n    display: block;}\n.installments-layer-money-icon {\n    vertical-align: top;\n    height: 100%;}\n.installments-layer-input {\n    width: 80%;\n    margin-left: 10px;\n    height: 100%;\n    position: relative;\n}\n.custom-placeholder {\n    color: #acacac;\n    font-weight: normal;\n    margin-left: 6px;\n}\n.custom-input{\n    position: absolute;    left: 0;    white-space: nowrap;}\n.installments-layer-extra-info {\n    font-size: 10px;\n    color: #262626;\n    margin-top: 12px;\n}\n.installments-layer-total-amount {\n    margin: auto 3px;\n}\n.installments-layer-remaining-amount {\n    color: #F2270C;\n    margin: auto 3px;\n}\n.installments-layer-footer {\n    position: absolute;\n    bottom: 24px;\n    left: 0;\n    width: 100%;\n    height: auto;\n}\n.installments-layer-footer-btn {\n    background-image: linear-gradient(135deg, #F2140C 0%, #F2270C 70%, #F24D0C 100%);\n    height: 38px;\n    line-height: 38px;\n    margin: 6px 18px;\n    font-size: 13px;\n    color: #FFFFFF;\n    text-align: center;\n    display: block;\n    border-radius: 42px;\n}\n.installments-layer-footer-cancel {\n    display: block;\n    font-size: 12px;\n    text-align: center;\n    color: #8C8C8C;\n    margin-top: 12px;\n}\n.installments-layer-back {\n    position: absolute;\n    display: inline-block;\n    text-indent: -100000px;\n    width: 30px;\n    height: 30px;\n    left: 18px;\n    top: 50%;\n    transform: translateY(-50%);    -webkit-transform: translateY(-50%);    background-size: 18px;\n    background-repeat: no-repeat;\n    background-position: center left;\n}\n.installments-layer-extra-screen .installments-layer-title {\n    text-align: center;\n}\n.installments-readme {\n    font-size: 13px;\n    line-height: 1.5;\n    height: 85%;\n    height: calc(100% - 84px);\n    overflow-y: auto;\n}\n.installments-readme-item {\n    margin-bottom: 6px;\n}\n.installments-readme-item-question, .installments-readme-item-answer {\n    color:#262626;\n    display: flex;\n    display: -webkit-flex;\n}\n.installments-readme-item-answer {\n    color:#8C8C8C;}\n.installments-readme-item-symbol {    width: 22px;    flex-shrink: 0;    -webkit-flex-shrink: 0;}\n@media screen and (device-width: 320px) and (max-height: 490px) and (-webkit-device-pixel-ratio: 2){\n    .installments-layer-content { padding-top: 0px;}\n    .installments-layer-input-wrapper{  padding-top: 6px;padding-bottom: 6px; height: 48px;}}\nbody .jr-aks-sec-keyboard-container {\n    z-index: 992;\n}\n.jr-aks-sec-keyboard-container .ensure {    background-color: transparent;    background-image: linear-gradient(135deg, #F2140C 0%, #F2270C 70%, #F24D0C 100%) !important;    font-family: "PingFang SC", "Helvetica Neue",Helvetica,"Microsoft YaHei","寰蒋闆呴粦",Arial,sans-serif;    box-shadow: inset 0 -1px 0 0 #f2140c !important;}\n.keyboard-pointer::after{\n    content: \'\';\n    width: 0px;    border-right: 2px solid #262626;\n    border-left: 4px solid transparent;\n    opacity: 1;\n    -webkit-animation: focus 1s forwards infinite;\n}\n.keyboard-pointer::-webkit-scrollbar {\n    display: none;\n}\n@-webkit-keyframes focus {\n    from {\n        opacity: 1;\n    }\n    to {\n        opacity: 0;\n    }\n}\n@media screen and (-webkit-device-pixel-ratio: 3) and (device-height: 812px) and (device-width: 375px) {    body .itemKH {        height: 230px !important;    }}</style>'), $("body").append('<div class="COMM_MASK ' + b + '" id="' + b + '" ></div>').append(this.wrapper), this.wrapper.find(".installments-layer-input").attr("id", e), this.mask = $("#" + b), this.inputDom = this.wrapper.find(".custom-input"), this.initKeyBoard(), this.bind()
        }, bind: function () {
            var a = this;
            this.wrapper.on("click", ".j-close", function () {
                a.hide();
                var b = a.wrapper.find(".installments-layer-main-screen"),
                    c = a.wrapper.find(".installments-layer-extra-screen");
                (b.hasClass("out") || c.hasClass("in")) && (event.preventDefault(), c.removeClass("in"), b.removeClass("out"))
            }).on("click", ".installments-layer-sub-title", function (b) {
                b.preventDefault(), a.keyboard && a.keyboard.close(), a.wrapper.find(".installments-layer-main-screen").addClass("out"), a.wrapper.find(".installments-layer-extra-screen").addClass("in"), a.initInstruction(), Ariel.mpingEvent("MCashierNew_SubStepToastRule", location.href), Ariel.mpingEvent("MCashierNew_SubStepRulePageExpo", location.href)
            }).on("click", ".j-back", function (b) {
                b.preventDefault(), a.wrapper.find(".installments-layer-extra-screen").removeClass("in"), a.wrapper.find(".installments-layer-main-screen").removeClass("out")
            }).on("click", ".installments-layer-footer-btn", function (b) {
                b.preventDefault(), a.validate() && (a.options.onConfirm(a.inputValue), Ariel.mpingEvent("MCashierNew_SubStepConfirmPayment", location.href, a.inputValue))
            }).on("click", ".installments-layer-footer-cancel", function (b) {
                b.preventDefault(), a.options.onCancel()
            })
        }, config: function (a) {
            if (Ariel.override(this.options, a), a.totalAmount && this.wrapper.find(".installments-layer-total-amount").html("楼" + Number(a.totalAmount).toFixed(2)), a.remainingAmount && this.wrapper.find(".installments-layer-remaining-amount").html("楼" + Number(a.remainingAmount).toFixed(2)), a.suggestedAmount) {
                var c = Math.min(this.options.suggestedAmount, this.options.remainingAmount);
                c = b(c), this.inputDom.text(c), this.inputValue = c, this.wrapper.find(".custom-placeholder").addClass("u-hide")
            }
            "1" == a.alreadyPaid && this.wrapper.find(".installments-layer-footer-cancel").remove()
        }, validate: function () {
            var a = this, c = this.inputDom.text(), d = Number(c), e = !1;
            return d > a.options.remainingAmount ? (Ariel.Components.Pop.show("鎮ㄨ緭鍏ラ噾棰濊繃澶�"), d = Math.min(a.options.suggestedAmount, a.options.remainingAmount), e = !0) : d < a.options.minAmount && d < a.options.remainingAmount && (Ariel.Components.Pop.show("鎮ㄨ緭鍏ラ噾棰濊繃灏�"), d = Math.min(a.options.minAmount, a.options.remainingAmount), e = !0), this.inputValue = b(d), e && (this.inputDom.text(a.inputValue), this.wrapper.find(".custom-placeholder").addClass("u-hide")), !e
        }, initKeyBoard: function () {
            var a = this, b = this.inputDom, c = this.wrapper.find(".custom-placeholder"),
                d = this.wrapper.find(".installments-layer-input-delete"), e = {
                    keyboardInputId: this.inputId,
                    keyboardParam: {
                        keyboardType: "8",
                        digitalKeyboardType: "2",
                        isEncrypt: "0",
                        confirmBtnText: "纭畾",
                        confirmBtnColor: "red"
                    },
                    onInput: function (a) {
                        var d = b.text();
                        if ("." == a && "" == d && (d = "0"), d.indexOf(".") >= 0) {
                            if ("." == a) return !1;
                            if ((d.split(".")[1] || "").length >= 2) return !1
                        }
                        d.length >= 10 || (b.text(d + a), c.addClass("u-hide"))
                    },
                    onDelete: function () {
                        var a = b.text();
                        a.length <= 0 || (a = a.substring(0, a.length - 1), b.text(a), c[a.length ? "addClass" : "removeClass"]("u-hide"))
                    },
                    onConfirm: a.validate.bind(a),
                    onClosed: function () {
                        d.removeClass("shown"), b.removeClass("keyboard-pointer")
                    },
                    onShow: function () {
                        d.addClass("shown"), b.addClass("keyboard-pointer"), Ariel.mpingEvent("MCashierNew_SubStepToastModify", location.href)
                    }
                };
            d.on("click", function (d) {
                d.stopPropagation(), b.text(""), a.inputValue = 0, c.removeClass("u-hide")
            }), Ariel.JSLoader.load("//jrsecstatic.jdpay.com/jr-sec-dev-static/jdkeyboard.min.js", function () {
                a.keyboard = KeyboardManager.getInstance(), a.wrapper.find(".installments-layer-input").on("click", function (b) {
                    b.preventDefault(), a.keyboard.show({pure: !0, keyboards: [e]})
                })
            })
        }, initInstruction: function () {
            if (this.instructionInited) return !0;
            if (this.options.graduallyPayReadme.length) {
                var a = [];
                this.options.graduallyPayReadme.forEach(function (b) {
                    a.push('<div class="installments-readme-item"><div class="installments-readme-item-question"><span class="installments-readme-item-symbol">Q锛�</span><span class="installments-readme-item-q">' + b.Q + '</span></div><div class="installments-readme-item-answer"><span class="installments-readme-item-symbol">A锛�</span><span class="installments-readme-item-q">' + b.A + "</span></div></div>")
                }), this.wrapper.find(".installments-readme").html(a.join("")), this.instructionInited = !0
            }
        }, show: function () {
            this.mask.show(), this.wrapper.show().addClass("installments-layer-in"), Ariel.mpingEvent("MCashierNew_SubStepToastExpo", location.href)
        }, hide: function () {
            this.keyboard && this.keyboard.close(), this.wrapper.removeClass("installments-layer-in"), this.mask.hide(), setTimeout(function () {
                this.wrapper.hide()
            }.bind(this), 160)
        }
    }), Common.Components.Installments = d
}(window), Payment = function (a) {
    var b = this;
    $.each(a, function (a, c) {
        b[a] = c
    }), b._init(), b._initEvent()
}, Ariel.override(Payment.prototype, {
    _wrapper: null,
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="title-vice">---------</span></a></li>',
    _jdPaymentTemplate: null,
    _otherWrapper: ".p-other-pay-list",
    _jdPeymentsWrapper: ".jd-pay-list",
    _isDefaultPayment: !1,
    _initiativeSelected: !1,
    _isJdPayment: !1,
    _titleMain: null,
    _titleSimple: null,
    _titleVice: null,
    _availableMoney: null,
    _specialCss: null,
    _payType: null,
    _status: 0,
    _data: null,
    _jdPaymentData: null,
    _client: null,
    _pageScroller: null,
    _total: null,
    _tip: null,
    logo: null,
    realAmount: null,
    _plansExpand: !1,
    isNewFlag: !1,
    _init: function () {
        if (this._isJdPayment && this._jdPaymentTemplate) {
            var a = this;
            if ("HONEYPAY" == a.code) if (1 == Ariel.guidOpenHoneyPay && 1 == a._data.guidOpenHoneyPay) {
                Ariel.mpingEvent("MCashierNew_IntimateCardGuideExpo", location.href);
                var b = '<div class="honeyPay-toast"></div>';
                a._jdPaymentTemplate = a._jdPaymentTemplate.replace("{$honeyPayToast}", b), setTimeout(function () {
                    a._wrapper.find(".honeyPay-toast").remove()
                }, 1e4)
            } else a._jdPaymentTemplate = a._jdPaymentTemplate.replace("{$honeyPayToast}", ""); else if ("BANKCARD" == a.code) if (a._titleMain && a._titleMain.indexOf("(") > -1) {
                var c = a._switchBankTitleNumberToText(a._titleMain);
                a._jdPaymentTemplate = a._jdPaymentTemplate.replace("{$aria-label}", 'aria-label="' + c + '"')
            } else a._jdPaymentTemplate = a._jdPaymentTemplate.replace("{$aria-label}", "");
            this._wrapper = $(a._jdPaymentTemplate)
        } else this._wrapper = $(this._template);
        this._initDescription(), "3" == this._status && this._wrapper.addClass("disabled-pay-list-item").attr("data-pop", this._tip ? this._tip : "姝ゆ敮浠樻柟寮忎笉鍙敤"), "5" == this._status && (Ariel.mpingEvent("MCashierNew_MixPayShowUp", location.href), this._wrapper.addClass("need-combine")), "7" == this._status && this._wrapper.addClass("hide-payment"), this._isDefaultPayment && (this._initiativeSelected ? Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, this._maiDianPayType + "_1_" + (this.recommand ? 1 : 0)) : Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, this._maiDianPayType + "_0_" + (this.recommand ? 1 : 0)), this.selectPament(), "1" == this._status && this.setJdPaymentsDesc()), this._plansExpand
    },
    _initEvent: function () {
        var a = this;
        this._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") && "applePay" != a._payType && "applePaySF" != a._payType || ("applePaySF" == a._payType && 1 == a.isNewFlag ? Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)) : "applePaySF" == a._payType ? (a._maiDianPayType = "JdSF", Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0))) : Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), b.stopPropagation(), "1" == a._status && a.setJdPaymentsDesc(), a.selectPament()))
        }), a.initTapEvent()
    },
    initTapEvent: function () {
        var a = this;
        this._wrapper.find(".jdpayment-info.choose-preferential-bar").bind("tap", function () {
            Ariel.mpingEvent("MCashierNew_DiscountEntrance", location.href), "1" == a._status && a.setJdPaymentsDesc(), $(this).hasClass("canTap") && Common.Components.chooseCouponLayer.show()
        })
    },
    _initDescription: function () {
        this._wrapper.find(".title-main").html(this._titleMain), this._titleVice ? (this._wrapper.find(".title-vice").html(this._titleVice), this._wrapper.find(".title-main").addClass("title-main-hasvice"), this._wrapper.addClass("pay-has-vice")) : this._wrapper.find(".title-vice").remove(), this._specialCss && this._wrapper.addClass(this._specialCss), this.logo && this._wrapper.find(".pay-icon").css("background-image", "url(" + this.logo + ")"), this.payPositionType && this._wrapper.addClass(this.payPositionType), this.setDefaultDescription()
    },
    startPayAction: function () {
    },
    _callJdPayAction: function (a) {
        var b = this, c = b._client;
        Common.Net.ajax({
            url: "isJDpayPaymentAcc.action", type: "POST", data: a, success: function (d) {
                if (!(d && d.success & d.isJDpay2Valid && d.isApp && "wp" != c)) return void (d.isJDpay2Valid && !d.isApp && d.cashierUrl ? window.location.href = d.cashierUrl : Ariel.Components.Pop.show(d.message ? d.message : "鏀粯寮傚父锛岃閫夋嫨鍏朵粬鏀粯鏂瑰紡鏀粯"));
                if (b.protocol) {
                    b.protocol.params.params.payId = a.payId;
                    var e = JSON.stringify(b.protocol.params);
                    window.location.href = b.protocol.url + "?params=" + e
                } else switch (c) {
                    case"apple":
                    case"iPad":
                    case"android":
                        window.location.href = 'openApp.jdMobile://communication?params={"type":"12","payId":"' + a.payId + '"}';
                        break;
                    default:
                        window.location.href = 'openApp.jdMobile://communication?params={"type":"12","payId":"' + a.payId + '"}'
                }
            }
        })
    },
    setDefaultDescription: function () {
    },
    selectPament: function () {
        if (selectedPayment = this, this.removeSelected(), !selectedPayment._wrapper.hasClass("selected")) {
            selectedPayment._wrapper.addClass("selected");
            var a = selectedPayment._wrapper.find(".pay-list-link");
            a.attr("role") && a.attr("aria-selected") && a.attr("aria-selected", "true"), this.setPayButton(), this._isJdPayment ? "5" == this._status ? this.combinePaySelected() : "1" == this._status && this.showJdPaymentDesc() : this.setOtherPaymentDesc(), this._pageScroller.refresh()
        }
        this.judgeDefault()
    },
    judgeDefault: function () {
        $(".list-item.list-link-item.pay-list-item.jdpay-list-item.baitiao").hasClass("selected") ? $(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage.judge-checked").hasClass("checked") || $(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage.judge-checked").addClass("checked") : $(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage.judge-checked").hasClass("checked") && $(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage.judge-checked.checked").removeClass("checked"), this._pageScroller.refresh()
    },
    setPayButton: function (a) {
        if (window.selectedPayment && (!selectedPayment || selectedPayment.code === this.code)) {
            var b = a || (5 == this._status ? "缁勫悎鏀粯楼" + this._total : this._titleSimple + "鏀粯楼" + this.realAmount);
            b = b.replace("鏀粯鏀粯", "鏀粯"), $(".pay-next").html(b)
        }
    },
    setJdPaymentsDesc: function () {
    },
    showJdPaymentDesc: function () {
        $(".pay-list-item.selected").css("height", "auto")
    },
    hideJdPaymentDesc: function () {
        $(".jd-pay-list .pay-list-item.selected").hasClass("jd-cart-pay") || $(".jd-pay-list .pay-list-item.selected").hasClass("jd-jxj-pay") || $(".jd-pay-list .pay-list-item.selected").hasClass("show") ? $(".jd-pay-list .pay-list-item.selected").hasClass("show") && ($(".jd-pay-list .pay-list-item.selected").addClass("hideLastPlan"), $(".jd-pay-list .pay-list-item.selected").find(".show-or-hide-title").html("鏌ョ湅鍏ㄩ儴")) : $(".jd-pay-list .pay-list-item.selected").css("height", "61px")
    },
    combinePaySelected: function () {
    },
    removeSelected: function () {
        1 != this._plansExpand && this.hideJdPaymentDesc();
        var a = $(".pay-list-item.selected").find(".pay-list-link");
        a.attr("role") && a.attr("aria-selected") && a.attr("aria-selected", "false"), $(".pay-list-item.selected").removeClass("selected"), "CREDITINSTALLMENT" != this.code && $(".jd-pay-list .pay-list-item .credit-choose-coupons").css("display", "none")
    },
    setOtherPaymentDesc: function () {
    },
    disablePayment: function (a) {
        this._wrapper.addClass("disabled-pay-list-item"), this._wrapper.attr("data-pop", a)
    },
    render: function () {
        this._isJdPayment && !this._data.moveToOtherPay ? ($(this._jdPeymentsWrapper).append(this._wrapper), $(this._jdPeymentsWrapper).show()) : ($(this._otherWrapper).append(this._wrapper), $(this._otherWrapper).show()), this._wrapper.hasClass("hide-payment") || this._wrapper.show()
    }
}), QQWalletPayment = Ariel.extend(Payment, {
    _specialCss: "qq-Wallet-pay",
    _payType: "qqWalletPay",
    _maiDianPayType: "qqWalletPay",
    startPayAction: function () {
        var a = this, b = (a._client, Ariel.parseGetParameters()), c = b.payId;
        window.location.href = 'openApp.jdMobile://communication?params={"type":"13","payId":"' + c + '"}'
    }
}), WeixinPayment = Ariel.extend(Payment, {
    _specialCss: "weixin-pay",
    _payType: "weixin",
    _maiDianPayType: "weiXinPay",
    startPayAction: function () {
        var a = this, b = a._client, c = Ariel.parseGetParameters(), d = c.payId;
        if (a.protocol) {
            a.protocol.params.params.payId = c.payId;
            var e = JSON.stringify(a.protocol.params);
            window.location.href = a.protocol.url + "?params=" + e
        } else {
            var f = a._data.liteApp ? "openjdlite" : "openApp.jdMobile";
            switch (b) {
                case"android":
                case"apple":
                    window.location.href = f + '://communication?params={"type":"10","payId":"' + d + '"}';
                    break;
                case"wp":
                    window.external.Notify(f + '://communication?params={"type":"10","payId":"' + d + '"}');
                    break;
                case"iPad":
                    window.location.href = f + '://communication?params={"type":"10","payId":"' + d + '"}';
                    break;
                default:
                    window.location.href = f + '://communication?params={"type":"10","payId":"' + d + '"}'
            }
        }
    }
}), WeixinWapPayment = Ariel.extend(Payment, {
    _specialCss: "weixin-pay", _payType: "wapWeixinPay", _floatLayer: null, startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters();
        Ariel.Components.Loading.show(), 0 == b.appId.indexOf("jd_m_mini") ? Common.Net.ajax({
            url: "weiXinMiniPay.action",
            type: "POST",
            data: {},
            success: function (a) {
                a && a.map && a.success && wx.miniProgram.redirectTo({url: "/pages/wepay/wepay?prepay_id=" + a.map.prepay_id}), Ariel.Components.Loading.hide()
            }
        }) : Common.Net.ajax({
            url: "../index.action",
            skipProcessUrl: !0,
            data: {functionId: "wapWeiXinPay", body: JSON.stringify(b)},
            success: function (b) {
                b && "0" == b.code && b.deepLink && "" != b.deepLink ? (b._total = a._total, Common.Components.WapPayMsgBox._setFinishData(b), a._callWeixinPay(b), setTimeout(function () {
                    Ariel.Components.Loading.hide(), Common.Components.WapPayMsgBox.show()
                }, 5e3)) : b && "0" == b.code && b.mweb_url && "" != b.mweb_url ? (window.location.href = b.mweb_url, Ariel.Components.Loading.hide()) : (Ariel.Components.Loading.hide(), Ariel.Components.Pop.show(b.message ? b.message : "寰俊鏀粯澶辫触锛�"))
            }
        })
    }, _callWeixinPay: function (a) {
        if (a && a.deepLink) {
            var b = $(".weixin-wap-deeplink");
            (!b || b.length <= 0) && $("#viewport").append($('<a href="javascript:void(0);"  class="weixin-wap-deeplink"></a>')), $(".weixin-wap-deeplink").attr("href", a.deepLink), $(".weixin-wap-deeplink").click()
        }
    }, render: function () {
        if (this._isJdPayment ? ($(this._jdPeymentsWrapper).append(this._wrapper), $(this._jdPeymentsWrapper).show()) : ($(this._otherWrapper).append(this._wrapper), $(this._otherWrapper).show()), this._wrapper.hasClass("hide-payment") || this._wrapper.show(), "1" == window._floatLayer) {
            var a = Ariel.parseGetParameters();
            Common.Components.WapPayMsgBox._setFinishData({
                isH5: !0,
                mobliePayId: a.payId
            }), Common.Components.WapPayMsgBox.show()
        }
    }
}), WeixinInnerPayment = Ariel.extend(Payment, {
    _specialCss: "weixin-pay", _payType: "weiXinGzh", startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters();
        a.is_weixin() ? b.code ? (Ariel.Components.Loading.show(), Common.Net.ajax({
            url: "weiXinGzhPay.action",
            data: {weiXinGzhCode: b.code},
            success: function (c) {
                function d() {
                    WeixinJSBridge.invoke("getBrandWCPayRequest", c.params, function (c) {
                        Ariel.Components.Loading.hide(), "get_brand_wcpay_request:ok" == c.err_msg && window.Common.FinishPage.go(b, {_total: a._total})
                    })
                }

                c && "0" == c.code ? "undefined" == typeof WeixinJSBridge ? document.addEventListener ? document.addEventListener("WeixinJSBridgeReady", d, !1) : document.attachEvent && (document.attachEvent("WeixinJSBridgeReady", d), document.attachEvent("onWeixinJSBridgeReady", d)) : d() : (Ariel.Components.Loading.hide(), Ariel.Components.Pop.show(c.message ? c.message : "鏀粯澶辫触锛岃閲嶈瘯锛�"))
            }
        })) : Common.Net.ajax({
            url: "getXinGzhCodeUrl.action", data: {}, success: function (a) {
                a && "0" == a.code ? window.location.href = a.weiXinGzhshareUrl : Ariel.Components.Pop.show(a.message ? a.message : "鏀粯澶辫触锛岃閲嶈瘯锛�")
            }
        }) : Ariel.Components.Pop.show("鍙兘鍦ㄥ井淇″唴鏀粯锛�")
    }, is_weixin: function () {
        return "micromessenger" == navigator.userAgent.toLowerCase().match(/MicroMessenger/i)
    }
}), WeixinOtherPayment = Ariel.extend(Payment, {
    _specialCss: "weixin-other-pay",
    _payType: "weixinOtherPay",
    _maiDianPayType: "weiXinDaiFuPay",
    _SHARE_IMAGE_URL: "/img/pay/share.png",
    _duplicate: null,
    startPayAction: function () {
        var a = this;
        if (null != a._duplicate) {
            if ((new Date).valueOf() - a._duplicate < 500) return void (a._duplicate = null)
        } else a._duplicate = (new Date).valueOf();
        var b = Ariel.parseGetParameters();
        Ariel.mpingEvent("JDCashier_WeChatpayment", location.href, 0), setTimeout(function () {
            window.location.href = "other-pay-info.html?appId=" + b.appId + "&payId=" + b.payId + "&total=" + a._total + "&client=" + a._client
        }, 200)
    }
}), ApplePayment = Ariel.extend(Payment, {
    _specialCss: "apple-pay",
    _payType: "applePay",
    _maiDianPayType: "applePay",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="mark"></span><span class="other-desc"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters(), c = a._data || {};
        if (a.protocol) {
            a.protocol.params.params.payId = b.payId;
            var d = JSON.stringify(a.protocol.params);
            window.location.href = a.protocol.url + "?params=" + d
        } else window.location.href = 'openApp.jdMobile://communication?params={"type":"11","payId":"' + b.payId + '","hasAnimate":"' + c.hasAnimate + '"}'
    },
    setDefaultDescription: function () {
        var a = this;
        a._data;
        this._wrapper.attr("bindingVice", a._data.bindingVice), this._wrapper.attr("bindingMain", a._data.bindingMain), this._wrapper.attr("unbindingVice", a._data.unbindingVice), this._wrapper.attr("unbindingMain", a._data.unbindingMain), this._wrapper.attr("binding", a._data.binding);
        var b = parseInt(a._data.binding);
        if (1 == a._status) {
            switch (b) {
                case 0:
                    a._data.unbindingVice && this._wrapper.find(".other-desc").html(a._data.unbindingVice), a._data.unbindingMain && this._wrapper.find(".title-main").html(a._data.unbindingMain), a._wrapper.find(".title-vice").hide(), a._wrapper.find(".title-main").removeClass("title-main-hasvice");
                    break;
                case 1:
                    a._data.bindingVice && this._wrapper.find(".other-desc").html(a._data.bindingVice), a._data.bindingMain && this._wrapper.find(".title-main").html(a._data.bindingMain), a._wrapper.find(".title-vice").hide(), a._wrapper.find(".title-main").removeClass("title-main-hasvice");
                    break;
                case 2:
                    a._wrapper.addClass("lead-binding-card"), a._data.tip && this._wrapper.find(".other-desc").html(a._data.tip), a._data.bindingMain && this._wrapper.find(".title-main").html(a._data.unbindingMain), a._wrapper.find(".title-vice").hide(), a._wrapper.find(".title-main").removeClass("title-main-hasvice")
            }
            a._getPromotion()
        }
    },
    setOtherPaymentDesc: function () {
        var a = this;
        a._wrapper.hasClass("lead-binding-card") && a.startPayAction()
    },
    _getPromotion: function () {
        var a = this;
        Common.Net.ajax({
            url: "getApplePayPromotionText.action",
            type: "POST",
            data: {type: "0"},
            success: function (b) {
                0 == b.code && b.resultParam && "SUCCESS" == b.resultParam.retCode && b.resultParam.applePayPromotionText && (a._wrapper.find(".mark").text(b.resultParam.applePayPromotionText), a._wrapper.find(".mark").css("display", "inline-block"))
            }
        })
    }
}), UnionPayment = Ariel.extend(Payment, {
    _specialCss: "union-pay",
    _maiDianPayType: "yinLianPay",
    _payType: "cups",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = a._client, c = Ariel.parseGetParameters(), d = c.payId;
        switch (b) {
            case"wp":
                window.external.Notify('openApp.jdMobile://communication?params={"type":"4","payId":"' + d + '"}');
                break;
            case"androidPad":
                window.location.href = 'openApp.jdaPad://communication?params={"type":"4","payId":"' + d + '"}';
                break;
            case"iPad":
                0 == orderType ? window.location.href = 'openApp.jdMobile://communication?params={"type":"4","payId":"' + d + '"}' : window.location.href = 'openApp.jdaPad://communication?params={"type":"4","payId":"' + d + '"}';
                break;
            case"o2otjandroid":
                window.location.href = 'openApp.o2otjMobile://communication?params={"type":"4","payId":"' + d + '"}';
                break;
            default:
                window.location.href = 'openApp.jdMobile://communication?params={"type":"4","payId":"' + d + '"}'
        }
        try {
            var e = navigator.userAgent;
            if (e.match(/(iPhone\sOS)\s([\d_]+)/) || e.match(/(iPad).*OS\s([\d_]+)/)) {
                var f = {method: "notifyMessageToCustomWebView", params: {flag: "1"}};
                window.webkit.messageHandlers.JDAppUnite.postMessage(f)
            }
        } catch (a) {
            console.log(a)
        }
    }
}), _BaiTiaoPayment = Ariel.extend(Payment, {
    _specialCss: "blank-note-pay baitiao",
    _payType: "",
    _maiDianPayType: "baiTiaoPay",
    _isJdPayment: !0,
    _payParams: {},
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item hideLastPlan"><a href="javascript:void(0);" class="pay-list-link COMM_BOTTOM_1PX_BORDER" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="bt-recommend-icon"></span><span class="mark bt-choose-coupons"></span><span class="title-vice">---------</span></a><a href="javascript:void(0);" class="jdpayment-info choose-stage-bar"><span class="title stage-way">鍒嗘湡鏂瑰紡</span><span class="content selected-stage"></span></a><div class="blank-note-stage-layer blank-note-stage-layer1">     <ul class="blank-note-stage-list"></ul></div></li>',
    _stageData: {},
    _choosedStageData: {plan: $(".blank-note-pay .selected-stage em").attr("data-plan") || 1},
    allPlans: [1, 3, 6, 12, 24],
    _choosedCouponData: null,
    _defaultCoupon: null,
    startPayAction: function () {
        var a = this;
        a._client;
        a._choosedCouponData = a._choosedCouponData || "", a._payParams = {
            channelId: a.channelId,
            channelType: a.code,
            planId: a._choosedStageData.plan,
            couponId: a._choosedCouponData.couponId || "",
            planInfo: a._choosedStageData.planInfo || "",
            activityId: a._choosedCouponData.activityId || "",
            channelStatus: a._status,
            requireUUID: a.requireUUID
        };
        Ariel.parseGetParameters();
        a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a.showDefaultPreText(), a._total < 10 && $(a._wrapper).find(".blank-note-stage-layer1").addClass("total-less-10"), a._defaultCoupon = b.defaultCoupon, a._choosedStageData.plan = b.defaultPlanId;
                break;
            case 3:
                a.disablePayment(b.tip), $(a._wrapper).find(".blank-note-tip").append(b.tip), this._wrapper.find(".mark").removeClass("bt-choose-coupons");
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.find(".blank-note-stage-layer1").remove(), a._wrapper.find(".mark").css("display", "inline-block"), this._wrapper.find(".mark").removeClass("bt-choose-coupons")
        }
        a.recommand && a.recommandIcon ? a._wrapper.find(".bt-recommend-icon").css("background-image", "url(" + a.recommandIcon + ")") : a._wrapper.find(".bt-recommend-icon").remove()
    },
    setJdPaymentsDesc: function () {
        var a = this;
        1 == a._status && a._setDefaultBlankNoteLayers(), a._choosedStageData && 24 == a._choosedStageData.plan && a._wrapper.hasClass("hideLastPlan") && (a._wrapper.removeClass("hideLastPlan"), $(".show-or-hide-title").text("鏀惰捣"))
    },
    initTapEvent: function () {
        var a = this;
        this._wrapper.find(".bt-choose-coupons").bind("tap", function () {
            window.selectPament != a && $(a._wrapper).click(), Ariel.mpingEvent("MCashierNew_DiscountEntrance", location.href);
            var b = setTimeout(function () {
                (a.couponData.canUseCouponList && 0 == !a.couponData.canUseCouponList.length || a.couponData.cantUseCouponList && 0 == !a.couponData.cantUseCouponList.length) && Common.Components.chooseBTCouponLayer.show(), clearTimeout(b)
            }, 10)
        }), a._wrapper.delegate(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage", "tap", function () {
            var b = $(this);
            if (1 == a._plansExpand && (a.setJdPaymentsDesc(), a.selectPament()), !b.hasClass("checked")) {
                var c = parseInt(b.attr("data-li")), d = a.allPlans[c];
                a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.checked").removeClass("checked"), b.addClass("checked"), a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.judge-checked").removeClass("judge-checked"), b.addClass("judge-checked"), a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LOGO.selected").removeClass("selected"), b.find(".COMM_BLANK_NOTE_STAGE_LOGO").addClass("selected"),
                a._stageData && (a._choosedStageData = a._stageData[c]), a.setStageDataDesc(), a.selectPament(), Ariel.mpingEvent("MCashierNew_Instalment", location.href, d + "_1"), Common.Components.chooseBTCouponLayer.chooseToCoupon(a._getRecommendCoupon(d))
            }
        })
    },
    fillAndChooseStage: function () {
        var a = this;
        Common.Components.BlankNoteLayers.updataStageLayer(a._choosedCouponData, function (b, c, d, e, f) {
            var g = a._getOderByPlan(a._getShouldSelectStage());
            if (b) {
                var h = a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").eq(g);
                h.addClass("checked"), h.addClass("judge-checked");
                var i = h.find(".COMM_BLANK_NOTE_STAGE_LOGO"), j = a._choosedCouponData;
                j.discountLogoType ? ("mianxi" == j.discountLogoType ? i.removeClass("zhekou") : i.removeClass("mianxi"), i.addClass(j.discountLogoType), i.addClass("selected"), i.html(j.discountLogoText)) : (i.removeClass("mianxi"), i.removeClass("zhekou"), i.removeClass("selected"), i.html(""))
            }
            !f || "2" != f && 2 != f || e && (a._data.recommendCoupons = e), c && (a.couponData = c, Common.Components.chooseBTCouponLayer.initCouponLayer(c), Common.Components.chooseBTCouponLayer.chooseToCoupon(d, !0)), 4 == g && a._wrapper.hasClass("hideLastPlan") && (a._wrapper.removeClass("hideLastPlan"), $(".show-or-hide-title").text("鏀惰捣"), a._pageScroller.refresh()), a.judgeDefault(), b && (a._stageData = b), b && (a._choosedStageData = b[g]), a.setStageDataDesc(), setTimeout(function () {
                a._pageScroller.refresh()
            }, 1e3)
        })
    },
    setStageDataDesc: function () {
        var a = this;
        a._choosedStageData && (a.realAmount = a._choosedStageData.realPayAmount, a.setPayButton()), a._wrapper.find(".selected-stage").html("搴旇繕鎬婚<em data-plan=" + a._choosedStageData.plan + "> 楼" + a._choosedStageData.total + "</em>")
    },
    _setDefaultBlankNoteLayers: function () {
        var a = this, b = a._data || {};
        Common.Components.chooseBTCouponLayer.onSelectedCoupon = function (c, d) {
            a._choosedCouponData = -1 === c ? {
                plans: ["1", "3", "6", "12", "24"],
                couponInfo: d || b.preferentiaNum || b.baiTiaoText || "鏆傛棤鍙敤"
            } : a.couponData.canUseCouponList[c], a._setPreferentBarText(a._choosedCouponData.couponInfo), a.fillAndChooseStage()
        }, Common.Components.chooseBTCouponLayer.initCouponLayer(a.couponData), Common.Components.chooseBTCouponLayer.chooseToCoupon(a._choosedCouponData || a._defaultCoupon)
    },
    _getShouldSelectStage: function () {
        var a = this, b = a._choosedStageData.plan, c = a._choosedCouponData.plans || [1, 3, 6, 12, 24];
        return $.inArray(b, c) < 0 && $.inArray(b + "", c) < 0 && (b = a._choosedCouponData.defaultPlan || c[c.length - 1]), parseInt(b)
    },
    _getOderByPlan: function (a) {
        for (var b = this.allPlans, c = 0; c < b.length; c++) if (b[c] == a) return c;
        return -1
    },
    _getRecommendCoupon: function (a) {
        var b = this, c = b._data || {}, d = c.recommendCoupons, e = d ? d.length : 0;
        if (e > 0) for (var f = 0; f < e; f++) if (parseInt(d[f].defaultPlan) === parseInt(a)) return d[f];
        return !1
    },
    _setPreferentBarText: function (a) {
        var b = this, c = b._wrapper.find(".bt-choose-coupons");
        !a || "鏆備笉浣跨敤" == c.find("span").eq(0).text && "鏆備笉浣跨敤" == a || c.html("<span>" + a + '</span><span class="bt-coupons-icon"></span>')
    },
    showDefaultPreText: function () {
        var a, b = this, c = b._data || {};
        b.couponData && b.couponData.canUseCouponList && 0 != b.couponData.canUseCouponList.length || (a = c.baiTiaoText), c.defaultCoupon && c.defaultCoupon.preferentialWay && (a = c.defaultCoupon.preferentialWay), a && b.hasCoupon() ? (b._wrapper.find(".mark").html(a + '<span class="bt-coupons-icon"></span>'), b._wrapper.find(".mark").css("display", "inline-block")) : b._wrapper.find(".mark").css("display", "none")
    },
    hasCoupon: function () {
        return this.couponData.canUseCouponList && this.couponData.canUseCouponList.length > 0 || this.couponData.cantUseCouponList && this.couponData.cantUseCouponList.length > 0
    }
}), QianBaoPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "qianBaoPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    _choosedCouponData: {},
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), JdApplePayment = Ariel.extend(Payment, {
    _specialCss: "jd-apple-pay",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "JdApplePay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters(), c = a._data || {};
        window.location.href = 'openApp.jdMobile://communication?params={"type":"11","payId":"' + b.payId + '","payChannel":"jd","hasAnimate":"' + c.hasAnimate + '"}'
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a._getPromotion(b.preferentiaNum), JdApplePay = a;
                break;
            case 3:
                a.disablePayment(b.tip), $(a._wrapper).addClass("disabled-pay-list-item").attr("data-pop", b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block");
                break;
            case 7:
                a._wrapper.css("display", "none"), JdApplePay = a
        }
    },
    callJdApplePay: function () {
        var a = this;
        a._status = 1, a._getPromotion(), a.selectPament()
    },
    _getPromotion: function (a) {
        var b = this;
        Common.Net.ajax({
            url: "getApplePayPromotionText.action",
            type: "POST",
            data: {type: "0"},
            success: function (c) {
                0 == c.code && c.resultParam && "SUCCESS" == c.resultParam.retCode && c.resultParam.applePayPromotionText ? (b._wrapper.find(".mark").text(c.resultParam.applePayPromotionText), b._wrapper.find(".mark").show()) : a && (b._wrapper.find(".mark").text(a), b._wrapper.find(".mark").css("display", "inline-block"))
            }
        })
    }
}), BaiTiaoQuickPayment = Ariel.extend(Payment, {
    _specialCss: "blank-note-quick-pay",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "baiTiaoQuickPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item hideLastPlan"><a href="javascript:void(0);" class="pay-list-link COMM_BOTTOM_1PX_BORDER" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="bt-recommend-icon"></span><span class="mark"></span><span class="title-vice">---------</span></a><a href="javascript:void(0);" class="jdpayment-info choose-stage-bar"><span class="title stage-way">鍒嗘湡鏂瑰紡</span><span class="content selected-stage"></span></a><div class="blank-note-stage-layer blank-note-stage-layer2">     <p class="warnning-tip">璁㈠崟閲戦灏忎簬楼10锛屼粎鏀寔涓嶅垎鏈�</p>     <ul class="blank-note-stage-list"></ul></div></li>',
    _stageData: {},
    _choosedStageData: {plan: $(".blank-note-quick-pay .selected-stage em").attr("data-plan") || 1},
    _recommendCoupons: [],
    allPlans: [1, 3, 6, 12, 24],
    _choosedCouponData: null,
    _defaultCoupon: null,
    startPayAction: function () {
        var a = this, b = a._client;
        a._choosedCouponData = a._choosedCouponData || "", a._payParams = {
            channelId: a.channelId,
            channelType: a.code,
            planId: a._choosedStageData.plan || a.planId || "",
            couponId: a._choosedCouponData.couponId || "",
            activityId: a._choosedCouponData.activityId || "",
            channelStatus: a._status,
            requireUUID: a.requireUUID,
            planInfo: a._choosedStageData.planInfo || a.planInfo || ""
        };
        var c = Ariel.parseGetParameters();
        Common.Net.ajax({
            url: "isJDpayPaymentAcc.action", type: "POST", data: a._payParams, success: function (d) {
                if (!(d && d.success & d.isJDpay2Valid && d.isApp && "wp" != b)) return void (d.isJDpay2Valid && !d.isApp && d.cashierUrl ? (Ariel.Components.Loading.hide(), window.location.href = d.cashierUrl) : (Ariel.Components.Loading.hide(), Ariel.Components.Pop.show(d.message ? d.message : "鏀粯寮傚父锛岃閫夋嫨鍏朵粬鏀粯鏂瑰紡鏀粯")));
                if (a.protocol) {
                    a.protocol.params.params.payId = c.payId;
                    var e = JSON.stringify(a.protocol.params);
                    window.location.href = a.protocol.url + "?params=" + e
                } else switch (Ariel.Components.Loading.hide(), b) {
                    case"apple":
                    case"iPad":
                    case"android":
                        window.location.href = 'openApp.jdMobile://communication?params={"type":"12","payId":"' + c.payId + '"}';
                        break;
                    default:
                        window.location.href = 'openApp.jdMobile://communication?params={"type":"12","payId":"' + c.payId + '"}'
                }
            }
        })
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._recommendCoupons = b.recommendCoupons, a._status) {
            case 1:
                if (a._recommendCoupons && a._recommendCoupons.length > 0) {
                    var c = a._getRecommendCoupon(b.btqPlanId);
                    a._defaultCoupon = c, c && c.couponInfo && (a._wrapper.find(".mark").text(c.couponInfo), a._wrapper.find(".mark").css("display", "inline-block"))
                } else a._choosedCouponData = {
                    couponInfo: "鏃犲彲鐢�",
                    couponId: "",
                    activityId: "",
                    couponType: "",
                    defaultPlan: "",
                    plans: a.allPlans,
                    allPlans: a.allPlans,
                    channelType: a.code,
                    planRate: a.planRate
                };
                a._total < 10 && $(a._wrapper).find(".blank-note-stage-layer2").addClass("total-less-10"), a._titleMain = "婵€娲荤櫧鏉″苟鏀粯";
                break;
            case 3:
                a.disablePayment(b.tip), $(a._wrapper).find(".blank-note-tip").append(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.find(".blank-note-stage-layer2").remove(), a._wrapper.find(".blank-note-stage-layer2-switch").remove(), a._wrapper.find(".mark").css("display", "inline-block")
        }
        a.recommand && a.recommandIcon ? a._wrapper.find(".bt-recommend-icon").css("background-image", "url(" + a.recommandIcon + ")") : a._wrapper.find(".bt-recommend-icon").remove()
    },
    setJdPaymentsDesc: function () {
        var a = this;
        a._choosedStageData && 24 == a._choosedStageData.plan && a._wrapper.hasClass("hideLastPlan") && (a._wrapper.removeClass("hideLastPlan"), a._pageScroller.refresh()), a.setInitStageEvent()
    },
    initTapEvent: function () {
        var a = this;
        a._wrapper.delegate(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage", "tap", function () {
            var b = $(this);
            if (!b.hasClass("checked")) {
                var c = parseInt(b.attr("data-li")), d = a.allPlans[c];
                a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.checked").removeClass("checked"), b.addClass("checked"), a._choosedStageData = a._stageData[c], a.setStageDataDesc(), a.selectPament(), Ariel.mpingEvent("MCashierNew_Instalment", location.href, d + "_1"), a.chooseToCoupon(a._getRecommendCoupon(d)), a.fetchNotes()
            }
        })
    },
    _getRecommendCoupon: function (a) {
        var b = this, c = b._data || {}, d = c.recommendCoupons, e = d ? d.length : 0;
        if (e > 0) for (var f = 0; f < e; f++) if (parseInt(d[f].defaultPlan) === parseInt(a)) return b._choosedCouponData = {
            couponInfo: d[f].couponInfo,
            couponId: d[f].couponId,
            activityId: d[f].activityId,
            couponType: d[f].couponType,
            defaultPlan: d[f].defaultPlan,
            plans: d[f].plans,
            allPlans: b.allPlans,
            channelType: b.code,
            planRate: b.planRate
        }, d[f];
        return b._choosedCouponData = {
            couponInfo: "鏃犲彲鐢�",
            couponId: "",
            activityId: "",
            couponType: "",
            defaultPlan: "",
            plans: b.allPlans,
            allPlans: b.allPlans,
            channelType: b.code,
            planRate: b.planRate
        }, !1
    },
    setInitStageEvent: function () {
        var a, b = this;
        Common.Components.BaiTiaoQuickLayers.updataStageLayer(b._choosedCouponData, function (c) {
            a = parseInt(b._wrapper.find(".canChooseStage.checked").attr("data-li")), 4 == a && b._wrapper.hasClass("hideLastPlan") && (b._wrapper.removeClass("hideLastPlan"), b._pageScroller.refresh()), b._choosedStageData = c[a], b._stageData = c, b.setStageDataDesc(), b._pageScroller.refresh()
        })
    },
    setStageDataDesc: function () {
        var a = this;
        a.realAmount = a._choosedStageData.realPayAmount, a.setPayButton();
        1 == a._choosedStageData.plan || a._choosedStageData.plan;
        a._wrapper.find(".selected-stage").html("搴旇繕鎬婚<em data-plan=" + a._choosedStageData.plan + "> 楼" + a._choosedStageData.total + "</em>")
    },
    _getOderByPlan: function (a) {
        for (var b = this.allPlans, c = 0; c < b.length; c++) if (b[c] == a) return c;
        return -1
    },
    fetchNotes: function () {
        var a = this;
        Common.Components.BaiTiaoQuickLayers.updataStageLayer(a._choosedCouponData, function (b) {
            var c = a._getOderByPlan(a._choosedStageData.plan);
            a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.checked").removeClass("checked"), a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM").eq(c).addClass("checked"), 4 == c && a._wrapper.hasClass("hideLastPlan") && (a._wrapper.removeClass("hideLastPlan"), a._pageScroller.refresh()), a._stageData = b, a._choosedStageData = b[c], a.setStageDataDesc(), a._pageScroller.refresh()
        })
    },
    matchCoupon: function (a, b) {
        return !!a.couponId && (a.couponId.indexOf("nothing") < 0 ? a.couponId == b.couponId : !(!a.activityId || a.activityId != b.activityId))
    },
    chooseToCoupon: function (a) {
        var b = this, c = b._recommendCoupons || [], d = c.length;
        if (0 !== d && a) {
            if (a.couponId || a.activityId) for (var e = 0; e < d; e++) if (b.matchCoupon(a, c[e])) return b._wrapper.find(".mark").text(a.couponInfo), void b._wrapper.find(".mark").css("display", "inline-block")
        } else b._wrapper.find(".mark").text(), b._wrapper.find(".mark").css("display", "none")
    }
}), XiaoJinKuPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "xiaoJinKuPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"> <a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false">   <span class="pay-icon"></span>   <span class="title-main">----</span>   <span class="mark xjk-coupoon"></span>   <span class="title-vice">---------</span> </a></li>',
    _choosedCouponData: {},
    isFirst: !0,
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID,
            payMarketingUUID: a._choosedCouponData.payMarketingUUID || "",
            prizeId: a._choosedCouponData.prizeId || ""
        }, a._callJdPayAction(a._payParams)
    },
    _initEvent: function () {
        var a = this;
        this._wrapper.find(".xjk-coupoon").bind("tap", function () {
            Ariel.mpingEvent("MCashierNew_DiscountEntrance", location.href), a.selectPament(), a.isFirst && (a.setJdPaymentsDesc(), a.isFirst = !1), Common.Components.chooseCouponLayer.show()
        }), a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") || (Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), b.stopPropagation(), "1" == a._status && a.isFirst && a.setJdPaymentsDesc(), a.selectPament()))
        })
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a.couponData.couponAndCutOffs && a.couponData.couponAndCutOffs.length > 0 && b.preferentialWay && (a._wrapper.find(".mark").html("<span>" + b.preferentialWay + '</span><span class="coupons-icon"></span>'), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip), this._wrapper.find(".mark").removeClass("xjk-coupoon");
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block"), this._wrapper.find(".mark").removeClass("xjk-coupoon")
        }
    },
    setJdPaymentsDesc: function () {
        var a = this;
        a.couponData.couponAndCutOffs && a.couponData.couponAndCutOffs.length > 0 ? (a.setChoosedCouponEvent(), Common.Components.chooseCouponLayer.initCouponLayer(a.couponData), a.isFirst = !1, a._wrapper.find(".xjk-coupoon").css("display", "inline-block")) : (a._wrapper.find(".xjk-coupoon").hide(), a._wrapper.addClass("no-jdpayment-info"))
    },
    setChoosedCouponEvent: function () {
        var a = this;
        Common.Components.chooseCouponLayer.onSelectedCoupon = function (b) {
            b ? (a._wrapper.find(".xjk-coupoon").html("<span>" + b.promotionDesc + '</span><span class="coupons-icon"></span>'), a._choosedCouponData = b, a.realAmount = a._choosedCouponData.showAmount || a._choosedCouponData.realAmount || a._total) : (a._choosedCouponData = {}, a.realAmount = a._total, a._wrapper.find(".xjk-coupoon").text('<span>鏃犲彲鐢�</span><span class="coupons-icon"></span>')), a.selectPament()
        }
    }
}), SalesBankCardPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "cardpay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    _choosedCouponData: {},
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID,
            payMarketingUUID: a._choosedCouponData.payMarketingUUID || "",
            prizeId: a._choosedCouponData.prizeId || ""
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), BankCardPayment = Ariel.extend(Payment, {
    _specialCss: "jd-cart-pay",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    cartData: {},
    _maiDianPayType: "bankCardPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item" style="height:auto"><a href="javascript:void(0);" class="pay-list-link no-border" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main accessbility_bank" {$aria-label} role="text">----</span><span class="mark jd-choose-coupons"></span><span class="title-vice">---------</span></a></li>',
    _choosedCartData: {},
    _choosedCouponData: {},
    _defaultCoupon: null,
    clickFlag: !1,
    _switchFontObj: {0: "闆�", 1: "涓€", 2: "浜�", 3: "涓�", 4: "鍥�", 5: "浜�", 6: "鍏�", 7: "涓�", 8: "鍏�", 9: "涔�"},
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a._choosedCartData.channelId || a.channelId || "",
            channelStatus: a._status,
            channelType: a._choosedCartData.code || a.code || "",
            requireUUID: a.requireUUID,
            payMarketingUUID: a._choosedCouponData.payMarketingUUID || "",
            prizeId: a._choosedCouponData.prizeId || ""
        }, a._callJdPayAction(a._payParams)
    },
    _initEvent: function () {
        var a = this;
        a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a.clickFlag = !0, a._wrapper.hasClass("selected") || (Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), b.stopPropagation(), a.selectPament()))
        }), a._wrapper.find(".jd-choose-coupons").bind("tap", function () {
            if (a.clickFlag = !0, Ariel.mpingEvent("MCashierNew_CardDiscountClick", location.href), a._wrapper.hasClass("selected") || a._wrapper.addClass("selected"), event.preventDefault(), event.stopPropagation(), a._choosedCartData.allCoupons && a._choosedCartData.allCoupons.couponAndCutOffs && a._choosedCartData.allCoupons.couponAndCutOffs.length > 1 && Common.Components.chooseBankCouponLayer.show(), a._canUseCount && a._canUseCount > 1 && 1 == a._status) {
                var b = Ariel.parseGetParameters(), c = {appId: b.appId, payId: b.payId, channelId: a.channelId},
                    d = a._defaultCoupon;
                a._initBankCouponsLayers(), a._choosedCouponData && (d = a._choosedCouponData.payMarketingUUID), Common.Components.chooseBankCouponLayer.updateCouponLists(c, d)
            }
            a.selectPament()
        })
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a._defaultCoupon = b.defaultCoupon, a._choosedCartData = b.defaultCard, a._setCouponsDesc(), a._defaultCoupon && a._setDefChoosedCouponData(a._defaultCoupon, a._choosedCartData);
                break;
            case 3:
                a.disablePayment(b.tip), a._wrapper.find(".choose-cart-bar").hide();
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.find(".mark").css("display", "inline-block")
        }
        a._payParams = {
            channelId: a.channelId,
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }
    },
    _initBankCouponsLayers: function () {
        var a = this, b = a._data || {};
        Common.Components.chooseBankCouponLayer.onSelectedCoupon = function (c, d) {
            a._choosedCouponData = -1 === c ? {promotionDesc: d || b.preferentiaNum || "鏆傛棤鍙敤"} : d, a._setPreferentBarText(a._choosedCouponData.promotionDesc)
        }
    },
    _setDefChoosedCouponData: function (a, b) {
        var c = this;
        if (c._choosedCouponData = {payMarketingUUID: a}, b.allCoupons) for (var d = b.allCoupons.couponAndCutOffs || [], e = 0; e < d.length; e++) d[e].payMarketingUUID === a && (c._choosedCouponData.prizeId = d[e].prizeId, c.realAmount = d[e].showAmount || d[e].realAmount)
    },
    _setCouponsDesc: function () {
        var a = this;
        a._choosedCartData && a._choosedCartData.preferentiaNum ? a._wrapper.find(".mark").css("display", "inline-block") : $(a._wrapper).find(".mark").css("display", "none"), a._setPreferentBarText(a._choosedCartData.preferentiaNum)
    },
    setJdPaymentsDesc: function () {
    },
    _setBankCouponsLayers: function () {
    },
    setPayButtonDesc: function (a) {
        var b = a || (5 == this._status ? "缁勫悎鏀粯楼" + this._total : this._titleSimple + "鏀粯楼" + this.realAmount);
        $(".pay-next").html(b)
    },
    _setPreferentBarText: function (a) {
        var b = this, c = b._wrapper.find(".jd-choose-coupons");
        b._choosedCouponData && b._choosedCouponData.showAmount ? b.realAmount = b._choosedCouponData.showAmountStr || b._choosedCouponData.showAmount || b._total : b.realAmount = b._total, (b._isDefaultPayment || b.clickFlag) && b.setPayButtonDesc(), !a || "鏆備笉浣跨敤" == c.find("span").eq(0).text && "鏆備笉浣跨敤" == a || (b._choosedCartData && b._canUseCount > 1 ? c.html("<span>" + a + '</span><span class="jd-coupons-icon"></span>') : c.html("<span>" + a + "</span>"))
    },
    setSelectedCartEvent: function (a) {
        var b = this;
        a && a.channelName && b._wrapper.find(".title-main").html(a.channelName), a && a.logo && b._wrapper.find(".pay-icon").css("background-image", "url(" + a.logo + ")"), a && a.tip ? b._wrapper.find(".title-vice").html(a.tip).show() : b._wrapper.find(".title-vice").html("").hide();
        var c = a && a.otherCardInfo;
        c ? c.otherCardChannelID != a.channelId ? (b._wrapper.find(".selected-cart").text(c.otherCardDesc), "PROMOTION" != c.otherCardDescType && b._wrapper.find(".selected-cart").css("color", "#848689")) : b._wrapper.find(".selected-cart").text("") : b._wrapper.find(".choose-cart-bar").css("display", "none"), b.realAmount = a && a.realAmount || b._total, b._titleMain = a && "JDP_ADD_NEWCARD" == a.channelId ? "浣跨敤鏂板崱" : "閾惰鍗�", b._choosedCartData = a, b.cartData = a && a.allCoupons || {}
    },
    _switchBankTitleNumberToText: function (a) {
        var b = this;
        try {
            var c = b._titleMain.split("("), d = c[0], e = c[1], f = e.split(""), g = f.slice(0, 4);
            for (var h in g) b._switchFontObj.hasOwnProperty(g[h]) && (g[h] = b._switchFontObj[g[h]]);
            return d + ("(" + g[0] + g[1] + g[2] + g[3] + ")")
        } catch (a) {
            return ""
        }
    }
}), GangBenPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "gangBenPay",
    _choosedCouponData: {},
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), jdBalancePayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "jdBalancePay",
    _choosedCouponData: {},
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), JingBeanPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _choosedCouponData: {},
    _maiDianPayType: "jingBeanPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), JdQuickPayment = Ariel.extend(Payment, {
    _specialCss: "apple-pay-sf",
    _payType: "applePaySF",
    _maiDianPayType: "applePaySF",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link no-border" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="applePaySF-other-desc"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = Ariel.parseGetParameters();
        window.location.href = 'openApp.jdMobile://communication?params={"type":"14","payId":"' + a.payId + '","payChannel":"jd"}'
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._wrapper.addClass("lead-binding-card"), a._status) {
            case 1:
                b.promotionDesc && (a._wrapper.find(".mark").text(b.promotionDesc), a._wrapper.find(".mark").css("display", "inline-block")), b.newCashierTip && a._wrapper.find(".applePaySF-other-desc").html(b.newCashierTip), a._wrapper.find(".title-vice").hide(), a._wrapper.find(".title-main").removeClass("title-main-hasvice")
        }
    },
    setOtherPaymentDesc: function () {
        var a = this;
        a._wrapper.hasClass("lead-binding-card") && a.startPayAction()
    }
}), JdCreditCardPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _maiDianPayType: "creditcart",
    _isJdPayment: !0,
    _payParams: {},
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item hideLastPlan"><a href="javascript:void(0);" class="pay-list-link COMM_BOTTOM_1PX_BORDER" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="title-vice">---------</span></a><a href="javascript:void(0);" class="jdpayment-info choose-preferential-bar canTap first-item"><span class="title preferential-way">鍒嗘湡鏂瑰紡</span><span class="credit-choose-coupons"></span><span class="content selected-preferential credit"></span></a><div class="blank-note-stage-layer blank-note-stage-layer3">     <ul class="blank-note-stage-list"></ul></div></li>',
    _stageData: {},
    _choosedStageData: {plan: $(".selected-stage em").attr("data-plan") || 1},
    _choosedCreditData: null,
    _defaultBankInfo: null,
    _choosedCoupon: null,
    _creditRecommendCouponList: null,
    _allPlans: null,
    startPayAction: function () {
        var a = this;
        a._client;
        a._choosedCreditData = a._choosedCreditData || "", a._payParams = {
            channelId: a._choosedCreditData.bankId,
            channelType: a.code,
            planId: a._choosedStageData.plan,
            planInfo: a._choosedStageData.planInfo || "",
            merchantFeeSubSideBy: a._choosedStageData.merchantFeeSubSideBy || "",
            bankPlanRate: a._choosedStageData.bankPlanRate || "",
            channelStatus: a._status,
            requireUUID: a.requireUUID,
            bankCode: a._choosedCreditData.bankCode,
            isNewCard: a._choosedCreditData.isNewCard,
            couponId: a._choosedCoupon.couponId || "",
            activityId: a._choosedCoupon.activityId || ""
        };
        Ariel.parseGetParameters();
        a._callJdPayAction(a._payParams)
    },
    _initEvent: function () {
        var a = this;
        a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") || (Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), b.stopPropagation(), a.selectPament(), a.setJdPaymentsDesc()))
        }), a._wrapper.delegate(".choose-preferential-bar", "tap", function () {
            Ariel.mpingEvent("MCashierNew_CreditChange", location.href, a._maiDianPayType), Common.Components.chooseCreditLayer.onSelectedCart = function (b) {
                a.setChoosedCreditEvent(b)
            }, Common.Components.chooseCreditLayer.show()
        }), $(a._wrapper).delegate(".blank-note-stage-layer3-switch", "tap", function () {
            a._wrapper.hasClass("hideLastPlan") ? (Ariel.mpingEvent("MCashierPay_BTCheck", location.href, "open"), a._wrapper.removeClass("hideLastPlan"), a._wrapper.find(".show-or-hide-title").text("鏀惰捣"), a._pageScroller.refresh()) : (Ariel.mpingEvent("MCashierPay_BTCheck", location.href, "close"), a._wrapper.addClass("hideLastPlan"), a._wrapper.find(".show-or-hide-title").text("鏌ョ湅鍏ㄩ儴"), a._pageScroller.refresh())
        }), a._wrapper.delegate(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.canChooseStage", "tap", function () {
            var b = $(this);
            if (!b.hasClass("checked")) {
                var c = parseInt(b.attr("data-li")), d = a._allPlans[c];
                a._wrapper.find(".pay-list-item").removeClass("selected"), a._wrapper.addClass("selected"), a._wrapper.find(".COMM_BLANK_NOTE_STAGE_LAYER_LIST_ITEM.checked").removeClass("checked"), b.addClass("checked"), a.selectPament(), a.fetchCouponStageEvent(a._getRecommendCoupon(d)), Ariel.mpingEvent("MCashierNew_CreditPayWay", location.href, a._choosedStageData.plan + "_" + a._choosedStageData.bankName)
            }
        }), this._wrapper.find(".credit-choose-coupons").bind("tap", function (b) {
            b.preventDefault(), b.stopPropagation(), Ariel.mpingEvent("MCashierNew_InstalmentDiscountClick"), Common.Components.creditCouponListsLayer.updateCouponLists(a._choosedCreditData, a._choosedCoupon), a._wrapper.hasClass("selected") || a._wrapper.addClass("selected"), a.selectPament(), a.setCouponChoosedEvent()
        })
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a._defaultBankInfo = b.defaultBankInfo, a._choosedCreditData = b.defaultBankInfo, a._allPlans = b.defaultBankInfo && b.defaultBankInfo.defaultPlanNumList, b.defaultBankInfo && b.defaultBankInfo.bankNameShow && a._wrapper.find(".preferential-way").html(b.defaultBankInfo.bankNameShow), a._wrapper.find(".choose-cart-bar").attr("data-card-id", a.bankCode);
                break;
            case 3:
                a.disablePayment(b.tip), a._wrapper.find(".choose-cart-bar").hide();
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.find(".mark").css("display", "inline-block")
        }
    },
    setJdPaymentsDesc: function () {
        var a = this, b = a.creditCardData;
        b && Common.Components.chooseCreditLayer.initCardList(b, a._defaultBankInfo.uniqueChannelId), a.setChoosedCreditEvent()
    },
    setCouponChoosedEvent: function () {
        var a, b = this;
        Common.Components.creditCouponListsLayer.onSelectedCoupon = function (c) {
            a = parseInt(b._wrapper.find(".canChooseStage.checked").attr("data-li"));
            var d = b._allPlans && b._allPlans[a] || b._data.defaultPlan;
            b._choosedCoupon = c ? {
                canUse: c.canUse,
                couponId: c.couponId,
                couponType: c.couponType,
                couponTypeDesc: c.couponTypeDesc || "",
                activityId: c.activityId,
                activityType: c.activityType,
                defaultPlan: c.defaultPlan,
                plans: c.plans,
                couponInfo: c.couponInfo,
                bankCode: b._choosedCreditData.bankCode,
                bankId: b._choosedCreditData.bankId,
                isNewCard: b._choosedCreditData.isNewCard
            } : {
                couponInfo: "璇烽€夋嫨浼樻儬",
                couponTypeDesc: "璇烽€夋嫨浼樻儬",
                couponId: "",
                activityId: "",
                couponType: "",
                defaultPlan: d,
                activityType: "",
                plans: b._allPlans,
                bankCode: b._choosedCreditData.bankCode,
                bankId: b._choosedCreditData.bankId,
                isNewCard: b._choosedCreditData.isNewCard
            }, b.fetchCouponStageEvent(b._choosedCoupon)
        }
    },
    fetchCouponStageEvent: function () {
        var a, b = this;
        Common.Components.CreditBlankNoteLayers.updataStageLayerBecaseOfCoupon(b._choosedCoupon, function (c) {
            if (a = parseInt(b._wrapper.find(".canChooseStage.checked").attr("data-li")), c) a >= 4 && b._wrapper.hasClass("hideLastPlan") && ($(".show-or-hide-title").text("鏀惰捣"), b._wrapper.removeClass("hideLastPlan"), b._pageScroller.refresh()), b._choosedStageData = c && c[a], b._stageData = c, b.setStageDataDesc(), b._pageScroller.refresh(); else {
                b.realAmount = "0.00";
                var d = b._allPlans && b._allPlans[a];
                b._choosedStageData = {
                    total: "0.00",
                    realPayAmount: "0.00",
                    plan: d
                }, b.setPayButton(), b._wrapper.find(".selected-stage").html("")
            }
            b._setPreferentBarText(b._choosedCoupon.couponInfo)
        })
    },
    setChoosedCreditEvent: function (a) {
        var b = this;
        b._choosedCreditData = a, b._choosedCreditData = a ? {
            bankCode: a.bankCode,
            defaultPlanId: a.defaultPlanId,
            bankName: a.bankName,
            bankId: a.bankId,
            requireUUID: b.requireUUID,
            isNewCard: a.cardAd,
            bankNameShow: a.bankNameShow,
            creditRecommendCouponList: a.creditRecommendCouponList,
            channelId: a.channelId,
            recommendPlanId: a.recommendPlanId
        } : {
            bankCode: b._defaultBankInfo.bankCode,
            bankName: b._defaultBankInfo.bankName,
            defaultPlanId: b._defaultBankInfo.defaultPlan || "",
            bankId: b._defaultBankInfo.bankId || "",
            requireUUID: b.requireUUID,
            isNewCard: b._defaultBankInfo.isNewCard,
            bankNameShow: b._defaultBankInfo.bankNameShow,
            creditRecommendCouponList: b._defaultBankInfo.creditRecommendCouponList,
            channelId: b._defaultBankInfo.channelId,
            recommendPlanId: b._defaultBankInfo.defaultPlan || ""
        }, b._wrapper.find(".preferential-way").html(b._choosedCreditData.bankNameShow), b.setInitStageEvent(b._choosedCreditData)
    },
    setInitStageEvent: function () {
        var a, b = this;
        Common.Components.CreditBlankNoteLayers.updataStageLayerBecaseOfCard(b._choosedCreditData, function (c) {
            if (a = parseInt(b._wrapper.find(".canChooseStage.checked").attr("data-li")), c) {
                a >= 4 ? (b._wrapper.removeClass("hideLastPlan"), $(".show-or-hide-title").text("鏀惰捣"), b._pageScroller.refresh()) : (b._wrapper.hasClass("hideLastPlan") || b._wrapper.addClass("hideLastPlan"), b._wrapper.find(".show-or-hide-title").text("鏌ョ湅鍏ㄩ儴"), b._pageScroller.refresh());
                var d = c.planNumList && c.planNumList[a];
                if (c.canUseCount && c.canUseCount > 0) {
                    if (c.creditCouponList && c.creditCouponList.length > 0) {
                        var e = c.creditCouponList && c.creditCouponList[0];
                        b._choosedCoupon = {
                            canUse: e.canUse,
                            couponId: e.couponId,
                            couponType: e.couponType,
                            couponInfo: e.couponInfo,
                            couponTypeDesc: e.couponTypeDesc || "",
                            activityId: e.activityId,
                            activityType: e.activityType,
                            defaultPlan: e.defaultPlan,
                            plans: e.plans,
                            bankCode: b._choosedCreditData.bankCode,
                            bankId: b._choosedCreditData.bankId,
                            isNewCard: b._choosedCreditData.isNewCard
                        }
                    } else b._choosedCoupon = {
                        couponInfo: "璇烽€夋嫨浼樻儬",
                        couponTypeDesc: "璇烽€夋嫨浼樻儬",
                        couponId: "",
                        activityId: "",
                        couponType: "",
                        defaultPlan: d,
                        activityType: "",
                        plans: "",
                        bankCode: b._choosedCreditData.bankCode,
                        bankId: b._choosedCreditData.bankId,
                        isNewCard: b._choosedCreditData.isNewCard
                    };
                    b._creditRecommendCouponList = c.creditRecommendCouponList, b._wrapper.find(".mark").css("display", "inline-block"), b._wrapper.find(".credit-choose-coupons").show(), b._setPreferentBarText(b._choosedCoupon.couponInfo)
                } else b._wrapper.find(".credit-choose-coupons").hide(), b._wrapper.find(".mark").css("display", "none"), b._choosedCoupon = {
                    couponInfo: "鏆傛棤鍙敤浼樻儬",
                    couponTypeDesc: "鏆傛棤鍙敤浼樻儬",
                    couponId: "",
                    activityId: "",
                    couponType: "",
                    defaultPlan: d,
                    activityType: "",
                    plans: "",
                    bankCode: b._choosedCreditData.bankCode,
                    bankId: b._choosedCreditData.bankId,
                    isNewCard: b._choosedCreditData.isNewCard
                }, b._creditRecommendCouponList = [];
                b._choosedStageData = c.planList && c.planList[a], b._stageData = c.planList, b._allPlans = c.planNumList || b._defaultBankInfo.defaultPlanNumList, b.setStageDataDesc(), b._pageScroller.refresh()
            } else b.realAmount = "0.00", b.setPayButton(), b._wrapper.find(".selected-stage").html(""), b._wrapper.find(".credit-choose-coupons").css("display", "none")
        })
    },
    _getRecommendCoupon: function (a) {
        var b = this, c = parseInt(a), d = b._creditRecommendCouponList || [], e = d ? d.length : 0;
        if (e > 0) for (var f = 0; f < e; f++) if (parseInt(d[f].defaultPlan) === c) return b._choosedCoupon = {
            canUse: d[f].canUse,
            couponId: d[f].couponId,
            couponType: d[f].couponType,
            couponInfo: d[f].couponInfo,
            couponTypeDesc: d[f].couponTypeDesc || "",
            activityId: d[f].activityId,
            activityType: d[f].activityType,
            defaultPlan: d[f].defaultPlan,
            plans: d[f].plans,
            bankCode: b._choosedCreditData.bankCode,
            bankId: b._choosedCreditData.bankId,
            isNewCard: b._choosedCreditData.isNewCard
        }, b._choosedCoupon;
        return b._choosedCoupon = {
            couponInfo: "璇烽€夋嫨浼樻儬",
            couponTypeDesc: "璇烽€夋嫨浼樻儬",
            couponId: "",
            activityId: "",
            couponType: "",
            defaultPlan: c,
            activityType: "",
            plans: "",
            bankCode: b._choosedCreditData.bankCode,
            bankId: b._choosedCreditData.bankId,
            isNewCard: b._choosedCreditData.isNewCard
        }, b._choosedCoupon
    },
    setStageDataDesc: function () {
        var a = this;
        if (a.realAmount = a._choosedStageData && a._choosedStageData.realPayAmount, a._choosedCreditData.isNewCard) {
            var b = "鍒�" + a._choosedStageData.plan + "鏈熶粯娆� 楼" + a.realAmount;
            a.setPayButton(b)
        } else a.setPayButton();
        1 == a._choosedStageData.plan || a._choosedStageData.plan;
        a._wrapper.find(".selected-stage").html("搴旇繕鎬婚<em data-plan=" + a._choosedStageData.plan + "> 楼" + a._choosedStageData.total + "</em> (鍒�" + a._choosedStageData.plan + "鏈�)")
    },
    _setPreferentBarText: function (a) {
        var b = this, c = b._wrapper.find(".credit-choose-coupons");
        !a || "鏆備笉浣跨敤浼樻儬" == c.find("span").eq(0).text && "鏆備笉浣跨敤浼樻儬" == a || c.html("<span>" + a + '</span><span class="credit-coupons-icon"></span>')
    }
}), JingXiJinPayment = Ariel.extend(Payment, {
    _specialCss: "jd-jxj-pay",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    jxjData: {},
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item" style="height:auto"><a href="javascript:void(0);" class="pay-list-link no-border" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="jxj-mark"></span><span class="logo-icon"></span><div class="jxj-icon-container"><span class="title-vice jxj-info-icon">---------</span></div></a></li>',
    _choosedJxjData: {},
    _choosedCouponData: {},
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a._choosedJxjData.channelId || a.channelId || "",
            channelStatus: a._choosedJxjData.status || a._status || "",
            channelType: a._choosedJxjData.code || a.code || "",
            requireUUID: a.requireUUID,
            accountCode: a._choosedJxjData.accountCode || ""
        }, a._callJdPayAction(a._payParams)
    },
    _initEvent: function () {
        var a = this, b = this._data && this._data.jingXiJinList || [];
        a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") || $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), $(b.target).hasClass("logo-icon") || b.stopPropagation(), a.selectPament(), a.setJdPaymentsDesc())
        }), b.length > 1 ? a._wrapper.find(".choose-jxj-bar").show() : a._wrapper.find(".choose-jxj-bar").hide(), a._wrapper.delegate(".choose-jxj-bar", "tap", function () {
            3 !== a._status ? (a.selectPament(), Common.Components.chooseJingXiJinLayer.onSelectedJxj = function (b) {
                a.setSelectedJxjEvent(b), a.setPayButton()
            }) : Common.Components.chooseJingXiJinLayer.onSelectedJxj = function (b) {
                a.setSelectedJxjEvent(b)
            }, Common.Components.chooseJingXiJinLayer.show()
        }), a._wrapper.delegate(".logo-icon", "tap", function (b) {
            $(this).css("pointer-events", "none"), a._choosedJxjData.showSkuList && Common.Components.chooseJingXiJinLayer.initJxjSkuList(a._choosedJxjData)
        })
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a._wrapper.find(".list-no-use").text(""), a._wrapper.find(".jxj-mark").hide(), a._wrapper.find(".choose-jxj-bar").attr("data-jxj-id", a.channelId);
                break;
            case 3:
                a.disablePayment(b.tip), a._wrapper.find(".list-no-use").text("鏆傛棤鍙敤");
                break;
            case 5:
                a._wrapper.find(".list-no-use").text(""), a._wrapper.find(".jxj-mark").html("闇€缁勫悎鏀粯"), a._wrapper.find(".jxj-mark").css("display", "inline-block")
        }
        a._payParams = {
            channelId: a.channelId,
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a.setJdPaymentsDesc()
    },
    setJdPaymentsDesc: function () {
        var a = this, b = a._data.jingXiJinList || {};
        b && (Common.Components.chooseJingXiJinLayer.onSelectedJxj = function (b) {
            a.setSelectedJxjEvent(b)
        }, a._choosedJxjData.channelId ? Common.Components.chooseJingXiJinLayer.initJxjList(b, a._choosedJxjData.channelId) : Common.Components.chooseJingXiJinLayer.initJxjList(b, a._payParams.channelId))
    },
    setSelectedJxjEvent: function (a) {
        var b = this;
        b._wrapper.find(".title-main").html(a.channelName), b._wrapper.find(".pay-icon").css("background-image", "url(" + a.logo + ")"), 5 == a.status && a.statusDesc ? b._wrapper.find(".jxj-mark").html(a.statusDesc).show() : b._wrapper.find(".jxj-mark").html("").hide(), a.tip ? b._wrapper.find(".title-vice").html(a.tip).show() : b._wrapper.find(".title-vice").html("").hide(), a.showSkuList ? b._wrapper.find(".logo-icon").show() : b._wrapper.find(".logo-icon").hide(), b.realAmount = b._total, b._titleMain = 5 == a.status ? "缁勫悎" : "鑶ㄨ儉閲�", b._choosedJxjData = a
    }
}), BaDaTongtPayment = Ariel.extend(Payment, {
    _specialCss: "badatong-pay", _maiDianPayType: "OCPay", _payType: "", startPayAction: function () {
        var a = this, b = a._client, c = Ariel.parseGetParameters(), d = c.payId;
        switch (b) {
            case"android":
            case"apple":
                window.location.href = 'openApp.jdMobile://communication?params={"type":"6","payId":"' + d + '"}';
                break;
            case"wp":
                window.external.Notify('openApp.jdMobile://communication?params={"type":"6","payId":"' + d + '"}');
                break;
            case"iPad":
                window.location.href = 'openApp.jdMobile://communication?params={"type":"6","payId":"' + d + '"}';
                break;
            default:
                window.location.href = 'openApp.jdMobile://communication?params={"type":"6","payId":"' + d + '"}'
        }
    }, _initEvent: function () {
        var a = this;
        a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") || $(a._wrapper).hasClass("disabled-pay-list-item") || (Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), b.preventDefault(), b.stopPropagation(), a.selectPament(), a.getRate())
        })
    }, setDefaultDescription: function () {
        var a = this;
        a._isDefaultPayment && a.getRate()
    }, getRate: function () {
        var a = Ariel.parseGetParameters(), b = this, c = "鍏仈閫氭敮浠�";
        Ariel.Components.Loading.show(), Common.Net.ajax({
            url: "octopusPayRate.action",
            type: "POST",
            data: a,
            success: function (a) {
                if (Ariel.Components.Loading.hide(), a && "0" == a.code) c = "<div class='pay-btn-container'><p class='pay-btn-title'>" + a.orderPrice + "</p><p class='pay-btn-rate'>" + a.exBuyPrice + "</p></div>"; else {
                    var d = {title: "鑾峰彇姹囩巼澶辫触", description: "璇烽噸璇�", cancel: "鍙栨秷", next: "閲嶈瘯", onClickButton: b};
                    c = "鍏仈閫氭敮浠�", Ariel.Components.PopAgreeAndCancelBox.show(d, !0)
                }
                b.setPayButtonDesc(c)
            }
        })
    }, setPayButtonDesc: function (a) {
        this.setPayButton(a)
    }
}), JdQuickAndroidPayment = Ariel.extend(Payment, {
    _specialCss: "jd-quick-android",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "JdQuickAndroid",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters();
        a._data;
        window.location.href = 'openApp.jdMobile://communication?params={"type":"5","payId":"' + b.payId + '"}'
    },
    setPayButton: function (a) {
        var b = this, c = b._data || {};
        c.name && $(".pay-next").html(c.name + "鏀粯楼" + this.realAmount)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.androidIcon && a._wrapper.find(".quick-icon").html('<img src="' + b.androidIcon + '" alt="" width="100%" />'), a._getPromotion(b.preferentiaNum);
                break;
            case 3:
                $(a._wrapper).addClass("disabled-pay-list-item").attr("data-pop", b.tip), $(a._wrapper).find(".title-vice span").css("color", "#bababa"), b.androidIcon && a._wrapper.find(".quick-icon").html('<img src="' + b.androidIcon + '" alt="" width="100%" />');
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block"), b.androidIcon && a._wrapper.find(".quick-icon").html('<img src="' + b.androidIcon + '" alt="" width="100%" />');
                break;
            case 7:
                b.androidIcon && a._wrapper.find(".quick-icon").html('<img src="' + b.androidIcon + '" alt="" width="100%" />'), a._wrapper.css("display", "none"), JdApplePay = a
        }
    },
    callJdApplePay: function () {
        var a = this;
        a._status = 1, a._getPromotion(), a.selectPament()
    },
    _getPromotion: function (a) {
        var b = this;
        Common.Net.ajax({
            url: "getApplePayPromotionText.action",
            type: "POST",
            data: {type: "1"},
            success: function (c) {
                0 == c.code && c.resultParam && "SUCCESS" == c.resultParam.retCode && c.resultParam.applePayPromotionText ? (b._wrapper.find(".mark").text(c.resultParam.applePayPromotionText), b._wrapper.find(".mark").css("display", "inline-block")) : a && (b._wrapper.find(".mark").text(a), b._wrapper.find(".mark").css("display", "inline-block"))
            }
        })
    }
}), EarthBankCardPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "bankCardPay",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    _choosedCouponData: {},
    startPayAction: function () {
        var a = this;
        a._client, Ariel.parseGetParameters();
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code || "",
            requireUUID: a.requireUUID,
            payMarketingUUID: a._choosedCouponData.payMarketingUUID || "",
            prizeId: a._choosedCouponData.prizeId || ""
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                b.preferentiaNum && (a._wrapper.find(".mark").text(b.preferentiaNum), a._wrapper.find(".mark").css("display", "inline-block"));
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
        a._payParams = {
            channelId: a.channelId,
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }
    }
}), JincaiPayment = Ariel.extend(Payment, {
    _specialCss: "",
    _payType: "",
    _isJdPayment: !0,
    _payParams: {},
    _maiDianPayType: "",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this;
        Ariel.parseGetParameters(), a._data;
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID,
            productCode: a._data.productCode
        }, a._callJdPayAction(a._payParams)
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".mark").text("闇€缁勫悎鏀粯"), a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block")
        }
    }
}), HuaweiPayment = Ariel.extend(Payment, {
    _specialCss: "android-huawei",
    _payType: "",
    _maiDianPayType: "",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="mark"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = Ariel.parseGetParameters();
        a._data;
        window.location.href = 'openApp.jdMobile://communication?params={"type":"5","payId":"' + b.payId + '"}'
    },
    setDefaultDescription: function () {
        var a = this, b = a._data || {};
        switch (a._status) {
            case 1:
                a._getPromotion();
                break;
            case 3:
                a.disablePayment(b.tip);
                break;
            case 5:
                a._wrapper.find(".jdpayment-info").remove(), a._wrapper.addClass("no-jdpayment-info"), a._wrapper.find(".mark").css("display", "inline-block"), a._wrapper.find(".mark").text("闇€缁勫悎鏀粯");
                break;
            case 7:
                a._wrapper.css("display", "none"), JdApplePay = a
        }
    },
    _getPromotion: function () {
        var a = this;
        Common.Net.ajax({
            url: "getApplePayPromotionText.action",
            type: "POST",
            data: {type: "1"},
            success: function (b) {
                0 == b.code && b.resultParam && "SUCCESS" == b.resultParam.retCode && b.resultParam.applePayPromotionText && (a._wrapper.find(".mark").text(b.resultParam.applePayPromotionText), a._wrapper.find(".mark").css("display", "inline-block"))
            }
        })
    }
}), WingPayment = Ariel.extend(Payment, {
    _specialCss: "wing-pay",
    _maiDianPayType: "bestPay",
    _payType: "",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = (a._client, Ariel.parseGetParameters());
        b.payId;
        window.location.href = 'openApp.jdMobile://communication?params={"type":"1210","payId":"' + b.payId + '"}'
    }
}), WingWapPayment = Ariel.extend(Payment, {
    _specialCss: "wap-wing-pay",
    _maiDianPayType: "wapBestPay",
    _payType: "",
    _template: '<li class="list-item list-link-item pay-list-item other-pay-list-item"><a href="javascript:void(0);" class="pay-list-link" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="quick-icon"></span><span class="title-vice">---------</span></a></li>',
    startPayAction: function () {
        var a = this, b = (a._client, Ariel.parseGetParameters());
        b.payId;
        Common.Net.ajax({
            url: "../index.action",
            skipProcessUrl: !0,
            data: {functionId: "wapBestPay", body: JSON.stringify(b)},
            success: function (a) {
                a && "0" == a.code && a.mweb_url && "" != a.mweb_url ? window.location.href = a.mweb_url : (Ariel.Components.Loading.hide(), Ariel.Components.Pop.show(a.message ? a.message : "缈兼敮浠樺け璐ワ紒"))
            }
        })
    }
}), IntimatePayment = Ariel.extend(Payment, {
    _isJdPayment: !0,
    _maiDianPayType: "JDP_HONEYPAY",
    _jdPaymentTemplate: '<li class="list-item list-link-item pay-list-item jdpay-list-item" style="height:auto"><a href="javascript:void(0);" class="pay-list-link no-border" role="option" aria-selected="false"><span class="pay-icon"></span><span class="title-main">----</span><span class="mark"></span><span class="title-vice">---------</span></a>{$honeyPayToast}</li>',
    _initEvent: function () {
        var a = this;
        a._wrapper.find(".pay-list-link").bind("tap", function (b) {
            a._wrapper.hasClass("selected") || (Ariel.mpingEvent("MCashierNew_PaymentMethod", location.href, a._maiDianPayType + "_1_" + (a.recommand ? 1 : 0)), $(a._wrapper).hasClass("disabled-pay-list-item") || (b.preventDefault(), b.stopPropagation(), a.selectPament()))
        })
    },
    startPayAction: function () {
        var a = this;
        a._payParams = {
            channelId: a.channelId || "",
            channelStatus: a._status,
            channelType: a.code,
            requireUUID: a.requireUUID
        }, a._callJdPayAction(a._payParams)
    }
}), PayPage = Ariel.extend(Page, {
    _WEIXIN_KEY: "weixin",
    _WEIXIN_WAP_KEY: "wapWeiXinPay",
    _WEIXIN_INNER_KEY: "weiXinGzh",
    _UNION_PAY_KEY: "cups",
    _WEIXIN_OTHER_KEY: "weiXinDFPay",
    _APPLE_PAY_KEY: "applePay",
    _JD_APPLE_PAY_KEY: "QUICKPASS",
    _QQ_WALLET_KEY: "qqWalletPay",
    _BAITIAO_KEY: "BAITIAO",
    _BAITIAOQUICK_KEY: "BAITIAOQUICK",
    _QIANBAO_KEY: "QBB",
    _XIAOJINKU_KEY: "XJK",
    _BANKCARD_KEY: "BANKCARD",
    _SALESBANKCARD_KEY: "SALESBANKCARD",
    _GANGBEN_KEY: "GB",
    _JDBALANCE_KEY: "JDBALANCE",
    _JINGBEAN_KEY: "JINGBEAN",
    _NET_BANK_KEY: "newJdpay",
    _APPLE_PAY_SF_KEY: "applePaySF",
    _BADATONG_PAY_KEY: "octopusPay",
    _CREDIT_CARD_KEY: "CREDITINSTALLMENT",
    _JINGXIJIN_KEY: "JXJ",
    _JDQUICK_KEY: "ANDROID_QUICKPASS",
    _EARTHBANKCARD_KEY: "FOREIGNBANKCARD",
    _JINCAI_KEY: "JINCAI",
    _HUAWEI_PAY_KEY: "huaweiPay",
    _WING_PAY_KEY: "bestPay",
    _WING_PAY_WAP_KEY: "wapBestPay",
    _GRADUALLY_PAY_KEY: "GRADUALLY_PAY",
    _INTIMATE_PAY_KEY: "HONEYPAY",
    _data: {},
    _txtSafeCode: null,
    _defaultPaymentCode: null,
    _popProtocolSelected: !1,
    isliteApp: !1,
    init: function () {
        var a = navigator.userAgent.toLocaleLowerCase();
        a.indexOf("android") >= 0 && (this.isAndroid = !0, $("body").addClass("body-android")), a.indexOf("iphone") >= 0 && (this.isIphone = !0, $("body").addClass("body-iOS"));
        var b = this;
        b.isliteApp = window.isliteApp, $("#viewport").after('<div class="preload-image"></div>'), b._getUaDarkmode(), PayPage.__super__.init.call(b), b._getData(), b._changeSystemDarkSkin()
    },
    initEvent: function () {
        var a = this;
        PayPage.__super__.initEvent.call(a), a.startPay(), a.applePayBindCard(), $(".p-all-switch").bind("tap", function (b) {
            a._data._payParams && (a.otherPaymentTimer || (a.otherPaymentTimer = !0, setTimeout(function () {
                a.otherPaymentTimer = !1
            }, 500), Ariel.mpingEvent("MCashierNew_SecToastEntrance"), a._initOtherJDLayer(a._data._payParams.jdOtherPayChannelList), Common.Components.chooseOtherJDLayer.show(), a._data.graduallyPay && Ariel.mpingEvent("MCashierNew_SubStepRecExpo", location.href, a._data.graduallyPayInfo.remainingAmount))), b.preventDefault(), b.stopPropagation()
        }), $(".pop-protocol").live("click", function () {
            a._popProtocolSelected ? $(this).removeClass("checked") : $(this).addClass("checked"), a._popProtocolSelected = !a._popProtocolSelected
        })
    },
    startPay: function () {
        var a = this;
        $(".confirm-pay").bind("click", function () {
            selectedPayment && (Ariel.mpingEvent("MCashierNew_ConfirmPayment", location.href, selectedPayment._maiDianPayType + "_" + (a._data ? a._data._orderId : "")), selectedPayment.startPayAction(), selectedPayment.setPayButton("姝ｅ湪鏀粯"))
        })
    },
    _checkCashierProtect: function (a) {
        var b = navigator.userAgent.toLocaleLowerCase();
        try {
            if (b.indexOf("iphone") >= 0) {
                var c = {method: "notifyMessageToCustomWebView", params: {checkCashierNum: a}};
                window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.JDAppUnite && window.webkit.messageHandlers.JDAppUnite.postMessage && window.webkit.messageHandlers.JDAppUnite.postMessage(c)
            }
            if (b.indexOf("android") >= 0) {
                var d = {checkCashierNum: a};
                d = JSON.stringify(d), window.JdAndroid && window.JdAndroid.checkCashierProtect && window.JdAndroid.checkCashierProtect(d)
            }
        } catch (a) {
        }
    },
    _getData: function (a) {
        var b = this;
        Ariel.Components.Loading.show();
        var c = Ariel.parseGetParameters(), d = {};
        if (Common.Components.changeSkin(c), ["code", "changePay", "graduallyPayFlag", "graduallyPayAmount"].forEach(function (a) {
            c[a] && (d[a] = c[a])
        }), void 0 !== a && (a.cancelGraduallyPay && (delete d.graduallyPayFlag, delete d.graduallyPayAmount), a.params)) for (var e in a.params) a.params.hasOwnProperty(e) && (d[e] = a.params[e]);
        c.isCombinePay && 1 == c.isCombinePay && (d.refresh = 1, Ariel.mpingEvent("MCashierPay_GroupSDKQuitRefresh", location.href)), d.lastPage = document.referrer, Common.Net.ajax({
            url: "index.action", type: "POST", data: d, success: function (a) {
                if (Ariel.Components.Loading.hide(), !a) return $(".pay-list-item").show(), $(".JS-page-scorller").show(), $(".home-notice-wrap").hide(), $(".order-bar").remove(), $(".new-pay-disabled").show(), void $(".new-pay-bar-container").addClass("new-jd-pay-disabled");
                if (b.resetPaymentHTML(), a.payParamsObject) {
                    if (b._addIndexDescription(a), b.isliteApp && b._resolveLiteAppData(a.payParamsObject), a.payParamsObject.orderExceptionInfo && b._showOrderExceptionInfo(a), a.payParamsObject.commonPopupInfo && Ariel.isNotEmptyObject(a.payParamsObject.commonPopupInfo) && b._showCommonDialog(a.payParamsObject.commonPopupInfo), a.payParamsObject.combineToastFlag && Ariel.Components.Pop.show(a.payParamsObject.combineToastMessage || "褰撳墠璁㈠崟涓嶅彲鏀粯锛岃鎮ㄧ◢鍚庨噸璇曟垨鑰呰仈绯诲鏈嶏紙1001锛�"), a.payParamsObject.jdPayChannelList) for (var c = 0; c < a.payParamsObject.jdPayChannelList.length; c++) {
                        var d = a.payParamsObject.jdPayChannelList[c];
                        d.code == b._INTIMATE_PAY_KEY && 1 == d.guidOpenHoneyPay && (Ariel.guidOpenHoneyPay = !0)
                    }
                    if (b._formatData(a), b._displayHeader("html5" == a.payParamsObject.version ? 1 : 0), a.payParamsObject.mobile && a.payParamsObject.mobileKey) try {
                        var e = new JSEncrypt, f = Ariel.hexCharToStr(a.payParamsObject.mobileKey),
                            g = f.split("").reverse().join("");
                        e.setPrivateKey(g);
                        var h = e.decrypt(a.payParamsObject.mobile);
                        h && b._displayMobile(h)
                    } catch (a) {
                    }
                }
                a.payParamsObject.success ? (b._initAllPayments(), b._initAllOtherPayments(), b._displayAllPayWrap(), b.updateInstallment(), b._displayAllJDPay(a), $(".newPay-btn-container").show(), b._pageScroller.refresh()) : (a.payParamsObject.tip && Ariel.Components.Pop.show(a.payParamsObject.tip), $(".pay-list-item").show(), $(".JS-page-scorller").show(), $(".p-other-pay-title-bar").show().text("璇烽€夋嫨鏀粯鏂瑰紡"), $(".home-notice-wrap").hide(), $(".order-bar").remove(), b.isliteApp ? b._resolveLiteAppAbnormal() : ($(".new-pay-disabled").show(), $(".new-pay-bar-container").addClass("new-jd-pay-disabled"), b._disableAllPayments(a.payParamsObject.tip))), a.payParamsObject.demotionWay && ($(".new-pay-disabled").show(), $(".new-pay-bar-container").addClass("new-jd-pay-disabled"), $(".new-pay-disabled-text").text(a.payParamsObject.demotionWay));
                var i = $("#viewport").height() - $(".order-bar-wrap").height();
                $(".page-ct").css("height", i), b._pageScroller.refresh();
                var j = a.payParamsObject && a.payParamsObject.checkCashierNum || "0";
                b._checkCashierProtect(j), b._sendMonitorData()
            }
        })
    },
    _showCommonDialog: function (a) {
        var b = navigator.userAgent.toLocaleLowerCase();
        try {
            if (b.indexOf("iphone") >= 0) {
                var c = {method: "notifyMessageToCustomWebView", params: {showCommonDialog: a}};
                window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.JDAppUnite && window.webkit.messageHandlers.JDAppUnite.postMessage && window.webkit.messageHandlers.JDAppUnite.postMessage(c)
            }
            if (b.indexOf("android") >= 0) {
                var d = JSON.stringify(a);
                window.JdAndroid && window.JdAndroid.showCommonDialog && window.JdAndroid.showCommonDialog(d)
            }
        } catch (a) {
        }
    },
    _resolveLiteAppData: function (a) {
        try {
            a.jdPayChannelList = [], a.jdOtherPayChannelList = [], a.jdPayStatus = "1";
            var b = [];
            if (a.payChannelList && a.payChannelList.length) for (var c = 0; c < a.payChannelList.length; c++) {
                var d = a.payChannelList[c];
                d.code != this._WEIXIN_KEY && d.code != this._WEIXIN_OTHER_KEY || b.push(d)
            }
            a.payChannelList = b, $(".new-pay-bar-container").hide(), $(".pay-list-block").hide()
        } catch (a) {
        }
    },
    _resolveLiteAppAbnormal: function () {
        new WeixinPayment({
            _isDefaultPayment: !1,
            _titleMain: "寰俊鏀粯",
            _status: "3"
        }).render(), new WeixinOtherPayment({_isDefaultPayment: !1, _titleMain: "寰俊濂藉弸浠ｄ粯", _status: "3"}).render()
    },
    _getUaDarkmode: function () {
        window.getDarkmode = function (a) {
            var b = JSON.parse(a);
            b && 1 == b.data ? $("body").addClass("is-dark-mode") : $("body").removeClass("is-dark-mode")
        };
        var a = {
            routerURL: "router://JDWebViewBusinessModule/getJDAppearanceState",
            routerParam: "",
            callBackName: "getDarkmode",
            callBackId: "payIndex"
        };
        window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.JDAppUnite && window.webkit.messageHandlers.JDAppUnite.postMessage && window.webkit.messageHandlers.JDAppUnite.postMessage({
            method: "callSyncRouterModuleWithParams",
            params: JSON.stringify(a)
        })
    },
    _changeSystemDarkSkin: function () {
        window.jdAppearanceDidChangedNotification = function (a) {
            1 == a ? $("body").addClass("is-dark-mode") : $("body").removeClass("is-dark-mode")
        }
    },
    _addIndexDescription: function (a) {
        var b = this;
        window._floatLayer = a.payParamsObject.floatLayer, a.payParamsObject.homeLetter && b._initNoticeTip(a.payParamsObject.homeLetter);
        var c = a.payParamsObject.indexPopupConfig || {}, d = c.content || "瓒呰繃鏀粯鏃舵晥鍚庤鍗曞皢琚彇娑堬紝璇峰敖蹇畬鎴愭敮浠樸€�", e = {
            dialogSwitch: a.payParamsObject.dialogSwitch ? 1 : 0,
            title: c.title || "",
            content: d,
            rightBtn: c.backBtn || "",
            leftBtn: c.continueBtn || "",
            source: "balance" == c.from ? "banlance" : c.from,
            orderType: a.payParamsObject.orderType || 0
        };
        e.rightBtnUrl = c.backBtnUrl, c.closeType && (e.closeType = c.closeType), c.urlParam && (e.rightBtnUrl += "?params=" + JSON.stringify(c.urlParam), e.rightBtnUrl = e.rightBtnUrl.replace("[", ""), e.rightBtnUrl = e.rightBtnUrl.replace("]", ""), e.source && (e.rightBtnUrl = e.rightBtnUrl.replace("{$source}", e.source)));
        var f = JSON.stringify(e);
        closeIndexPage = function () {
            return d
        }, closeIndexPageSwitch = function () {
            return f
        }, window.JdAndroid && window.JdAndroid.setDialogTips && window.JdAndroid.setDialogTips(d), window.JdAndroid && window.JdAndroid.setConfigJson && window.JdAndroid.setConfigJson(f), a.payParamsObject.needVerify && (b._earthPopTitle = a.payParamsObject.indexInfo || "璇锋偍纭浠ヤ笅淇℃伅", b._verifyCustomer()), removePayLoading = function () {
            Ariel.Components.Loading.hide(), selectedPayment && selectedPayment.setPayButton()
        }
    },
    _showOrderExceptionInfo: function (a) {
        var b = this;
        a.payParamsObject.homeLetter && b._initNoticeTip(a.payParamsObject.homeLetter);
        var c = a.payParamsObject.orderExceptionInfo || {}, d = {
            content: c.content || "",
            rightBtn: c.checkOrder || "",
            leftBtn: c.close || "",
            mtaParams: a.payParamsObject.orderId
        }, e = {
            content: c.content || "",
            rightBtn: c.checkOrder || "",
            leftBtn: c.close || "",
            urlOpen: c.urlOpen || "",
            mtaParams: a.payParamsObject.orderId
        };
        d.rightBtnUrl = c.urlOpen, c.urlParam && (d.rightBtnUrl += "?params=" + JSON.stringify(c.urlParam), e.urlParam = encodeURI(JSON.stringify(c.urlParam)));
        var f = JSON.stringify(d);
        showNotifyDialog = function () {
            return f
        };
        navigator.userAgent;
        if ("apple" == a.payParamsObject.client) if (b.isliteApp) {
            var g = {method: "notifyMessageToCustomWebView", params: {orderStatus: "21"}};
            window.webkit.messageHandlers.JDAppUnite.postMessage(g)
        } else window.location.href = 'jdmobileCashier://orderPopToast?params={"orderStatus":"21"}';
        var h = JSON.stringify(e);
        window.JdAndroid && window.JdAndroid.showNotifyDialog && window.JdAndroid.showNotifyDialog(h), removePayLoading = function () {
            Ariel.Components.Loading.hide(), selectedPayment && selectedPayment.setPayButton()
        }
    },
    _initPrintFinger: function (a) {
        pin = a.identityKey;
        var b = document.getElementsByTagName("head")[0], c = document.createElement("script");
        c.src = "//payrisk.jd.com/m.html", b.appendChild(c), c.onload = c.onreadystatechange = function (a) {
            if (!this.readyState || "loading" != this.readyState) {
                var c = document.createElement("script");
                c.type = "text/javascript", c.src = "//payrisk.jd.com/js/m.js", b.appendChild(c)
            }
        }
    },
    _verifyCustomer: function () {
        var a = this;
        Common.Components.HelpDialog.show(Common.Components.HelpDialog.AUTHENTICATION, a._earthPopTitle), $(".confirm-auther-btn").live("click", function () {
            var b = $.trim($("#holderName").val()), c = $.trim($("#holderId").val()), d = Ariel.parseGetParameters();
            return b && c ? a._popProtocolSelected ? (d.holderName = a.encrypt(encodeURI(b)), d.holderId = a.encrypt(c), d._format_ = "json", Ariel.Components.Loading.show(), void Common.Net.ajax({
                url: "verify.action",
                type: "POST",
                data: d,
                success: function (b) {
                    Ariel.Components.Loading.hide();
                    var c = b.verifyInfo || "韬唤淇℃伅楠岃瘉鏈€氳繃";
                    b.success ? (Common.Components.HelpDialog.hide(), Ariel.Components.Pop.show(c)) : (Common.Components.HelpDialog.hide(), Ariel.Components.Pop.show(c, function () {
                        Common.Components.HelpDialog.show(Common.Components.HelpDialog.AUTHENTICATION, a._earthPopTitle), a._popProtocolSelected = !1
                    }))
                }
            })) : void Ariel.Components.Pop.show("璇峰悓鎰忎笅鏂瑰崗璁紝鍐嶈繘琛屼笅涓€姝ユ搷浣�", null, !0) : (Common.Components.HelpDialog.hide(), void Ariel.Components.Pop.show("璇疯緭鍏ユ纭殑濮撳悕鍜岃韩浠借瘉鍙�", function () {
                Common.Components.HelpDialog.show(Common.Components.HelpDialog.AUTHENTICATION, a._earthPopTitle), a._popProtocolSelected = !1
            }))
        })
    },
    _formatData: function (a) {
        var b = this;
        if (a.payParamsObject.success) {
            b._data = {
                graduallyPay: a.payParamsObject.graduallyPay,
                graduallyPayInfo: a.payParamsObject.graduallyPayInfo,
                client: a.payParamsObject.client,
                total: a.payParamsObject.payprice,
                jdPayStatus: a.payParamsObject.jdPayStatus || 0,
                paymentList: [],
                otherPaymentList: [],
                countdownTime: a.payParamsObject.countdownTime,
                countdownPopInfo: a.payParamsObject.countdownPopInfo,
                _jdOtherPaymentsNameList: [],
                _jdOtherPaymentsName: "",
                preSellData: a.payParamsObject.preSellData,
                _payParams: a.payParamsObject,
                _orderId: a.payParamsObject.orderId
            };
            var c = a.payParamsObject;
            b._defaultPaymentCode = b._getDefaultPayment(c), b._formatOtherPmentData(c.payChannelList, "otherPayment", c), 0 == b._data.jdPayStatus && b._formatPmentData(c.jdPayChannelList, "jdPayment", c)
        }
    },
    _formatOtherPmentData: function (a, b, c) {
        if (a && 0 != a.length) {
            var d = this;
            d._data.otherPaymentList = [];
            for (var e = a.length, f = 0; f < e; f++) {
                var g = a[f];
                g.code == d._NET_BANK_KEY && 0 == g.status && (d._data.jdPayStatus = 1), d._data.otherPaymentList[f] = {
                    recommand: g.recommand || !1,
                    recommandIcon: g.recommandIcon || "",
                    uniqueChannelId: g.uniqueChannelId,
                    canUseCount: g.canUseCount || 0,
                    code: g.code,
                    channelName: g.channelName,
                    payWay: g.payWay,
                    payPositionType: b,
                    desc: g.tip,
                    status: g.status,
                    data: d._getPaymentData(g, c),
                    isDefault: d._getDefPaymentFlag(g),
                    logo: g.logo || !1,
                    realAmount: g.realAmount || d._data.total,
                    couponData: g.allCoupons || g.baiTiaoCoupons || "",
                    requireUUID: c.requireUUID,
                    channelId: g.channelId,
                    planInfo: g.planInfo,
                    planId: g.planId,
                    plansExpand: !1,
                    defaultPlanId: g.defaultPlanId,
                    planRate: g.planRate,
                    isNewFlag: g.isNewFlag,
                    protocol: g.protocol,
                    creditCardData: g.bindingCardArray
                }
            }
        }
    },
    _formatPmentData: function (a, b, c) {
        if (a && 0 != a.length) {
            var d = this;
            d._data.paymentList = [];
            for (var e = a.length, f = 0; f < e; f++) {
                var g = a[f];
                "閾惰鍗�" == g.channelName && (g = d._getFirstBankCard(a, c)), g.code == d._NET_BANK_KEY && 0 == g.status && (d._data.jdPayStatus = 1), g.code === this._CREDIT_CARD_KEY && this.pingbackForCreditCard(g);
                var h;
                h = g.code == d._INTIMATE_PAY_KEY && g.channelNamePre && g.channelNameMiddle && g.channelNameTail ? g.channelNamePre + "<span class='title-main-relation'>" + g.channelNameMiddle + "</span>" + g.channelNameTail : g.channelName, d._data.paymentList[f] = {
                    recommand: g.recommand || !1,
                    recommandIcon: g.recommandIcon || "",
                    channelNamePre: g.channelNamePre || "",
                    channelNameMiddle: g.channelNameMiddle || "",
                    channelNameTail: g.channelNameTail || "",
                    initiativeSelected: g._initiativeSelected,
                    uniqueChannelId: g.uniqueChannelId,
                    canUseCount: g.canUseCount,
                    code: g.code,
                    channelName: h,
                    payWay: g.payWay,
                    payPositionType: b,
                    desc: g.tip,
                    status: g.status,
                    data: d._getPaymentData(g, c),
                    isDefault: d._getDefPaymentFlag(g),
                    logo: g.logo || !1,
                    realAmount: g.realAmount || d._data.total,
                    couponData: g.allCoupons || g.baiTiaoCoupons || "",
                    requireUUID: c.requireUUID,
                    channelId: g.channelId,
                    planInfo: g.planInfo,
                    planId: g.planId,
                    plansExpand: !1,
                    defaultPlanId: g.defaultPlanId,
                    planRate: g.planRate,
                    isNewFlag: g.isNewFlag,
                    protocol: g.protocol,
                    creditCardData: g.bindingCardArray
                }
            }
            d._getJdOtherPaymentsName(a)
        }
    },
    pingbackForCreditCard: function (a) {
        a.bindingCardArray && a.bindingCardArray.length && a.bindingCardArray.forEach(function (a) {
            Ariel.mpingEvent("MCashierNew_InstalmentDiscountExpo", location.href, a.creditCouponList && a.creditCouponList.length > 0 ? "A" : "B")
        })
    },
    _getDefPaymentFlag: function (a) {
        var b = this;
        if (a.uniqueChannelId) {
            if (a.uniqueChannelId == b._defaultPaymentCode) return !0
        } else if (a.code == b._defaultPaymentCode) return !0;
        return !1
    },
    _getFirstBankCard: function (a, b) {
        for (var c = {}, d = 0; d < a.length; d++) c[a[d].uniqueChannelId] = 1;
        for (var d = 0; d < b.jdOtherPayChannelList.length; d++) {
            var e = b.jdOtherPayChannelList[d];
            if ("BANKCARD" === e.code && 1 != c[e.uniqueChannelId]) return e
        }
    },
    _getJdOtherPaymentsName: function (a) {
        for (var b = this, c = [], d = "", e = {}, f = !0, g = [], h = b._data._payParams.jdOtherPayChannelList || [], i = 0; i < a.length; i++) e[a[i].uniqueChannelId] = 1, "BANKCARD" === a[i].code && "JDP_ADD_NEWCARD" === a[i].channelId && 1 == a[i].status && (f = !1);
        for (var i = 0; i < h.length; i++) {
            var j = h[i].uniqueChannelId;
            1 != e[j] && 1 == h[i].status && g.push(h[i])
        }
        for (var i = 0; i < h.length; i++) {
            var j = h[i].uniqueChannelId;
            if (1 != e[j] && "BANKCARD" === h[i].code && "JDP_ADD_NEWCARD" !== h[i].channelId) {
                c.push("鍏朵粬閾惰鍗�");
                break
            }
        }
        if (f) for (var i = 0; i < g.length; i++) if ("BANKCARD" === g[i].code && "JDP_ADD_NEWCARD" === g[i].channelId && 1 == g[i].status) {
            c.push("缁戝畾鏂板崱");
            break
        }
        for (var i = 0; i < g.length; i++) if ("SALESBANKCARD" != g[i].code && "BANKCARD" != g[i].code) {
            c.push(g[i].channelSimpleName || "");
            break
        }
        c.length > 0 ? (c.length > 2 && c.pop(), d = c.join("銆�")) : d = "鏌ョ湅", b._data._jdOtherPaymentsName = d
    },
    _getDefaultPayment: function (a) {
        var b = this;
        if (a.recentUserPayManner && a.recentUserPayManner != b._NET_BANK_KEY) {
            if (a.payChannelList) for (var c = 0; c < a.payChannelList.length; c++) if (a.recentUserPayManner == a.payChannelList[c].code && "1" == a.payChannelList[c].status) return a.recentUserPayManner;
            if (a.jdPayChannelList) for (var c = 0; c < a.jdPayChannelList.length; c++) if (a.jdPayChannelList[c].uniqueChannelId == a.recentUserPayManner && ("1" == a.jdPayChannelList[c].status || "5" == a.jdPayChannelList[c].status)) return a.recentUserPayManner
        }
        if (a.jdPayChannelList && 0 == a.jdPayStatus) for (var c = 0; c < a.jdPayChannelList.length; c++) if ("1" == a.jdPayChannelList[c].status || "5" == a.jdPayChannelList[c].status) return a.jdPayChannelList[c].uniqueChannelId;
        if (a.payChannelList) for (var c = 0; c < a.payChannelList.length; c++) if ("1" == a.payChannelList[c].status && a.payChannelList[c].code != b._NET_BANK_KEY) return a.payChannelList[c].code
    },
    _displayAllJDPay: function (a) {
        if (a.payParamsObject.jdOtherPayChannelList && a.payParamsObject.jdOtherPayChannelList.length) {
            var b = this, c = a.payParamsObject.jdOtherPayChannelList;
            Common.Components.chooseOtherJDLayer.onSelectedCart = function (a) {
                if (a) {
                    if (a.code === b._GRADUALLY_PAY_KEY) return b.installment.show(), void Ariel.mpingEvent("MCashierNew_SubStepRec", location.href, b._data.graduallyPayInfo.remainingAmount);
                    selectedPayment = a, b._updateJDPayment(a), "JDP_ADD_NEWCARD" == a.channelId && (Ariel.mpingEvent("MCashierNew_ConfirmPayment", location.href, selectedPayment._maiDianPayType + "_" + (b._data ? b._data._orderId : "")), selectedPayment.startPayAction(), selectedPayment.setPayButton("姝ｅ湪鏀粯"))
                }
            }, Ariel.mpingEvent("MCashierNew_SecToastEntranceExpo"), b._initOtherJDLayer(c)
        }
    },
    _initOtherJDLayer: function (a) {
        Common.Components.chooseOtherJDLayer.setIsGraduallyPayMode(this.isGraduallyPayMode()), Common.Components.chooseOtherJDLayer.initCardList(a || [], selectedPayment && selectedPayment.uniqueChannelId ? selectedPayment.uniqueChannelId : "")
    },
    _updateJDPayment: function (a) {
        $(".jd-pay-list").html("");
        for (var b = this, c = a.uniqueChannelId, d = b._data.paymentList, e = b._data._payParams.jdOtherPayChannelList, f = [], g = 0; g < e.length; g++) e[g].uniqueChannelId === c && f.push(e[g]);
        for (var h = 0; h < d.length; h++) {
            var i = d[h].uniqueChannelId, j = d[h].status;
            if (i !== c && 7 !== j && 0 !== j) for (var g = 0; g < e.length; g++) i === e[g].uniqueChannelId && f.push(e[g])
        }
        for (; f.length > 3;) f.pop();
        b._defaultPaymentCode = c, b._formatPmentData(f, "jdPayment", b._data._payParams), b._initAllPayments(), $(".p-jd-switch-pays").text(b._data._jdOtherPaymentsName).attr({"aria-hidden": !0}), b._pageScroller.refresh()
    },
    _displayAllPayWrap: function () {
        var a = this, b = a._data.total.split(".");
        b[1] = b[1] ? "." + b[1] : "", $(".JS-page-scorller").show(), a._data.countdownTime && a._data.countdownTime > 0 && (Common.NewCountDeadLine.started && Common.NewCountDeadLine.stop(), Common.NewCountDeadLine.countingDown(a._data.countdownTime, a._data.countdownPopInfo)), $(".JS-pay-total").html('<span class="JS-pay-total-icon">楼</span>' + b[0] + "<em>" + b[1] + "</em>"), Ariel.mpingEvent("MCashierNew_MoneyExpo", location.href, a._data._orderId);
        try {
            b[1].length > 1 && (b[2] = "0" == b[1].substr(1, 1) ? "闆�" : b[1].substr(1, 1), b[3] = "", b[1].length > 2 && (b[3] = "0" == b[1].substr(2, 1) ? "闆�" : b[1].substr(2, 1), b[3] = "2" == b[3] ? "浜�" : b[3])), $(".pay-total").attr({
                role: "text",
                "aria-label": b[0] + "鐐�" + b[2] + " " + b[3] + "鍏�"
            })
        } catch (a) {
        }
        if ($(".order-bar-wrap").show(), 1 == a._data.jdPayStatus || 0 == $(".jdpay-list-item").length) $(".p-other-pay-list").show(), $(".p-all-switch").hide(), $(".new-pay-bar-container").hide(), $(".pay-list-block").hide(); else {
            var c = a._data._payParams.jdOtherPayChannelList;
            c && c.length > 0 ? ($(".p-all-switch").css("display", "block"), $(".p-jd-switch-pays").text(a._data._jdOtherPaymentsName).attr({"aria-hidden": !0})) : $(".p-all-switch").hide(), $(".p-title-bar").show(), $(".jd-pay-list").show(), $(".JS-page-ct .p-other-pay-list").show()
        }
        0 == $(".p-other-pay-list li").length && ($(".p-other-pay-title-bar").hide(), $(".p-other-pay-list").hide()), a._data.preSellData && a._data.preSellData.btnDesc && a._setPreSell(), $(".pay-next").show()
    },
    installmentInited: !1,
    isGraduallyPayMode: function () {
        return !!this._data.graduallyPay
    },
    updateInstallment: function () {
        if (this.addGoBackFlag(), !this._data.graduallyPay) return void $(".installments-container").addClass("u-hide");
        if (this.initInstallment(), this._data.graduallyPayInfo.alreadyPaidAmount || (this._data.graduallyPayInfo.alreadyPaidAmount = 0), this._data.graduallyPayInfo.remainingAmount = Number(this._data.graduallyPayInfo.totalAmount) - Number(this._data.graduallyPayInfo.alreadyPaidAmount), String(this._data.graduallyPayInfo.remainingAmount).indexOf(".") >= 0 && (this._data.graduallyPayInfo.remainingAmount = this._data.graduallyPayInfo.remainingAmount.toFixed(2)), $(".installments-container").html(this._data.graduallyPayInfo.graduallyPayTip).removeClass("u-hide"), $(".JS-pay-total").append('<span class="JS-pay-total-installments-icon"></span>'), this.installment.config(this._data.graduallyPayInfo), Ariel.mpingEvent("MCashierNew_SubStepEntranceExpo", location.href, this._data.total), this._data._payParams.switch && "1" == this._data._payParams.switch.isFirstPopSwitch && !Ariel.Storage.get("graduallyPayToast")) {
            var a = $(".JS-pay-tip");
            a.children().length > 0 ? a.append('<div class="graduallyPay-toast has-countdownTime"></div>') : a.append('<div class="graduallyPay-toast"></div>'), Ariel.mpingEvent("MCashierNew_SubStepGuideExpo", location.href), setTimeout(function () {
                a.find(".graduallyPay-toast").remove()
            }, 1e4), Ariel.Storage.add("graduallyPayToast", "true")
        }
    },
    addGoBackFlag: function () {
        try {
            window.JdAndroid && window.JdAndroid.setExitOrNot("1")
        } catch (a) {
        }
        window.viewPageName = function () {
            return "pay-index"
        }
    },
    initInstallment: function () {
        if (!this.installmentInited) {
            var a = this;
            this.installment = new Common.Components.Installments, this.installment.init(), this.installment.config({
                onConfirm: function (b) {
                    a.installment.hide(), a._getData({params: {graduallyPayFlag: 1, graduallyPayAmount: b}})
                }, onCancel: function () {
                    a.installment.hide(), a._getData({cancelGraduallyPay: !0})
                }
            }), $(".JS-pay-total").on("click", ".JS-pay-total-installments-icon", function (b) {
                b.stopPropagation(), a.installment.show(), Ariel.mpingEvent("MCashierNew_SubStepEntrance", location.href, a._data.total)
            }), this.installmentInited = !0, this._data.graduallyPayInfo.popup && 1 == this._data.graduallyPayInfo.popup && a.installment.show()
        }
    },
    _getPaymentData: function (a, b) {
        var c = this, d = {};
        switch (a.code) {
            case c._APPLE_PAY_KEY:
                d.bindingMain = a.channelName, d.bindingVice = a.tip, d.unbindingMain = a.unbindingMain, d.unbindingVice = a.unbindingVice, d.binding = a.binding, d.hasAnimate = b.hasAnimate;
                break;
            case c._APPLE_PAY_SF_KEY:
                d.promotionDesc = a.promotionDesc, d.newCashierTip = a.tip;
                break;
            case c._BAITIAO_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum), a.preferentialWay && (d.preferentialWay = a.preferentialWay), void 0 !== a.defaultCouponId || void 0 !== a.defaultActivityId ? (d.defaultCoupon = {
                    couponId: a.defaultCouponId || "",
                    activityId: a.defaultActivityId || "",
                    preferentialWay: a.preferentiaNum || ""
                }, Ariel.mpingEvent("MCashierNew_DiscountExpo", location.href, "A")) : (d.defaultCoupon = null, Ariel.mpingEvent("MCashierNew_DiscountExpo", location.href, "B")), d.baiTiaoText = a.baiTiaoText || "鏆備笉浣跨敤浼樻儬", d.defaultPlanId = a.defaultPlanId || 1, "object" == typeof a.recommendCoupons && a.recommendCoupons.length > 0 ? d.recommendCoupons = a.recommendCoupons : d.recommendCoupons = [], a.recommendPlanId, d.recommendPlanId = a.recommendPlanId;
                break;
            case c._BAITIAOQUICK_KEY:
                Ariel.mpingEvent("MCashierNew_DiscountExpo", location.href, "A"), a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum), void 0 !== a.couponCode && (d.couponCode = a.couponCode), void 0 !== a.activityId && (d.activityId = a.activityId), d.btqPlanId = a.btqPlanId || 1, void 0 !== a.defaultCouponId || void 0 !== a.defaultActivityId ? d.defaultCoupon = {
                    couponId: a.defaultCouponId || "",
                    activityId: a.defaultActivityId || ""
                } : d.defaultCoupon = null, "object" == typeof a.recommendCoupons && a.recommendCoupons.length > 0 ? d.recommendCoupons = a.recommendCoupons : d.recommendCoupons = [], a.recommendPlanId, d.recommendPlanId = a.recommendPlanId;
                break;
            case c._JD_APPLE_PAY_KEY:
                d.moveToOtherPay = !(!c._data.preSellData || 1 != c._data.preSellData.type), d.hasAnimate = b.hasAnimate, a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._XIAOJINKU_KEY:
                a.preferentialWay && (d.preferentialWay = a.preferentialWay), a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._WEIXIN_KEY:
            case c._WEIXIN_OTHER_KEY:
                c.isliteApp && (d.liteApp = !0);
                break;
            case c._WEIXIN_INNER_KEY:
            case c._WEIXIN_WAP_KEY:
            case c._WING_PAY_WAP_KEY:
            case c._QIANBAO_KEY:
            case c._GANGBEN_KEY:
            case c._JDBALANCE_KEY:
            case c._JINGBEAN_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._JDQUICK_KEY:
                d.hasAnimate = b.hasAnimate, a.androidIcon && (d.androidIcon = a.androidIcon), a.name && (d.name = a.name), a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._BANKCARD_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum), void 0 !== a.defaultCouponId ? (d.defaultCoupon = a.defaultCouponId || "", Ariel.mpingEvent("MCashierNew_CardDiscountExpo", location.href, "A")) : (d.defaultCoupon = null, Ariel.mpingEvent("MCashierNew_CardDiscountExpo", location.href, "B")), d.defaultCard = a || {}, d.bankText = a.bankText || "鏆備笉浣跨敤浼樻儬";
                break;
            case c._EARTHBANKCARD_KEY:
            case c._SALESBANKCARD_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._CREDIT_CARD_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum), void 0 !== a.defaultBankName || void 0 !== a.defaultBankCode || void 0 !== a.defaultPlan || void 0 !== a.defaultPlanNumList ? (d.defaultBankInfo = {
                    bankName: a.defaultBankName || "",
                    bankCode: a.defaultBankCode || "",
                    defaultPlan: a.defaultPlan || "",
                    bankId: a.defaultBankId || "",
                    isNewCard: a.cardAd,
                    uniqueChannelId: a.uniqueChannelId,
                    channelId: a.defaultChannelId,
                    bankNameShow: a.bankNameShow,
                    creditRecommendCouponList: a.defaultCreditRecommendCouponList || [],
                    creditCouponList: a.defaultCreditCouponList || [],
                    defaultPlanNumList: a.defaultPlanNumList
                }, d.defaultPlan = a.defaultPlan || "", d.defaultPlanNumList = a.defaultPlanNumList) : d.defaultBankInfo = {};
                break;
            case c._JINGXIJIN_KEY:
                if (b.otherAccountInfoList && (d.jingXiJinList = b.otherAccountInfoList, !d.jingXiJinList)) return;
                break;
            case c._JINCAI_KEY:
                a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum), a.productCode && (d.productCode = a.productCode);
                break;
            case c._HUAWEI_PAY_KEY:
                a.androidIcon && (d.androidIcon = a.androidIcon), a.preferentiaNum && (d.preferentiaNum = a.preferentiaNum);
                break;
            case c._INTIMATE_PAY_KEY:
                a.guidOpenHoneyPay && 1 == a.guidOpenHoneyPay && (d.guidOpenHoneyPay = !0);
                break;
            default:
                void 0 !== a.info && (d = {tip: a.info})
        }
        return d
    },
    _initAllOtherPayments: function () {
        for (var a = this, b = a._data.otherPaymentList, c = b.length, d = 0; d < c; d++) {
            var e = b[d].code, f = b[d].status;
            if (0 != f) {
                var g = {
                    recommand: b[d].recommand || !1,
                    recommandIcon: b[d].recommandIcon || "",
                    _isGraduallyPayMode: this.isGraduallyPayMode(),
                    _isDefaultPayment: b[d].isDefault,
                    _titleMain: b[d].channelName,
                    _titleSimple: b[d].payWay || "",
                    _titleVice: b[d].desc,
                    _payType: e,
                    _status: f,
                    _canUseCount: b[d].canUseCount || 0,
                    _plansExpand: 1 == f && b[d].plansExpand,
                    _total: a._data.total,
                    _data: b[d].data,
                    _client: a._data.client,
                    _pageScroller: a._pageScroller,
                    logo: b[d].logo,
                    realAmount: b[d].realAmount,
                    requireUUID: b[d].requireUUID,
                    payPositionType: b[d].payPositionType,
                    couponData: b[d].couponData,
                    channelId: b[d].channelId,
                    uniqueChannelId: b[d].uniqueChannelId,
                    code: b[d].code,
                    planInfo: b[d].planInfo,
                    planId: b[d].planId,
                    defaultPlanId: b[d].defaultPlanId,
                    planRate: b[d].planRate,
                    isNewFlag: b[d].isNewFlag,
                    protocol: b[d].protocol || "",
                    creditCardData: b[d].creditCardData
                }, h = null;
                switch (e) {
                    case a._WEIXIN_KEY:
                        h = new WeixinPayment(g);
                        break;
                    case a._WEIXIN_WAP_KEY:
                        h = new WeixinWapPayment(g);
                        break;
                    case a._WEIXIN_INNER_KEY:
                        h = new WeixinInnerPayment(g);
                        break;
                    case a._QQ_WALLET_KEY:
                        h = new QQWalletPayment(g);
                        break;
                    case a._UNION_PAY_KEY:
                        h = new UnionPayment(g);
                        break;
                    case a._WEIXIN_OTHER_KEY:
                        h = new WeixinOtherPayment(g);
                        break;
                    case a._APPLE_PAY_KEY:
                        h = new ApplePayment(g);
                        break;
                    case a._BAITIAO_KEY:
                        Ariel.mpingEvent("MCashierNew_BTExpo", location.href), h = new _BaiTiaoPayment(g);
                        break;
                    case a._BAITIAOQUICK_KEY:
                        Ariel.mpingEvent("MCashierNew_SetBTExpo", location.href), h = new BaiTiaoQuickPayment(g);
                        break;
                    case a._QIANBAO_KEY:
                        h = new QianBaoPayment(g);
                        break;
                    case a._XIAOJINKU_KEY:
                        h = new XiaoJinKuPayment(g);
                        break;
                    case a._BANKCARD_KEY:
                        h = new BankCardPayment(g);
                        break;
                    case a._EARTHBANKCARD_KEY:
                        h = new EarthBankCardPayment(g);
                        break;
                    case a._SALESBANKCARD_KEY:
                        h = new SalesBankCardPayment(g);
                        break;
                    case a._GANGBEN_KEY:
                        h = new GangBenPayment(g);
                        break;
                    case a._JDBALANCE_KEY:
                        h = new jdBalancePayment(g);
                        break;
                    case a._JINGBEAN_KEY:
                        h = new JingBeanPayment(g);
                        break;
                    case a._JD_APPLE_PAY_KEY:
                        h = new JdApplePayment(g);
                        break;
                    case a._APPLE_PAY_SF_KEY:
                        h = new JdQuickPayment(g);
                        break;
                    case a._CREDIT_CARD_KEY:
                        h = new JdCreditCardPayment(g);
                        break;
                    case a._JINGXIJIN_KEY:
                        h = new JingXiJinPayment(g);
                        break;
                    case a._BADATONG_PAY_KEY:
                        h = new BaDaTongtPayment(g);
                        break;
                    case a._JDQUICK_KEY:
                    case a._JDQUICK_KEY:
                        h = new JdQuickAndroidPayment(g);
                        break;
                    case a._JINCAI_KEY:
                        h = new JincaiPayment(g);
                        break;
                    case a._HUAWEI_PAY_KEY:
                        h = new HuaweiPayment(g);
                        break;
                    case a._WING_PAY_KEY:
                        h = new WingPayment(g);
                        break;
                    case a._WING_PAY_WAP_KEY:
                        h = new WingWapPayment(g);
                        break;
                    case a._INTIMATE_PAY_KEY:
                        Ariel.mpingEvent("MCashierNew_IntimateCardPaymentExpo", location.href), h = new IntimatePayment(g)
                }
                if (h) {
                    if (3 == f) {
                        var i = b[d].data.tip;
                        h.disablePayment(i || "璇ユ敮浠樻柟寮忎笉鍙敤")
                    }
                    h.render()
                }
            }
        }
    },
    resetPaymentHTML: function () {
        $(".jd-pay-list, .p-other-pay-list").html("")
    },
    _initAllPayments: function () {
        for (var a = this, b = a._data.paymentList, c = b.length, d = 0; d < c; d++) {
            var e = b[d].code, f = b[d].status;
            if (0 != f) {
                var g = {
                    recommand: b[d].recommand || !1,
                    recommandIcon: b[d].recommandIcon || "",
                    _initiativeSelected: b[d].initiativeSelected,
                    _isGraduallyPayMode: this.isGraduallyPayMode(),
                    _isDefaultPayment: b[d].isDefault,
                    _titleMain: b[d].channelName,
                    _titleSimple: b[d].payWay || "",
                    _titleVice: b[d].desc,
                    _payType: e,
                    _status: f,
                    _canUseCount: b[d].canUseCount || 0,
                    _plansExpand: 1 == f && b[d].plansExpand,
                    _total: a._data.total,
                    _data: b[d].data,
                    _client: a._data.client,
                    _pageScroller: a._pageScroller,
                    logo: b[d].logo,
                    realAmount: b[d].realAmount,
                    requireUUID: b[d].requireUUID,
                    payPositionType: b[d].payPositionType,
                    couponData: b[d].couponData,
                    channelId: b[d].channelId,
                    uniqueChannelId: b[d].uniqueChannelId,
                    code: b[d].code,
                    planInfo: b[d].planInfo,
                    planId: b[d].planId,
                    defaultPlanId: b[d].defaultPlanId,
                    planRate: b[d].planRate,
                    isNewFlag: b[d].isNewFlag,
                    protocol: b[d].protocol || "",
                    creditCardData: b[d].creditCardData
                }, h = null;
                switch (e) {
                    case a._WEIXIN_KEY:
                        h = new WeixinPayment(g);
                        break;
                    case a._WEIXIN_WAP_KEY:
                        h = new WeixinWapPayment(g);
                        break;
                    case a._WEIXIN_INNER_KEY:
                        h = new WeixinInnerPayment(g);
                        break;
                    case a._QQ_WALLET_KEY:
                        h = new QQWalletPayment(g);
                        break;
                    case a._UNION_PAY_KEY:
                        h = new UnionPayment(g);
                        break;
                    case a._WEIXIN_OTHER_KEY:
                        h = new WeixinOtherPayment(g);
                        break;
                    case a._APPLE_PAY_KEY:
                        h = new ApplePayment(g);
                        break;
                    case a._BAITIAO_KEY:
                        Ariel.mpingEvent("MCashierNew_BTExpo", location.href, g.recommand ? 1 : 0), h = new _BaiTiaoPayment(g);
                        break;
                    case a._BAITIAOQUICK_KEY:
                        Ariel.mpingEvent("MCashierNew_SetBTExpo", location.href, g.recommand ? 1 : 0), h = new BaiTiaoQuickPayment(g);
                        break;
                    case a._QIANBAO_KEY:
                        h = new QianBaoPayment(g);
                        break;
                    case a._XIAOJINKU_KEY:
                        h = new XiaoJinKuPayment(g);
                        break;
                    case a._BANKCARD_KEY:
                        h = new BankCardPayment(g);
                        break;
                    case a._EARTHBANKCARD_KEY:
                        h = new EarthBankCardPayment(g);
                        break;
                    case a._SALESBANKCARD_KEY:
                        h = new SalesBankCardPayment(g);
                        break;
                    case a._GANGBEN_KEY:
                        h = new GangBenPayment(g);
                        break;
                    case a._JDBALANCE_KEY:
                        h = new jdBalancePayment(g);
                        break;
                    case a._JINGBEAN_KEY:
                        h = new JingBeanPayment(g);
                        break;
                    case a._JD_APPLE_PAY_KEY:
                        h = new JdApplePayment(g);
                        break;
                    case a._APPLE_PAY_SF_KEY:
                        h = new JdQuickPayment(g);
                        break;
                    case a._CREDIT_CARD_KEY:
                        h = new JdCreditCardPayment(g);
                        break;
                    case a._JINGXIJIN_KEY:
                        h = new JingXiJinPayment(g);
                        break;
                    case a._BADATONG_PAY_KEY:
                        h = new BaDaTongtPayment(g);
                        break;
                    case a._JDQUICK_KEY:
                    case a._JDQUICK_KEY:
                        h = new JdQuickAndroidPayment(g);
                        break;
                    case a._JINCAI_KEY:
                        h = new JincaiPayment(g);
                        break;
                    case a._HUAWEI_PAY_KEY:
                        h = new HuaweiPayment(g);
                        break;
                    case a._WING_PAY_KEY:
                        h = new WingPayment(g);
                        break;
                    case a._WING_PAY_WAP_KEY:
                        h = new WingWapPayment(g);
                        break;
                    case a._INTIMATE_PAY_KEY:
                        Ariel.mpingEvent("MCashierNew_IntimateCardPaymentExpo", location.href), h = new IntimatePayment(g)
                }
                if (h) {
                    if (3 == f) {
                        var i = b[d].data.tip;
                        h.disablePayment(i || "璇ユ敮浠樻柟寮忎笉鍙敤")
                    }
                    h.render()
                }
            }
        }
        Ariel.guidOpenHoneyPay = !1
    },
    _disableAllPayments: function (a) {
        new WeixinPayment({
            _isDefaultPayment: !1,
            _titleMain: "寰俊鏀粯",
            _status: "3",
            _tip: a,
            _titleVice: "寰俊瀹夊叏鏀粯"
        }).render();
        new UnionPayment({
            _isDefaultPayment: !1,
            _titleMain: "閾惰仈鏀粯",
            _status: "3",
            _tip: a,
            _titleVice: "閾惰仈鏀粯"
        }).render()
    },
    _initNoticeTip: function (a) {
        $(".position-change");
        $(".home-notice-wrap ").html('<span class="notice-icon"></span><span class="notice-close">x</span><span class="notice-content" ><marquee  loop="-1" scrollAmount= "2" scrolldelay="1">' + a + "</marquee></span>"), $(".home-notice-wrap").show(), $(".home-notice-wrap .notice-close").click(function () {
            Ariel.mpingEvent("MCashierWe_Close", location.href), $(".home-notice-wrap").hide()
        })
    },
    _displayMobile: function (a) {
        if (a) {
            $(".mobile-number").text(a), $(".order-bar-wrap").addClass("has-phone-number")
        }
    },
    encrypt: function (a) {
        RSAUtils.setMaxDigits(131);
        var b = RSAUtils.getKeyPair(publicKey.po, "", publicKey.mu), c = a.split("").reverse().join("");
        return RSAUtils.encryptedString(b, c)
    },
    applePayBindCard: function () {
        var a = this;
        return changeApplePayTitle = function (b) {
            var c = $(".apple-pay"), d = $(".apple-pay-sf");
            b && (b = parseInt(b));
            var e = c.find(".apple-pay-mark").length > 0 ? c.find(".apple-pay-mark") : "";
            switch (b) {
                case 0:
                    break;
                case 1:
                    c.find(".title-vice").html(c.attr("unbindingVice")), c.find(".title-main").html(c.attr("unbindingMain")), c.find(".title-main").append(e), c.attr("binding", "0");
                    break;
                case 2:
                    c.find(".title-vice").html(c.attr("bindingVice")), c.find(".title-main").html(c.attr("bindingMain")), c.find(".title-main").append(e), c.attr("binding", "1");
                    break;
                case 3:
                    var f = "JDP_QUICKPASS", g = a._getApplePayPayment(f);
                    a._updateJDPayment(g), JdApplePay && JdApplePay.callJdApplePay(), c.hide(), d.hide()
            }
            a._pageScroller.refresh()
        }, status
    },
    _getApplePayPayment: function (a) {
        for (var b = this, c = {}, d = b._data._payParams.jdOtherPayChannelList, e = 0; e < d.length; e++) d[e].uniqueChannelId === a && (d[e].status = 1, c = d[e]);
        return c
    },
    _setPreSell: function () {
        var a = this, b = a._data.preSellData, c = '<span class="pre-sale-payment-tip">' + b.titleDesc + "</span>",
            d = '<p class="pre-sale-btn-tip">' + b.btnDesc + "</p>";
        $("#viewport").append(d);
        var e;
        if (b.type && 1 == b.type) e = $(".new-pay-bar"), $(".choose-jxj-bar").hide(); else {
            if (2 != b.type) return;
            e = $(".p-other-pay-title-bar")
        }
        e.append(c)
    }
});