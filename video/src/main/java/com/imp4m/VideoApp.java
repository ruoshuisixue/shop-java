package com.imp4m;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.imp4m.*")
//@EnableDiscoveryClient
public class VideoApp {
    public static void main(String[] args) {
        SpringApplication.run(VideoApp.class, args);
    }

}
